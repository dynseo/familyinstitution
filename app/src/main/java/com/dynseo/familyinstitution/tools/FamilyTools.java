package com.dynseo.familyinstitution.tools;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.dynseo.familyinstitution.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Maxime Gelly on 15/06/2016.
 */
public class FamilyTools {

    public static String formatTodayYesterdayOrTommorrow(Context context, Date date) {
        Date today = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        DateFormat dateFormatRelative = new SimpleDateFormat(context.getResources().getString(R.string.time_format));
        DateFormat dateFormatAbsolute = new SimpleDateFormat(context.getResources().getString(R.string.date_time_days_months));
        Calendar calendar = Calendar.getInstance();

        //if aDate is today
        if ((dateFormat.format(date)).substring(0, 2).equals(dateFormat.format(today).substring(0, 2))) {
            return context.getString(R.string.today) + " " + dateFormatRelative.format(date);
        } else {
            //if aDate is yesterday
            calendar.setTime(today);
            calendar.add(Calendar.DATE, -1);
            if (dateFormat.format(date).substring(0, 2).equals(dateFormat.format(calendar.getTime()).substring(0, 2))) {
                return context.getString(R.string.yesterday) + " "  + dateFormatRelative.format(date);
            } else {
                //if aDate is tomorrow
                calendar.add(Calendar.DATE, 2);
                if (dateFormat.format(date).substring(0, 2).equals(dateFormat.format(calendar.getTime()).substring(0, 2))) {
                    return context.getString(R.string.tomorrow) + " " + dateFormatRelative.format(date);
                } else {
                    return dateFormatAbsolute.format(date);
                }
            }
        }
    }

    public static String formatDate(Context context, Date date){
        DateFormat dateFormat = new SimpleDateFormat(context.getResources().getString(R.string.date_time_months));
        if(date == null)
            return "";
        else
            return dateFormat.format(date);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String url,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(url,options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(url,options);
    }


    /*.°============================= Managing android 6.0 inApp permissions =========================°.*/
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    /*.°===============================================================================================°.*/
}
