package com.dynseo.familyinstitution.fragments;

import android.app.FragmentManager;

import com.dynseo.familyinstitution.R;

/**
 * Created by Maxime Gelly on 09/06/2016.
 */
public class ListLinkedUsersFragment extends com.dynseo.family.ListLinkedUsersFragment {

    @Override
    public void gotoWriteMessageFragment(){

        WriteMessageFragment f = new WriteMessageFragment();
                FragmentManager fragmentManager = getFragmentManager();

                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container_fragments, f,"WriteMessage")
                        .addToBackStack(null)
                        .commit();
    }

}
