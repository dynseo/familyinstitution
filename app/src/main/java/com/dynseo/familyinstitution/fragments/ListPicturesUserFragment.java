package com.dynseo.familyinstitution.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.models.Message;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.activities.HomeActivity;
import com.dynseo.familyinstitution.tasks.PageLoaderInterface;
import com.dynseo.familyinstitution.tasks.LoadGalleryImagesTask;
import com.dynseo.familyinstitution.tools.FamilyTools;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseo.stimart.utils.StimartTextView;
import com.dynseolibrary.tools.ImageLoader;

import java.util.List;

/**
 * Created by Maxime Gelly on 17/06/2016.
 */
public class ListPicturesUserFragment extends Fragment implements PageLoaderInterface {

    private final String TAG = "ListUsersFragment";
    private ImageLoader imageLoader;

    private View rootView;
    private ViewPager theGallery;

    private boolean fullScreen = false;

    StimartTextView noPermission;

    public ListPicturesUserFragment() {}

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        rootView = inflater.inflate(R.layout.page_list_pictures, container, false);
        noPermission = (StimartTextView) rootView.findViewById(R.id.item_txtNoPermission);

        if(!FamilyTools.hasPermissions(getActivity(), HomeActivity.PERMISSIONS)){
            ActivityCompat.requestPermissions(getActivity(), HomeActivity.PERMISSIONS, HomeActivity.REQUEST_PERMISSION_WRITE_EXTERNAL);
            noPermission.setVisibility(View.VISIBLE);
        } else {
            loadPicturesUserFragment();
        }

        return rootView;
    }

    public void loadPicturesUserFragment(){

        noPermission.setVisibility(View.GONE);

        fullScreen = false;
        //Gathering the list of pictures associated to a user
        ExtractorFamily extractor = new ExtractorFamily(getActivity().getApplicationContext());
        extractor.open();

//        imageLoader = new ImageLoader(getActivity(), AppResourcesManager.getAppResourcesManager().getPathImages());
        imageLoader = new ImageLoader(getActivity(), AppResourcesManager.getAppResourcesManager().getPathImagesFamily());

        //Selecting all the messages that has an attached file and that are not trashed
        String request = ExtractorFamily.COL_PERSON_ID
                + " LIKE \"" + Person.currentPerson.getServerId() + "\" AND "
                + ExtractorFamily.COL_TRASH + " NOT LIKE \"T\"" + " AND "
                + ExtractorFamily.COL_URL_IMAGE_SERVER + "<> \'\'" ;
        List<Message> messageList = extractor.getMessageList(request);
        extractor.close();

        theGallery = (ViewPager) rootView.findViewById(R.id.gallery_item);
        //Impossible to do it in xml...
        theGallery.setPageMargin(50);
        setGalleryNavigation(rootView);

        if (messageList.size()!=0){
            //Now extracting every bitmap of the messages
            LoadGalleryImagesTask loadGalleryImages =
                    new LoadGalleryImagesTask(this,getActivity(),imageLoader,messageList, getResources());
            loadGalleryImages.execute();

        } else {

            StimartTextView itemNoPictures = (StimartTextView) rootView.findViewById(R.id.item_txtNoPictures);
            itemNoPictures.setVisibility(View.VISIBLE);

            ImageView leftArrow = (ImageView) rootView.findViewById(R.id.left_arrow);
            leftArrow.setVisibility(View.GONE);
            ImageView rightArrow = (ImageView) rootView.findViewById(R.id.right_arrow);
            rightArrow.setVisibility(View.GONE);

        }
    }

    private void populateGallery() {
        Log.d(TAG, "populateGallery");
        theGallery.setAdapter(new theImageAdapter());
    }

    private void registerClickCallback(ImageView imageView,final int position) {
        Log.d(TAG, "registerClickCallback");
        imageView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //this will log the page number that was click
                Log.i("TAG", "This page was clicked: " + position);
                //Display the gallery item selected fullscreen
                fullScreen = true;
                ShowPictureFullScreenFragment showPictureFullScreenFragment =
                        new ShowPictureFullScreenFragment(imageLoader.getBitmapsForImagesWithIds().get(position));
                FragmentManager fragmentManager = getFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.frame_container_fragments, showPictureFullScreenFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private void setGalleryNavigation(View theRootView){
        ImageView leftArrow = (ImageView) theRootView.findViewById(R.id.left_arrow);
        ImageView rightArrow = (ImageView) theRootView.findViewById(R.id.right_arrow);
        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (theGallery.getCurrentItem() != imageLoader.getBitmapsForImagesWithIds().size())
                    theGallery.setCurrentItem(theGallery.getCurrentItem() + 1);
            }
        });
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (theGallery.getCurrentItem() != 0)
                    theGallery.setCurrentItem(theGallery.getCurrentItem() - 1);
            }
        });

    }

    public class theImageAdapter extends PagerAdapter {

        public theImageAdapter() {}

        public int getCount() {
            return imageLoader.getBitmapsForImagesWithIds().size();
        }

        public Object instantiateItem(ViewGroup collection, final int position) {

            ImageView imageView = new ImageView(getActivity());
            String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), imageLoader.getBitmap(position), "Title" + position, null);
            if (path!=null){
                Uri uri = Uri.parse(path);
                imageView.setImageURI(uri);

                registerClickCallback(imageView,position);
                collection.addView(imageView);
            }
            return imageView;
        }

        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((ImageView) view);
        }

        public boolean isViewFromObject(View view, Object object) {
            // TODO Auto-generated method stub
            return (view == object);
        }

    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        //If the next fragment is the fullScreenPicture,
        //we eventually get back to this one so we skip the recycling
        if (!fullScreen && imageLoader!=null)
            imageLoader.recycleAllImages();
    }

    @Override
    public void onImagesLoaded(){

        if (imageLoader.getBitmapsForImagesWithIds().size()==0){
            theGallery.setVisibility(View.GONE);
            StimartTextView noPics = (StimartTextView) rootView.findViewById(R.id.item_txtNoPictures);
            noPics.setVisibility(View.VISIBLE);
        } else {
            //Thus creating the list
            populateGallery();
        }

    }

}