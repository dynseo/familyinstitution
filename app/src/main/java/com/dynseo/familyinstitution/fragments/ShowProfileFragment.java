package com.dynseo.familyinstitution.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.activities.HomeActivity;
import com.dynseo.familyinstitution.tools.FamilyTools;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.utils.StimartTextView;

import java.util.Date;

/**
 * Created by Maxime Gelly on 06/06/2016.
 */
public class ShowProfileFragment extends Fragment {

    private final String TAG = "ShowProfileFragment";

    public ShowProfileFragment() {}

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.page_profile_user_layout, container, false);

        //Show the profile info
        ((HomeActivity) getActivity()).showProfileInformation(true);

        StimartTextView profileName = (StimartTextView) rootView.findViewById(R.id.profile_name);
        StimartTextView profileSurname = (StimartTextView) rootView.findViewById(R.id.profile_surname);
        StimartTextView profileBirthdate = (StimartTextView) rootView.findViewById(R.id.profile_birthdate);

        //We use TextView and not StimartTextView in order to change the background of the TV (not possible in customClass StimTV)
        TextView listMessagesLabel = (TextView) rootView.findViewById(R.id.list_messages_label);
        TextView listPicturesLabel = (TextView) rootView.findViewById(R.id.list_pictures_label);
        ImageView profileAvatar = (ImageView) rootView.findViewById(R.id.profile_avatar);

        profileName.setText(Person.currentPerson.getName());
        profileSurname.setText(Person.currentPerson.getFirstName());

        Date date = Person.currentPerson.getBirthdate();
        profileBirthdate.setText(FamilyTools.formatDate(getActivity(),date));

//        profileAvatar.setImageResource(Person.currentPerson.isMale()? R.drawable.icon_man : R.drawable.icon_woman);
        if(Person.currentPerson.isAdult()) {
            if (Person.currentPerson.isMale())
                profileAvatar.setImageResource(com.dynseo.stimart.R.drawable.icon_man);
            else if (Person.currentPerson.isFemale())
                profileAvatar.setImageResource(com.dynseo.stimart.R.drawable.icon_woman);
        }
        else {
            if (Person.currentPerson.isMale())
                profileAvatar.setImageResource(com.dynseo.stimart.R.drawable.icon_boy);
            else if (Person.currentPerson.isFemale())
                profileAvatar.setImageResource(com.dynseo.stimart.R.drawable.icon_girl);
        }

        final Typeface customFontBold = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.default_typeface_bold));

        listMessagesLabel.setTypeface(customFontBold);
        listMessagesLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show the list of messages for this user
                ListUserMessagesFragment listUserMessagesFragment = new ListUserMessagesFragment(false, false, getActivity());
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container_fragments, listUserMessagesFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        listPicturesLabel.setTypeface(customFontBold);
        listPicturesLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show the list of pictures for this user
                ListPicturesUserFragment listPicturesUserFragment = new ListPicturesUserFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container_fragments, listPicturesUserFragment,"ListPicturesUser")
                        .addToBackStack(null)
                        .commit();
            }
        });

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.show();

        ListUserMessagesFragment listUserMessagesFragment = new ListUserMessagesFragment(false, false, getActivity());
        ListPicturesUserFragment listPicturesUserFragment = new ListPicturesUserFragment();

        FragmentManager fragmentManager = getFragmentManager();

        fragmentManager.beginTransaction()
                .replace(R.id.profile_messages, listUserMessagesFragment)
                .commit();

        fragmentManager.beginTransaction()
                .replace(R.id.profile_pictures, listPicturesUserFragment,"ListPicturesUser")
                .commit();

        progressDialog.dismiss();

        return rootView;

    }

}
