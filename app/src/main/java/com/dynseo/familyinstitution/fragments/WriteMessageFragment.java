package com.dynseo.familyinstitution.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.activities.HomeActivity;
import com.dynseo.familyinstitution.tools.FamilyTools;
import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.utils.StimartTextView;
import com.dynseolibrary.tools.CustomKeyboard;
import com.dynseolibrary.tools.Tools;

import java.util.Calendar;

/**
 * Created by Maxime Gelly on 08/06/2016.
 */
public class WriteMessageFragment extends Fragment {

    private final String TAG = "WriteMessageFragment";

    public ExtractorFamily extractor;
    User theUser;
    String userId;
    String personId = Person.currentPerson.getServerId();
    String answeredObject,answer;
    StimartTextView noPermission;
    private int objectMaxCharacters = 200;
    private boolean pictureAllowed = false;

    public WriteMessageFragment(){
        theUser = User.currentUserOpen;
    }
    @SuppressLint("ValidFragment")
    public WriteMessageFragment(User aUser,String anObject, String anAnswer){
        theUser = aUser;
        answeredObject = anObject;
        answer = anAnswer;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.page_write_message_layout,container, false);

        Log.d(TAG," : onCreate");

        //Before all, asking the user if he authorises picture management
        noPermission = (StimartTextView) rootView.findViewById(R.id.item_txtNoPermission);

        if(!FamilyTools.hasPermissions(getActivity(), HomeActivity.PERMISSIONS)){
            ActivityCompat.requestPermissions(getActivity(), HomeActivity.PERMISSIONS, HomeActivity.REQUEST_PERMISSION_WRITE_EXTERNAL_WRITE);
            noPermission.setVisibility(View.VISIBLE);
        } else {
            pictureAllowed = true;
            noPermission.setVisibility(View.GONE);
        }

        final StimartTextView labelName = (StimartTextView) rootView.findViewById(R.id.textView1);
        final TextView txtName = (TextView) rootView.findViewById(R.id.item_txtName);
        final StimartTextView previousMessageText = (StimartTextView) rootView.findViewById(R.id.previous_message_text);
        txtName.setText(theUser.getPseudo(getActivity()));
        userId = theUser.getServerId();

        final Button btnZoomPlus = (Button) rootView.findViewById(R.id.zoom_plus);
        final Button btnZoomMoins = (Button) rootView.findViewById(R.id.zoom_moins);
        final Button btnCancel = (Button) rootView.findViewById(R.id.cancel);
        final Button attached = (Button) rootView.findViewById(R.id.attached);
        final Button btnSendMessage = (Button) rootView.findViewById(R.id.send);
        final EditText editObjetMessage = (EditText) rootView.findViewById(R.id.message_object_text);
        final EditText editMessage = (EditText) rootView.findViewById(R.id.message_text);

        //Print the answer object
        if (answeredObject!=null){
            editObjetMessage.setText(String.format("%s %s",getString(R.string.answer_label), answeredObject));
            editMessage.setFocusableInTouchMode(true);
            editMessage.requestFocus();
        }
        //Print the previous message
        previousMessageText.setText(answer);

        HomeActivity.myCustomKeyboard.registerEditText(editObjetMessage);
        HomeActivity.myCustomKeyboard.registerEditText(editMessage);
        HomeActivity.myCustomKeyboard.showCustomKeyboard(rootView);

        btnZoomMoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editMessage.setTextSize(38);
                editObjetMessage.setTextSize(38);
                txtName.setTextSize(38);
                labelName.setTextSize(38);
                btnCancel.setTextSize(30);
                attached.setTextSize(30);
                btnSendMessage.setTextSize(30);

                btnZoomMoins.setBackgroundResource(R.drawable.message_zoom_out_disable);
                btnZoomPlus.setBackgroundResource(R.drawable.message_zoom_in);
            }
        });

        btnZoomPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editMessage.setTextSize(60);
                editObjetMessage.setTextSize(60);
                txtName.setTextSize(60);
                labelName.setTextSize(60);
                btnCancel.setTextSize(40);
                attached.setTextSize(40);
                btnSendMessage.setTextSize(40);

                btnZoomMoins.setBackgroundResource(R.drawable.message_zoom_out);
                btnZoomPlus.setBackgroundResource(R.drawable.message_zoom_in_disabled);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Hiding the keyboard
                KeyboardView mKeyboardView = (KeyboardView) getActivity().findViewById(R.id.the_custom_keyboard_view);
                mKeyboardView.setVisibility(View.GONE);
                if (!editMessage.getText().toString().equals("") && !editObjetMessage.getText().toString().equals("")){
                    ((HomeActivity) getActivity()).alertLeavingPage();
                }

            }
        });

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isFieldEmpty(editObjetMessage,editMessage) && !isFieldTooLong(editObjetMessage)) {
                    insertMessage(rootView);

//                    Tools.showToast(getActivity(),getString(R.string.message) + " " + getString(R.string.sent));
                    Tools.showToast(getActivity(),getString(R.string.message_sent));

                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                }

            }
        });

        attached.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if (pictureAllowed){
                final Dialog customDialog = new Dialog(getActivity());
                customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                customDialog.setCancelable(true);
                customDialog.setContentView(R.layout.dialog_add_photo_layout);

                //Dialog transparent
                customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                LinearLayout btnA = (LinearLayout) customDialog.findViewById(R.id.ButtonAlpha);
                LinearLayout btnB = (LinearLayout) customDialog.findViewById(R.id.ButtonGama);

                btnA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((HomeActivity) getActivity()).captureImage();
                        customDialog.dismiss();
                    }
                });
                btnB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((HomeActivity) getActivity()).selectImageFromFile();
                        customDialog.dismiss();
                    }
                });
                customDialog.show();
            } else {
                Tools.showToast(getActivity(),getString(R.string.noPermission));
            }

            }
        });

        return rootView;
    }

    private boolean isFieldEmpty(EditText object, EditText message){
        if (object.getText().toString().equals("")) {
            Tools.showToast(getActivity(), getString(R.string.empty_object));
            return true;
        }

        if (message.getText().toString().equals("")) {
            Tools.showToast(getActivity(), getString(R.string.empty_message));
            return true;
        }
        return false;
    }

    private boolean isFieldTooLong(EditText object){
        if (object.getText().toString().length() >= objectMaxCharacters) {
            Tools.showToast(getActivity(), getString(R.string.object_too_long));
            return true;
        }
        return false;
    }

    public void insertMessage(View rootView) {
        EditText msgObject = (EditText) rootView.findViewById(R.id.message_object_text);
        EditText msgText = (EditText) rootView.findViewById(R.id.message_text);
        Calendar calendar = Calendar.getInstance();

        Message messageToAdd;

        if (((HomeActivity) getActivity()).fileUri !=null){
            messageToAdd = new Message(msgObject.getText().toString(),
                    msgText.getText().toString(), userId, personId,
                    calendar.getTime(), "F", ((HomeActivity) getActivity()).fileUri.toString());
        } else {
            messageToAdd = new Message(msgObject.getText().toString(),
                    msgText.getText().toString(), userId, personId,
                    calendar.getTime(), "F", "");
        }
        messageToAdd.setTrash("F");

        Log.i("calendar.getTime()", calendar.getTime().toString());
        Log.i("messageToAddTime", messageToAdd.getDateStrFR());
        extractor = new ExtractorFamily(getActivity());
        extractor.open();
        extractor.insertMessage(messageToAdd);
        extractor.close();

    }

    public void setPictureAllowed(){
        noPermission.setVisibility(View.GONE);
        pictureAllowed = true;
    }

}