package com.dynseo.familyinstitution.fragments;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;

import com.dynseo.family.DisplayListMessagesFragment;
import com.dynseo.family.models.Message;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.activities.HomeActivity;

/**
 * Created by Maxime Gelly on 06/06/2016.
 */
public class ListUserMessagesFragment extends DisplayListMessagesFragment {

    private final String TAG = "ListUserMessagesFragment";

    public ListUserMessagesFragment(){}

    @SuppressLint({"ValidFragment", "LongLogTag"})
    public ListUserMessagesFragment(boolean animator, boolean forAnimatorSentMessage, Context theActivity) {
        super(animator,forAnimatorSentMessage);

        Log.d(TAG,"entering listUsersMessagesFragment");
        //Show the profile info
        ((HomeActivity) theActivity).showProfileInformation(true);
    }

    @Override
    protected void gotoDisplayMessageFragment(Message clickedMessage) {
            DisplayMessageFragment f = new DisplayMessageFragment(clickedMessage);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container_fragments, f)
            .addToBackStack(null)
            .commit();
    }
}