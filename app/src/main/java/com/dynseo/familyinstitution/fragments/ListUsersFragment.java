package com.dynseo.familyinstitution.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.models.Message;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.activities.HomeActivity;
import com.dynseo.stimart.utils.StimartTextView;
import com.dynseo.stimart.common.models.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ListUsersFragment extends Fragment {

    private final String TAG = "ListUsersFragment";
    private ExtractorFamily extractor;

    //Data list
    private List<Person> listPersons;
    //UI list (the two lists are linked by the adapter)
    private ListView list;

    public boolean readMessages;

    public ListUsersFragment() {}

    @SuppressLint("ValidFragment")
    public ListUsersFragment(boolean read) {
        this.readMessages = read;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.page_list_messages_or_users_layout, container, false);
        list = (ListView) rootView.findViewById(R.id.display_users_or_messages_listView);

        //Print the label
        StimartTextView listUsersLabel = (StimartTextView) rootView.findViewById(R.id.list_users_label);
        listUsersLabel.setText(readMessages ? getString(R.string.list_users_unread_label) : getString(R.string.btn_list_users));

        //Hiding the profile info
        ((HomeActivity) getActivity()).showProfileInformation(false);

        //Gather every profile associated with the account (device)
        extractor = new ExtractorFamily(getActivity().getApplicationContext());
        extractor.open();
        Person[] players = extractor.getPersons(
                ExtractorFamily.COL_SERVER_ID + " is not null AND " +
                ExtractorFamily.COL_PERSON_ROLE + " <> \"A\" OR " +
                ExtractorFamily.COL_PERSON_ROLE +" is null ");
        extractor.close();

        //Managing the list so it returns only the list of users with unread messages if readMessages = false,
        //So it returns the list of user with the list of all messages if readMessages = true
        //And sort them by Name

        manageList(players);

        //Displaying a message if the list is empty
        if (listPersons.size() == 0) {
            Log.d(TAG, "no users associated");
            StimartTextView txtNoUsers = (StimartTextView) rootView.findViewById(R.id.item_txtNoMessage);
            txtNoUsers.setText(readMessages ? getString(R.string.noMessagesAvailable) : getString(R.string.noUsersAvailable));
            txtNoUsers.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            //Builing the view only if the list is not empty
        } else {
            Log.d(TAG, "at least one user associated");
            populateListView();
            registerClickCallback();
        }

        Log.d(TAG, "nbUser = " + listPersons.size());
        return rootView;
    }

    private void populateListView() {
        Log.d(TAG, "populateListView, " + "readMessages : " + readMessages);
        myListAdapter adapter = new myListAdapter();
        list.setAdapter(adapter);
    }

    private void registerClickCallback() {
        Log.d(TAG, "registerClickCallback, " + "readMessages : " + readMessages);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
                Log.d(TAG, "registerClickCallback : " + "item Clicked");
                //Reach the selected user
                Person.currentPerson = listPersons.get(position);
                //Displaying the user profile page if readMessages is false
                ((HomeActivity) getActivity()).loadUserFragment(HomeActivity.SHOW_PROFILE_FRAGMENT);
            }
        });
    }

    private void manageList(Person[] players) {
        //First we sort all the players
        Log.d(TAG,"players not sorted : " + Arrays.toString(players));
        sortPersons(players);
        Log.d(TAG,"players sorted : " + Arrays.toString(players));

        //If we are to print the unread messages, we delete everyone who has no unread messages
        if (readMessages) {
            listPersons = new ArrayList<>();
            for (Person thePerson : players) {
                if (getMessagesNumber(thePerson,false) != 0)
                    listPersons.add(thePerson);
            }
        } else {
            listPersons = new ArrayList<>(Arrays.asList(players));
        }
    }

    private void sortPersons(Person[] players){
        Arrays.sort(players, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
    }

    private int getMessagesNumber(Person aPerson, boolean read) {
        extractor.open();

        String request;
        int nbMessages;
        if (read){
            request = ExtractorFamily.COL_PERSON_ID + " = \"" + aPerson.getServerId() + "\"" + " AND " + ExtractorFamily.COL_TRASH + " NOT LIKE \"T\" "
                    + "AND " + ExtractorFamily.COL_FROM_USER_TO_PERSON + " LIKE \"T\" ";
            List<Message> listMessages = extractor.getMessageList(request);
            nbMessages = listMessages.size();
        } else {

            request = ExtractorFamily.COL_PERSON_ID
                    + " LIKE \"" + aPerson.getServerId()
                    + "\" AND " + ExtractorFamily.COL_CHECKED + " LIKE \"F\" "
                    + " AND " + ExtractorFamily.COL_TRASH + " NOT LIKE \"T\" "
                    + "AND " + ExtractorFamily.COL_FROM_USER_TO_PERSON + " LIKE \"T\" ";
            nbMessages = extractor.getCountOfMessages(request);
        }

        Log.d(TAG, "request:"+request);
        extractor.close();
        return nbMessages;
    }


    class myListAdapter extends ArrayAdapter<Person> {

        public myListAdapter() {
            super(getActivity(), readMessages ? R.layout.item_user_layout : R.layout.item_user_messages_layout, listPersons);
        }

        @Override
        public int getCount() {
            return listPersons.size();
        }

        @Override
        public Person getItem(int i) {
            return listPersons.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @SuppressWarnings("ResourceType")
        public View getView(final int position, View convertView, ViewGroup parent) {

            int layoutId = readMessages ? R.layout.item_user_layout : R.layout.item_user_messages_layout;

            if ((convertView == null)) {
                convertView = getActivity().getLayoutInflater().inflate(layoutId, parent, false);
            }

            /** Customize the lign of the list
             - a custom icon
             - the user pseudo
             - the number of unread messages or the total number of messages (read and unread)
             */

            ImageView iconItem = (ImageView) convertView.findViewById(R.id.item_icon);
            if (listPersons.get(position).getSex() == null){
                iconItem.setImageResource(R.drawable.icon_nosex);
            } else if(listPersons.get(position).isAdult()) {
                if (listPersons.get(position).isMale())
                    iconItem.setImageResource(com.dynseo.stimart.R.drawable.icon_man);
                else if (listPersons.get(position).isFemale())
                    iconItem.setImageResource(com.dynseo.stimart.R.drawable.icon_woman);
            }
            else {
                if (listPersons.get(position).isMale())
                    iconItem.setImageResource(com.dynseo.stimart.R.drawable.icon_boy);
                else if (listPersons.get(position).isFemale())
                    iconItem.setImageResource(com.dynseo.stimart.R.drawable.icon_girl);
            }

            StimartTextView firstnameText = (StimartTextView) convertView.findViewById(R.id.item_txtFirstname);
//            firstnameText.setText(listPersons.get(position).getPseudo(getActivity().getApplicationContext()));
            firstnameText.setText(String.format("%s %s",
                    listPersons.get(position).getName(),
                    listPersons.get(position).getFirstName()));

            //Both ways, printing the list messages button with a different background
            //And a badge with the number of unread messages

            ImageButton mailList = (ImageButton) convertView.findViewById(R.id.btn_message_list);
            mailList.setFocusable(false);

            ImageView mailNotification = (ImageView) convertView.findViewById(R.id.notification);

            if (getMessagesNumber(listPersons.get(position),true) == 0){
                mailNotification.setVisibility(View.GONE);
                mailList.setBackgroundResource(R.drawable.list_messages_button_none);
                mailList.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.background_button_messages_none));
            }else{
                mailNotification.setVisibility(View.VISIBLE);
                TypedArray notifications = getResources().obtainTypedArray(R.array.notification);
                if (getMessagesNumber(listPersons.get(position),false) != 0){
                    if (getMessagesNumber(listPersons.get(position),false) > 9)
                        mailNotification.setBackgroundResource(notifications.getResourceId(9, 0));
                    else
                        mailNotification.setBackgroundResource(notifications.getResourceId(
                                getMessagesNumber(listPersons.get(position),false) - 1, 0));
                }
                mailList.setBackgroundResource(R.drawable.list_messages_button);
                mailList.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.background_button_messages));
                notifications.recycle();
            }
            mailList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Person.currentPerson = listPersons.get(position);
                    ((HomeActivity) getActivity()).loadUserFragment(HomeActivity.LIST_MESSAGES_FRAGMENT);
                }
            });

            if (!readMessages) {
                //Defining the buttons
                ImageButton details = (ImageButton) convertView.findViewById(R.id.btn_details);
                //Set focusable is usefull because on a getView, if the buttons have focus, the line has none,
                //so it's not clickable and the registerClickCallBack has no use
                details.setFocusable(false);
                details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Person.currentPerson = listPersons.get(position);
                        ((HomeActivity) getActivity()).loadUserFragment(HomeActivity.SHOW_PROFILE_FRAGMENT);
                    }
                });

                ImageButton write = (ImageButton) convertView.findViewById(R.id.btn_write);
                write.setFocusable(false);
                write.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Person.currentPerson = listPersons.get(position);
                        ((HomeActivity) getActivity()).loadUserFragment(HomeActivity.LIST_ASSOCIATED_USERS_FRAGMENT);
                    }
                });

            }

            return convertView ;
        }
    }

}
