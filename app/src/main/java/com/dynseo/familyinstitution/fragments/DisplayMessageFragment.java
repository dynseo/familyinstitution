package com.dynseo.familyinstitution.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.tools.FamilyTools;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseo.stimart.utils.StimartTextView;
import com.dynseolibrary.platform.AppManager;

import java.io.File;

/**
 * Created by Maxime Gelly on 01/06/2016.
 */
public class DisplayMessageFragment extends Fragment {

    protected Message currentMessage;
    private final String TAG = "DisplayMessageFragment";

    public String IMAGE_SAVE_PATH = AppResourcesManager.getAppResourcesManager().getPathImagesFamily();

    public DisplayMessageFragment(){
    }
    @SuppressLint("ValidFragment")
    public DisplayMessageFragment(Message messageClicked){
        currentMessage = messageClicked;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.page_show_message_layout, container, false);

        Log.d(TAG,"onCreateView");

        Log.d(TAG,"message shown : " + currentMessage.getMessage() + ", id : "+currentMessage.getServerId());

//        IMAGE_SAVE_PATH = "/data/data/" + getActivity().getPackageName() + "/";

        StimartTextView messageFrom = (StimartTextView) rootView.findViewById(R.id.txtFrom);
        StimartTextView messageObject = (StimartTextView) rootView.findViewById(R.id.txtObjetMessage);
        StimartTextView messageContent = (StimartTextView) rootView.findViewById(R.id.txtMessage);
        StimartTextView messageDate = (StimartTextView) rootView.findViewById(R.id.txtDate);
        ImageView messageAttached = (ImageView) rootView.findViewById(R.id.imgZone);
        Button btnDelete = (Button) rootView.findViewById(R.id.btnDelete);
        Button buttonRespond = (Button) rootView.findViewById(R.id.btnRespond);
        Button buttonClose = (Button) rootView.findViewById(R.id.btnCross);

        final ExtractorFamily extractor = new ExtractorFamily(getActivity().getApplicationContext());
        extractor.open();

        messageFrom.setText(extractor.getUserWithServerId(currentMessage.getUserId()).getPseudo(getActivity().getApplicationContext()));
        final User theUser = extractor.getUserWithServerId(currentMessage.getUserId());
        messageObject.setText(currentMessage.getObjetMessage());
        messageContent.setText(currentMessage.getMessage());
        messageDate.setText(FamilyTools.formatTodayYesterdayOrTommorrow(getActivity(),currentMessage.getDate()));
        extractor.close();

        Log.d(TAG,"urlImage de l'image du message : "+currentMessage.getUrlImageServer());

        String url = IMAGE_SAVE_PATH + currentMessage.getServerId() + ".jpg";

        Log.d(TAG,"urlImage du fichier : "+url);

        if (!currentMessage.getUrlImageServer().equals("")) {
            Uri uri = Uri.parse(url);
            Log.d(TAG, "uri : " + uri);
            messageAttached.setVisibility(View.VISIBLE);
            messageAttached.setImageURI(uri);
        }

        //Delete message
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog customDialog = new Dialog(getActivity());
                customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                customDialog.setCancelable(true);
                customDialog.setContentView(R.layout.dialog_delete_message_layout);

                // Dialog transparent
                customDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));

                Button btnYes = (Button) customDialog.findViewById(R.id.btnYes);
                Button btnNo = (Button) customDialog.findViewById(R.id.btnNo);

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Calling updateMessageState will send the information to the server,

                        //Delete the associated picture if there's one
                        if (currentMessage.getServerId() != null){
                            String IMAGE_SAVE_PATH_BASE = "/data/data/" + AppManager.getAppManager().getPackageName() + "/";
                            File fdelete = new File(IMAGE_SAVE_PATH_BASE + currentMessage.getServerId() + ".jpg");
                            if (fdelete.exists()) {
                                if (fdelete.delete()) {
                                    Log.d("file Deleted :", IMAGE_SAVE_PATH_BASE + currentMessage.getServerId() + ".jpg");
                                } else {
                                    Log.d("file not Deleted :", IMAGE_SAVE_PATH_BASE + currentMessage.getServerId() + ".jpg");
                                }
                            }
                        }
                        //Setting the extractor to trash will allow not to display it
                        extractor.open();
                        extractor.setTrash(currentMessage);
                        extractor.close();

                        //Close current message
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack();

                        customDialog.dismiss();
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        customDialog.dismiss();
                    }
                });

                customDialog.show();

            }
        });

        messageAttached.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShowPictureFullScreenFragment f = new ShowPictureFullScreenFragment(currentMessage);
                FragmentManager fragmentManager = getFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.frame_container_fragments, f)
                    .addToBackStack(null)
                    .commit();
            }
        });

        buttonRespond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WriteMessageFragment f = new WriteMessageFragment(theUser,currentMessage.getObjetMessage(),currentMessage.getMessage());
                FragmentManager fragmentManager = getFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.frame_container_fragments, f,"WriteMessage")
                        .addToBackStack(null)
                        .commit();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                fm.popBackStack();
            }
        });

        return rootView;
    }
}
