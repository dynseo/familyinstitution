package com.dynseo.familyinstitution.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dynseo.family.models.Message;
import com.dynseo.familyinstitution.R;
import com.dynseo.stimart.common.models.AppResourcesManager;

/**
 * Created by Maxime Gelly on 16/06/2016.
 */
public class ShowPictureFullScreenFragment extends Fragment {

    private static final String TAG = "DisplayFullScreenPictureFragment";

    private Message currentMessage;
    private Bitmap imageToDisplay;

    public ShowPictureFullScreenFragment(){
    }
    @SuppressLint("ValidFragment")
    public ShowPictureFullScreenFragment(Bitmap toDisplay){ imageToDisplay = toDisplay;}

    @SuppressLint("ValidFragment")
    public ShowPictureFullScreenFragment(Message messageClicked){
        currentMessage = messageClicked;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.family_display_fullscreen_picture_layout, container, false);

        final ImageView imgV =(ImageView) rootView.findViewById(R.id.item_image);

        if (imageToDisplay == null){
//            String IMAGE_SAVE_PATH = "/data/data/" + getActivity().getPackageName() + "/";
            String IMAGE_SAVE_PATH = AppResourcesManager.getAppResourcesManager().getPathImagesFamily();
            String url = IMAGE_SAVE_PATH + currentMessage.getServerId() + ".jpg";
            Uri uri = Uri.parse(url);
            imgV.setImageURI(uri);

            imgV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                DisplayMessageFragment f = new DisplayMessageFragment(currentMessage);
                FragmentManager fragmentManager = getFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.frame_container_fragments, f)
                        .addToBackStack(null)
                        .commit();
                }
            });
        } else {
            imgV.setImageBitmap(imageToDisplay);
            imgV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.popBackStack();
                }
            });
        }


        return rootView;
    }
}




