package com.dynseo.familyinstitution.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.text.method.LinkMovementMethod;

import com.dynseo.familyinstitution.R;
import com.dynseo.stimart.utils.StimartTextView;


/**
 * Created by Maxime Gelly on 30/03/2016.
 */

@SuppressLint("NewApi")
public class AboutUsActivity extends PreferenceActivity {

    private static final String TAG = "AboutUsActivity";

    public static Intent getIntent(Context context) {
        return new Intent(context, AboutUsActivity.class);
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        if (actionBar!=null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        int preferenceId = this.getResources().getIdentifier("preferences_about_" + getString(R.string.app_lang), "xml", getPackageName());
        this.addPreferencesFromResource(preferenceId);

        //=== Who are we  and contact us
        //==============================
        Preference aboutUsPref = (Preference) findPreference("aboutUs");
        aboutUsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference arg0) {
                AlertDialog.Builder b = new AlertDialog.Builder(AboutUsActivity.this);
                AlertDialog dialog = b.create();
                dialog.setIcon(android.R.drawable.ic_dialog_alert);
                dialog.setTitle(R.string.pref_who_are_we);
                dialog.setMessage(getString(R.string.legal));
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                return true;
            }
        });

        Preference emailUsPref = (Preference) findPreference("emailUs");
        emailUsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference arg0) {
                final StimartTextView CustomTextView = new StimartTextView(AboutUsActivity.this);
                CustomTextView.setText(R.string.pref_contact_us_description);
                CustomTextView.setMovementMethod(LinkMovementMethod.getInstance()); // this is important to make the links clickable

                AlertDialog.Builder b = new AlertDialog.Builder(AboutUsActivity.this);
                AlertDialog dialog = b.create();
                dialog.setIcon(android.R.drawable.ic_dialog_email);
                dialog.setTitle(R.string.pref_contact_us);
                dialog.setView(CustomTextView);
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                return true;
            }
        });

        Preference firsAppPref = findPreference("firsApp");
        firsAppPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference arg0) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pref_first_app_description)));
                Log.d("go to first App", "" + intent);
                startActivity(intent);
                return true;
            }
        });

        Preference secondAppPref = findPreference("secondApp");
        secondAppPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference arg0) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pref_second_app_description)));
                Log.d("go to second App",""+intent);
                startActivity(intent);
                return true;
            }
        });

        if (getString(R.string.app_lang).equals("fr")){

            Preference thirdAppPref = findPreference("thirdApp");
            thirdAppPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                public boolean onPreferenceClick(Preference arg0) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pref_third_app_description)));
                    Log.d("go to third App",""+intent);
                    startActivity(intent);
                    return true;
                }
            });

            Preference fourthAppPref = findPreference("fourthApp");
            fourthAppPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                public boolean onPreferenceClick(Preference arg0) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pref_fourth_app_description)));
                    Log.d("go to fourth App",""+intent);
                    startActivity(intent);
                    return true;
                }
            });

            Preference fifthApp = findPreference("fifthApp");
            fifthApp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                public boolean onPreferenceClick(Preference arg0) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pref_fifth_app_description)));
                    Log.d("go to fifth App", "" + intent);
                    startActivity(intent);
                    return true;
                }
            });

            Preference sixthApp = findPreference("sixthApp");
            sixthApp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                public boolean onPreferenceClick(Preference arg0) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.pref_sixth_app_description)));
                    Log.d("go to sixthApp", "" + intent);
                    startActivity(intent);
                    return true;
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AboutUsActivity.this, HomeActivity.class);
        startActivity(intent);
        this.finish();
    }
}
