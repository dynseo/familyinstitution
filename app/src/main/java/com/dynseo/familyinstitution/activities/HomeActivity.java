package com.dynseo.familyinstitution.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.inputmethodservice.KeyboardView;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bugsnag.android.Bugsnag;
import com.bugsnag.android.MetaData;
//import com.dynseo.family.dao.AndroidDatabaseManager;
import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.server.SynchronizationData;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.fragments.ListLinkedUsersFragment;
import com.dynseo.familyinstitution.fragments.ListPicturesUserFragment;
import com.dynseo.familyinstitution.fragments.ListUserMessagesFragment;
import com.dynseo.familyinstitution.fragments.ListUsersFragment;
import com.dynseo.familyinstitution.fragments.ShowProfileFragment;
import com.dynseo.familyinstitution.fragments.WriteMessageFragment;
import com.dynseo.stimart.common.activities.DeviceVerificationActivity;
import com.dynseo.stimart.common.activities.InformationActivity;
import com.dynseo.stimart.common.activities.MainActivity;
import com.dynseo.stimart.common.activities.SettingsActivity;
import com.dynseo.stimart.common.activities.UpdateAppActivity;
import com.dynseo.stimart.common.activities.UpdateResourcesActivity;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.utils.DialogManager;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseo.stimart.utils.StimartTextView;
import com.dynseo.stimart.utils.SubscribeAndAddAccount;
import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.SynchroFinishInterface;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.platform.server.SynchronizationTask;
import com.dynseolibrary.tools.CustomKeyboard;
import com.dynseolibrary.tools.Tools;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * Created by Maxime Gelly on 27/05/2016.
 */
public class HomeActivity extends MainActivity implements SynchroFinishInterface {

    public final static String TAG = "HomeActivity";

    //Pictures constants
    public static final int RESULT_LOAD_IMAGE = 11;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final String IMAGE_DIRECTORY_NAME = "DynseoFamily";

    //Fragments constants
    public static final int LIST_ASSOCIATED_USERS_FRAGMENT = 20;
    public static final int LIST_PICTURES_FRAGMENT = 21;
    public static final int LIST_MESSAGES_FRAGMENT = 22;
    public static final int SHOW_PROFILE_FRAGMENT = 23;

    //In app permissions
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL = 1;
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL_WRITE = 2;
    public static final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public Uri fileUri;
    String picturePath;

    public static CustomKeyboard myCustomKeyboard; //Never ever keep it static, it would have weird behaviour

    public View.OnClickListener quitButtonListener;
    public View.OnClickListener quitWritingButtonListener;
    public View.OnClickListener loadAssociatedUsers;
    public View.OnClickListener loadListPictures;
    public View.OnClickListener loadListMessages;
    public View.OnClickListener loadProfile;

    private DialogManager currentDialog;

    //ManageDialog Stimart part
    protected boolean doSynchro = false;
    boolean skipOnResume = false;
    public static final int RESULT_CODE_DO_SYNCHRO	 = 2;
    ImageButton subscribe;
    SubscribeAndAddAccount subscribeAndAddAccount;
    protected boolean connected = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"onCreate : parent MenuAppliActivity");
        // Using the same onCreate for device verification purposes
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate : Home Activity");
        setContentView(R.layout.activity_home_layout);
        AppManager.getAppManager(this);
        AppResourcesManager.getAppResourcesManager(this);

        subscribe = (ImageButton) findViewById(R.id.menu_button_subscribe);
        context = this;

        if (Tools.isDevelopmentMode(this)) {
            ConnectionConstants.setBaseUrl("http://test.stimart.com/app?newPilotSession=true", "http://test.stimart.com/pilot?service=");
            //ConnectionConstants.setBaseUrl("http://192.168.1.32/stimart/app?newPilotSession=true", "http://192.168.1.32/stimart/pilot?service=");
        }
        else {
            Bugsnag.init(this);
        }

        //Creating the keyboard
        // use our custom keybord "azerty_single_word" (azerty without the SPACE key)
        KeyboardView myKeyboardView = (KeyboardView) findViewById(R.id.the_custom_keyboard_view);
        myCustomKeyboard = new CustomKeyboard(this, myKeyboardView, com.dynseo.stimart.R.xml.azerty);

        initDialogs();

        // Print the institution name
        displayInstitutionName();

        //load the first fragment for unread messages
        ListUsersFragment f = new ListUsersFragment(true);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container_fragments, f)
                .addToBackStack(null)
                .commit();

        Button btnListMessages = (Button) findViewById(R.id.btn_list_messages);
        btnListMessages.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"reload unread messages fragment");
                loadListMessagesFragment(true);
            }
        });

        Button btnListUsers = (Button) findViewById(R.id.btn_list_users);
        btnListUsers.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"reload list Users fragment");
                loadListMessagesFragment(false);
            }
        });

        final ImageView menuImageProfile = (ImageView) findViewById(R.id.menu_image_profile);
        menuImageProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Checking if an user is selected
                if (menuImageProfile.getVisibility() == View.VISIBLE) {
                    ShowProfileFragment f = new ShowProfileFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container_fragments, f)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });

        ImageView icon = (ImageView) findViewById(R.id.family_icon);
        icon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loadListMessagesFragment(false);
            }
        });

        ImageView aboutUs = (ImageView) findViewById(R.id.about_us_icon);
        aboutUs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(AboutUsActivity.getIntent(getApplicationContext()));
                HomeActivity.this.finish();
            }
        });

    }

    /**
     * this method allow to use same onCreate with uploading only once different layout
     */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_WRITE_EXTERNAL) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        ListPicturesUserFragment fragment = (ListPicturesUserFragment) getFragmentManager().findFragmentByTag("ListPicturesUser");
                        fragment.loadPicturesUserFragment();
                    } else {
                        ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSION_WRITE_EXTERNAL);
                    }
                }
            }
        } else if (requestCode == REQUEST_PERMISSION_WRITE_EXTERNAL_WRITE){
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        WriteMessageFragment fragment = (WriteMessageFragment) getFragmentManager().findFragmentByTag("WriteMessage");
                        fragment.setPictureAllowed();
                    } else {
                        ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSION_WRITE_EXTERNAL_WRITE);
                    }
                }
            }
        }
    }


    public void loadListMessagesFragment(boolean read){
        //load the fragment for unread messages
        ListUsersFragment f = new ListUsersFragment(read);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container_fragments, f)
                .addToBackStack(null)
                .commit();
    }

    public void showProfileInformation(boolean show){
        //Discovering the view receiving the fragment
        ImageView menuImageProfile = (ImageView) findViewById(R.id.menu_image_profile);
        StimartTextView playerName = (StimartTextView) findViewById(R.id.player_name);

        //Changing the menu UI
        if (show){
            if (Person.currentPerson != null){
                playerName.setVisibility(View.VISIBLE);
                menuImageProfile.setVisibility(View.VISIBLE);
                playerName.setText(Person.currentPerson.getPseudo(this));
//                menuImageProfile.setImageResource(Person.currentPerson.isMale()? R.drawable.icon_man : R.drawable.icon_woman);
                if(Person.currentPerson.isAdult()) {
                    if (Person.currentPerson.isMale())
                        menuImageProfile.setImageResource(com.dynseo.stimart.R.drawable.icon_man);
                    else if (Person.currentPerson.isFemale())
                        menuImageProfile.setImageResource(com.dynseo.stimart.R.drawable.icon_woman);
                }
                else {
                    if (Person.currentPerson.isMale())
                        menuImageProfile.setImageResource(com.dynseo.stimart.R.drawable.icon_boy);
                    else if (Person.currentPerson.isFemale())
                        menuImageProfile.setImageResource(com.dynseo.stimart.R.drawable.icon_girl);
                }
            }
        } else {
            playerName.setVisibility(View.GONE);
            menuImageProfile.setVisibility(View.GONE);
        }
    }

    public void loadUserFragment(final int requestCode){
        currentDialog = new DialogManager(HomeActivity.this,
                com.dynseo.stimart.R.drawable.background_button_menu,
                com.dynseo.stimart.R.drawable.background_dialog_menu,
                getString(com.dynseo.stimart.R.string.mailTitle),
                getString(com.dynseo.stimart.R.string.confirmation) + Person.currentPerson.getPseudo(HomeActivity.this)
                        + " ?");
        currentDialog.setDialogString();
        switch (requestCode) {
            case LIST_ASSOCIATED_USERS_FRAGMENT:
                currentDialog.oui.setOnClickListener(loadAssociatedUsers);
                break;
            case LIST_PICTURES_FRAGMENT:
                currentDialog.oui.setOnClickListener(loadListPictures);
                break;
            case LIST_MESSAGES_FRAGMENT:
                currentDialog.oui.setOnClickListener(loadListMessages);
                break;
            case SHOW_PROFILE_FRAGMENT:
                currentDialog.oui.setOnClickListener(loadProfile);
                break;
        }
        currentDialog.setCloseListener(false, true);
        currentDialog.showDialog();
    }

    public void displayInstitutionName(){
        //Write the name of the institution to welcome
        StimartTextView menuTitleWelcome = (StimartTextView) findViewById(R.id.menu_title_welcome);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String institutionType = preferences.getString(ConnectionConstants.SHARED_PREFS_INSTITUTION, null);

        if (institutionType==null||institutionType.equals("")){
            menuTitleWelcome.setText(getString(R.string.welcome));
        } else {
            menuTitleWelcome.setText(institutionType);
        }
        Log.d(TAG,"institution name"+institutionType);
    }

    /* Called when the second activity's finished */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult", "requestCode "+ requestCode + " resultCode "+ resultCode);
        switch (requestCode) {

            //Device verification and synchronization
            case 0:
                if (resultCode == RESULT_OK) {

                    if( currentStep == STEP_VERIFICATION ){
                        Log.d("current Step", "verification success");
                        currentStep = STEP_VERIFICATION_SUCCESS;

                        //When the synchro has finished, print the institution name
                        displayInstitutionName();
                        //Now introducing the fragments that represents the list of users
                        loadListMessagesFragment(false);

                    }
                }
                else if (resultCode == RESULT_KO) {
                    if( currentStep == STEP_VERIFICATION ) {
                        Log.d("current Step", "verification success");
                        currentStep = STEP_VERIFICATION_FAILURE;
                    }
                }
                else if (resultCode == RESULT_CODE_DO_SYNCHRO)
                    doSynchro = true;

                break;

            //Handling the camera and its result
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    previewCapturedImage();
                } else if (resultCode == RESULT_CANCELED) {
                    Tools.showToast(this, getString(R.string.error_cancelled_camera));
                } else {
                    Tools.showToast(this, getString(R.string.error_capture_camera));
                }
                break;

            //Handling the file explorer and its result (the selected attached picture)
            case RESULT_LOAD_IMAGE:
                if (resultCode == RESULT_OK && data != null) {
                    previewSelectedImage(data);
                }
                break;
        }
    }

    public void initDialogs(){
        extractor = new ExtractorFamily(this);

        quitButtonListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentDialog != null) {
                    currentDialog.endDialog();
                    HomeActivity.this.finish();
                }
            }
        };

        quitWritingButtonListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentDialog != null) {
                    //Return to previous page
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                    currentDialog.endDialog();
                }
            }
        };

        /**
         * The reason why we defined all the listeners to navigate through the fragments
         * is that we want a confirmation when we enter a user's page.
         * The dialog are unsynchro so we define every onClickListener
         */
        loadAssociatedUsers = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentDialog != null) {
                    showProfileInformation(true);
                    ListLinkedUsersFragment f = new ListLinkedUsersFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container_fragments, f)
                            .addToBackStack(null)
                            .commit();
                    currentDialog.endDialog();
                }
            }
        };

        loadListPictures = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentDialog != null) {
                    ListPicturesUserFragment f = new ListPicturesUserFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container_fragments, f,"ListPicturesUser")
                            .addToBackStack(null)
                            .commit();
                    currentDialog.endDialog();
                }
            }
        };

        loadListMessages = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentDialog != null) {
                    ListUserMessagesFragment f = new ListUserMessagesFragment(false, false, HomeActivity.this);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container_fragments, f)
                            .addToBackStack(null)
                            .commit();
                    currentDialog.endDialog();
                }
            }
        };

        loadProfile = new View.OnClickListener() {
            public void onClick(View v) {
                if (currentDialog != null) {
                    ShowProfileFragment f = new ShowProfileFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container_fragments, f)
                            .addToBackStack(null)
                            .commit();
                    currentDialog.endDialog();
                }
            }
        };

    }

    @Override
    protected void onResume() {

        super.onResume();
        //Print the institution name
        displayInstitutionName();

    }

    public void initializeDialogInformation() {
        // ------------------------------------------- //

        int[] textViewIds = { R.id.textViewAppFullName, R.id.textViewAppLang,
                R.id.textViewAppVersion, R.id.textViewOSVersion,
                R.id.textViewEndSub, R.id.textViewBeginSub,
                R.id.textViewSynchroData, R.id.textViewSynchroRes,
                R.id.textViewAppUpdate,
                R.id.textViewSerialNumber,
        };
        String[] appInfo = {
                AppManager.getAppManager().getAppFullDisplayName(),
                AppManager.getAppManager().getLang()
                        + (!AppManager.getAppManager().getLangAlter().equals(AppManager.getAppManager().getLang())?
                        " (" + AppManager.getAppManager().getLangAlter() + ")":""),
                AppManager.getAppManager().getVersionNameAndCode(),
                Build.VERSION.RELEASE,
                Tools.formattedAsDate(
                        preferences.getString(ConnectionConstants.SHARED_PREFS_BEGIN_SUBSCRIPTION,"")),
                Tools.formattedAsDate(preferences.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION,"")),
                Tools.formattedAsDateTime(preferences.getString(ConnectionConstants.SHARED_PREFS_SYNCHRO_DATE,"")),
                Tools.formattedAsDateTime(preferences.getString(ConnectionConstants.SHARED_PREFS_SYNCHRO_RES_DATE,"")),
                Tools.formattedAsDateTime(preferences.getString(ConnectionConstants.SHARED_PREFS_FIRST_LAUNCH,"")),
                AppManager.getAppManager().getSerialNumber(),
        };
        String[] infoLabels = this.getResources().getStringArray(
                R.array.infoLabels);

        Log.d(TAG, "initializeDialogInformation()");
        if (dialogAboutUs == null)
            dialogAboutUs = new Dialog(this, R.style.DialogFullscreen);

        dialogAboutUs.setContentView(R.layout.dialog_about_us_layout);

		/* show version number and... on the info dialog */
        for (int i = 0; i < appInfo.length; i++) {
            TextView textViewInfo = (TextView) dialogAboutUs
                    .findViewById(textViewIds[i]);
            textViewInfo.setText(Html.fromHtml(infoLabels[i] + " <b>"
                    + appInfo[i] + "</b>"));
        }

        TextView textPresentation = (TextView) dialogAboutUs
                .findViewById(R.id.content);

		/* show version number and... on the info dialog */
        textPresentation.setMovementMethod(LinkMovementMethod.getInstance());

        if (ConnectionConstants.BASE_URL.contains("test.stimart.com") || ConnectionConstants.BASE_URL.contains("192.168.1.32")) {
            TextView textViewServer = (TextView) dialogAboutUs.findViewById(R.id.textViewServer);
            textViewServer.setVisibility(View.VISIBLE);
            textViewServer.setText(String.format("%s %s", "Server : ",ConnectionConstants.BASE_URL));
        }

        if (Tools.isDevelopmentMode(this)) {
            TextView textViewInfo = (TextView) dialogAboutUs.findViewById(textViewIds[2]);
            textViewInfo.setTextColor(Color.RED);
        }

        Button fermer = (Button) dialogAboutUs
                .findViewById(R.id.button_confirm);
        fermer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialogAboutUs.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed(){
        FragmentManager fm = getFragmentManager();

        //Hiding the keyboard when leaving the write activity
        if (fm.findFragmentById(R.id.frame_container_fragments) instanceof WriteMessageFragment ) {
            alertLeavingPage();
            hideKeyboard();
        } else if (fm.findFragmentById(R.id.frame_container_fragments) instanceof ListUsersFragment) {
            alertLeavingApp();
        } else {
            //If the BackStack is not empty, we renew last transaction
            fm.popBackStack();
        }

    }

    public void alertLeavingApp(){
        //Are you sure dialog
        currentDialog = new DialogManager(HomeActivity.this,
                com.dynseo.stimart.R.drawable.background_button_menu, 0,
                com.dynseo.stimart.R.string.dialog_quit_titre,
                com.dynseo.stimart.R.string.dialog_quit_main_message);
        currentDialog.setDialog();
        currentDialog.oui.setOnClickListener(quitButtonListener);
        currentDialog.setCloseListener(false, true);
        currentDialog.showDialog();
    }

    public void alertLeavingPage(){
        currentDialog = new DialogManager(HomeActivity.this,
                com.dynseo.stimart.R.drawable.background_button_menu, 0,
                com.dynseo.stimart.R.string.dialog_quit_titre,
                R.string.leaving_writing);
        currentDialog.setDialog();
        currentDialog.oui.setOnClickListener(quitWritingButtonListener);
        currentDialog.setCloseListener(false, true);
        currentDialog.showDialog();
    }

    public void hideKeyboard(){
        KeyboardView mKeyboardView = (KeyboardView) findViewById(R.id.the_custom_keyboard_view);
        mKeyboardView.setVisibility(View.GONE);
    }

    //=======================================================================================================================//
    // CAMERA *** CAMERA *** CAMERA *** CAMERA *** CAMERA *** CAMERA *** CAMERA *** CAMERA *** CAMERA ***
    //=======================================================================================================================//

    //=======================================================================================================================//
    private void previewSelectedImage(Intent theData) {
        Uri selectedImage = theData.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(
                selectedImage, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            fileUri = Uri.parse(picturePath);
            Log.d(TAG,"file uri : " + fileUri);
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        FragmentManager fm = getFragmentManager();

        if (fm.findFragmentById(R.id.frame_container_fragments) instanceof WriteMessageFragment ) {

            ImageView imgPreview = (ImageView) findViewById(R.id.imgPreview);
            imgPreview.setImageBitmap(BitmapFactory.decodeFile(picturePath, options));
            imgPreview.setVisibility(View.VISIBLE);

        }
    }

    private void previewCapturedImage() {
        try {
            //We try to write on the Write message fragment
            FragmentManager fm = getFragmentManager();

            //Hiding the keyboard when leaving the write activity
            if (fm.findFragmentById(R.id.frame_container_fragments) instanceof WriteMessageFragment ) {
                ImageView previewImage = (ImageView) findViewById(R.id.imgPreview);
                previewImage.setVisibility(View.VISIBLE);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;

                final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                        options);
                Log.d(TAG ," previewCapturedImage");
                Log.d(TAG, fileUri.getPath());
                previewImage.setImageBitmap(bitmap);

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        Log.d(TAG,"Uri : " + fileUri);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public void selectImageFromFile(){
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG," getOutputMediaFile");
                Log.d(TAG, IMAGE_DIRECTORY_NAME);
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    //**********==========Stimart manageDialog Part==========**********//

    public boolean isDateValid() {
        return isDateValid(0);
    }
    public boolean isDateValid(int nbDays) {
        // Get expiration date from shared preference and put it on LocalDate form
        String expirationDateStr = preferences.getString(
                ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, null);
        Log.i(TAG, "expiration is set to :  " + expirationDateStr);
        if (expirationDateStr != null) {

            Instant nowInstant = Instant.now();
            DateTime nowDate = nowInstant.toDateTime();
            LocalDate now = nowDate.toLocalDate();

            if( nbDays != 0 )
                now = now.plusDays(30);

            int year = Integer.valueOf(expirationDateStr.substring(0, 4));
            int month = Integer.valueOf(expirationDateStr.substring(5, 7));
            int day = Integer.valueOf(expirationDateStr.substring(8, 10));
            LocalDate expirationDate = new LocalDate(year, month, day);

            Log.i(TAG, "Actual Date : " + now + "  , Expiration date : " + expirationDate);

            return expirationDate.isAfter(now);
        }
        return false;
    }


    public void manageDialogs() {		//called by onResume or
        Log.d("----- onResume","currentRound =" + currentStep);

        boolean doVerification  = false;
        boolean doSynchroData 	= false;
        boolean doResources		= false;
        boolean doSubscription  = false;
        boolean doSubscriptionForVisit  = false;
        boolean doSubscriptionInfoEnded = false;
        boolean doCode = false;

        boolean authorized = AppManager.getAppManager().isSubscriptionValid();
        Log.d(TAG, "authorized " + authorized + ", skipOnResume " + skipOnResume);
        //The status is OK in sp, check if date expired
        if ( authorized ) {
            if( !isDateValid() ) {
                if (AppManager.getAppManager().getFreemiumName() != null) {    //go back to pure freemium
                    authorized = true;
                    AppManager.getAppManager().setIsSubscriptionValid("KO");
                    preferences.edit().putString(ConnectionConstants.SHARED_PREFS_STATE_SUBSCRIPTION, "KO").
                            putString(ConnectionConstants.SHARED_PREFS_BEGIN_SUBSCRIPTION, null).
                            putString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, null).apply();
                } else {
                    authorized = false;                //Date has expired, change status to false
                    AppManager.getAppManager().setIsSubscriptionValid("KO");
                    preferences.edit().putString(ConnectionConstants.SHARED_PREFS_STATE_SUBSCRIPTION, "KO").apply();
                }
            }
        }
        else if(AppManager.getAppManager().getFreemiumName() != null) {
            authorized = true;
        }

        if( authorized && skipOnResume ) {
            skipOnResume = false;
            return;
        }

        String mode = AppManager.getAppManager().getMode();
        boolean needsCode = AppManager.getAppManager().needsCode();
        boolean skipAccountConnection = AppManager.getAppManager().skipAccountConnection();
        String institutionType = preferences.getString(ConnectionConstants.SHARED_PREFS_INSTITUTION_TYPE, "");
        Log.d("mode et institutionType" , mode + " | " + institutionType);
        if (mode.equals("") || ( mode.equals("subscription") && !institutionType.equals("HOME") ) || Tools.isResourceTrue(this, R.string.needsCode))
            subscribe.setVisibility(View.INVISIBLE);
        else
            subscribe.setVisibility(View.VISIBLE);

        if (currentStep == STEP_START) {
            Log.d("verification step", "verification");

            if( AppManager.getAppManager().getFreemiumName() != null ) {
                //My apk is a freemium one
                if (mode.equals("")) {                //== First launch of a freemium
                    deleteAPK();
                    doVerification = true;
                }
                else
                    doResources = true;       // 	doResource each time we launch appli
            }
            else {
                if (mode.equals("")) {                //== First launch
                    deleteAPK();
                    doVerification = true;
                }
                else if (mode.equals("visit")) {    //=== Visit
                    if (!authorized) {    			//This the end of the visit mode
                        doSubscriptionForVisit = true;
                    }
                    else {
                        Log.d(TAG, "testIdentification() : test Internet : authorized=" + Http.isOnline(getApplicationContext()));
                        if (Http.isOnline(getApplicationContext())) {  //The app tells me "I am authorized", I check on the server that I have not cheated
                            doVerification = true;
                        }
                        else  // TODO : mettre en place le systeme de ressource dont on a parlé (voir trello) ==> add variable to each game to know if it need ressourcess
                            doResources = true;
                    }
                }
                else if (mode.equals("subscription")) {
                    if (!authorized) {				//This the end of subscription mode
                        if( AppManager.getAppManager().isHome(preferences) )
                            doSubscription = true;
                        else
                            doSubscriptionInfoEnded = true;
                    }
                    else {
                        doResources = true;
                    }
                }
            }
        }
        else if(currentStep == STEP_VERIFICATION_FAILURE) {		//invalidDevice
            // Device is unknown, show dialog:Connect, SignUp, Visit
            subscribeAndAddAccount = SubscribeAndAddAccount.getInstance(this);

            //For EA skip the Connect, SingUp, Visit dialog
            if (skipAccountConnection) {
                subscribeAndAddAccount.showConsumeCodeFragment();
            }
            else {
                if (!(subscribeAndAddAccount.getDialogConnectOrSignUp(true).isAdded()))
                    subscribeAndAddAccount.getDialogConnectOrSignUp(true).show(this.getFragmentManager(), "Connexion");
            }
            return; 	//Do not continue, we pop-up dialog with visit, create or connect
        }
        else if(currentStep == STEP_VERIFICATION_SUCCESS) {		//device identified
            Log.d("verification step", "verification to synchro");
            deleteGuestPersonAndResults(getString(R.string.app_name), getString(R.string.guest));

            //For EA, if the device is known, we have to send them back to us
            //Because the device already has an account associated to an email
            //and creating another account for EA may cause conflicts;
            /*if ( skipAccountConnection ){
                try {
                    Account account = Account.getAccountFromSP(preferences);
                    //Fixed bug, two cases :  if a user already have a stimart app which institutiton type is home
                    if ((account != null && (account.getCode() == null || account.getCode().equals("")))
                            //Second case : same but if institution type is institution
                            || account == null){
                        //This step means the device is already known (Stimart Account)
                        //We need to show the dialog consumeCode that will return an error "contact us"
                        subscribeAndAddAccount = SubscribeAndAddAccount.getInstance(this);
                        subscribeAndAddAccount.showConsumeCodeFragment();
                        Log.d(TAG, "wanted behaviour");
                    }
                    else {
                        if ( !authorized )     //This the end of subscription mode
                            doSubscription = true;
                        else {
                            if( mode.equals("visit") )
                                doResources = true;
                            else
                                doSynchroData = true;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if ( needsCode ) {
                try {
                    Account account = Account.getAccountFromSP(preferences);
                    if (account == null || account.getCode() == null || account.getCode().equals("")) {
                        doCode = true;
                    } else {
                        if ( !authorized )     //This the end of subscription mode
                            doSubscription = true;
                        else {
                            if( mode.equals("visit") )
                                doResources = true;
                            else
                                doSynchroData = true;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/

//            else if ( AppManager.getAppManager().isFreemiumActivated() ) {
            if ( AppManager.getAppManager().isFreemiumActivated() ) {
                doResources = true;//This the end of subscription mode
            }
            else {
                if ( !authorized )     //This the end of subscription mode
                    doSubscription = true;
                else {
                    if( mode.equals("visit") )
                        doResources = true;
                    else
                        doSynchroData = true;
                }
            }
        }
        else if (currentStep == STEP_VISIT) {
            Person guest = new Person(getString(R.string.guest), getString(R.string.app_name), new Date(), Person.sexm);
            Log.d(TAG, "Guest : " + guest.toString());
            if (checkPerson(guest).equals(getString(R.string.ok)))	// does not exist
                insertPersonLocally(guest, true, false);

            Person.currentPerson = guest;
            showPerson();
            //We have chosen visit mode, start resources
            doResources = true;
        }
        else if (currentStep == STEP_LOGIN_SUCCESS) {	//We have finished login successfully
            /*if (needsCode) {
                try {
                    Account account = Account.getAccountFromSP(preferences);
                    if (account == null || account.getCode() == null || account.getCode().equals("")) {
                        doCode = true;
                    }
                    else if( AppManager.getAppManager().isSubscriptionValid() &&
                            !AppManager.getAppManager().getMode().equals("visit") ) {
                        doSynchroData = true;
                    }
                    else {
                        doResources = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if( AppManager.getAppManager().isSubscriptionValid() &&*/
            if( AppManager.getAppManager().isSubscriptionValid() &&
                    !AppManager.getAppManager().getMode().equals("visit") ) {
                doSynchroData = true;
            }
            else {
                doResources = true;
            }
        }
        else if (currentStep == STEP_SYNCHRO) {	//We have finished synchronization
            doResources = true;
        }
        else if(currentStep == STEP_CREATE_ACCOUNT_SUCCESS) {        //account created
            doResources = true;
        }
        Log.d("onResume", "mode :" + mode + " doSubscription :" + doSubscription +", " + doSubscriptionForVisit +", " + doSubscriptionInfoEnded);

        //--------------------- Where to go now
        if( doVerification ) {
            currentStep = STEP_VERIFICATION;
            nextActivity(DeviceVerificationActivity.class, true);
        }
        else if(doSynchroData) {
            currentStep = STEP_SYNCHRO;
            Log.d("synchro step","synchro");
            nextActivity(com.dynseo.family.SynchronizationActivity.class, false, "autoLaunch", true, "type", "data");
        }
        else if( doSubscription ) {
            activateSubscription();	//will start by querying the server... to reset the dates and status
        }
        else if( doSubscriptionForVisit ) {
            //No need to start by querying the server, in visit mode, if Internet connection, it has been done
            SubscribeAndAddAccount subscribeAndAddAccount = SubscribeAndAddAccount.getInstance(this);
            subscribeAndAddAccount.showDialogSubscribe(
                    this.getResources().getString(R.string.subscription),
                    String.format(this.getResources().getString(R.string.subscription_text_for_visit_expired),
                            this.getResources().getString(R.string.subscription_price)),
                    this.getResources().getString(R.string.subscribe), false);
        }
        else if( doSubscriptionInfoEnded ) {
            nextActivity(InformationActivity.class, false);
        }
        else if (doCode) {
            subscribeAndAddAccount = SubscribeAndAddAccount.getInstance(this);
            subscribeAndAddAccount.showConsumeCodeFragment();
        }
        else {
            //NB : if we need to download resources, we will go through this part twice :
            //     before and after resources download
            boolean okResources = true;
            if( doResources )
                if(!Tools.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    ActivityCompat.requestPermissions(this, PERMISSIONS,200);
                    okResources = false;
                } else {
                    okResources = testResources();
                }
            //okResources = testResources();

            if( okResources ) {			//Nothing to do with the resources, we do last step
                currentStep = STEP_ALL_DONE;
                autoSynchro();

                Log.d("----- onResume", "personAlwaysIdentified =" + personAlwaysIdentified +  ", connected =" + connected  + ", currentPlayer =" + Person.currentPerson);
                if (personAlwaysIdentified && !connected) {
                    showDialogProfileIdentification();
                }
            }
        }
    }

    public void activateSubscription(){

        ActivateSubscriptionTask activateSubscriptionTask = new ActivateSubscriptionTask();
        try {
            String response = activateSubscriptionTask.execute().get();
            Log.d("activateSubscription", "response = " + response);

            try {
                if (response == null || response.equals("")) {
                    if( currentStep == STEP_QUIT_ON_SUBSCRIPTION_EXPIRED )
                        return;
                }
                else {
                    JSONObject jsonResponse = new JSONObject(response);
                    String subscription = jsonResponse.optString("subscriptionState", null);
                    if (subscription != null ) {
                        ConnectionConstants.setSubscriptionDates(jsonResponse, preferences, true);
                        if( currentStep == STEP_QUIT_ON_SUBSCRIPTION_EXPIRED )
                            return;

                        if (subscription.equals("OK")) {

                            String mode = AppManager.getAppManager().getMode();
                            if( !mode.equals("visit") )
                                Tools.showToast(HomeActivity.this, getString(com.dynseo.stimart.R.string.subscription_activated));

                            // download all resources if needed
                            if( AppManager.getAppManager().getFreemiumName() != null ) {
                                String resourcesType = AppManager.getAppManager().getResourcesType();
                                if (resourcesType != null && resourcesType.equals(getString(R.string.freemium_resources))) {
                                    startResources(UpdateResourcesActivity.class,false,UpdateResourcesActivity.KEY_UPDATE_PURPOSE_NO_RESOURCES,true,null,null);
                                }
                            }
                        }
                    }
                    else {        //probably no connection
                        if( currentStep == STEP_QUIT_ON_SUBSCRIPTION_EXPIRED )
                            return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        String mode = AppManager.getAppManager().getMode();
        boolean isValid = AppManager.getAppManager().isSubscriptionValid();

        Resources res = this.getResources();
        String dialogButton = res.getString(com.dynseo.stimart.R.string.subscribe);
        String dialogTitle  = res.getString(com.dynseo.stimart.R.string.subscription);
        String dialogMessage = null;

        if (mode.equals("visit")) {
            if (isDateValid()) {
                dialogMessage = String.format(res.getString(com.dynseo.stimart.R.string.subscription_text_for_visit),
                        res.getString(com.dynseo.stimart.R.string.subscription_price));
            }
            else {
                dialogMessage = String.format(res.getString(com.dynseo.stimart.R.string.subscription_text_for_visit_expired),
                        res.getString(com.dynseo.stimart.R.string.subscription_price));
            }
        }
        else if( mode.equals("subscription") ) {
            if (isDateValid()){
                dialogButton = res.getString(com.dynseo.stimart.R.string.extend);
                dialogTitle  = res.getString(com.dynseo.stimart.R.string.subscription_renew);

                int messageId = com.dynseo.stimart.R.string.subscription_text_for_renew_future;
                if( !isDateValid(30) )
                    messageId = com.dynseo.stimart.R.string.subscription_text_for_renew_close_future;

                dialogMessage = String.format(res.getString(messageId),
                        Tools.formattedAsDate(preferences.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, "")),
                        res.getString(com.dynseo.stimart.R.string.subscription_price));
            }
            else {
                dialogMessage = String.format(res.getString(com.dynseo.stimart.R.string.subscription_text_for_renew),
                        Tools.formattedAsDate(preferences.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, "")),
                        res.getString(com.dynseo.stimart.R.string.subscription_price));
            }
        }
        else if( mode.startsWith("freemium") ) {
            if ( isDateValid()){
                dialogButton = res.getString(com.dynseo.stimart.R.string.extend);
                dialogTitle  = res.getString(com.dynseo.stimart.R.string.subscription_renew);

                int messageId = com.dynseo.stimart.R.string.subscription_text_for_renew_future;
                if( !isDateValid(30) )
                    messageId = com.dynseo.stimart.R.string.subscription_text_for_renew_close_future;
                dialogMessage = String.format(res.getString(messageId),
                        Tools.formattedAsDate(preferences.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, "")),
                        res.getString(com.dynseo.stimart.R.string.subscription_price));
            }
            else {
                dialogMessage = String.format(res.getString(com.dynseo.stimart.R.string.subscription_text_for_freemium),
                        res.getString(com.dynseo.stimart.R.string.subscription_price));
            }
        }
        subscribeAndAddAccount = SubscribeAndAddAccount.getInstance(this);
        subscribeAndAddAccount.showDialogSubscribe(dialogTitle, dialogMessage, dialogButton, false);
    }

    public void startResources(final Class nextClass, final boolean forResult,
                               final String key, final boolean value, final String type, final String valueType){
        if (Tools.isAvailableMemoryEnoughForDownload(HomeActivity.this.getResources().getInteger(R.integer.space_needed_for_download_resources))){
            if (Http.isOnline(HomeActivity.this) && !Http.isWifiOn(HomeActivity.this)){
                currentDialog = new DialogManager(HomeActivity.this,
                        R.drawable.background_button_menu,
                        R.drawable.background_dialog_menu,
                        getString(R.string.error_data_connection_title),
                        getString(R.string.error_data_connection)
                );
                currentDialog.setDialogString();
                currentDialog.oui.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        currentDialog.endDialog();
                        nextActivity(nextClass,forResult,key,value,type,valueType);
                    }
                });
                currentDialog.setCloseListener(false, true);
                currentDialog.showDialog();
            } else {
                nextActivity(nextClass,forResult,key,value,type,valueType);
            }
        } else {
            Tools.showToast(this,getResources().getString(R.string.not_enough_space_on_device));
        }
    }

    @Override
    public void onSynchroFinish() {
        //Load ListUsersMessagesFragment
        loadListMessagesFragment(true);
    }

    public class ActivateSubscriptionTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                Log.i("TAG","activateSubscription()");
                String path =  StimartConnectionConstants.urlActivateSubscription();
                Log.d("activateSubscription",path);
                String serverResponse = Http.queryServer(path);
                return serverResponse;
            }
            catch (Exception e) {
                Log.e("error", e.toString());
                return null;
            }
        }
    }

    public boolean testResources(){
        return true;
    }

    public void autoSynchro() { // Do Synchronization automatically every 6 hours

        if (Http.isOnline(this)) {

            String mode = AppManager.getAppManager().getMode();
            if( AppManager.getAppManager().isSubscriptionValid() &&
                    !mode.equals("") && !mode.equals("visit") ) {
                String synchroDate = preferences.getString(StimartConnectionConstants.SHARED_PREFS_SYNCHRO_DATE, null);
                if (synchroDate != null) {
                    try {
                        synchroDate = synchroDate.replace(" ", "T"); // exp : 2015-08-17T15:18:00
                        DateTime lastSynchroDate = new DateTime(synchroDate);

                        Instant nowInstant = Instant.now();
                        DateTime now = nowInstant.toDateTime();
                        Period p = new Period(lastSynchroDate, now);
                        Log.d("period", "lastSynchroDate = " + lastSynchroDate + " | now = " + now + " | period = " + p.getHours());
                        if (p.getHours() >= 6 || p.getDays()>= 1 || p.getWeeks() >= 1) {
                            Log.d("autoSynchro", "autoSynchro YEEEESSSSS");
                            SynchronizationData synchronization = new SynchronizationData(this, preferences, "data");
                            SynchronizationTask synchronizationTask = new SynchronizationTask(synchronization, this , preferences);
                            synchronizationTask.execute();
                        } else {
                            Log.d("autoSynchro", "autoSynchro NOOOOOO, delay = "+p.getHours());
                        }
                    }
                    catch(Exception e) {
                        preferences.edit().putString(StimartConnectionConstants.SHARED_PREFS_SYNCHRO_DATE, null).apply();
                        //Send synchroDate and serialNumber to BugSnag
                        MetaData metaData = new MetaData();
                        metaData.addToTab("User", "synchroDate", synchroDate);

                        Bugsnag.notify(new Exception("autoSynchro"), metaData);
                    }
                } else {
                    Log.d("autoSynchro", "first autosynchro because synchroDate is null");
                    SynchronizationData synchronization = new SynchronizationData(this, preferences, "data");
                    SynchronizationTask synchronizationTask = new SynchronizationTask(synchronization, this , preferences);
                    synchronizationTask.execute();
                }
            }
        }
        else
            Log.d("NoInternet", "noInternet");
    }

    /* when user click on a button on the recipe_cat_main interface */
    public void onClick(View v) throws JSONException, ClassNotFoundException {
        Log.i(TAG, "onClick() - choose game");
        int id = v.getId();

		/*
		 * If you are in a library you have to change all the switch/case
		 * statements to if/else blocks from ADT version 14
		 */
        skipOnResume = false;

        // === info
        if (id == R.id.menu_button_info) {
            skipOnResume = true;
            initializeDialogInformation();
            dialogAboutUs.show();
        }
        // === subscribe
        else if (id == R.id.menu_button_subscribe) {
            activateSubscription();
        }
        // === synchro
        else if (id == R.id.menu_button_synchro) {
            if (dialogSyncUpdate == null)
                initializeDialogSyncUpdate();
            dialogSyncUpdate.show();
        }
        // === settings
        else if (id == R.id.menu_button_settings) {
            skipOnResume = true;
            Log.e(TAG, "Settings");
            nextActivity(SettingsActivity.class, false);
        }
        // === connection
        else if (id == R.id.menu_button_connexion) {
            skipOnResume = true;
            if (connected && !personAlwaysIdentified) { // a player is
                // connected, the button performs a disconnection
                Person.currentPerson = null;
                showPerson();
            } else { // we load the dialog to select a profile
                showDialogProfileIdentification();
            }
        }
        // === profile creation
//        else if (existCreateButton && id == R.id.menu_button_create) {
        else if (id == R.id.menu_button_create) {
            skipOnResume = true;
            if (connected) { // If connected, the create button is used to show
                // profile
                if (dialogShowProfile == null)
                    initializeDialogShowProfile();
                showPersonInPersonForm();
            } else {
                showDialogCreatePerson();
            }
        }
    }

    public void initializeDialogSyncUpdate() {
        // ------------------------------------------- //

        dialogSyncUpdate = new Dialog(this,
                R.style.DialogFullscreen);
        dialogSyncUpdate.setContentView(R.layout.dialog_synchro_update_layout);

        Button buttonSynchro = (Button) dialogSyncUpdate
                .findViewById(R.id.buttonSynchro);
//        Button buttonUpdateResources = (Button) dialogSyncUpdate
//                .findViewById(R.id.buttonUpdateResources);
        Button buttonUpdate = (Button) dialogSyncUpdate
                .findViewById(R.id.buttonUpdate);

        buttonSynchro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if( AppManager.getAppManager().isSubscriptionValid() &&
                        !AppManager.getAppManager().getMode().equals("visit") ) {
                    dialogSyncUpdate.dismiss();

                    nextActivity(SynchronizationActivity.class, false, "autoLaunch", false, "type", "data");
                }
                else {			//No synchro if visit or freemium (if freemium, isSubscriptionValid is false)
                    Tools.showToast(HomeActivity.this, getString(R.string.no_synchro_in_visit_mode));
                }
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                Intent intent = new Intent(HomeActivity.this, AndroidDatabaseManager.class);
//                startActivity(intent);
//
                if( AppManager.getAppManager().isSubscriptionValid() &&
                        !AppManager.getAppManager().getMode().equals("visit") ) {
                    dialogSyncUpdate.dismiss();
                    nextActivity(UpdateAppActivity.class, false);
                }
                else {			//No synchro if visit or freemium (if freemium, isSubscriptionValid is false)
                    Tools.showToast(HomeActivity.this, getString(R.string.no_synchro_in_visit_mode));
                }
            }
        });

//        buttonUpdateResources.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if( AppManager.getAppManager().isSubscriptionValid() &&
//                        !AppManager.getAppManager().getMode().equals("visit") ) {
//                    dialogSyncUpdate.dismiss();
//                    startResources(SynchronizationActivity.class, false, "autoLaunch", false, "type", "resources");
//                }
//                else {			//No synchro if visit or freemium (if freemium, isSubscriptionValid is false)
//                    Tools.showToast(HomeActivity.this, getString(R.string.no_synchro_in_visit_mode));
//                }
//            }
//        });
    }

}
