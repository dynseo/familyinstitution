package com.dynseo.familyinstitution.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;

import com.dynseo.family.models.Message;
import com.dynseo.familyinstitution.R;
import com.dynseo.familyinstitution.tools.FamilyTools;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseolibrary.tools.ImageLoader;

import java.util.List;

/**
 * Created by Maxime Gelly on 11/07/2016.
 */
public class LoadGalleryImagesTask extends AsyncTask<Void, Void, Void> {

    private ProgressDialog aDialog;
    private ImageLoader imageLoader;
    private List<Message> messageList;
    private String IMAGE_SAVE_PATH;
    private PageLoaderInterface requester;
    private Resources resources;

    public LoadGalleryImagesTask(PageLoaderInterface imageLoaderInterface, Context context,
                                 ImageLoader anImageLoader, List<Message> aListOfMessages, Resources res) {

        aDialog = new ProgressDialog(context);
        requester = imageLoaderInterface;
        imageLoader = anImageLoader;
        messageList = aListOfMessages;
//        IMAGE_SAVE_PATH = "/data/data/" + context.getPackageName() + "/";
        IMAGE_SAVE_PATH = AppResourcesManager.getAppResourcesManager().getPathImagesFamily();
        resources= res;

    }

    /** progress dialog to show user that the backup is processing. */
    /** application context. */
    @Override
    protected void onPreExecute() {
        aDialog.setMessage(resources.getString(R.string.wait));
        aDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        int i = 0;
        for(Message message : messageList){
            String url = IMAGE_SAVE_PATH + message.getServerId() + ".jpg";

            //Developer.android best practice to load bitmap into UI Receiver
            imageLoader.addBitmap(i, FamilyTools.decodeSampledBitmapFromResource(url, 400, 400));
            i++;
        }

        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (aDialog.isShowing()) {
            aDialog.dismiss();
        }
        requester.onImagesLoaded();
    }

}
