package com.dynseo.familyinstitution.tasks;

/**
 * Created by Maxime Gelly on 11/07/2016.
 */
public interface PageLoaderInterface {

    void onImagesLoaded();
}
