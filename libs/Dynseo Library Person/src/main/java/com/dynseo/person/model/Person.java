package com.dynseo.person.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mathias on 07/10/2016.
 */

public interface Person {
    public String getName();
    public String getFirstName();
    public JSONObject toJSON() throws JSONException;
}
