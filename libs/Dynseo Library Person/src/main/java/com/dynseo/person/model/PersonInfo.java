package com.dynseo.person.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * All info about a player playing connected to the TV application.
 *
 * Created by amadeus on 25.04.16.
 */
public class PersonInfo {
    static PersonFactory personFactory;

    private Person person;
    private JSONObject jsonPlayer;
    private int score;
    /**
     * 1: Last answer was right
     * 2: Last answer was wrong
     * 0 and all others: Neutral state
     */
    private int lastAnswerSuccessState;

    static public void setPersonFactory(PersonFactory aPersonFactory) {
        personFactory = aPersonFactory;
    }

    public PersonInfo() {

    }
    public PersonInfo(JSONObject jPlayer, int score, int lastAnswerState) throws JSONException {
        this.person = personFactory.createPerson(jPlayer);
        this.score = score;
        this.lastAnswerSuccessState = lastAnswerState;

        try {
            this.jsonPlayer = person.toJSON();
        } catch (JSONException e) {
            // TODO
        }
    }

    public PersonInfo(JSONObject jPlayer, int score) throws JSONException {
        this(jPlayer, score, 0);
    }

    public PersonInfo(JSONObject jPlayer) throws JSONException {
        this(jPlayer, 0);
    }

//    public PersonInfo(Player player, int score, int lastAnswerState) {
//        this.player = player;
//        this.score = score;
//        this.lastAnswerSuccessState = lastAnswerState;
//
//        try {
//            this.jsonPlayer = player.toJSON();
//        } catch (JSONException e) {
//            // TODO
//        }
//    }
//
//    public PersonInfo(Player player, int score) {
//        this(player, score, 0);
//    }
//
//    public PersonInfo(Player player) {
//        this(player, 0);
//    }

    public Person getPerson() {
        return person;
    }

    public JSONObject getJsonPlayer() {
        return jsonPlayer;
    }

    public int getScore() {
        return score;
    }

    public void resetScore() {
        this.score = 0;
    }

    public void incrementScore() {
        score++;
    }

    public int getLastAnswerState() {
        return lastAnswerSuccessState;
    }

    public void setLastAnswerSuccessful() {
        lastAnswerSuccessState = 1;
    }

    public boolean isLastAnswerSuccessful() {
        return lastAnswerSuccessState == 1;
    }

    public void setLastAnswerFailure() {
        lastAnswerSuccessState = 2;
    }

    public boolean isLastAnswerFailure() {
        return lastAnswerSuccessState == 2;
    }

    public void resetLastAnswerState() {
        lastAnswerSuccessState = 0;
    }

    @Override
    public boolean equals(Object other){
        if (other == null) {
            return false;
        }

        if (other == this) {
            return true;
        }

        if (!(other instanceof PersonInfo)) {
            return false;
        }

        PersonInfo info = (PersonInfo) other;
        return this.getPerson().getName().equals(info.getPerson().getName()) &&
               this.getPerson().getFirstName().equals(info.getPerson().getFirstName());
    }
}
