package com.dynseo.stimart.common.models;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mathias on 07/10/2016.
 */

public class PersonFactory implements com.dynseo.person.model.PersonFactory {

    public PersonFactory() {

    }

    @Override
    public Person createPerson(JSONObject aJSONplayer) throws JSONException {
        return new Person(aJSONplayer) ;
    }
}
