package com.dynseo.stimart.common.server;

import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;

import com.dynseo.stimart.common.activities.UpdateResourcesActivity;
import com.dynseolibrary.tools.Tools;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Mathias on 21/10/2016.
 */

public class DownloadFile {
    private static final String TAG = "DownloadFile";
    private static final int TIME_OUT_CONNECTION = 5000;
    private static final int TIME_OUT_SOCKET = 120000;

    DownloadFileInterface requester;
    String savePath;

    public DownloadFile(DownloadFileInterface requester, String savePath) {
        Log.d(TAG, "DownloadFile");
        this.requester = requester;
        this.savePath = savePath;

        createSDCardDir();
    }

    public void downloadFileByUrl(final String urlString, final  String fileName) {
        Log.d(TAG, "downloadFileByUrl");
        Log.d("downloadFileByUrl", urlString);
        int count;
        try {
            URL url = new URL(urlString);
            URLConnection conection = url.openConnection();
            conection.connect();
            int lengthOfFile = conection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(), 8192);
            final File file = new File(savePath + File.separator + fileName);

            FileOutputStream output = new FileOutputStream(file);
            byte data[] = new byte[102400];
            long total = 0;
            while ((count = input.read(data)) != -1 && !Thread.currentThread().isInterrupted()) {
                total += count;
                requester.onDownloadProgress(total, lengthOfFile);
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
            if(!Thread.currentThread().isInterrupted()) {
                requester.onDownloadSuccess();
            }
            else {
                Log.d(TAG, "Re-download resources");
                Thread downloadThread = new Thread() {
                    public void run() {
                        downloadFileByUrl(urlString, fileName);
                    }
                };
                downloadThread.start();
            }
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            requester.onDownloadFailure(e);
        }
    }

    public void createSDCardDir() {
        Log.d(TAG, "createSDCardDir()");
        Log.d("createSDCardDir", savePath);
        if (Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {
            File file = new File(savePath);
            if (!file.exists()) {
                if (file.mkdirs()) {
                    Log.i(TAG, "dir don't exist, make dir ok :" + savePath);
                }
                else {
                    Log.e(TAG, "dir don't exist, error - make dir");
                }
            }
        }
    }

}

