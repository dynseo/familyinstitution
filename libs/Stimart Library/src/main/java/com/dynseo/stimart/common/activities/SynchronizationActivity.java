

package com.dynseo.stimart.common.activities;

import com.dynseo.stimart.R;
import com.dynseo.stimart.common.models.SynchronizationFactory;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.platform.SynchroFinishInterface;
import com.dynseolibrary.platform.SynchroInterface;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.platform.server.SynchronizationTask;
import com.dynseolibrary.tools.Tools;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.reflect.Constructor;

/**
 * User synchronisation activity
 * modified by Viet and Dominique
 * @date 14/03/2016
 */
public abstract class SynchronizationActivity extends Activity implements SynchroFinishInterface {

    private static String TAG = "SynchronizationActivity";

    protected SharedPreferences sharedPrefs;

    Dialog dialog;
    protected ProgressBar progressBar;
    protected Button continueBtn;
    protected TextView text;
    protected OnClickListener quit;
    protected int endMessage;

    protected String synchroClass;
    protected String codeForEndOfSynchro;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        boolean autoLaunch = intent.getExtras().getBoolean("autoLaunch");
        final String valueType = intent.getExtras().getString("type");

        // Management of specialized synchroInterface
        synchroClass = intent.getExtras().getString("synchroClass");
        codeForEndOfSynchro = intent.getExtras().getString("code");

        Log.d(TAG, "onCreate()");
        Log.d(TAG, String.valueOf(autoLaunch) + "     " + valueType + " codeForEndOfSynchro :" + codeForEndOfSynchro);

        dialog = new Dialog(SynchronizationActivity.this, R.style.DialogFullscreen);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_synchronization_layout);

        TextView titreSynchro = (TextView) dialog.findViewById(R.id.textView1);
        continueBtn = (Button) dialog.findViewById(R.id.button_begin_synchro);
        text = (TextView) dialog.findViewById(R.id.text);
        progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);

        quit = new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
        };

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        int titleId = -1;
        int lastSyncRes = R.string.dernieresynchro;
        int noSyncRes   = R.string.passynchro;
        String sharedPrefKey = StimartConnectionConstants.SHARED_PREFS_SYNCHRO_DATE;

        if (valueType != null && valueType.equals("resources")) {
            titleId = R.string.update_resources;
            lastSyncRes = R.string.dernieresynchro_res;
            noSyncRes   = R.string.passynchro_res;
            sharedPrefKey = StimartConnectionConstants.SHARED_PREFS_SYNCHRO_RES_DATE;
        }

        if( titleId != -1 )
            titreSynchro.setText(titleId);

        String synchroDate = sharedPrefs.getString(sharedPrefKey, null);
        synchroDate = Tools.formattedAsDateTime(synchroDate);

        /* for the first synchronization or when user delete local data */
        if (synchroDate == null) {
            text.setText(noSyncRes);
        } else {
            text.setText(getString(lastSyncRes) + " " + synchroDate);
        }

        if( autoLaunch ) {
          launch(valueType);
        }
        else {
            final OnClickListener launch = new OnClickListener() {
                    public void onClick(View v) {
                        launch(valueType);
                    }
                };
                continueBtn.setOnClickListener(launch);

                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        finish();
                    }
                });
        }
        dialog.show();

    }

    protected abstract void launch (String valueType);

//    protected void launch(String valueType) {
//        if (Http.isOnline(getApplicationContext())) {
//            SynchroInterface synchronization = null;
//            if (synchroClass != null) {
//                try {
//                    Class syncClass = Class.forName(synchroClass);
//                    Constructor constructor =
//                            syncClass.getDeclaredConstructor(Context.class, SharedPreferences.class, String.class);
//                    synchronization = (SynchroInterface) (constructor.newInstance(this, sharedPrefs, valueType));
//                } catch (Exception e) {
//                    return;
//                }
//            }
//            if (valueType == null || valueType.equals("data")) {
//                if (synchronization == null)
//                    synchronization = synchronizationFactory.createSynchronizationData(this, sharedPrefs, valueType);
//                text.setText(R.string.synchroencours);
//                endMessage = R.string.synchrotermine;
//            } else if (valueType.equals("resources")) {
//                if (synchronization == null)
//                    synchronization = synchronizationFactory.createSynchronizationResources(this, sharedPrefs, valueType, codeForEndOfSynchro);
//                text.setText(R.string.synchroencours_res);
//                endMessage = R.string.synchrotermine_res;
//            }
//            SynchronizationTask synchronizationTask = new SynchronizationTask(synchronization, this, sharedPrefs);
//            synchronizationTask.execute();
//            continueBtn.setEnabled(false);
//            continueBtn.setBackgroundResource(R.drawable.round_button_off);
//            continueBtn.setOnClickListener(quit);
//            progressBar.setVisibility(View.VISIBLE);
//        } else
//            Tools.showToast(getApplicationContext(), getString(R.string.erreurconnexioninternet));
//    }

    @Override
    public void onSynchroFinish() {
        progressBar.setVisibility(View.GONE);
        if(continueBtn != null) {
            continueBtn = (Button) dialog.findViewById(R.id.button_begin_synchro);
            continueBtn.setEnabled(true);
            continueBtn.setBackgroundResource(R.drawable.background_button_menu_rounded);
            continueBtn.setText(R.string.retour);
        }

        if(text != null) {
            text = (TextView) dialog.findViewById(R.id.text);
            text.setText(endMessage);
        }
    }
}