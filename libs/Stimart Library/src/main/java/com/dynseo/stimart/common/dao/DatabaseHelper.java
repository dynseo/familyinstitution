package com.dynseo.stimart.common.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Database for saving game results
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = "DatabaseHelper";
    private static final String DB_NAME = "suividb.sqlite";
//    private static final int DB_VERSION = 14;	//This version introduces cleanResidentRecords
    private static final int DB_VERSION = 24;	//From 20 to 21 : added points to result, 21->22 : serverId in table Result
    public SQLiteDatabase myDataBase;
    Context context;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */ 
    public DatabaseHelper(Context context) {
    	super(context, DB_NAME, null, DB_VERSION);
        Log.d(TAG,"constructor");
    	this.context = context;
    }

	@Override
	public synchronized void close() {
		Log.d(TAG,"close");
		if (myDataBase != null)
			myDataBase.close();
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "onCreate()");
		db.execSQL(Extractor.TABLE_PERSON_CREATE);
		Log.i(TAG, "TABLE_PERSON_CREATE");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		Log.d(TAG, "onUpgrade()");
		Cursor cursor1 = db.query(Extractor.TABLE_PERSON, null, null, null, null, null, null);
		/* Add column conflitId, pseudo, role to table PERSON if not exists */
		boolean conflictId_column_exist = false;
		boolean role_column_exist = false;

		String names1 [] = cursor1.getColumnNames();
		
		for (String name : names1) {
			if (name.equals(Extractor.COL_CONFLICT_ID)) {
				conflictId_column_exist = true;
			}
			else if (name.equals(Extractor.COL_PERSON_ROLE)) {
				role_column_exist = true;
			}
		}

		if (!conflictId_column_exist) {
			Log.d(TAG, "ALTER_TABLE_PERSON");
			db.execSQL(Extractor.ALTER_TABLE_PERSON);
			Log.d(TAG, "ALTER_TABLE_PERSON");
		}
		if (!role_column_exist) {
			Log.d(TAG, "ALTER_TABLE_PERSON_FOR_ROLE");
			db.execSQL(Extractor.ALTER_TABLE_PERSON_FOR_ROLE);			//add column ROLE to table PERSON 
			db.execSQL(Extractor.ALTER_TABLE_PERSON_SERVER_ID_ROLE);	
			Log.d(TAG, "ALTER_TABLE_PERSON_FOR_ROLE");
		}

		cursor1.close();
		
		/* Clean person records, if needed */
//		Extractor.cleanResidentRecords(db);

	}

	public ArrayList<Cursor> getData(String Query){
		//get writable database
		SQLiteDatabase sqlDB = this.getWritableDatabase();
		String[] columns = new String[] { "mesage" };
		//an array list of cursor to save two cursors one has results from the query
		//other cursor stores error message if any errors are triggered
		ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
		MatrixCursor Cursor2= new MatrixCursor(columns);
		alc.add(null);
		alc.add(null);

		try{
			String maxQuery = Query ;
			//execute the query results will be save in Cursor c
			Cursor c = sqlDB.rawQuery(maxQuery, null);

			//add value to cursor2
			Cursor2.addRow(new Object[] { "Success" });

			alc.set(1,Cursor2);
			if (null != c && c.getCount() > 0) {
				alc.set(0,c);
				c.moveToFirst();

				return alc ;
			}
			return alc;
		} catch(SQLException sqlEx){
			Log.d("printing exception", sqlEx.getMessage());
			//if any exceptions are triggered save the error message to cursor an return the arraylist
			Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
			alc.set(1,Cursor2);
			return alc;
		} catch(Exception ex){
			Log.d("printing exception", ex.getMessage());

			//if any exceptions are triggered save the error message to cursor an return the arraylist
			Cursor2.addRow(new Object[] { ""+ex.getMessage() });
			alc.set(1,Cursor2);
			return alc;
		}
	}
}