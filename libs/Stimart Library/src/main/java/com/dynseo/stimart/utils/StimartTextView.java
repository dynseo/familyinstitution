package com.dynseo.stimart.utils;

import com.dynseo.stimart.R;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class StimartTextView extends TextView {

	public StimartTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		parseAttributes(context, attrs);
	}

	public StimartTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		parseAttributes(context, attrs);
	}

	public StimartTextView(Context context) {
		super(context);
	}

	/*
	public void setTypeface(Typeface tf, int style) {
		if (style == Typeface.BOLD) {
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), getContext().getResources()
							.getString(R.string.default_typeface_bold)));
		} else {
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), getContext().getResources()
							.getString(R.string.default_typeface)));
		}
	}
	*/


	/**
	 * Parse the attributes.
	 *
	 * @param context The Context the view is running in, through which it can access the current theme, resources, etc.
	 * @param attrs   The attributes of the XML tag that is inflating the view.
	 */
	private void parseAttributes(Context context, AttributeSet attrs) {
		int index = 0;
		int attrsCount = attrs.getAttributeCount();
		while (index < attrsCount && !attrs.getAttributeName(index).equals("textStyle")) {
			index++;
		}
		boolean isBold = false;
		if (index < attrsCount) {
			if (attrs.getAttributeName(index).equals("textStyle")) {
				try {
					if (Integer.decode(attrs.getAttributeValue(index)) == Typeface.BOLD) {
						isBold = true;
					}
				} catch(NumberFormatException e) {
					System.err.println(e);
				}
			}
		}
		if (isBold) {
			super.setTypeface(Typeface.createFromAsset(
					context.getAssets(), context.getResources()
							.getString(R.string.default_typeface_bold)));
		}
		else {
			super.setTypeface(Typeface.createFromAsset(
					context.getAssets(), context.getResources()
							.getString(R.string.default_typeface)));
		}
	}
}