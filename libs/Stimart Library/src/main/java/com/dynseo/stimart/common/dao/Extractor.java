package com.dynseo.stimart.common.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;

import com.dynseo.stimart.R;
import com.dynseo.stimart.common.models.GamePersonInfo;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.utils.StringFormatter;

/**
 *  Extractor for the user interface 
 */
public class Extractor {
	
	private static final String TAG = "Extractor";
    protected Context context;
    protected DatabaseHelper dbHelper;
    protected SQLiteDatabase db;
	
	public static final String TABLE_PERSON = "person";
	public static final String TABLE_RESULT = "result";
	public static final String TABLE_GAME_PERSON_INFO = "gamePersonInfo";
	public static final String COL_ID_PERSON = "idPerson";
	
	// ********************** TABLES FOR STIMART ********************* //
	// ********************** TABLE FROM SERVER ********************** //
	public static final String COL_ID = "id";
	public static final int NUM_COL_ID = 0 ;
	public static final String COL_SERVER_ID = "serverId";
	public static final int NUM_COL_SERVER_ID = 1 ;
	
	// ************************* TABLE PERSON ************************* //
	public static final String COL_PERSON_ROLE = "role";
	public static final String COL_NAME = "name";
	public static final String COL_FIRSTNAME = "firstname";
	public static final String LOWER_COL_FIRSTNAME = "lower(firstname)";
	public static final String COL_PSEUDO = "pseudo";
	public static final String COL_BIRTHDATE = "birthdate";
	public static final String COL_SEX = "sex";
	public static final String COL_CONFLICT_ID = "conflictId";

	public static final int	NUM_COL_NAME = 2;
	public static final int	NUM_COL_FIRSTNAME = 3;
	public static final int NUM_COL_PSEUDO = 4;
	public static final int	NUM_COL_BIRTHDATE = 5;
	public static final int NUM_COL_SEX = 6;
	public static final int NUM_COL_CONFLICT_ID = 7;
	public static final int	NUM_COL_PERSON_ROLE = 8;

	

	
	// ************************* TABLE GAME_PERSON_INFO ************************* //
	public static final String COL_INFO1 = "info1";
	public static final String COL_INFO2 = "info2";
//	public static final int NUM_COL_INFO = 4;

    /* Create table PERSON */
    static final String TABLE_PERSON_CREATE = "CREATE TABLE " + TABLE_PERSON + "(" + COL_ID +" PRIMARY KEY NOT NULL, "
    													+ COL_SERVER_ID + "," 
    													+ COL_NAME + "," + COL_FIRSTNAME + "," 
    													+ COL_PSEUDO + "," + COL_BIRTHDATE + "," + COL_SEX  + "," 
    													+ COL_CONFLICT_ID + "," + COL_PERSON_ROLE + ");";
    

    static final String ALTER_TABLE_PERSON = "ALTER TABLE "+ TABLE_PERSON + " ADD "+ COL_CONFLICT_ID + " INT";
    //SQLIte only can add a column at the end of a table, AFTER is not recognized 
    //static final String ALTER_TABLE_PERSON_FOR_ROLE 	= "ALTER TABLE "+ TABLE_PERSON + " ADD "+ COL_PERSON_ROLE + " AFTER " + COL_SERVER_ID;
    static final String ALTER_TABLE_PERSON_FOR_ROLE 	= "ALTER TABLE "+ TABLE_PERSON + " ADD "+ COL_PERSON_ROLE;
    static final String ALTER_TABLE_PERSON_SERVER_ID_ROLE = "UPDATE "+ TABLE_PERSON + " SET " +  COL_PERSON_ROLE + "='A', " + COL_SERVER_ID + "=null WHERE " + COL_SERVER_ID + " = '-1'";

    



	//============================================================================================================//
	//============================================================================================================//
	public Extractor(Context context) {
		this.context = context;
		dbHelper = new DatabaseHelper(context);
	}
	
	public Extractor open() {
		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(context);
		}
		db = dbHelper.getWritableDatabase();
		Log.d("openDb", "db " + db );
		return this;
	}
	
	public void close() {
		 if (db != null) {
	        db.close();
	     }
	     if (dbHelper != null) {
	        dbHelper.close();
	     }
	     Log.d("close", "db " + db );
	}
	
	public SQLiteDatabase getBDD(){
		return db;
	}

	
	public long insertPerson(Person person) {
		ContentValues values = new ContentValues();
		values.put(COL_ID, this.getResidentNumber());
		values.put(COL_SERVER_ID, person.getServerId());
		values.put(COL_PERSON_ROLE, person.getRole());
		values.put(COL_FIRSTNAME, person.getFirstName() );
		values.put(COL_NAME, person.getName() );
		values.put(COL_BIRTHDATE, person.getBirthdateStrFormatDatabase(context));
		values.put(COL_SEX, person.getSex());
		values.put(COL_CONFLICT_ID, person.getConflictId());
		if(person.getConflictId() != null) {
			Log.i(TAG, "insertPerson conflictId = "+person.getConflictId());
		}
		return db.insert(TABLE_PERSON, null, values);
	}



	
	private static Person cursorToPerson(Cursor c) {
		
		if(c.getCount() == 0)
			return null;
		if(c.getPosition() == -1)
			c.moveToFirst();
		
		Person person = new Person();
		
		person.setId(c.getInt(NUM_COL_ID));
		person.setServerId(c.getString(NUM_COL_SERVER_ID));
		person.setRole(c.getString(NUM_COL_PERSON_ROLE));
		person.setFirstname(c.getString(NUM_COL_FIRSTNAME));
		person.setName(c.getString(NUM_COL_NAME));
		person.setBirthdate(c.getString(NUM_COL_BIRTHDATE));
		person.setSex(c.getString(NUM_COL_SEX));
		person.setConflictId(c.getString(NUM_COL_CONFLICT_ID));
		
		return person;
	}


	//============================================================================================================//
	// *** GET OBJECT DB *** //
	// *** PERSON *** //
	
	static public void cleanResidentRecords(SQLiteDatabase db) {
		
		/* ===== 1st step : we select all persons with serverId null and remove them if there exists
		                  a similar person with a serverId     ===== */
		
		//=== Select all the persons with serverId null
		Cursor cursor = db.query(TABLE_PERSON, null, COL_SERVER_ID + " is null", null, null, null, null);
		ArrayList<Integer> idsToDelete = new ArrayList<Integer>();
		Hashtable<Integer, Integer> idsToChange = new Hashtable<Integer, Integer> ();
		
		while(cursor.moveToNext()) {
			
			int id           = cursor.getInt(NUM_COL_ID);
			String name		 = cursor.getString(NUM_COL_NAME);
			String firstname = cursor.getString(NUM_COL_FIRSTNAME);
			
			Log.i(TAG, "Clean ? id, name, firstname :" + id + ", " + name + ", " + firstname);
			
			//=== Select, if exists, a person with serverId and same name and firstname
			Cursor c = db.query(TABLE_PERSON, null, COL_SERVER_ID + " is not null AND " + COL_NAME + " LIKE \"" + convertString(name) + "\" AND " +COL_FIRSTNAME+ " LIKE \"" + convertString(firstname) +"\"", null, null, null, null);
			
			if( c.getCount() != 0 ) { 
				c.moveToFirst();
				int idToKeep = c.getInt(NUM_COL_ID);
				
				c.close();
		
				//=== Attach the results with idPerson equal to id to idPerson equal to idToKeep
				//ContentValues values = new ContentValues();
				//values.put(COL_ID_PERSON, idToKeep);
				//db.update(TABLE_RESULT, values, COL_ID_PERSON + "=" + id, null);
				
				//=== Delete the person with id equal to id 
				//db.delete(TABLE_PERSON, COL_ID + "=" + id, null);
				idsToDelete.add(id);
				idsToChange.put(id, idToKeep);
				
				Log.i(TAG, "Clean ? we moved :" + id +" to " + idToKeep);
			}
		}
		cursor.close();
		
		for(int idToDelete : idsToDelete) {
			
			Integer idToKeep = idsToChange.get(idToDelete);
			if( idToKeep != null ) {
				ContentValues values = new ContentValues();
				values.put(COL_ID_PERSON, idToKeep);
				db.update(TABLE_RESULT, values, COL_ID_PERSON + "=" + idToKeep, null);
			}
			db.delete(TABLE_PERSON, COL_ID + "=" + idToDelete, null);
		}
		
		/* ===== 2nd step : we select all "doubles" with same serverId. We keep the first of them ===== */
		
		/* SELECT count(*) as count, serverId FROM person group by serverId having count(*) > 1 order by count */
		//query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
		
		String[] columns = {"count(*) as count", COL_SERVER_ID};
		//=== Select all the persons with serverId null
		cursor = db.query(TABLE_PERSON, columns, null, null, COL_SERVER_ID, "count > 1", "count");
		idsToDelete =  new ArrayList<Integer>();
		idsToChange = new Hashtable<Integer, Integer>();
		
		while(cursor.moveToNext()) {
			
			int count    = cursor.getInt(0);
			int serverId = cursor.getInt(1);
			
			Log.i(TAG, "Multiple records ? count, serverId :" + count + ", " + serverId);
			
			//=== Select all the persons with this serverId
			String[] returnColumns = {COL_ID};
			Cursor c = db.query(TABLE_PERSON, returnColumns, COL_SERVER_ID + "=\"" + serverId + "\"", null, null, null, null);
			int indexLoop = 0;
			int idToKeep  = 0;
			while( c.moveToNext() ) {
				int id =  c.getInt(0);
			
				if( indexLoop == 0 ) {
					idToKeep = id;
				}
				else {
					//=== Attach the results with idPerson equal to id to idPerson equal to idToKeep
					//ContentValues values = new ContentValues();
					//values.put(COL_ID_PERSON, idToKeep);
					//db.update(TABLE_RESULT, values, COL_ID_PERSON + "=" + id, null);
					
					//=== Delete the person with id equal to id 
					//db.delete(TABLE_PERSON, COL_ID + "=" + id, null);
					idsToDelete.add(id);
					idsToChange.put(id, idToKeep);
					 
					Log.i(TAG, "Doubles ? we moved :" + id +" to " + idToKeep);
				}	
				indexLoop++;		
			}			//end of while
			c.close();
		}				//loop on those entries with multiple serverIds
		cursor.close();
		
		for(int idToDelete : idsToDelete) {
			
			Integer idToKeep = idsToChange.get(idToDelete);
			if( idToKeep != null ) {
				ContentValues values = new ContentValues();
				values.put(COL_ID_PERSON, idToKeep);
				db.update(TABLE_RESULT, values, COL_ID_PERSON + "=" + idToKeep, null);
			}
			db.delete(TABLE_PERSON, COL_ID + "=" + idToDelete, null);
		}
		
		return;
	}
	
	public Person getResidentWithId(int id){
		Cursor c = db.query(TABLE_PERSON, null, COL_ID + " = " + id, null, null, null, null);
		//return cursorToPlayer(c);
		Person p = cursorToPerson(c);
		c.close();
		return p;
	}
	
	public Person getResidentWithNameFirstname(String name, String firstname){
		Cursor c = db.query(TABLE_PERSON,null, COL_NAME + " LIKE \"" + convertString(name) +"\" AND " +COL_FIRSTNAME+ " LIKE \"" + convertString(firstname) +"\"", null, null, null, null);
		//return cursorToPlayer(c);
		Person p = cursorToPerson(c);
		c.close();
		return p;
	}
	
	public Person getResidentWithServerId(String serverId){
		Cursor c = db.query(TABLE_PERSON, null, COL_SERVER_ID + " LIKE \"" + serverId +"\"", null, null, null, null);
		//return cursorToPlayer(c);
		Person p = cursorToPerson(c);
		c.close();
		return p;
	}
	
	// get number of profile created by a particular
	public int getNumberProfileCreated(){
		Cursor c = db.query(TABLE_PERSON, null, null, null, null, null, null);
		return c.getCount();
	}

	
	// *** CHECK TRUE OR FALSE *** //
	/*
	public boolean checkIfUpToDate(int id) {
		Cursor cursor = db.query(TABLE_PERSON, null, null, null, null, null, null);
		while(cursor.moveToNext()) {
			if (cursor.getString(NUM_COL_SERVER_ID) == null)
				return false;
			else
				return true;
		}
		cursor.close();
		return false;
	}
	*/

	
	public int getResidentNumber() {
		Cursor cursor = db.query(TABLE_PERSON, null, "id = (SELECT MAX(id) from person)", null, null, null, null);
		int p = 0;
		if(cursor.getCount() > 0) {
			if(cursor.getPosition() == -1) 
				cursor.moveToFirst();
			p = cursor.getInt(0) + 1;
			System.out.println("id = "+p);
		}
		cursor.close();
		return p;
	}



	//============================================================================================================//
	// *** SET *** //
	public void setServerId(Person person, String serverId) {
		if (person != null && serverId != null && db != null) {
			ContentValues values = new ContentValues();
			values.put(COL_SERVER_ID, serverId);
			db.update(TABLE_PERSON, values, COL_ID + " = " + String.valueOf(person.getId()), null);
			person.setServerId(serverId);
		}
	}


	
	public void setConflictId(Person person, String conflictId) {
		if (person != null && conflictId != null && db != null) {
			ContentValues values = new ContentValues();
			values.put(COL_CONFLICT_ID, conflictId);
			db.update(TABLE_PERSON, values, COL_ID + " = " + String.valueOf(person.getId()), null);
			person.setConflictId(conflictId);
		}
	}
	
	public void removeConflictId(Person person) {
		if (person != null && db != null) {
			ContentValues values = new ContentValues();
			values.put(COL_FIRSTNAME, person.getFirstName());
			values.put(COL_NAME, person.getName() );
			values.put(COL_BIRTHDATE, person.getBirthdateStrFormatDatabase(context));
			values.put(COL_SEX, person.getSex());
			values.putNull(COL_CONFLICT_ID);
			db.update(TABLE_PERSON, values, COL_ID + " = " + String.valueOf(person.getId()), null);
			person.setConflictId(null);
		}
	}

	//============================================================================================================//
	// *** UPDATE *** //
	public void updatePerson(Person person) {
		if (person != null && db != null) {
			ContentValues values = new ContentValues();
			values.put(COL_PERSON_ROLE, person.getRole());
			values.put(COL_FIRSTNAME, person.getFirstName() );
			values.put(COL_NAME, person.getName() );
			values.put(COL_BIRTHDATE, person.getBirthdateStrFormatDatabase(context));
			values.put(COL_SEX, person.getSex());
			db.update(TABLE_PERSON, values, COL_SERVER_ID + " LIKE \"" + person.getServerId() + "\"", null);
		}
	}

	
	//============================================================================================================//
	// *** DELETE *** //
	public void deletePerson (Person person) {
		if (person != null && db != null) {
			//Get id of person having  a given serverId into table person
			//Delete results and GamePersonInfo having this personId
			Log.d(TAG, "Delete guest results");
			Cursor cursor = db.query(TABLE_PERSON, new String[] {COL_ID} , COL_SERVER_ID + " = \"" + person.getServerId() + "\"", null, null, null, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				int personId = cursor.getInt(cursor.getColumnIndex(COL_ID));
				Log.d(TAG, "PersonId to delete : " + personId);
				db.delete(TABLE_RESULT, COL_ID_PERSON + "=\"" + personId + "\"", null);
				db.delete(TABLE_GAME_PERSON_INFO, COL_ID_PERSON + "=" + personId, null);
				db.delete(TABLE_PERSON, COL_SERVER_ID + " LIKE \"" + person.getServerId() + "\"", null);
			}
		}
	}
	
	public void deleteGuestPerson(String name, String firstname){ //context.getString(R.string.guest)
		Log.d(TAG, "Delete guest profile");
		db.delete(TABLE_PERSON, COL_FIRSTNAME + " = \"" + convertString(firstname) + "\" AND " + COL_NAME + " = \"" + convertString(name) + "\"", null);
	}


	
	//getIds and getPseudos are not independent: they must return same data in same order
	public int[] getIds() {
		Cursor cursor = db.query(TABLE_PERSON, null, null, null, null, null, LOWER_COL_FIRSTNAME);
		int len = cursor.getCount();
		int[] result = new int[len];
		
		int j = 0;	
		while(cursor.moveToNext()) {
			result[j] = cursor.getInt(NUM_COL_ID) ;
			j++;
		}
		cursor.close();
		return result;
	}

	//getIds and getPseudos are not independent: they must return same data in same order
	public String[] getPseudos() {
		Cursor cursor = db.query(TABLE_PERSON, null, null, null, null, null, LOWER_COL_FIRSTNAME);
		int len = cursor.getCount();
		String[] result = new String[len];

		int j = 0;
		while(cursor.moveToNext()) {
			result[j] = StringFormatter.nameFormat(context.getString(R.string.local_name_format), cursor.getString(NUM_COL_NAME), cursor.getString(NUM_COL_FIRSTNAME));
			j++;
		}
		cursor.close();
		return result;
	}
	
	public Person[] getPersons(String condition) {
		Cursor cursor = db.query(TABLE_PERSON, null, condition, null, null, null, COL_FIRSTNAME);
		int len = cursor.getCount();
		Person[] persons = new Person[len];
		
		int j = 0;	
		while(cursor.moveToNext()) {
			persons[j] = cursorToPerson(cursor);
			j++;
		}
		cursor.close();
		return persons;
	}

	public Hashtable<Integer, String> getPersonsName(String condition) {

		Cursor cursor = db.query(TABLE_PERSON, new String[] {COL_ID, COL_NAME, COL_FIRSTNAME}, condition, null, null, null, null);

		if(cursor.getCount() == 0)
			return null;

		Hashtable<Integer, String> persons = new Hashtable<>();

		while(cursor.moveToNext()) {
			if(cursor.getPosition() == -1)
				cursor.moveToFirst();
			persons.put(cursor.getInt(0), cursor.getString(2) + " " + cursor.getString(1));
		}
		cursor.close();
		return persons;
	}

	public Hashtable<Integer, Integer> getPersonsServerId(String condition) {

		Cursor cursor = db.query(TABLE_PERSON, new String[] { COL_SERVER_ID, COL_ID }, condition, null, null, null, null);

		if(cursor.getCount() == 0)
			return null;

		Hashtable<Integer, Integer> persons = new Hashtable<>();

		while(cursor.moveToNext()) {
			if(cursor.getPosition() == -1)
				cursor.moveToFirst();
			Log.d("serverId et Id", cursor.getInt(0) + "    " + cursor.getInt(1));
			int serverId = cursor.getInt(0);
			if (serverId != 0)
				persons.put(serverId, cursor.getInt(1));
		}
		cursor.close();
		return persons;
	}

	public static String convertString(String aString) {
		return aString.replace("'","''");
	}

	//============================================================================================================//
}