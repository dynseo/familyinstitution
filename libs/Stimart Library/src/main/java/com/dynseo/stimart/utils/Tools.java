package com.dynseo.stimart.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.dynseo.stimart.common.models.AppResourcesManager;
//import com.dynseo.stimart.common.models.Game;

import android.annotation.SuppressLint;
import android.util.Log;

@SuppressLint("DefaultLocale")

public class Tools {

	private static final String TAG = "Tools";
	public static final int FILE_TYPE_AUDIO = 1;
	public static final int FILE_TYPE_IMAGE = 2;

	public static final String SERVER_IMAGE_DIRECTORY = "family/content/img";         //mis en dur dans le web
	public static final String IMAGE_NAME = "mobileDynseoFamily.png";

	/* check file existence on the SD card */
	public static boolean isFileExist (int fileType, String fileName) {
		String path = null;
		switch (fileType) {
		case FILE_TYPE_AUDIO:
			path = AppResourcesManager.getAppResourcesManager().getPathAudios();
			break;
		case FILE_TYPE_IMAGE:
			path = AppResourcesManager.getAppResourcesManager().getPathImages();
			break;
		default:
		}
		File file = new File(path+fileName);
		if (file.exists()) {
			return true;
		}
		else {
			Log.e(TAG, "file: "+fileName+" don't exist");
			return false;
		}
	}

	public static boolean deleteFiles (List<File> listFiles) {
		Log.d(TAG, "deleteListFiles");
		if (listFiles != null && listFiles.size() != 0) {

			for (File file : listFiles) {
				Log.d("file name", file.getName());
				file.delete();
			}
			return true;
		}
		return false;
	}

	public static List<File> getFilesByNames(String path, String[] names) {
		Log.d(TAG, "getListFilesByNames");
		ArrayList<File> inFiles = new ArrayList<File>();
		ArrayList<File> listFiles = getFilesInFolder(path);
		if (listFiles != null) {
			for (int i = 0 ; i < names.length ; i ++) {
				for (File file : listFiles) {
					if(file.getName().startsWith(names[i])){
						inFiles.add(file);
					}
				}
			}
			return inFiles;
		}
		return null;
	}

	public static ArrayList<File> getFilesInFolder(String path) {
		Log.d(TAG, "getListFilesInFolder");

		File parentDir = new File(path);
		ArrayList<File> inFiles = new ArrayList<File>();
		File[] files = parentDir.listFiles();
		if (files != null) {
			for (File file : files) {
				inFiles.add(file);
			}
			return inFiles;
		}
		return null;
	}

	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

}
