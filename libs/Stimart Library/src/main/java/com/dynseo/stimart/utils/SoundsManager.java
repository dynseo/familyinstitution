package com.dynseo.stimart.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.dynseo.stimart.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by amadeus on 19.04.16.
 */
public class SoundsManager {

    public interface SoundPoolLoadedCallback {
         void onSoundsLoaded();
    }

    private static SoundsManager instance;
    private SoundPool soundPool;
    private ArrayList<Integer> sounds;
    private HashMap<Integer, SoundSampleEntity> soundIdsToEntity;

    public synchronized static SoundsManager getInstance() {
        return instance;
    }

    public static void createInstance() {
        if (instance == null) {
            instance = new SoundsManager();
        }
    }

    public ArrayList<Integer> getSounds() {
        return sounds;
    }

    private void setSounds(ArrayList<Integer> sounds) {
        this.sounds = sounds;
    }

    private void setDefaultSounds() {
        ArrayList<Integer> sounds = new ArrayList<>();
        sounds.add(R.raw.sound_correct);
        sounds.add(R.raw.sound_wrong);

        setSounds(sounds);
    }

    private void initSoundPool(final SoundPoolLoadedCallback callback) {
        setDefaultSounds();

        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 100);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                SoundSampleEntity entity = getEntity(sampleId);

                if (entity != null) {
                    entity.setLoaded(status == 0);
                }

                if (sampleId == calculateMaxSampleId()) {
                    callback.onSoundsLoaded();
                }
            }
        });
    }

    public void initializeSoundPool(Context context, final SoundPoolLoadedCallback callback) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        initSoundPool(callback);
        initMapping(context);
    }

    public void initializeSoundPoolWithAdditionalSounds(Context context, ArrayList<Integer> additionalSongs, final SoundPoolLoadedCallback callback) {
        initSoundPool(callback);
        sounds.addAll(additionalSongs);
        initMapping(context);
    }

    private void initMapping(Context context) {
        int length = sounds.size();
        soundIdsToEntity = new HashMap<>();
        int index;

        for (index = 0; index < length; index++) {
            soundIdsToEntity.put(sounds.get(index), new SoundSampleEntity(0, false));
        }

        index = 0;

        for (Map.Entry<Integer, SoundSampleEntity> entry : soundIdsToEntity.entrySet()) {
            index++;
            entry.getValue().setSampleId(soundPool.load(context, entry.getKey(), index));
        }
    }

    private int calculateMaxSampleId() {
        int sampleId = 0;

        for (Map.Entry<Integer, SoundSampleEntity> entry : soundIdsToEntity.entrySet()) {
            SoundSampleEntity entity = entry.getValue();
            sampleId = (entity.getSampleId() > sampleId) ? entity.getSampleId() : sampleId;
        }

        return sampleId;
    }

    private SoundSampleEntity getEntity(int sampleId) {
        for (Map.Entry<Integer, SoundSampleEntity> entry : soundIdsToEntity.entrySet()) {
            SoundSampleEntity entity = entry.getValue();
            if (entity.getSampleId() == sampleId) {
                return entity;
            }
        }
        return null;
    }

    public void playSound(int resourceId) {
        SoundSampleEntity entity = soundIdsToEntity.get(resourceId);

        if (entity != null && entity.getSampleId() > 0 && entity.isLoaded()) {
            soundPool.play(entity.getSampleId(), 0.8f, 0.8f, 1, 0, 1f);
        }
    }

    public void playSoundForCorrectAnswer() {
        playSound(R.raw.sound_correct);
    }

    public void playSoundForWrongAnswer() {
        playSound(R.raw.sound_wrong);
    }

    public void release() {
        if (soundPool != null) {
            soundPool.release();
        }
    }

    public void stop() {
        if (soundPool != null) {
            for (Map.Entry<Integer, SoundSampleEntity> entry : soundIdsToEntity.entrySet()) {
                SoundSampleEntity entity = entry.getValue();
                soundPool.stop(entity.getSampleId());
            }
        }
    }

    private class SoundSampleEntity {
        private int sampleId;
        private boolean isLoaded;

        public SoundSampleEntity(int sampleId, boolean isLoaded) {
            this.isLoaded = isLoaded;
            this.sampleId = sampleId;
        }

        public int getSampleId() {
            return sampleId;
        }

        public void setSampleId(int sampleId) {
            this.sampleId = sampleId;
        }

        public boolean isLoaded() {
            return isLoaded;
        }

        public void setLoaded(boolean isLoaded) {
            this.isLoaded = isLoaded;
        }
    }
}
