package com.dynseo.stimart.utils;

import java.util.Random;

public class MathRandom {
    
	/* Purpose:			Returns a random integer from the range [min, max].
     */
    public static int chooseRandom(int min, int max){
    	Random rand = new Random();
    	return rand.nextInt(max+1 - min) + min;
    }
    
	// get [min,max], n integers
	public static int[] randomArray(int min, int max, int n) {
		int len = max - min + 1;
		if (max < min || n > len || n > max - min + 1) {
			return null;
		}
		// initialize an integer array of the giving field
		int[] source = new int[len];
		for (int i = min; i < max + 1; i++) {
			source[i - min] = i;
		}
		int[] result = new int[n];
		Random rd = new Random();
		int index = 0;
		for (int i = 0; i < result.length; i++) {
			// an index random from 0 to (len-2)
			index = Math.abs(rd.nextInt() % len--);
			// put it in the result list
			result[i] = source[index];
			// change the values
			source[index] = source[len];
		}
		return result;
	}

	/**
	 * Get n different integer [min, max]. Drop should be an integer from min to
	 * max, and result don't contain drop ex: randomArray(0,10,3,5) => return an
	 * array of 3 different integer that don't contain 5 from 0 to 10
	 * 
	 * @param min
	 * @param max
	 * @param n
	 * @param drop, integer that shouldn't be in the result
	 * @return integer array, size of n
	 */
	public static int[] randomArrayWithDrop(int min, int max, int n, int drop) {
		int len = max - min;
		if (max < min || n > len || drop < min || drop > max || n > max - min) {
			return null;
		}
		// initialize an integer array of the giving field
		int[] source = new int[len];
		for (int i = min; i < drop; i++) {
			source[i - min] = i;
		}
		for (int j = drop + 1; j < max + 1; j++) {
			source[j - 1] = j;
		}
		int[] result = new int[n];
		Random rd = new Random();
		int index = 0;
		for (int i = 0; i < result.length; i++) {
			// an index random from 0 to (len-2)
			index = Math.abs(rd.nextInt() % len--);
			// put it in the result list
			result[i] = source[index];
			// change the values
			source[index] = source[len];
		}
		return result;
	}
	
	// Implementing FishereYates shuffle
	static public void shuffleArray(int[] ar)
	{
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--)
		{
			int index = rnd.nextInt(i + 1);
		    // Simple swap
		    int a = ar[index];
		    ar[index] = ar[i];
		    ar[i] = a;
		}
	}
}
