package com.dynseo.stimart.utils.connection;

import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.tools.Tools;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class URLPath extends Activity {
	
	public static String getToken(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String prefstoken = sharedPrefs.getString(StimartConnectionConstants.SHARED_PREFS_APP_TOKEN, null);
		
		return prefstoken;
	}
	
	public static String setUrlPath(String prefstoken, String path) {
		//If token present put token on url
			if(prefstoken != null){
				path += StimartConnectionConstants.PARAM_TOKEN+ prefstoken;
			}
			//else put serial number on url
			else {
					path += StimartConnectionConstants.PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber();
			}
		return path;
	}
	
}
