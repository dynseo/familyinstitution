package com.dynseo.stimart.common.models;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mathias on 20/10/2016.
 */

public interface SynchronizationFactory {
    public SynchronizationData createSynchronizationData(Context context, SharedPreferences sharedPrefs, String type);
    public SynchronizationResources createSynchronizationResources(Context context, SharedPreferences sharedPrefs, String type, String codeForEndOfSynchro);
}
