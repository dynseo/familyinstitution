package com.dynseo.stimart.utils.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;


public class JSONParser {

	public String addN(HttpEntity entity) {
		BufferedReader reader = null;
		StringBuilder sb = new StringBuilder();
		String line = null;
		String result = null;
		InputStream inputStream = null;

		try {
			inputStream = entity.getContent();
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			reader = new BufferedReader(new InputStreamReader(inputStream,
					"UTF-8"), 8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		result = sb.toString();
		return result;
	}
}
