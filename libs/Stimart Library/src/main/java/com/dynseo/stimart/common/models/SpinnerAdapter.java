package com.dynseo.stimart.common.models;

import com.dynseo.stimart.R;
import com.dynseo.stimart.R.id;
import com.dynseo.stimart.R.layout;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerAdapter extends ArrayAdapter<String> {

	private String[] data;
	private Spinner spinner;
	LayoutInflater inflater;
	RadioButton radioButton;
	
	public SpinnerAdapter(Activity activity, int resource,
			String[] data, Spinner spinner) {
		super(activity, resource, data);
		// TODO Auto-generated constructor stub
		this.data = data;
		this.spinner = spinner;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	 @Override
	    public View getDropDownView(int position, View convertView,ViewGroup parent) {
	        return getCustomView(position, convertView, parent);
	    }
	 
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        return getCustomView(position, convertView, parent);
	    }
	 
	    // This funtion called for each row 
	    public View getCustomView(int position, View convertView, ViewGroup parent) {

	        View row = inflater.inflate(R.layout.profile_connection_spinner_item, parent, false);
	         
	        TextView pseudo = (TextView)row.findViewById(R.id.textViewSpinnerItem);
	        radioButton = (RadioButton)row.findViewById(R.id.radioButtonSpinnerItem);
	        
	        pseudo.setText(data[position]);
	        
	        int pos = spinner.getSelectedItemPosition();
	        if(position == pos) {
	        	radioButton.setChecked(true);
	        }
 
	        return row;
	      }
    
}


