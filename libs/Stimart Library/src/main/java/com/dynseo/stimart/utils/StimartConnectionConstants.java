package com.dynseo.stimart.utils;

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import com.dynseo.stimart.common.models.Person;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.tools.Tools;

public class StimartConnectionConstants extends ConnectionConstants{

	/**
	 * modified 17/04/2015 for synchronization
	 * by OUERTANI Houssem
	 */

	public static final String PARAM_LEVEL  = "&level=";
	public static final String PARAM_SCORE1 = "&score1=";
	public static final String PARAM_SCORE2 = "&score2=";
	public static final String PARAM_SCORE3 = "&score3=";
	public static final String PARAM_SCORE4 = "&score4=";
	public static final String PARAM_SCORE5 = "&score5=";
	public static final String PARAM_SCORE6 = "&score6=";
	public static final String PARAM_POINTS = "&points=";
	public static final String PARAM_GAME_INTERRUPTED = "&interrupted=";

	/* Services */
	private static final String VAL_ADD_SCORE  = "createResult";
	private static final String VAL_ADD_PERSON = "addPerson";
	private static final String VAL_SYNCHRO_PERSONS = "synchroPersons";
	private static final String VAL_SYNCHRO_RESULTS = "synchroResults";
	private static final String VAL_UPDATE_RESOURCES = "updateResources";

	public static final String INDENTIFICATION_ERROR = "identification_error";
	public static final String SUBSCRIPTION_ERROR	 = "subscription_error";


	public static final int DEFAULT_SUBSCRIPTION_DURATION = 3 ;

	public static final String SHARED_PREFS_SUFFIX_INDEX   	   = "_index";
	public static final String SHARED_PREFS_SUFFIX_DAY         = "_day";

	public static final String JSON_PARAM_STATUS	= "status";
	public static final String JSON_PARAM_ERROR		= "error";
	public static final String JSON_PARAM_BEGIN_SUBSCRIPTION 	= "beginDate" ;
	public static final String JSON_PARAM_SUBSCRIPTION_DURATION = "subscription" ;
	public static final String JSON_PARAM_DEVICE_NOT_EXISTS 	= "deviceNotExists";
	public static final String JSON_PARAM_RESULT_ID   = "resultId";
	public static final String JSON_PARAM_SERVER_ID   = "serverId";
	public static final String JSON_PARAM_CONFLICT_ID = "conflictId";
	public static final String JSON_PARAM_SOLVED	  = "solved";
	public static final String JSON_PARAM_FIRSTNAME   = "firstname";
	public static final String JSON_PARAM_NAME		  = "name";
	public static final String JSON_PARAM_ACTIVE	  = "active";
	public static final String JSON_PARAM_SEX		  = "sex";
	public static final String JSON_PARAM_PERSON_ROLE = "role";
	public static final String JSON_PARAM_BIRTHDATE = "birthdate";
	public static final String JSON_PARAM_UPDATE_NEEDED		= "updateNeeded";
	public static final String JSON_PARAM_NEW_APP_VERSION	= "versionNumber";
	public static final String JSON_PARAM_PERSON_ID = "personId";

	public static final String ERROR = "error";
	public static final String TOKEN = "token";
	public static final String ADD_PERSON_TRUE	 = "true";
	public static final String ADD_PERSON_FALSE = "false";
	public static final String TRUE 	 = "T";
	public static final String FALSE 	 = "F";
	public static final String YES 	 = "YES";
	public static final String NO 	 = "NO";
	public static final String MERGE = "Merge";
	public static final String SPLIT = "Split";
	public static final String UTF8	 = "UTF-8";

	/* Constants for message */
	public static final String MULTI_PART_PARAM_RECEIVER_UUID = "receiverUserId";
	public static final String MULTI_PART_PARAM_OBJECT_MESSAGE = "objectMessage";
	public static final String MULTI_PART_PARAM_MESSAGE = "message";
	public static final String MULTI_PART_PARAM_SENDER_PERSON_ID = "senderPersonId";
	public static final String MULTI_PART_PARAM_FROM_USER_TO_PERSON = "fromUser2Person";
	public static final String MULTI_PART_PARAM_DATE = "date";
	public static final String MULTI_PART_PARAM_CHECKED = "checked";
	public static final String MULTI_PART_DIRECTORY = "directory";
	public static final String MULTI_PART_FILE = "file";
	public static final String PARAM_MESSAGE_ID = "&messageId=";
	public static final String PARAM_CONTEXT = "&context=";

	public static final String JSON_PARAM_USER_ID = "userId";
	public static final String JSON_PARAM_OBJET_MESSAGE = "objectMessage";
	public static final String JSON_PARAM_MESSAGE = "textMessage";
	public static final String JSON_PARAM_FROM_USER_TO_PERSON = "fromUser2Person";
	public static final String JSON_PARAM_DATE = "date";
	public static final String JSON_PARAM_CHECKED = "checked";
	public static final String JSON_PARAM_TRASH = "trash";
	public static final String VAL_CONTEXT_RECEIVED = "received";
	public static final String VAL_TYPE_TRASH = "trash";
	public static final String JSON_PARAM_NAME_USER = "name";
	public static final String JSON_PARAM_FIRSTNAME_USER = "firstname";
	public static final String JSON_PARAM_URL_IMAGE_SERVER = "urlImageServer";
	public static final String JSON_PARAM_LINK_ID = "linkId";
	public static final String JSON_PARAM_ROLE = "role";
	public static final String JSON_PARAM_ALIAS_PERSON = "aliasPerson";
	public static final String JSON_PARAM_MESSAGE_ID = "messageId";

	public static final String VAL_SYNCHRO_MESSAGES_AND_USERS = "synchroMessagesAndUsers";
	public static final String VAL_CHECKED_MESSAGE = "T";
//	public static final String VAL_UNCHECKED_MESSAGE = "F";		//not used
	public static final String VAL_TYPE_CHECKED = "checked";
	public static final String VAL_DOWNLOAD_IMAGE = "downloadImage";
	public static final String VAL_ADD_MESSAGE = "sendMessage";
//	public static final String VAL_ADD_MESSAGE = "embeddedAddMessage";
//	public static final String VAL_ADD_MESSAGE = "addMessageTablette";
	public static final String VAL_UPDATE_MESSAGE = "updateMessageState";
	//public static final String VAL_UPDATE_MESSAGE = "embeddedUpdateMessage";


	//================ Build the URL to add a person on the server =====================//
	public static String urlAddPerson(Person person, boolean existLocal)
					throws UnsupportedEncodingException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String theURL = BASE_URL + PARAM_SERVICE + VAL_ADD_PERSON
			+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
			+ PARAM_EXIST_LOCAL + ((existLocal) ? ADD_PERSON_TRUE : ADD_PERSON_FALSE)
			+ PARAM_NAME + URLEncoder.encode(person.getName(), UTF8)
			+ PARAM_FIRSTNAME  + URLEncoder.encode(person.getFirstName(), UTF8)
			+ PARAM_SEX + person.getSex();

		if( person.getConflictId() != null )
			theURL += PARAM_CONFLICT + person.getConflictId();
		if( person.getBirthdate() != null )
			theURL += PARAM_BIRTHDATE + dateFormat.format(person.getBirthdate());

		return theURL;
	}

//	//================ Build the URL to add a person on the server =====================//
//	public static String urlAddResult(Result result)
//					throws UnsupportedEncodingException {
//
//		Score aScore = result.getScore();
//		if( aScore == null )
//			aScore = new Score();
//
//		String url =  BASE_URL + PARAM_SERVICE + VAL_ADD_SCORE
//				+ PARAM_SERIAL_NUMBER + Tools.getManufacturerSerialNumber()
//				+ PARAM_SERVERID + result.getPlayerServerId()
//				+ PARAM_GAME + result.getGameName()
//				+ PARAM_DATE +  URLEncoder.encode(result.getDate(), UTF8)
//				+ PARAM_DURATION + aScore.getTime()
//				+ (result.getLevel() == -1 ? "" : PARAM_LEVEL + result.getLevel())
//				+ (aScore.getScore1() == -1 ? "" : PARAM_SCORE1 + aScore.getScore1())
//				+ (aScore.getScore2() == -1 ? "" : PARAM_SCORE2 + aScore.getScore2())
//				+ (aScore.getScore3() == -1 ? "" : PARAM_SCORE3 + aScore.getScore3())
//				+ (aScore.getScore4() == -1 ? "" : PARAM_SCORE4 + aScore.getScore4())
//				+ (aScore.getScore5() == -1 ? "" : PARAM_SCORE5 + aScore.getScore5())
//				+ (aScore.getScore6() == -1 ? "" : PARAM_SCORE6 + aScore.getScore6())
//				+ (aScore.getNbPoints() == -1 ? "" : PARAM_POINTS + aScore.getNbPoints())
//				+ PARAM_APP + AppManager.getAppManager().getAppSubTypeName()
//				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
//				+ PARAM_DEVICE_TYPE
//				;
//			if (!aScore.isGamefinished())
//				url += PARAM_GAME_INTERRUPTED + "1";
//
//
//		Log.e("test", url);
//		return url;
//	}

	//================ Build the URL to synchronize persons on the server =====================//
	public static String urlSynchroPersons() {
		String theURL = BASE_URL + PARAM_SERVICE + VAL_SYNCHRO_PERSONS
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang();
		return theURL;
	}
	//================ Build the URL to synchronize persons on the server =====================//
	/*public static String urlSynchroResults() {
		String theURL = BASE_URL + PARAM_SERVICE + VAL_SYNCHRO_RESULTS
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang();
		return theURL;
	}
	//================ Build the URL to update resources on the device =====================//
	public static String urlUpdateResources() {
		String theURL = BASE_URL + PARAM_SERVICE + VAL_UPDATE_RESOURCES
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang();
		return theURL;
	}*/

	/* ******************** */
	/* Build url for FAMILY */
	/* ******************** */
	//================ Build the URL to get messages, links and users =====================//
	public static String urlGetMessagesAndUsers() {

		String theUrl = BASE_URL + PARAM_SERVICE + VAL_SYNCHRO_MESSAGES_AND_USERS
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang();
		return theUrl;
	}

	//================ Build the URL to send message =====================//
	public static String urlSendMessage() {

		String theUrl = BASE_URL_POST + VAL_ADD_MESSAGE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_FORMAT_OUTPUT_JSON;
		return theUrl;
	}
	//================ Build the URL to update message =====================//
	public static String urlUpdateMessage(String messageServerIdMessage, String messageToTrash)	{

		String theUrl = BASE_URL + PARAM_SERVICE + VAL_UPDATE_MESSAGE
				+ PARAM_FORMAT_OUTPUT_JSON
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_MESSAGE_ID + messageServerIdMessage
				+ PARAM_CONTEXT + VAL_CONTEXT_RECEIVED
//				+ PARAM_TYPE + VAL_TYPE_CHECKED;
				+ PARAM_TYPE + (messageToTrash.equals("T") ? VAL_TYPE_TRASH : VAL_TYPE_CHECKED);
		return theUrl;
	}
	//================ Build the URL to downbload images in messages =====================//
	public static String urlDownloadPath(String messageId)	{

		String theUrl = BASE_URL + PARAM_SERVICE + VAL_DOWNLOAD_IMAGE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_MESSAGE_ID + messageId;
		return theUrl;
	}

}
