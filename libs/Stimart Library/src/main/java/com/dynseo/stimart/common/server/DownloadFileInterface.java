package com.dynseo.stimart.common.server;

/**
 * Created by Mathias on 21/10/2016.
 */

public interface DownloadFileInterface {
    void onDownloadSuccess();
    void onDownloadFailure(Exception e);
    void onDownloadProgress(long total, int lengthOfFile);
}
