package com.dynseo.stimart.utils;

import com.dynseo.stimart.R;
import com.dynseolibrary.tools.Tools;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DialogManager {

		public Context context;
		private Dialog dialog;

	private int title;
		private int text;
		private String titleString;
		private String textString;
		private String scoreString;
		
		public Button non;
		public Button oui;
		private TextView dialogText;
		private TextView dialogTitle;
		private TextView dialogScore;
		
		private RelativeLayout dialoglayout;
		
		private int buttonDrawableId;
		private int backgroundDrawableId;
		private int colorGameBackgroundId;

		public DialogManager(Context a, int buttonDrawableId, int backgroundDrawableId, int title) {
			this.context = a;
			this.buttonDrawableId = buttonDrawableId;
			this.title = title;
			this.backgroundDrawableId = backgroundDrawableId;
		}

		public DialogManager(Context a, int buttonDrawableId, int backgroundDrawableId, int title, int text) {
			this(a, buttonDrawableId, backgroundDrawableId, title);
			this.text = text;
		}
		
		public DialogManager(Context a, int buttonDrawableId, int backgroundDrawableId, int colorGameBackgroundId, int title, int text) {
			this(a, buttonDrawableId, backgroundDrawableId, title, text);
			this.colorGameBackgroundId = colorGameBackgroundId;
		}

		public DialogManager(Activity a, int buttonDrawableId,  int backgroundDrawableId, int colorGameBackgroundId, int title, int text, String score) {
			this(a, buttonDrawableId, backgroundDrawableId, colorGameBackgroundId, title, text);
			this.scoreString = score;
		}

		public DialogManager(Activity a, int buttonDrawableId,  int backgroundDrawableId, String title, String text) {
			this.context = a;
			this.buttonDrawableId = buttonDrawableId;
			this.titleString = title;
			this.textString = text;
			this.backgroundDrawableId = backgroundDrawableId;
		}
		
		public DialogManager(Activity a, int buttonDrawableId,  int backgroundDrawableId, int colorGameBackgroundId, String title, String text) {
			this(a, buttonDrawableId, backgroundDrawableId, title, text);
			this.colorGameBackgroundId = colorGameBackgroundId;
		}
		
		public DialogManager(Activity a, int buttonDrawableId,  int backgroundDrawableId, int colorGameBackgroundId, String title, String text, String score) {
			this(a, buttonDrawableId, backgroundDrawableId, colorGameBackgroundId, title, text);
			this.scoreString = score;
		}
		
		public void setDialog() {
			dialog = new Dialog(context, R.style.DialogFullscreen);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.dialog_exit_game_layout);
			dialoglayout = (RelativeLayout) dialog.findViewById(R.id.dialogprincipal);
			if (backgroundDrawableId > 0) {
				dialoglayout.setBackgroundResource(backgroundDrawableId);
			}
			dialogText = (TextView) dialog.findViewById(R.id.text_dialog_exit_game);
			dialogScore = (TextView) dialog.findViewById(R.id.score_dialog_exit_game);

			dialogTitle = (TextView) dialog.findViewById(R.id.titre_dialog_exit_game);
			non = (Button) dialog.findViewById(R.id.non);
			oui = (Button) dialog.findViewById(R.id.oui);
			
			setDialogContent();
		}

		public void setDialogString() {
			dialog = new Dialog(context, R.style.DialogFullscreen);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.dialog_exit_game_layout);
			dialoglayout = (RelativeLayout) dialog.findViewById(R.id.dialogprincipal);
			dialogText = (TextView) dialog.findViewById(R.id.text_dialog_exit_game);
			dialogTitle = (TextView) dialog.findViewById(R.id.titre_dialog_exit_game);
			dialogScore = (TextView) dialog.findViewById(R.id.score_dialog_exit_game);
			
			non = (Button) dialog.findViewById(R.id.non);
			oui = (Button) dialog.findViewById(R.id.oui);
			
			setDialogContentString();		
		}

		public void setDialogContentString() {
			dialogText.setText(textString);
			dialogTitle.setText(titleString);
			if (Tools.isResourceTrue(context, R.string.score_display) && scoreString != null && !scoreString.isEmpty()) {
				dialogScore.setText(scoreString);
				dialogScore.setTypeface(Typeface.createFromAsset(
						context.getAssets(), context.getResources()
						.getString(R.string.score_typeface)));
				dialogScore.setTextColor(context.getResources().getColor(colorGameBackgroundId));
				((GradientDrawable)dialogScore.getBackground()).setStroke(5, context.getResources().getColor(colorGameBackgroundId));
				dialogScore.setVisibility(View.VISIBLE);
			}
			oui.setBackgroundResource(buttonDrawableId);
			non.setBackgroundResource(buttonDrawableId);
		}
		
		public void setDialogContent() {
			dialogText.setText(text);
			dialogTitle.setText(title);
			if (Tools.isResourceTrue(context, R.string.score_display) && scoreString != null && !scoreString.isEmpty()) {
				dialogScore.setText(scoreString);
				dialogScore.setTypeface(Typeface.createFromAsset(
						context.getAssets(), context.getResources()
						.getString(R.string.score_typeface)));
				dialogScore.setTextColor(context.getResources().getColor(colorGameBackgroundId));
				((GradientDrawable)dialogScore.getBackground()).setStroke(5, context.getResources().getColor(colorGameBackgroundId));
				dialogScore.setVisibility(View.VISIBLE);
			}
			oui.setBackgroundResource(buttonDrawableId);
			non.setBackgroundResource(buttonDrawableId);
		}
		
		public void showDialog() {
			if (dialog != null && context != null && !((Activity)context).isFinishing()) {
				dialog.show();
			}
		}
		
		public void endDialog() {
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
		}

	public void showDelayedDialog(final int duration){
		final Handler handlerDelayedDialog = new Handler();
		Runnable runnableDelayedDialog = new Runnable() {
			public void run() {
				try {
					Thread.sleep(duration);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
					return;
				}
				handlerDelayedDialog.post(new Runnable(){
					public void run() {
						if (dialog != null)
							dialog.show();
					}
				});
			}
		};
		Thread threadDelayedDialog = new Thread(runnableDelayedDialog);
		threadDelayedDialog.start();
	}

		public Dialog getDialog() {
			return dialog;
		}
		
		public void setCloseListener(boolean forYes, boolean forNo) {
			android.view.View.OnClickListener closeListener = new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			};
			if( forYes )
				this.oui.setOnClickListener(closeListener);
			if( forNo )
				this.non.setOnClickListener(closeListener);
		}
		public void setScoreString(String score){
			this.scoreString = score;
		}
		
		public boolean isShowing() {
			return dialog.isShowing();
		}
	}

