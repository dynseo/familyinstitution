package com.dynseo.stimart.common.models;

import com.dynseolibrary.platform.SynchroInterface;

/**
 * Created by Mathias on 21/10/2016.
 */

public interface SynchronizationResources extends SynchroInterface {
}
