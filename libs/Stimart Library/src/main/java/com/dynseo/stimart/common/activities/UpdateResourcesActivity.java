package com.dynseo.stimart.common.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.stimart.R;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseo.stimart.common.server.DownloadFile;
import com.dynseo.stimart.common.server.DownloadFileInterface;
import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.tools.Tools;
import com.dynseolibrary.tools.zip.UnZipper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;

/**
 * Activity that take charge in updates automatic of
 * resources (images, audios, etc)
 * Step 1: verify whether need to update
 * Step 2: download resources.zip to folder ".res" (hidden folder)
 * Step 3: unzip resources.zip
 * @author SUN Jiaji
 * @date 29-07-2014
 */
public class UpdateResourcesActivity extends Activity implements DownloadFileInterface {

	/** Called when the activity is first created. */
	private static final String TAG = "UpdateResourcesActivity";

	private SharedPreferences sharedPrefs;

	public static final String KEY_UPDATE_PURPOSE_NO_RESOURCES = "no_resources_found";
	public static final String KEY_CURRENT_VERSION_NAME = "current_version_name";
	protected static final int DOWNLOAD_FINISH = 1;
	protected static final int DOWNLOAD_FAIL = 2;
	protected static final int DOWNLOAD_FAIL_SERVER_ERROR = 3;

	private ProgressDialog progressBar;
	boolean afterProcessing;
	int logoDrawableId;

	Thread downloadThread;

	/* get the current package name (init in MenuAppliActivity) for class Game */
	String savePath;
	public static final String FILE_NAME = "resources.zip";

	ProgressDialogHandler progressDialogHandler;
	Context context = this;
	/* dialogs */
	AlertDialog dialogCancelDownload;
	boolean isDownloadFinished;
	String currentVersionName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");
		afterProcessing = false;
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

		progressDialogHandler = new ProgressDialogHandler();
		savePath = AppResourcesManager.getAppResourcesManager().getPathResourcesPrevious();
		logoDrawableId = R.drawable.logo;

		if (!Http.isOnline(context)) {
        	/* no Internet, can't download resources, exit */
			Tools.showToastLong(getApplicationContext(), getString(R.string.update_res_no_internet));
			terminateApplication();
		}
		else {
			/* if no resources, download directly */
			if (getIntent().getBooleanExtra(KEY_UPDATE_PURPOSE_NO_RESOURCES, false)) {
				Log.i(TAG, "no resources, download directly");
				currentVersionName = null;
				checkResoucesZipFile();
			}
			/* else we check resources updates */
			else {
				Log.i(TAG, "have resources, check update");

				currentVersionName = getIntent().getStringExtra(KEY_CURRENT_VERSION_NAME);
				String mode = AppManager.getAppManager().getMode();
				String theURL = StimartConnectionConstants.urlCheckResourcesUpdate(currentVersionName, mode);

				CheckResourcesUpdateTask task = new CheckResourcesUpdateTask();
				task.execute(theURL);
			}
		}
	}

	public void beginDownload() {
		Log.d(TAG, "beginUpdate()");

		createProgressBar();
		if (!((Activity) context).isFinishing())
			progressBar.show();

		String mode = AppManager.getAppManager().getMode();
		final String theURL = StimartConnectionConstants.urlDownloadResourcesUpdate(currentVersionName, mode);

		Thread downloadThread = new Thread() {
			public void run() {
				DownloadFile downloadFile = new DownloadFile(UpdateResourcesActivity.this, savePath);
				downloadFile.downloadFileByUrl(theURL, FILE_NAME);
			}
		};
		downloadThread.start();
	}

	public boolean isDownloadThreadInterrupted() {
		return downloadThread.isInterrupted();
	}

	public void createDialogCancelDownload() {
		Log.d(TAG, "createDialogCancelDownload()");
		dialogCancelDownload = new AlertDialog.Builder(context, R.style.DialogFullscreen)
				.setIcon(R.drawable.alert_logo)
				.setTitle(getResources().getString(R.string.download_resources))
				.setMessage(getResources().getString(R.string.cancel_download))
				.setCancelable(false)
				.setPositiveButton(getResources().getString(R.string.oui), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
				/* quit the entire application, stop resource update, user can't use application */
						Log.d(TAG, "here");
						terminateApplication();
					}
				})
				.setNegativeButton(getResources().getString(R.string.non), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialogCancelDownload.dismiss();
						progressBar.show();
					}
				}).create();
	}

	protected void createProgressBar() {
		Log.d(TAG, "createProgressBar()");
		progressBar = new ProgressDialog(UpdateResourcesActivity.this, R.style.DialogFullscreen);
		progressBar.getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
		progressBar.setCanceledOnTouchOutside(false);
		progressBar.setIcon(logoDrawableId);
		progressBar.setTitle(getResources().getString(R.string.downloading_resources));
		progressBar.setMessage(getResources().getString(R.string.please_wait));
		progressBar.setMax(100);
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setProgressNumberFormat(null);
		progressBar.setProgressPercentFormat(NumberFormat.getPercentInstance());
		progressBar.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				createDialogCancelDownload();
				dialogCancelDownload.show();
			}
		});
	}


	@Override
	public void onDownloadSuccess() {
		Log.d(TAG,"onDownloadSuccess()");
		isDownloadFinished = true;
		Message msg = new Message();
		msg.what = DOWNLOAD_FINISH;
		progressDialogHandler.sendMessage(msg);
		UnZipper.unZip(savePath, savePath, FILE_NAME);
		installGeoGameMap();
//		if (progressBar != null && progressBar.isShowing()) {
//			progressBar.dismiss();
//		}
		/* put the app version (it will be used for identifying updates) */
		sharedPrefs.edit().putString(AppManager.KEY_CURRENT_VERSION_NAME, AppManager.getAppManager().getVersionName()).commit();
		Log.i(TAG,"current version set to "+AppManager.getAppManager().getVersionName());
		if (AppManager.getAppManager().getFreemiumName() != null &&
				!AppManager.getAppManager().isSubscriptionValid() )
			AppManager.getAppManager().setResourcesType(getString(R.string.freemium_resources));
		else
			AppManager.getAppManager().setResourcesType(getString(R.string.all_resources));

		backToMenuActivity();
	}

	@Override
	public void onDownloadFailure(Exception e) {
		Log.e(TAG,"onDownloadFailure - "+e.getMessage());
		e.printStackTrace();
		Message msg = new Message();
		msg.what = DOWNLOAD_FAIL;
		progressDialogHandler.sendMessage(msg);
		terminateApplication();
	}

	@Override
	public void onDownloadProgress(long total, int lengthOfFile) {
		if (progressBar != null) {
			progressBar.setProgress((int) ((total * 100) / lengthOfFile));
		}
	}

	public final class ProgressDialogHandler extends Handler {
		public void handleMessage(Message msg) {
			if (msg.what == DOWNLOAD_FINISH) {
				if (dialogCancelDownload != null && dialogCancelDownload.isShowing()) {
					dialogCancelDownload.dismiss();
				}
				if (progressBar != null && progressBar.isShowing() && (!((Activity) context).isFinishing())) {
					progressBar.dismiss();
					progressBar = new ProgressDialog(UpdateResourcesActivity.this, R.style.DialogFullscreen);
					progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
					progressBar.setTitle(R.string.installing);
					progressBar.setIcon(R.drawable.install_logo);
					progressBar.setMessage(getResources().getString(R.string.please_wait));
					progressBar.setCanceledOnTouchOutside(false);
					progressBar.setCancelable(false);
					progressBar.show();
				}
			}
			else if (msg.what == DOWNLOAD_FAIL) {
				/* here we do 2 times just for a toast longer */
				Tools.showToastLong(getApplicationContext(), getString(R.string.error_download));
			}
			else if (msg.what == DOWNLOAD_FAIL_SERVER_ERROR) {
				/* here we do 2 times just for a toast longer */
				Tools.showToastLong(getApplicationContext(), getString(R.string.error_server));
			}
		}
	}

	private boolean checkResourcesUpdate(String checkUpdatePath, Context context) {
		Log.d(TAG, "checkResourcesUpdate()");
		boolean isNeedUpdate = true;
		String serverResponse;
		try {
			Log.i(TAG, "checkUpdatePath : " + checkUpdatePath);

			serverResponse = Http.queryServer(checkUpdatePath);
			Log.i(TAG , "checkUpdate - server response : " + serverResponse);

			JSONObject o = new JSONObject(serverResponse);
			String updateNeeded = o.getString(StimartConnectionConstants.JSON_PARAM_UPDATE_NEEDED);

			if (updateNeeded.equals(StimartConnectionConstants.YES)) {
				Log.i(TAG,"need update");
				isNeedUpdate = true;
			}
			else {
				Log.i(TAG,"don't need update");
				isNeedUpdate = false;
			}
		} catch (JSONException e) {
			Log.e(TAG, "JSON instanciate or read exception");
			e.printStackTrace();
			isNeedUpdate = false;
		}
		return isNeedUpdate;
	}

	public void checkResoucesZipFile() {
		String resourcesFilePath = Environment.getExternalStorageDirectory()+"/Download"+File.separator+FILE_NAME;
		if (new File(resourcesFilePath).exists()) {
			Log.e(TAG, "resources zip file found in folder \"Download\"");
			installFromLocalFile(Environment.getExternalStorageDirectory()+"/Download");
		}
		else {
			Log.e(TAG, "resources zip file not found in folder \"Download\", download from server");
			beginDownload();
		}
	}

	public void installGeoGameMap() {
		File fileMap = new File(AppResourcesManager.getAppResourcesManager().getPathMap());
		Log.e("test", AppResourcesManager.getAppResourcesManager().getPathMap());
		Log.e("test", fileMap.getPath());
		if (fileMap.exists()) {
			Log.e("test", "map exist, change path");
			fileMap.renameTo(new File(Environment.getExternalStorageDirectory(),"osmdroid"));
		}
	}

	public void installFromLocalFile(String resourcesFilePath) {
		Log.d(TAG,"installFromLocalFile()");
		isDownloadFinished = true;

		Message msg = new Message();
		msg.what = DOWNLOAD_FINISH;

		progressDialogHandler.sendMessage(msg);
		UnZipper.unZip(resourcesFilePath, savePath, FILE_NAME);

		installGeoGameMap();

//		if (progressBar != null && progressBar.isShowing()) {
//			progressBar.dismiss();
//		}
		/* put the app version (it will be used for identifying updates) */
		sharedPrefs.edit().putString(AppManager.KEY_CURRENT_VERSION_NAME, AppManager.getAppManager().getVersionName()).commit();
		Log.i(TAG,"current version set to "+AppManager.getAppManager().getVersionName());

		backToMenuActivity();
	}


	private void backToMenuActivity() {
		Log.d(TAG, "backToMenuActivity()");
		afterProcessing = true;
		overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		Intent intent = new Intent();
		setResult(MainActivity.RESULT_DO_RESOURCES, intent);
		finish();
	}

	private void terminateApplication() {
		overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		finish();
		if (MainActivity.context != null) {
			MainActivity.context.finish();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG,"onDestroy()");
		if (!isDownloadFinished) {
//			File file = null;
//			if (Environment.getExternalStorageState().equals(
//					Environment.MEDIA_MOUNTED)) {
//				file = new File(getExternalFilesDir(null).getPath());
//			}
			File file = new File(savePath);
			if (file.exists()) {
				file.delete();
			}
		}
		if (dialogCancelDownload != null) {
			dialogCancelDownload.dismiss();
		}
		if (progressBar != null) {
			progressBar.dismiss();
		}
	}

	@Override
	protected void onPause() {
		Log.d(TAG,"onPause()");
		super.onPause();
		if (MainActivity.context != null && !afterProcessing) {
			MainActivity.context.finish();
		}
		finish();

	}
	//==============================================================================
	// AsyncTask to check resources
	//==============================================================================
	private class CheckResourcesUpdateTask extends AsyncTask<String, Integer, String> {

		private static final String TAG = "CheckUpdateTask";
		@Override
		protected String doInBackground(String... params) {
			Log.d(TAG, "doInBackground()");
			if (checkResourcesUpdate(params[0],context)) {
				return StimartConnectionConstants.TRUE;
			}
			else {
				return StimartConnectionConstants.FALSE;
			}
		}

		protected void onPostExecute(String result) {
			Log.d(TAG, "onPostExecute()");
			if (result.equals(StimartConnectionConstants.TRUE)) {
				beginDownload();
			}
	    	/* this case: app updated but no need to update resources (a particular case) */
			else {
				/* put the app version (it will be used for identifying updates) */
				sharedPrefs.edit().putString(AppManager.KEY_CURRENT_VERSION_NAME, AppManager.getAppManager().getVersionName()).commit();
				Log.i(TAG,"current version set to "+ AppManager.getAppManager().getVersionName());
				backToMenuActivity();
			}
		}
	}

}
