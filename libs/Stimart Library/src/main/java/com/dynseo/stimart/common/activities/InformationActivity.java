package com.dynseo.stimart.common.activities;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.stimart.R;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SubscriptionInterface;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.tools.Tools;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class InformationActivity extends Activity {

	private static final String TAG = "InformationActivity";
	protected Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate()");

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		dialog = new Dialog(this, R.style.DialogFullscreen);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(R.layout.dialog_subscription_expired);
		TextView subscriptionText = (TextView) dialog.findViewById(R.id.subscriptionText);
		subscriptionText.setText(String.format((this.getResources().getText(R.string.subscription_expired_text_for_institution).toString()),
				Tools.formattedAsDate(preferences.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, "")) ));

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				Log.d(TAG, "onCancelListener - onCancel");
				dialog.dismiss();
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				finish();
				MainActivity.context.finish();
			}
		});

		//--- quitButton and its listener
		Button quitButton = (Button) dialog.findViewById(R.id.quitter);
		OnClickListener quitButtonListener = new OnClickListener() {
			public void onClick(View v) {
				MainActivity.currentStep = MainActivity.STEP_QUIT_ON_SUBSCRIPTION_EXPIRED;
				dialog.cancel();
			}
		};
		quitButton.setOnClickListener(quitButtonListener);

		dialog.show();
	}

}