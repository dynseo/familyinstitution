/**
HOUSSEM
*/
package com.dynseo.stimart.common.models;

public class NavigationClass {
	
	public Class activityClass;
	String activityClassName;
	
	public NavigationClass(Class aClass){
		this.activityClass = aClass;
	}
	public NavigationClass(String aClassName){
		activityClassName = aClassName;
	}
	
	public Class getActivityClass() throws ClassNotFoundException{
		if (this.activityClass != null)
			return this.activityClass;
		
		return Class.forName(this.activityClassName);
	}
	
	public static NavigationClass[] build(Object[] navClasses){
		if (navClasses == null)
			return null;
		
		NavigationClass[] navigationClasses = new NavigationClass[navClasses.length];
		for (int i = 0; i < navigationClasses.length; i++){
			if (navClasses[i] instanceof String)
				navigationClasses[i] = new NavigationClass((String)navClasses[i]);
			else
				navigationClasses[i] = new NavigationClass((Class)navClasses[i]);
		}
		return navigationClasses;
	}
	
	public static Class getAtIndex(NavigationClass[] navClasses, int index) throws ClassNotFoundException{
		NavigationClass aNavClass = navClasses[index];
		return aNavClass.getActivityClass();
		
	}
}