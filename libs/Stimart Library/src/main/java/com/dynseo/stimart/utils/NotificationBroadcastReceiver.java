package com.dynseo.stimart.utils;

import com.dynseo.stimart.R;
import com.dynseo.stimart.common.activities.MainActivity;
import com.dynseo.stimart.common.activities.SettingsActivity;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class NotificationBroadcastReceiver extends BroadcastReceiver {
	public NotificationManager notificationManager;
	public int ID_NOTIFICATION = 1;

	public void onReceive(Context context, Intent intent) {
		System.out.println("ONRECEIVE Broascast Receiver");

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Boolean notif = preferences.getBoolean("notification_isActivated",
				false);

		// on boot we re set the notification

		if (intent != null
				&& intent.getAction() != null
				&& intent.getAction().equals(
						"android.intent.action.BOOT_COMPLETED")) {
			// Set the alarm here.
			SettingsActivity.startAlert(context);
			return;
		}

		if (!notif) {
			// if notifications are disable we cancel the alarm
			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(context.ALARM_SERVICE);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, intent, 0);
			alarmManager.cancel(pendingIntent);
			return;
		}

		// specify the activity that will be started when the notification
		// message is clicked
		Intent notiIntent = new Intent(context, MainActivity.class);

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notiIntent, 0);

		Notification notification = new Notification.Builder(context)
				.setTicker(context.getString(R.string.notification_ticker))
				.setContentTitle(context.getString(R.string.notification_title))
				.setContentText(context.getString(R.string.notification_text))
				.setContentIntent(contentIntent).setSmallIcon(R.drawable.logo)
				.build();

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;

		// On creer un "gestionnaire de notification"//
		notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		// Enfin on ajoute notre notification et son ID a notre gestionnaire
		// de
		// notification
		notificationManager.notify(ID_NOTIFICATION, notification);

	}
}