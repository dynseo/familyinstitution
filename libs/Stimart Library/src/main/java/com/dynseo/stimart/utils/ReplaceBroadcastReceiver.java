package com.dynseo.stimart.utils;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

/**
 * BroadCast used for deleting APK file after an update
 * @author SUN Jiaji
 */
public class ReplaceBroadcastReceiver extends BroadcastReceiver {
	private static final String TAG="ApkDelete";
	private String apk_name = "/Stimart.apk";
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub
		Log.d("ReplaceBroadcastReceiver", TAG);
		deleteAPK(arg0, arg1);
	}
	public void deleteAPK(Context context, Intent intent) {
		File downLoadApk = new File(Environment.getExternalStorageDirectory(),"/data/data/" +context.getPackageName() + apk_name);
		Log.d("package name ", context.getPackageName());
		if(downLoadApk.exists()){
			downLoadApk.delete();
			Log.i(TAG, "APK file deleted!");
		}
	}
}
