package com.dynseo.stimart.common.activities;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;
import com.dynseo.stimart.R;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseo.stimart.common.server.DownloadFile;
import com.dynseo.stimart.common.server.DownloadFileInterface;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.tools.Tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Window;
/**
 * Activity that take charge in updates automatic of application
 * @author SUN Jiaji modified by THESEE Alexis
 * @date modified 10-03-2014
 * modified by Jiaji
 * @date 30-07-2014
 */
public class UpdateAppActivity extends Activity implements DownloadFileInterface{

	/** Called when the activity is first created. */
	private static final String TAG = "Update";
	private ProgressDialog progressBar;
	private static final int TIME_OUT_CONNECTION = 5000;
	private static final int TIME_OUT_SOCKET = 120000;

	String lastVersion;


	/* get the current package name (init in MenuAppliActivity) for class Game */
	String APK_SAVE_PATH ;
	//	public static final String APK_SAVE_PATH = "/data/data/"+ "com.dynseo.stimart" + "/";
	public static final String APK_NAME = "Stimart.apk";

	private Handler askInstallHandler;
	private Runnable askInstallRunnable;
	//	private Handler downloadHandler;
//	private Runnable downloadRunnable;
//	private Thread threadDownload;
	private CheckUpdateTask checkUpdateTask;
	private Context context;
	/* dialogs */
	private AlertDialog dialogAskUpdate;
	private AlertDialog dialogCancelDownload;
	private AlertDialog dialogInstall;
	int logoDrawableId;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate()");
		APK_SAVE_PATH = getExternalFilesDir(null).getPath() + "/data/data/"+ AppManager.getAppManager().getPackageName();
		Log.d("test", APK_SAVE_PATH);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		context = this;

		askInstallHandler = new Handler();
//        downloadHandler = new Handler();

		if ( !Http.isOnline(context) ) {
			Tools.showToastLong(getApplicationContext(), getString(R.string.erreurconnexioninternet));
			backToMenuActivity();
		}
		else {
			String theURL = StimartConnectionConstants.urlCheckAppUpdate();
			checkUpdateTask = new CheckUpdateTask();
			logoDrawableId = R.drawable.logo;
			checkUpdateTask.execute(theURL);
		}
	}

	public void createDialogAskUpdate(boolean needUpdate) {

		StringBuffer message = new StringBuffer();
		message.append( getResources().getString(R.string.current_version) );
		message.append( AppManager.getAppManager().getVersionNameAndCode() );
		message.append("\n");

		if (!needUpdate) {
			message.append(getResources().getString(R.string.no_need_update));

			dialogAskUpdate = new AlertDialog.Builder(context, R.style.DialogFullscreen)
					.setIcon(logoDrawableId)
					.setTitle(getResources().getString(R.string.application_update))
					.setMessage(message.toString())
					.setCancelable(false)
					.setNeutralButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							backToMenuActivity();
						}
					}).create();
		}
		else {
			message.append( getResources().getString(R.string.find_new_version) );
			message.append("\n");
			message.append( getResources().getString(R.string.ask_update) );

			dialogAskUpdate = new AlertDialog.Builder(context, R.style.DialogFullscreen)
					.setIcon(logoDrawableId)
					.setTitle(getResources().getString(R.string.application_update))
					.setMessage(message.toString())
					.setCancelable(false)
					.setPositiveButton(getResources().getString(R.string.update_now), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialogAskUpdate.dismiss();
							createProgressBar();
							progressBar.show();

							//downAppFile( StimartConnectionConstants.urlDownloadAppUpdate(lastVersion) );
							Thread downloadThread = new Thread() {
								public void run() {
									DownloadFile downloadFile = new DownloadFile(UpdateAppActivity.this, APK_SAVE_PATH);
									downloadFile.downloadFileByUrl(StimartConnectionConstants.urlDownloadAppUpdate(lastVersion), APK_NAME);
								}
							};
							downloadThread.start();
						}
					})
					.setNegativeButton(getResources().getString(R.string.update_later), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							backToMenuActivity();
						}
					}).create();
		}
	}

	public void createDialogCancelDownload() {

		dialogCancelDownload = new AlertDialog.Builder(context, R.style.DialogFullscreen)
				.setIcon(R.drawable.alert_logo)
				.setTitle(getResources().getString(R.string.application_update))
				.setMessage(getResources().getString(R.string.cancel_download))
				.setCancelable(false)
				.setPositiveButton(getResources().getString(R.string.oui), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteAPK();
						backToMenuActivity();
					}
				})
				.setNegativeButton(getResources().getString(R.string.non), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialogCancelDownload.dismiss();
						progressBar.show();
					}
				}).create();
	}

	public void createDialogInstall() {
		Log.i(TAG, "createDialogInstall");
		dialogInstall = new AlertDialog.Builder(context, R.style.DialogFullscreen).setTitle(getResources()
				.getString(R.string.download_finish)).setMessage(getResources()
				.getString(R.string.install_new_application))
				.setIcon(logoDrawableId)
				.setCancelable(false)
				.setPositiveButton(getResources().getString(R.string.oui)
						, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								installNewApk();
								finish();
								overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
							}
						}).setNegativeButton(getResources().getString(R.string.non), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteAPK();
						backToMenuActivity();
					}
				}).create();
	}

	protected void createProgressBar() {

		progressBar = new ProgressDialog(context, R.style.DialogFullscreen);
		progressBar.setCanceledOnTouchOutside(false);
		progressBar.setIcon(logoDrawableId);
		progressBar.setTitle(getResources().getString(R.string.downloading));
		progressBar.setMessage(getResources().getString(R.string.please_wait));
		progressBar.setMax(100);
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setProgressNumberFormat(null);
		progressBar.setProgressPercentFormat(NumberFormat.getPercentInstance());
		progressBar.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				createDialogCancelDownload();
				dialogCancelDownload.show();
			}
		});
	}

//	public void createDirIfNotExist() {
//			File file = new File(context.getExternalFilesDir(null).getPath() + APK_SAVE_PATH);
//			if (!file.exists()) {
//				if (file.mkdirs()) {
//					Log.i(TAG, "make dir ok");
//				}
//				else {
//					Log.e(TAG, "error - make dir");
//				}
//			}
//
//	}

//	public void createDirIfNotExist() {
//		Log.d(TAG, "createDirIfNotExist()");
//		if (Environment.MEDIA_MOUNTED.equals(Environment
//				.getExternalStorageState())) {
//			File file = new File(Environment.getExternalStorageDirectory() + APK_SAVE_PATH);
//			if (!file.exists()) {
//				if (file.mkdirs()) {
//					Log.i(TAG, "make dir ok");
//				}
//				else {
//					Log.e(TAG, "error - make dir");
//				}
//			}
//		}
//	}

	/*protected void downAppFile(final String url) {
		threadDownload = new Thread() {
			public void run() {
				Log.i("download","url = "+url);
				HttpGet get = new HttpGet(url);
				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is established.
				// The default value is zero, that means the timeout is not used.
				HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT_CONNECTION);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				HttpConnectionParams.setSoTimeout(httpParameters, TIME_OUT_SOCKET);
				HttpClient client = new DefaultHttpClient(httpParameters);
				HttpResponse response = null;
				InputStream inputStream = null;
				FileOutputStream fileOutputStream = null;
				try {
					response = client.execute(get);
				    int statusCode = response.getStatusLine().getStatusCode();
				    Log.i(TAG, "HTTP status code = "+statusCode);
				    if (statusCode != 200 && statusCode != 206) {
				    	throw new Exception("file not exist");
				    }
					HttpEntity entity = response.getEntity();
					long length = entity.getContentLength();
					Log.i("download", "download file size = "+(int) length);
					inputStream = entity.getContent();
					fileOutputStream = null;
					if (inputStream == null) {
						throw new RuntimeException("isStream is null");
					}
					// File file = new File(getFilesDir(),apkName);
					createDirIfNotExist();
					File file = new File(context.getExternalFilesDir(null).getPath() + APK_SAVE_PATH + APK_NAME);
					System.out.println(file);
					fileOutputStream = new FileOutputStream(file);
					byte[] buf = new byte[1024];
					int ch = -1;
					long total = 0;
					do {
						ch = inputStream.read(buf);
						if (ch <= 0  || isInterrupted()) {
							Log.i(TAG,"thread interrupted");
							break;
						}
						total += ch;
						progressBar.setProgress((int)((total*100)/length));

						fileOutputStream.write(buf, 0, ch);
					} while (true);
					inputStream.close();
					fileOutputStream.close();
					if(!isInterrupted()) {
						finishDownLoad();
					}
				} catch (ClientProtocolException e) {
					Log.e(TAG,"error1 - "+e.getMessage());
					e.printStackTrace();
					Looper.prepare();
					Tools.showToastLong(context, getString(R.string.error_download));
					backToMenuActivity();
					// attention ! code after loop() can't be executed
					Looper.loop();
				} catch (IOException e) {
					Log.e(TAG,"error2 - "+e.getMessage());
					e.printStackTrace();
					Looper.prepare();
					Tools.showToastLong(context, getString(R.string.error_server));
					backToMenuActivity();
					Looper.loop();
				} catch (Exception e) {
					Log.e(TAG,"error3 - "+e.getMessage());
					e.printStackTrace();
					Looper.prepare();
					Tools.showToastLong(context, getString(R.string.error_download));
					backToMenuActivity();
					Looper.loop();
				} finally {
					try {
						if(inputStream != null) {
							inputStream.close();
						}
						if(fileOutputStream != null) {
							fileOutputStream.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		};
		threadDownload.start();
	}*/
	//cancel progressBar and start new App
	/*protected void finishDownLoad() {
		Log.i(TAG,"haveDownload()");
		askInstallHandler.post(askInstallRunnable = new Runnable() {
			public void run() {
				progressBar.dismiss();
				// alert dialog show
				createDialogInstall();
				dialogInstall.show();
			}
		});
	}*/
	//install new application
	protected void installNewApk() {
		Log.i(TAG,"installNewApk()");
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		if (android.os.Build.VERSION.SDK_INT <= 23)
			intent.setDataAndType(Uri.fromFile(
					new File(APK_SAVE_PATH + File.separator + APK_NAME)),
					"application/vnd.android.package-archive");

		else {
			Uri apkURI = FileProvider.getUriForFile(
					context,
					context.getApplicationContext()
							.getPackageName() + ".provider", new File(APK_SAVE_PATH + File.separator + APK_NAME));
			intent.setDataAndType(apkURI, "application/vnd.android.package-archive");

			List<ResolveInfo> resolvedIntentActivities = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
			for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
				String packageName = resolvedIntentInfo.activityInfo.packageName;

				context.grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
			}
		}

//		intent.setDataAndType(Uri.fromFile(
//				new File(getFilesDir(),apkName)),
//				"application/vnd.android.package-archive");

		cleanSharedPreference();
		startActivity(intent);
	}

	protected void cleanSharedPreference(){
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		Log.i(TAG,"shared pref institution : " + pref.getString(StimartConnectionConstants.SHARED_PREFS_INSTITUTION,""));
		pref.edit().putString(StimartConnectionConstants.SHARED_PREFS_INSTITUTION, "").commit();
	}

	protected void deleteAPK() {
		File downLoadApk = new File(APK_SAVE_PATH + File.separator + APK_NAME);
		if(downLoadApk.exists()){
			downLoadApk.delete();
			Log.i(TAG, "APK file deleted!");
		}
	}

	private boolean checkUpdate(String updatePath) {

		boolean result = false;

		try {
////		int currentVersionCode = AppManager.getAppManager().getVersionCode();	//To be used when upload part by part

			Log.i(TAG, "path : " + updatePath);
			String serverResponse = Http.queryServer(updatePath);
			Log.i(TAG , "server response : " + serverResponse);

			JSONObject o = new JSONObject(serverResponse);

			String updateNeeded = o.getString(StimartConnectionConstants.JSON_PARAM_UPDATE_NEEDED);

			if(updateNeeded.equals(StimartConnectionConstants.YES)) {
				Log.i(TAG,"update needed");
				lastVersion = o.getString(StimartConnectionConstants.JSON_PARAM_NEW_APP_VERSION);
				Log.i(TAG,"last version : " + lastVersion);
//				downloadPath += ConnectionConstants.PARAM_APP_VERSION + newAppVersion;
//				Log.d(TAG,"download path : " + downloadPath);
				result = true;
			}
			else {
				Log.i(TAG,"no need to update");
			}

		} catch (JSONException e) {
			Log.e(TAG,"Json Instanciate or Read Exception");
			e.printStackTrace();
		}
		return result;
	}

	private void backToMenuActivity() {
		overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(TAG,"onDestroy()");
//		if (threadDownload != null) {
//			threadDownload.interrupt();
//		}
		if (askInstallHandler != null) {
			askInstallHandler.removeCallbacks(askInstallRunnable);
		}
//		if (downloadHandler != null) {
//			downloadHandler.removeCallbacks(downloadRunnable);
//		}
		if (dialogAskUpdate != null) {
			dialogAskUpdate.dismiss();
		}
		if (dialogCancelDownload != null) {
			dialogCancelDownload.dismiss();
		}
		if (dialogInstall != null) {
			dialogInstall.dismiss();
		}
		if (progressBar != null) {
			progressBar.dismiss();
		}
		if (checkUpdateTask != null) {
			checkUpdateTask.cancel(true);
		}
	}

	@Override
	public void onDownloadSuccess() {
		Log.i(TAG,"onDownloadSuccess()");
		askInstallHandler.post(askInstallRunnable = new Runnable() {
			public void run() {
				progressBar.dismiss();
				// alert dialog show
				createDialogInstall();
				dialogInstall.show();
			}
		});
	}

	@Override
	public void onDownloadFailure(Exception e) {
		Log.e(TAG,"onDownloadFailure - "+e.getMessage());
		e.printStackTrace();
		Looper.prepare();
		if (e instanceof ClientProtocolException) {
			Tools.showToastLong(context, getString(R.string.error_download));
		}
		else if (e instanceof IOException) {
			Tools.showToastLong(context, getString(R.string.error_server));
		}

		else if (e instanceof Exception) {
			Tools.showToastLong(context, getString(R.string.error_download));
		}
		backToMenuActivity();
		Looper.loop();
	}

	@Override
	public void onDownloadProgress(long total, int lengthOfFile) {
		if (progressBar != null) {
			progressBar.setProgress((int) ((total * 100) / lengthOfFile));
		}
	}

	//==============================================================================
	// AsyncTask to check apk
	//==============================================================================
	private class CheckUpdateTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {

			if( checkUpdate(params[0]) )
				return StimartConnectionConstants.TRUE;
			else
				return StimartConnectionConstants.FALSE;
		}

		protected void onPostExecute(String result) {

			if(result.equals(StimartConnectionConstants.TRUE)) {
				createDialogAskUpdate(true);
			}
			else {
				createDialogAskUpdate(false);
			}
			dialogAskUpdate.show();
		}
	}

}