package com.dynseo.stimart.common.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import com.dynseo.stimart.R;
import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.common.models.ScreenSize;
import com.dynseo.stimart.common.models.SpinnerAdapter;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.tools.Tools;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Mathias on 21/10/2016.
 */

public abstract class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    /**
     * Menu to choose a game to play
     */
    private final static String TAG = "MainActivity";
    // subscription is stored in preferences
    protected SharedPreferences preferences;

    protected Button buttonConnectionMain; // this button connection is the one in the
    protected Button buttonCreateOrShowProfile; // If not connected used to create, if

    protected Dialog dialogProfileConnection;
    protected Dialog dialogCreateProfile;
    protected Dialog dialogShowProfile;
    protected Dialog dialogAboutUs;
    protected Dialog dialogSyncUpdate;
    protected ProgressDialog pDialog;
    protected Spinner pseudoSpinner;

    protected boolean existCreateButton;
    protected boolean personAlwaysIdentified;

    /* array to store accounts fetched from the server */
    protected String[] pseudos;
    protected int[] ids;
    protected int indice;

    EditText firstname, name;
    RadioGroup radioGroup;

    public static Extractor extractor;
    public static int currentStep = 0;
    public static int STEP_START = 0;
    public static int RESULT_KO = 3;
    public static int RESULT_DO_RESOURCES = 4;
    public static int STEP_VERIFICATION = 1;            //When verification begins
    public static int STEP_VERIFICATION_SUCCESS = 10;
    public static int STEP_VERIFICATION_FAILURE = 11;
    public static int STEP_CREATE_ACCOUNT_SUCCESS = 12;
    public static int STEP_LOGIN_SUCCESS = 13;
    public static int STEP_SYNCHRO = 2;
    //static int STEP_RESOURCES    = 3;
    //static int STEP_SIGN_UP_OR_CONNECT = 4;
    public static int STEP_VISIT = 5;
    public static int STEP_ALL_DONE = 6;
    public static int STEP_QUIT_ON_SUBSCRIPTION_EXPIRED = 20;

    ArrayAdapter<String> spinnerArrayAdapter;
    EditText birthdate;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private static final String DUPLICATE_PERSON = "duplicatePerson";

    static public Activity context;

    protected AddPersonTask connexion;

    public static final int NB_MAX_PROFILE_FOR_PARTICULAR = 3;
    //In app permissions
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL = 1;
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL_WRITE = 2;
    public static final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};


    //@Override
    //protected void onCreate(Bundle savedInstanceState) {
        //Log.d(TAG, "onCreate()");
        //super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
//        setContentView(getLayoutId());
        //context = this;

        // initialize all the app parameters (used for download management)
        //AppManager appManager = AppManager.getAppManager(this);

        //Bugsnag.init(this);
        //Bugsnag.addToTab("User", "Serial Number", Tools.getManufacturerSerialNumber());
        //Bugsnag.notify(new RuntimeException("Non-fatal"));

        //ConnectionConstants.setBaseUrl("http://test.stimart.com/app?newPilotSession=true", "http://test.stimart.com/pilot?service=");
        //ConnectionConstants.setBaseUrl("http://192.168.1.32/stimart/app?newPilotSession=true", "http://192.168.1.32/stimart/pilot?service=");

        // Handle possible data accompanying notification message.
        // [START handle_data_extras]
//        if (getIntent().getExtras() != null) {
//            for (String key : getIntent().getExtras().keySet()) {
//                String value = getIntent().getExtras().getString(key);
//                //Log.d("Firebase" + TAG, "Key: " + key + " Value: " + value);
//            }
//        }
        // [END handle_data_extras]


        //preferences = PreferenceManager.getDefaultSharedPreferences(this);

        //skipOnResume = false;
        //currentStep = STEP_START;

        //personAlwaysIdentified = Tools.isResourceTrue(this, R.string.person_always_identified);


        //AppResourcesManager appResourcesManager = AppResourcesManager.getAppResourcesManager(context);

        //extractor = new Extractor(this);

        //initializeScreen();

        // Initialization of buttonConnectionMain and buttonCreateOrShowProfile
        //buttonConnectionMain = (Button) findViewById(R.id.menu_button_connexion);

        /*existCreateButton = false;
        int checkExistence = this.getResources().getIdentifier(
                "menu_button_create", "id", getPackageName());
        if (checkExistence != 0) {
            existCreateButton = true;
            buttonCreateOrShowProfile = (Button) findViewById(R.id.menu_button_create);
        }*/
//        initializeDialogProfileConnection();

//        String institutionType = preferences.getString(ConnectionConstants.SHARED_PREFS_INSTITUTION_TYPE, null);
        // app rating only for particular
//        Log.d("instututiontype", "" + institutionType);
//        if ((institutionType != null && institutionType.equals("HOME")) || (AppManager.getAppManager().getFreemiumName() != null)) {
//            AppRater.app_launched(this); // app rating
//
//            // if only 1 profile then connect to it
//            int nbCreatedProfile = getNbCreatedProfile();
//            if (nbCreatedProfile == 1) {
//                extractor.open();
//                int id = extractor.getIds()[0];
//                Person.currentPerson = extractor.getResidentWithId(id);
//                extractor.close();
//            }
//        }

        //showPerson(); // Will act differently if exists Person.currentPerson or not

 //   }

    /*protected int getLayoutId() {
        return R.layout.menu_appli_activity_layout;
    }*/

    @Override
    protected void onResume() {

        super.onResume();

        manageDialogs();
    }

    public void initializeScreen() {

        if (ScreenSize.screenHeight == 0) {
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            ScreenSize.screenWidth = dm.widthPixels;
            ScreenSize.screenHeight = dm.heightPixels;
            ScreenSize.screenSizeW = (float) ScreenSize.screenWidth
                    / (float) ScreenSize.widthReference;
            ScreenSize.screenSizeH = (float) ScreenSize.screenHeight
                    / (float) ScreenSize.heightReference;
            float density  = getResources().getDisplayMetrics().density;
            float dpHeight = dm.heightPixels / density;
            float dpWidth  = dm.widthPixels / density;
            Log.i(TAG, "density = " + density);
            Log.i(TAG, "screen width dp =" + dpWidth);
            Log.i(TAG, "screen heigth dp =" + dpHeight);
        }
        Log.i(TAG, "screen width =" + ScreenSize.screenWidth);
        Log.i(TAG, "screen heigth =" + ScreenSize.screenHeight);
    }


    // ==================================== Methods for Dialog initialization
    // ====================================//
    // dialogIdentification, dialogCreateProfile, dialogShowProfile,
    // dialogInformation, dialogSyncUpdate

    public void initializeDialogProfileConnection() {
        // ------------------------------------------- //
        // Bouton connexion affichage
        Log.d(TAG, "initializeDialogIdentification()");
        dialogProfileConnection = new Dialog(MainActivity.this,
                R.style.DialogFullscreen);
        dialogProfileConnection
                .setContentView(R.layout.dialog_profile_connection_layout);

        // === Set the connection button
        Button buttonConnection = (Button) dialogProfileConnection
                .findViewById(R.id.button_profile_connection);
        buttonConnection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // we search for the
                extractor.open();
                Person.currentPerson = extractor.getResidentWithId(ids[indice]);
                extractor.close();
                dialogProfileConnection.dismiss();
                showPerson();
            }
        });

        // === No create button on the recipe_cat_main layout. Insert in in dialog.
        if (!existCreateButton) {
            // === Set the button and listener
            Button buttonCreateProfileInDialog = (Button) dialogProfileConnection
                    .findViewById(R.id.button_create_profile);
            buttonCreateProfileInDialog.setVisibility(View.VISIBLE);
            buttonCreateProfileInDialog
                    .setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            try {
                                showDialogCreatePerson();
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    });
        }

        dialogProfileConnection
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        dialogProfileConnection.dismiss();
                    }
                });
    }

    public void initializeDialogCreateProfile() {
        // ------------------------------------------- //

        Log.d(TAG, "initializeDialogCreateProfile()");
        dialogCreateProfile = new Dialog(MainActivity.this,
                R.style.DialogFullscreen);
        dialogCreateProfile.setCanceledOnTouchOutside(false);
        dialogCreateProfile.setContentView(R.layout.dialog_create_profile_layout);
        // dialogCreateProfile.getWindow().setSoftInputMode(
        // WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // === Set the fields
        firstname = (EditText) dialogCreateProfile.findViewById(R.id.firstname);
        name = (EditText) dialogCreateProfile.findViewById(R.id.name);

        birthdate = (EditText) dialogCreateProfile
                .findViewById(R.id.picker_birthdate);
        birthdate.setInputType(InputType.TYPE_NULL);
        setDatePickerDialog(birthdate);
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });

        radioGroup = (RadioGroup) dialogCreateProfile
                .findViewById(R.id.radiogroup);

        // === Set the buttons and listeners
        Button buttonResetBirthdate = (Button) dialogCreateProfile
                .findViewById(R.id.button_reset_birthdate);
        buttonResetBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!birthdate.getText().toString().isEmpty())
                    birthdate.setText("");
            }
        });
        Button buttonCancel = (Button) dialogCreateProfile
                .findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialogCreateProfile.dismiss();
            }
        });

        Button buttonSave = (Button) dialogCreateProfile
                .findViewById(R.id.button_confirm);
        buttonSave.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                String prenom = firstname.getText().toString().trim();
                String nom = name.getText().toString().trim();

                if (nom.equals("")) {
                    Tools.showToast(getApplicationContext(),
                            getString(R.string.erreurnom));
                }
                else if (prenom.equals("")) {
                    Tools.showToast(getApplicationContext(),
                            getString(R.string.erreurprenom));
                }
                else {
                    String sexe = null;
                    if (radioGroup.getCheckedRadioButtonId() == R.id.male)
                        sexe = Person.sexm;
                    else if (radioGroup.getCheckedRadioButtonId() == R.id.female)
                        sexe = Person.sexf;

                    if (sexe == null) {
                        Tools.showToast(getApplicationContext(),
                                getString(R.string.erreursexe));
                    }
                    else {
                        int jour = -1, mois = -1, annee = -1;
                        if (fromDatePickerDialog != null) {
                            jour = fromDatePickerDialog.getDatePicker().getDayOfMonth();
                            mois = fromDatePickerDialog.getDatePicker().getMonth();
                            annee = fromDatePickerDialog.getDatePicker().getYear();
                        }
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(annee, mois, jour);

                        Person newResident = new Person(prenom, nom, calendar.getTime(), sexe);

                        String toast = checkPerson(newResident);
                        if (toast.equals(getString(R.string.ok))) {
                            // Si internet et pas visite mode on rajoute directement dans la bdd de
                            // stimart

                            if( Http.isOnline(getApplicationContext()) && !AppManager.getAppManager().getMode().equals("visit") ) {

                                Log.i(TAG, "Internet");
                                connexion = new AddPersonTask(
                                        getApplicationContext());
                                connexion.setPersonToAdd(newResident);
                                connexion.setDialog(dialogCreateProfile);
                                connexion.execute();
                            }
                            // Sinon on le met dans la SQLlite
                            else {
                                Log.i(TAG, "Pas internet");
                                dialogCreateProfile.dismiss();

                                // If personAlwaysIdentified we log with this person
                                insertPersonLocally(newResident,
                                        personAlwaysIdentified, true);
							/*
							 * recreate the dialog to show the newly added
							 * person
							 */
                                if (dialogProfileConnection != null && dialogProfileConnection.isShowing()) {
                                    dialogProfileConnection.dismiss();
                                }
                            }
                        } else if (pDialog != null) {
                            pDialog.dismiss();
                        } else
                            Tools.showToast(getApplicationContext(), toast);
                    }
                }
                firstname.setText("");
                name.setText("");
                birthdate.setText("");
                radioGroup.clearCheck();
            }
        });
    }

    public void initializeDialogShowProfile() {
        // ------------------------------------------- //

        Log.d(TAG, "initializeDialogShowProfile()");
        dialogShowProfile = new Dialog(MainActivity.this,
                R.style.DialogFullscreen);
        dialogShowProfile.setCanceledOnTouchOutside(false);
        dialogShowProfile.setContentView(R.layout.dialog_view_profile_layout);

        // === Set the button and listener
        Button buttonReturn = (Button) dialogShowProfile
                .findViewById(R.id.button_confirm);
        buttonReturn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialogShowProfile.dismiss();
            }
        });
    }

    public abstract void initializeDialogInformation();
         //------------------------------------------- //

//        int[] textViewIds = { R.id.textViewAppFullName, R.id.textViewAppLang,
//                R.id.textViewAppVersion, R.id.textViewOSVersion,
//                R.id.textViewEndSub, R.id.textViewBeginSub,
//                R.id.textViewSynchroData, R.id.textViewSynchroRes,
//                R.id.textViewAppUpdate,
//                R.id.textViewSerialNumber };
//        String[] appInfo = {
//                AppManager.getAppManager().getAppFullDisplayName(),
//                AppManager.getAppManager().getLangAlter(),
//                AppManager.getAppManager().getVersionNameAndCode(),
//                Build.VERSION.RELEASE,
//                Tools.formattedAsDate(
//                        preferences.getString(ConnectionConstants.SHARED_PREFS_BEGIN_SUBSCRIPTION,"")),
//                Tools.formattedAsDateTime(preferences.getString(ConnectionConstants.SHARED_PREFS_SYNCHRO_RES_DATE,"")),
//                Tools.formattedAsDateTime(preferences.getString(ConnectionConstants.SHARED_PREFS_FIRST_LAUNCH,"")),
//                Tools.getManufacturerSerialNumber()};
//        String[] infoLabels = this.getResources().getStringArray(
//                R.array.infoLabels);
//
//        Log.d(TAG, "initializeDialogInformation()");
//        if (dialogAboutUs == null)
//            dialogAboutUs = new Dialog(MainActivity.this, R.style.DialogFullscreen);
//
//        dialogAboutUs.setContentView(R.layout.dialog_about_us_layout);
//
//		/* show version number and... on the info dialog */
//        for (int i = 0; i < appInfo.length; i++) {
//            TextView textViewInfo = (TextView) dialogAboutUs
//                    .findViewById(textViewIds[i]);
//            textViewInfo.setText(Html.fromHtml(infoLabels[i] + " <b>"
//                    + appInfo[i] + "</b>"));
//        }
//
//        TextView textPresentation = (TextView) dialogAboutUs
//                .findViewById(R.id.content);
//
//		/* show version number and... on the info dialog */
//        // textAppVersion.setText("Version : " +
//        // AppManager.getAppManager().getVersionName());
//        // textOSVersion.setText("Version : " + Build.VERSION.RELEASE);
//        textPresentation.setMovementMethod(LinkMovementMethod.getInstance());
//
//        Button fermer = (Button) dialogAboutUs
//                .findViewById(R.id.button_confirm);
//        fermer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                dialogAboutUs.dismiss();
//            }
//        });
////        Button lang = (Button) dialogAboutUs.findViewById(R.id.changeLang);
////        lang.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).clearApplicationUserData();
////            }
////        });

// For change langue when i make a mistake
//    public void clearApplicationData() {
//        File cache = getCacheDir();
//        File appDir = new File(cache.getParent());
//        if(appDir.exists()){
//            String[] children = appDir.list();
//            for(String s : children){
//                if(!s.equals("lib")){
//                    deleteDir(new File(appDir, s));
//                    Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
//                }
//            }
//        }
//    }
//    public static boolean deleteDir(File dir) {
//        if (dir != null && dir.isDirectory()) {
//            String[] children = dir.list();
//            for (int i = 0; i < children.length; i++) {
//                boolean success = deleteDir(new File(dir, children[i]));
//                if (!success) {
//                    return false;
//                }
//            }
//        }
//        return dir.delete();
//    }

//    public void initializeDialogSyncUpdate() {
//        // ------------------------------------------- //
//
//        dialogSyncUpdate = new Dialog(MainActivity.this,
//                R.style.DialogFullscreen);
//        dialogSyncUpdate.setContentView(R.layout.dialog_synchro_update_layout);
//
//        Button buttonSynchro = (Button) dialogSyncUpdate
//                .findViewById(R.id.buttonSynchro);
//        Button buttonUpdateResources = (Button) dialogSyncUpdate
//                .findViewById(R.id.buttonUpdateResources);
//        Button buttonUpdate = (Button) dialogSyncUpdate
//                .findViewById(R.id.buttonUpdate);
//
//        buttonSynchro.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//
//                if( AppManager.getAppManager().isSubscriptionValid() &&
//                        !AppManager.getAppManager().getMode().equals("visit") ) {
//                    dialogSyncUpdate.dismiss();
//
//                    nextActivity(SynchronizationActivity.class, false, "autoLaunch", false, "type", "data");
//                }
//                else {			//No synchro if visit or freemium (if freemium, isSubscriptionValid is false)
//                    Tools.showToast(context, getString(R.string.no_synchro_in_visit_mode));
//                }
//            }
//        });
//
//        buttonUpdate.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//
//                if( AppManager.getAppManager().isSubscriptionValid() &&
//                        !AppManager.getAppManager().getMode().equals("visit") ) {
//                    dialogSyncUpdate.dismiss();
//                    nextActivity(UpdateAppActivity.class, false);
//                }
//                else {			//No synchro if visit or freemium (if freemium, isSubscriptionValid is false)
//                    Tools.showToast(context, getString(R.string.no_synchro_in_visit_mode));
//                }
//            }
//        });
//
//        buttonUpdateResources.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if( AppManager.getAppManager().isSubscriptionValid() &&
//                        !AppManager.getAppManager().getMode().equals("visit") ) {
//                    dialogSyncUpdate.dismiss();
//
//                    nextActivity(SynchronizationActivity.class, false, "autoLaunch", false, "type", "resources");
//                }
//                else {			//No synchro if visit or freemium (if freemium, isSubscriptionValid is false)
//                    Tools.showToast(context, getString(R.string.no_synchro_in_visit_mode));
//                }
//            }
//        });
//    }

    // ==================================== End of Dialogs initialization
    // ====================================//

//    public boolean hasResources() {
//        Log.d(TAG, "hasResources()");
//        File file1 = new File(AppResourcesManager.getAppResourcesManager().getPathAudios());
//        File file2 = new File(AppResourcesManager.getAppResourcesManager().getPathImagesPrevious());
//        if (!file1.exists() || !file2.exists()) {
//            Log.e(TAG, "resources files not complete");
//            return false;
//        }
//        return true;
//   }
//
    public void deleteAPK() {
        String APK_SAVE_PATH = "/data/data/"+ AppManager.getAppManager().getPackageName();
        String APK_NAME = "Stimart.apk";
        Log.d(TAG, "deleteAPK()");
        File downLoadApk = new File(Environment.getExternalStorageDirectory(),
                APK_SAVE_PATH + APK_NAME);
        File downLoadApk2 = new File(Environment.getExternalStorageDirectory(),
                "/Download/" + APK_NAME);
        if (downLoadApk.exists()) {
            downLoadApk.delete();
            Log.i(TAG, "APK1 file deleted!");
        }
        if (downLoadApk2.exists()) {
            downLoadApk2.delete();
            Log.i(TAG, "APK2 file deleted!");
        }
    }

    public void showDialogProfileIdentification() {
        Log.i(TAG, "showDialogProfileIdentification()");
        // Get pseudos in the local database
        extractor.open();
        ids = extractor.getIds();
        pseudos = extractor.getPseudos();
        extractor.close();
        Log.i(TAG, "pseudoLength = " + pseudos.length);

        if (pseudos.length == 0) { // No pseudo in the local database
            Tools.showToast(getApplicationContext(), getString(R.string.pasdeprofil));
            //if (personAlwaysIdentified) {
            if (dialogCreateProfile == null)
                initializeDialogCreateProfile();
            dialogCreateProfile.show();
            //}
        } else { // Some pseudo exist but nobody connected
            // Build the spinner to select pseudo
            pseudoSpinner = (Spinner) dialogProfileConnection
                    .findViewById(R.id.pseudospinner);
            spinnerArrayAdapter = new SpinnerAdapter(this,
                    R.layout.profile_connection_spinner_item, pseudos,
                    pseudoSpinner);
            pseudoSpinner.setAdapter(spinnerArrayAdapter);
            pseudoSpinner.setOnItemSelectedListener(this);
            // spinnerArrayAdapter.notifyDataSetChanged();
            dialogProfileConnection.show();
        }
    }


    public String addPerson(Person person, Context context) {
        Log.d(TAG, "addPerson()");
        String answer = null;
        try {
            String path = StimartConnectionConstants
                    .urlAddPerson(person, false); // false
            // means
            // doesn't
            // exist
            // locally
            Log.d("path addPerson", path);
            String serverResponse = Http.queryServer(path);
            JSONObject o = new JSONObject(serverResponse);
            try {
                Log.i(TAG, "JSONObject addPerson : " + o);
                answer = o.getString("serverId");

            } catch (JSONException e) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    protected void showPersonInPersonForm() {
        TextView firstname = (TextView) dialogShowProfile.findViewById(R.id.firstname);
        firstname.setText(Person.currentPerson.getFirstName());

        TextView name = (TextView) dialogShowProfile.findViewById(R.id.name);
        name.setText(Person.currentPerson.getName());

        TextView sex = (TextView) dialogShowProfile.findViewById(R.id.sex);
        sex.setText(Person.currentPerson.getSexString(this));

        TextView birthday = (TextView) dialogShowProfile.findViewById(R.id.birthday);
        birthday.setText(Person.currentPerson.getBirthdateStr());

        ImageView imagePseudo = (ImageView) dialogShowProfile.findViewById(R.id.profil_image);

        if(Person.currentPerson.isAdult()) {
            if (Person.currentPerson.isMale())
                imagePseudo.setImageResource(R.drawable.icon_man);
            else if (Person.currentPerson.isFemale())
                imagePseudo.setImageResource(R.drawable.icon_woman);
        }
        else {
            if (Person.currentPerson.isMale())
                imagePseudo.setImageResource(R.drawable.icon_boy);
            else if (Person.currentPerson.isFemale())
                imagePseudo.setImageResource(R.drawable.icon_girl);
        }

        dialogShowProfile.show();
    }

    protected void insertPersonLocally(Person newResident, boolean logNewResident, boolean showToast) {
        extractor.open();
        extractor.insertPerson(newResident);
        extractor.close();

        if (showToast)
            Tools.showToast(getApplicationContext(), getString(R.string.profilcree));

        if (logNewResident) {
            Person.currentPerson = newResident;
            showPerson();
        }
    }

    // Partie qui gere le clavier qui se cache quand on clique a cote
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                .getWindowToken(), 0);
    }

    public void setupUI(View view) {
        Log.d(TAG, "setupUI()");
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public String checkPerson(Person person) {
        Log.d(TAG, "checkPerson()");

        extractor.open();
        Person nomExistant = extractor.getResidentWithNameFirstname(
                person.getName(), person.getFirstName());
        extractor.close();
        if (nomExistant == null)
            return getString(R.string.ok);

        return getString(R.string.duplicatePerson);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapter, View v, int pos,
                               long arg3) {
        if (adapter == pseudoSpinner) {
            indice = pos;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

//    /**
//     * this method shows create profile dialog depending on user type (institution / particular)
//     * for particular : profile creation is limited by NB_MAX_PROFILE_FOR_PARTICULAR parameter
//     * @throws JSONException
//     */
    public void showDialogCreatePerson() throws JSONException{
        String institutionType = preferences.getString(ConnectionConstants.SHARED_PREFS_INSTITUTION_TYPE, null);
        //String accountType = Account.getAccountTypeFromSP(preferences);
        String mode = AppManager.getAppManager().getMode();

        Log.d(TAG, "accountType : " + institutionType);
        Log.d(TAG, "mode : " + mode);
        if ((institutionType != null && institutionType.equals("HOME")) || mode.equals("visit"))
        {
            if (getNbCreatedProfile() < NB_MAX_PROFILE_FOR_PARTICULAR){
                if (dialogCreateProfile == null)
                    initializeDialogCreateProfile();
                dialogCreateProfile.show();
            }
            else {
                Tools.showToast(context, getString(R.string.max_profile_created));
            }
        }
        else{
            if (dialogCreateProfile == null)
                initializeDialogCreateProfile();
            dialogCreateProfile.show();
        }
    }

    protected int getNbCreatedProfile(){
        extractor.open();
        int nbCreatedProfile = extractor.getNumberProfileCreated();
        extractor.close();
        Log.d(TAG, "nb profile : " + nbCreatedProfile);
        return nbCreatedProfile;
    }

    private void setDatePickerDialog(final EditText field){

        dateFormatter = new SimpleDateFormat(AppManager.getAppManager().getDateFormat());
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                field.setText(dateFormatter.format(newDate.getTime()));
            }

        },1930, 0, 1);   // for Edith et Joe 1930,0,1
        fromDatePickerDialog.getDatePicker().setCalendarViewShown(false);
    }

//    // ==============================================================================
//    // AsyncTask to add a person on the server
//    // ==============================================================================
    public class AddPersonTask extends AsyncTask<Void, Void, String> {

        private static final String TAG = "AddPersonTask";
        Context context;
        Person personToAdd;
        Dialog dialog;

        public AddPersonTask(Context context) {
            this.context = context;
        }

        public void setPersonToAdd(Person person) {
            personToAdd = person;
        }

        public void setDialog(Dialog dialog1) {
            dialog = dialog1;
        }

        @Override
        public void onPreExecute() {
            Log.d(TAG, "onPreExecute()");
            String creating = getResources().getString(R.string.creating);
            SpannableString ss1 = new SpannableString(creating);
            ss1.setSpan(new RelativeSizeSpan(2f), 0, ss1.length(), 0);
            pDialog = new ProgressDialog(MainActivity.this,
                    ProgressDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(ss1);
            pDialog.setCancelable(false);
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            Log.d(TAG, "doInBackground()");
            String result = addPerson(personToAdd, context);
            if (result != null) {
                Log.i("result = ", result);
            } else {
                Log.i(TAG, "NO INTERNET");
            }
            return result;
        }

        @Override
        public void onProgressUpdate(Void... games) {
        }

        public void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute()");
            if (result != null) {
                if (result.equals(DUPLICATE_PERSON)) {
                    pDialog.dismiss();
                    dialogCreateProfile.dismiss();
                    Tools.showToast(context,
                            context.getString(R.string.duplicatePerson));
                } else {
                    pDialog.dismiss();
                    personToAdd.setServerId(result);

                    // If personAlwaysIdentified we log with this person
                    insertPersonLocally(personToAdd, personAlwaysIdentified, true);
					/* recreate the dialog to show the newly added person */

                    if (dialogProfileConnection != null
                            && dialogProfileConnection.isShowing()) {
                        dialogProfileConnection.dismiss();
                    }
                    dialog.dismiss();
                }
            } else {
                Log.i(TAG, "ALWAYS NO INTERNET");
                // If personAlwaysIdentified we log with this person
                insertPersonLocally(personToAdd, personAlwaysIdentified, true);
                pDialog.dismiss();
                dialogCreateProfile.dismiss();
            }
        }
    }

    abstract public void manageDialogs();


    public void clearPerson() {
        Person.currentPerson = null;
        showPerson();
    }

    public void showPerson() {

    }

    protected void nextActivity(Intent nextIntent, boolean forResult) {
        if (forResult)
            startActivityForResult(nextIntent, 0);
        else
            startActivity(nextIntent);

        overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    protected void nextActivity(Class nextClass, boolean forResult) {
        nextActivity(new Intent(MainActivity.this, nextClass), forResult);
        overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    protected void nextActivity(Class nextClass, boolean forResult, String key, boolean value) {
        Intent intent = new Intent(MainActivity.this, nextClass);
        intent.putExtra(key, value);
        nextActivity(intent, forResult);
        overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    protected void nextActivity(Class nextClass, boolean forResult, String key, boolean value, String type, String valueType) {
        Intent intent = new Intent(MainActivity.this, nextClass);
        intent.putExtra(key,value);
        if (type != null && valueType != null){
            intent.putExtra(type,valueType);
        }
        nextActivity(intent, forResult);
        overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    public void deleteGuestPersonAndResults(String name, String firstname) {
        extractor.open();
        extractor.deleteGuestPerson(name, firstname);
        extractor.close();
    }

}
