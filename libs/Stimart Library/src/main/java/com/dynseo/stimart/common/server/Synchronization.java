package com.dynseo.stimart.common.server;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

//import com.dynseo.dynseolibraryfamily.family.utils.HTTPRequest;
import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.SynchroInterface;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Viet on 22/04/2016.
 */
public abstract class Synchronization implements SynchroInterface {

    protected static String TAG = "Synchronization";
    final static String IS_SYNCHRO_DATA = "data";

    protected boolean internet;

    Extractor extractor;
    protected SharedPreferences sharedPrefs;
    protected Context context;
    String typeOfSynchro;
    String codeForEndOfSynchro;

    public Synchronization(Context context, SharedPreferences sharedPrefs, String type, String codeForEndOfSynchro) {
        this(context, sharedPrefs, type);
        this.codeForEndOfSynchro = codeForEndOfSynchro;
    }

    public Synchronization(Context context, SharedPreferences sharedPrefs, String type) {
        this.sharedPrefs = sharedPrefs;
        this.context = context;
        this.typeOfSynchro = type;
    }

    @Override
    public void doSynchro() {

    }

    @Override
    public boolean authorizeSynchro() {
        return AppManager.getAppManager().authorizeSynchro(typeOfSynchro);
    }

    @Override
    public String endOfSynchro() {

        String path =  ConnectionConstants.urlSynchroEnd( typeOfSynchro.equals(IS_SYNCHRO_DATA),
                (codeForEndOfSynchro!=null), codeForEndOfSynchro);
        Log.i(TAG, "pathEndOfSynchro : " + path);

        String serverResponse = Http.queryServer(path);
        if( serverResponse != null ) {
            Log.i(TAG,"INTERNET OK");
            Log.i(TAG,"responseEndOfSynchro : " + serverResponse);

            internet = true;

            try {
                JSONObject o = new JSONObject(serverResponse);
                ConnectionConstants.setSubscriptionDates(o, sharedPrefs, false);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Log.i(TAG,"NO INTERNET");
            internet = false;
        }
        return serverResponse;
    }

    @Override
    public void onSynchroSuccess(int step) {

    }

    @Override
    public void onSynchroFailure() {

    }

    @Override
    public void onError(int errorCode) {

    }

//    @Override
//    public void manageResultOfResourcesDownload(){
//
//    }
    public void sendNewPersons(Context context) {
        Log.d("TAG","sendNewPersons()");
        if(extractor == null) {
            extractor = new Extractor(context);
        }
        extractor.open();

        Person[] persons = extractor.getPersons(Extractor.COL_SERVER_ID + " is null");
        for (int i = 0; i < persons.length; i++) {

            Person personToAdd = persons[i];
            System.out.println("player to send : "+ personToAdd);

            try {
                String path = StimartConnectionConstants.urlAddPerson(personToAdd, true);
                Log.e("sendn",personToAdd.toString());
                Log.e("sendpath",path);

                String serverResponse = Http.queryServer(path);
                if(serverResponse != null) {
                    Log.e("sendresponse", serverResponse);
                    String answer = null;

                    //the response is :
                    //	case 1 - serverId alone
                    // 	case 2 - serverId with other info (we are in the case of a solved conflict)
                    //	case 3 - conflictId (either it is new, either the local record was already marked with same conflictId)
                    try {
                        JSONObject o = new JSONObject(serverResponse);
                        try {
                            //=== case 1 : there is a serverId
                            answer = o.getString(StimartConnectionConstants.JSON_PARAM_SERVER_ID);
                            Log.e("serverId", answer);

                            if (answer != null) {		//case 1 or 2
                                extractor.setServerId(personToAdd, answer);
                            }
                        }
                        catch (JSONException e) {
                            try {
                                //=== case 3 : there is a conflictId
                                answer = o.getString(StimartConnectionConstants.JSON_PARAM_CONFLICT_ID);

                                if(answer != null) {		//case 3
                                    extractor.setConflictId(personToAdd, answer);
                                    Log.i("conflictId",answer);
                                }
                            }
                            catch (JSONException e2) {
                                //No serverId, no conflictId but information to solve the conflict
                                //Has personToAdd a conflict ?
                                if(personToAdd.getConflictId() != null) {
                                    //We had a conflict, now we have a serverId.
                                    //We remove the conflictId
                                    Log.i("TAG",personToAdd.getConflictId());

                                    String response = o.getString("person");
                                    Log.i("response",response);
                                    JSONObject obj = new JSONObject(response);
                                    answer = obj.getString(StimartConnectionConstants.JSON_PARAM_SOLVED);

                                    Log.i("answer",answer);

                                    if (answer.equals("Merge")) {
                                        personToAdd.setRole(obj.optString(StimartConnectionConstants.JSON_PARAM_PERSON_ROLE));
                                        personToAdd.setName(obj.optString(StimartConnectionConstants.JSON_PARAM_NAME));
                                        personToAdd.setFirstname(obj.optString(StimartConnectionConstants.JSON_PARAM_FIRSTNAME));
                                        Date birthdate = Tools.parseDate(obj.optString(StimartConnectionConstants.JSON_PARAM_BIRTHDATE));
                                        personToAdd.setBirthdate(birthdate);
                                        personToAdd.setSex(obj.optString(StimartConnectionConstants.JSON_PARAM_SEX));
                                    }
                                    extractor.setServerId(personToAdd, obj.optString(StimartConnectionConstants.JSON_PARAM_SERVER_ID));
                                    Log.v(TAG,"sendNewPersons() - remove conflictId");
                                    extractor.removeConflictId(personToAdd);
                                }
                            }
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }		//end of if there is a server response
            }
            catch(Exception eUrl) {
                eUrl.printStackTrace();
            }
        }			//end of loop on players
        extractor.close();
        Log.e(TAG, "sendNewPersons finished");
    }

    public void getPersons() {
        Log.d(TAG, "getPersons()");

        String path = StimartConnectionConstants.urlSynchroPersons();

        Log.e(TAG, "getPath - " + path);
        String serverResponse = Http.queryServer(path);
        Log.e(TAG, "server response - " + serverResponse);
        if( serverResponse != null ) {

            Log.i(TAG,"INTERNET OK");
            internet = true;

            try {
                extractor.open();
                JSONArray jsonArray = new JSONArray(serverResponse);

                Log.e(TAG, "JSON array: "+jsonArray.toString());
                int len = jsonArray.length();
                Log.e(TAG, "JSON array length = "+String.valueOf(len));

                for (int i = 0; i < len; i++) {
                    JSONObject o = jsonArray.getJSONObject(i);
                    Person personToAdd = new Person(o);

                    String active = o.getString(StimartConnectionConstants.JSON_PARAM_ACTIVE);

                    //The person has been set to Inactive, we delete it locally
                    if (active.equals(StimartConnectionConstants.VAL_INACTIVE_ACCOUNT)) {
                        if (existPlayerWithServerId(personToAdd)) {
                            Log.v(TAG,"getPersons() - delete person inactif");
                            extractor.deletePerson(personToAdd);
                        }
                    }
                    //The person is still active
                    else if (active.equals(StimartConnectionConstants.VAL_ACTIVE_ACCOUNT)) {
                        //We test if locally we have no one with the same serverId and no one with same name + firstname
                        //If no one, we insert, if exists we update
                        if (!existPlayerWithServerId(personToAdd) && !existPlayerWithNameFirstname(personToAdd)) {
                            Log.v(TAG,"getPersons() - add person ok");
                            extractor.insertPerson(personToAdd);
                        }
                        else {
                            Log.v(TAG,"getPersons() - update person");
                            extractor.updatePerson(personToAdd);
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            finally {
                extractor.close();
            }
        }
        else {
            Log.i(TAG,"NO INTERNET");
            internet = false;
        }
    }

    public boolean existPlayerWithNameFirstname(Person person) {
        Log.i(TAG, "existPlayerWithNameFirstname()");
        Person nomExistant = extractor.getResidentWithNameFirstname(person.getName(), person.getFirstName());
        return (nomExistant != null);
    }

    public boolean existPlayerWithServerId(Person person) {
        Log.i(TAG, "existPlayerWithServerId()");
        Person serverIdExistant = extractor.getResidentWithServerId(person.getServerId());
        return (serverIdExistant != null);
    }

    @Override
    public void manageResultOfResourcesDownload() {

    }

    @Override
    public void nextStepSynchro() {

    }
}
