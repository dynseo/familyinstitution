package com.dynseo.stimart.common.models;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;
import org.json.JSONException;

import com.dynseo.stimart.R;
import com.dynseo.stimart.utils.StringFormatter;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.tools.Tools;


public class Person implements com.dynseo.person.model.Person {
	
	private String _role;
	private String _firstname;
	private String _name;
	private String _serverId;
	private String _conflictId;
	private String _sex;
	private Date _birthdate;
	
	private int _id;
	
	public static String sexm = "M";
	public static String sexf = "F";

	protected GamePersonInfo _gamePersonInfo;

	static public Person currentPerson;

	public Person() {
		//this(null, null);	//well everything is already null
	}
	
	public Person(JSONObject jsonPerson) throws JSONException {
		_serverId  = jsonPerson.optString(StimartConnectionConstants.JSON_PARAM_SERVER_ID, null);
		_role  	   = jsonPerson.optString(StimartConnectionConstants.JSON_PARAM_PERSON_ROLE, null);
		_firstname = jsonPerson.optString(StimartConnectionConstants.JSON_PARAM_FIRSTNAME, null);
		_name = jsonPerson.getString(StimartConnectionConstants.JSON_PARAM_NAME);
		_sex  = jsonPerson.optString(StimartConnectionConstants.JSON_PARAM_SEX, sexm);
	
		_birthdate = Tools.parseDate( jsonPerson.optString(StimartConnectionConstants.JSON_PARAM_BIRTHDATE, null) );
	}

	public Person(String firstname, String name) {
		// _conflitId = 0;
		_role = null;
		_firstname = firstname;
		_name = name;
		_serverId = null;
		_conflictId = null;
	}

	public Person(String firstname, String name, Date birthdate) {
		this(firstname, name);
		_birthdate = birthdate;
	}

	public Person(String firstname, String name, String date, String sex, String serverId){
		this(firstname, name, date);
		this._sex = sex;
		this._serverId = serverId;
	}

	public Person(String firstname, String name, String birthdate) {
		this(firstname, name);
		_birthdate = Tools.parseDate(birthdate);		
	}

	public Person(String firstname, String name, Date birthdate, String sex) {
		this(firstname, name, birthdate);
		_sex = sex;
	}

	public Person(String firstname, String name, Date birthdate, String sexe,
			String serveurId) {
		this(firstname, name, birthdate, sexe);
		_serverId = serveurId;
	}

	/* ========== all the getters ========== */
	public int getId() {
		return _id;
	}

	public String getFirstName() {
		return _firstname;
	}
	
	public String getName() {
		return _name;
	}

	public String getServerId() {
		return _serverId;
	}
	
	public String getRole() {
		return _role;
	}

	public String getSex() {
		return _sex;
	}

	public Date getBirthdate() {
		return _birthdate;
	}

	public String getBirthdateStr() {
		if( _birthdate != null ) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(AppManager.getAppManager().getDateFormat());
			return dateFormat.format(_birthdate);
		}
		return null;
	}

	public String getBirthdateStrFormatDatabase(Context context) {
		if( _birthdate != null ) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return dateFormat.format(_birthdate);
		}
		return null;
	}

	public int getAge() {
		if(_birthdate != null) {
			long diffAsLong = (new Date()).getTime() - _birthdate.getTime();
			Calendar diffAsCalendar = Calendar.getInstance();
			diffAsCalendar.setTimeInMillis(diffAsLong);
			return diffAsCalendar.get(Calendar.YEAR) - 1970; // base time where timestamp=0, precisely 1/1/1970 00:00:00
		}
		else return 100;
	}

	public boolean isAdult() {
		return getAge()>18;
	}

	public String getSexString(Context context) {
		if (_sex != null) {
			if (_sex.equals(sexm)) {
				return context.getResources().getString(R.string.sex_male);
			} else if (_sex.equals(sexf)) {
				return context.getResources().getString(R.string.sex_female);
			}
		}
			return null;

	}

	public String getConflictId() {
		return _conflictId;
	}

	public String getPseudo(Context context) {
		return StringFormatter.nameFormat(context.getString(R.string.local_name_format), _name, _firstname);
	}

	public GamePersonInfo getGamePersonInfo() {
		return _gamePersonInfo;
	}

	/* ========== all the setters ========== */
	public void setId(int id) {
		_id = id;
	}

	public void setRole(String role) {
		_role = role;
	}

	public void setFirstname(String firstname) {
		_firstname = firstname;
	}

	public void setName(String name) {
		_name = name;
	}

	public void setBirthdate(Date birthdate) {
		_birthdate = birthdate;
	}

	public void setBirthdate(String birthdate) {
		_birthdate = Tools.parseDate(birthdate);
	}

	public void setSex(String sex) {
		_sex = sex;
	}

	public void setServerId(String serverId) {
		_serverId = serverId;
	}

	public void setConflictId(String id) {
		_conflictId = id;
	}

	public void setGamePersonInfo(GamePersonInfo gamePersonInfo) {
		_gamePersonInfo = gamePersonInfo;
	}
	
	/* ========== Other miscellaneous methods ========== */
	public boolean isMale() {
		return( _sex != null && _sex.equals(sexm) );
	}
	public boolean isFemale() {
		return( _sex != null && _sex.equals(sexf) );
	}
	
	public boolean isAnimator() {
		//the versions previous to DB_VERSION == 7 used serverId==-1
		return( (_serverId != null && _serverId.equals("-1")) || (_role != null && _role.equals("A")) );
	}

	public String toString() {
		return getId() + ", "+ _firstname + ", " + _name + ", " + _birthdate + ", " + _sex + ", " + _serverId + ", " + _conflictId;
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject res = new JSONObject();

		res.put(StimartConnectionConstants.JSON_PARAM_SERVER_ID, _serverId);
		res.put(StimartConnectionConstants.JSON_PARAM_PERSON_ROLE, _role);
		res.put(StimartConnectionConstants.JSON_PARAM_FIRSTNAME, _firstname);
		res.put(StimartConnectionConstants.JSON_PARAM_NAME, _name);
		res.put(StimartConnectionConstants.JSON_PARAM_SEX, _sex);
		res.put(StimartConnectionConstants.JSON_PARAM_BIRTHDATE, Tools.dateTimeToString(_birthdate));

		return res;
	}
}
