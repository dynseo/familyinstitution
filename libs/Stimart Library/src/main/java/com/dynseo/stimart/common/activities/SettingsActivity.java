package com.dynseo.stimart.common.activities;

import java.util.Calendar;
import com.dynseo.stimart.R;
import com.dynseo.stimart.utils.NotificationBroadcastReceiver;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

public class SettingsActivity  extends Activity {
	private static final String TAG = "SettingsActivity";
	private static final int intervalNotification = 60*24;	// 60mn/h * 24h between notifications = One day.

	private Context context = this;
	private SharedPreferences preferences;	
	private Switch notifSwitch;
	private TimePicker timePicker;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_settings_layout);

		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		int hour = preferences.getInt("notification_hour", 10);
		int minute = preferences.getInt("notification_minute", 0);
		
		notifSwitch = (Switch)findViewById(R.id.settings_notif_switch);
		notifSwitch.setChecked(preferences.getBoolean("notification_isActivated", false));
		
		timePicker = (TimePicker)findViewById(R.id.settings_notif_hour);
		timePicker.setIs24HourView(true);
		timePicker.setCurrentHour(hour);
		timePicker.setCurrentMinute(minute);

		Button cancelButton = (Button)findViewById(R.id.settings_validation_button_exit);
		cancelButton.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
				finish();
			}
		});
		
		Button saveButton = (Button)findViewById(R.id.settings_validation_button_save);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				preferences.edit().putInt("notification_hour", timePicker.getCurrentHour()).commit();				
				preferences.edit().putInt("notification_minute", timePicker.getCurrentMinute()).commit();
				preferences.edit().putBoolean("notification_isActivated", notifSwitch.isChecked()).commit();
				
				
				stopAlert(context);
				startAlert(context);

				overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
				finish();
			}
		});
		
	}
	
	public static void stopAlert(Context context) {
		Intent intent = new Intent(context, NotificationBroadcastReceiver.class);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
		alarmManager.cancel(pendingIntent);
	}
	
	public static void startAlert(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		
		Boolean notif = preferences.getBoolean("notification_isActivated", false);
		Log.e(TAG,"notif = "+notif);
		if(!notif) {
			stopAlert(context);
			return;
		}
		
		
		Intent intent = new Intent(context, NotificationBroadcastReceiver.class);
		
		
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, intent, 0);
		
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
		
		

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, preferences.getInt("notification_hour", 10));
		calendar.set(Calendar.MINUTE, preferences.getInt("notification_minute", 30));

		
		alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
				1000 * 60 * intervalNotification, pendingIntent);
	}
	
	
}
