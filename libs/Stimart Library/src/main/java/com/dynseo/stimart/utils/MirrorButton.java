package com.dynseo.stimart.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

public class MirrorButton extends Button {

    public MirrorButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

	protected void onDraw(Canvas canvas)
	{ 
		canvas.rotate(180,getWidth(),getHeight());
        canvas.translate(getWidth(),getHeight());
        super.onDraw(canvas);
    }
}