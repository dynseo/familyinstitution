package com.dynseo.stimart.common.models;

import android.content.Context;
import android.os.Environment;

import com.dynseo.stimart.utils.Tools;
import com.dynseolibrary.platform.AppManager;

import java.io.File;

/**
 * Created by HOUSSEM on 30/11/2015.
 */
public class AppResourcesManager {

    // this block statis for delete the previous images (colormind, puzzle and quizzle)
    static {

    }

    private static final String TAG = "AppResourcesManager";

    private static AppResourcesManager appResourcesManager;		//this is a singleton

    private String pathImages, pathAudios, pathMap, pathXml;
    private String pathImagesPrevious;       //For previous manner
    private String pathImagesFamily;
    private String pathResources;
    private String pathResourcesPrevious;
    private String pathXmlPrevious;

    private Context context;

    public AppResourcesManager(Context context){
        /* define where we save resources files */
        this.context = context;

        String dir = null;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            dir = context.getExternalFilesDir(null).getPath();
        }
		/* when there's no SD card */
        else {
            dir =  "/data/data/" + AppManager.getAppManager().getPackageName();
        }

        pathImagesPrevious = dir + "/res/images/";
        pathAudios = dir + "/res/audios/";
        pathMap = dir + "/res/osmdroid/";

        pathImages = dir + "/resources/images/";
        pathXml = dir + "/resources/xml/";
        pathImagesFamily = dir + "/";
        pathResources = dir + "/resources";
        pathResourcesPrevious = dir + "/res";
        pathXmlPrevious = dir + "/res/xml/";
    }

    public static AppResourcesManager getAppResourcesManager(Context context) {	//This one for initialization

        if( appResourcesManager == null ) {
            appResourcesManager = new AppResourcesManager(context);

            Tools.deleteFiles(Tools.getFilesByNames(appResourcesManager.getPathImagesPrevious(), new String[]{"colormind", "quizzle", "puzzle"}));
            Tools.deleteFiles(Tools.getFilesByNames(appResourcesManager.getPathXmlPrevious(), new String[] {"quizzle", "puzzle"}));
            new File(appResourcesManager.getPathXmlPrevious()).delete();
        }
        return appResourcesManager;
    }
    public static AppResourcesManager getAppResourcesManager() {					//This one, to be used once initialized
        return appResourcesManager;
    }

    /* ===== All the getter methods ==== */
    public String getPathImages() {
        return pathImages;
    }

    public String getPathImagesPrevious() {
        return pathImagesPrevious;
    }

    public String getPathAudios() {
        return pathAudios;
    }

    public String getPathMap() {
        return pathMap;
    }

    public String getPathXml() {
        return pathXml;
    }

    public String getPathImagesFamily() {
        return pathImagesFamily;
    }

    public String getPathResources() {
        return pathResources;
    }

    public String getPathResourcesPrevious() {
        return pathResourcesPrevious;
    }

    public String getPathXmlPrevious() {
        return pathXmlPrevious;
    }

}
