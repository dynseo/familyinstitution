/**
HOUSSEM
*/
package com.dynseo.stimart.common.models;

public class GamePersonInfo {

	private int id;
	private String info1, info2;
	
	public GamePersonInfo(int anId, String anInfo1, String anInfo2){
		id = anId;
		info1 = anInfo1;
		info2 = anInfo2;
	}

	public int getId() {
		return id;
	}
	
	public String getInfo1() {
		return info1;
	}

	public void setInfo1(String info1) {
		this.info1 = info1;
	}

	public String getInfo2() {
		return info2;
	}

	public void setInfo2(String info2) {
		this.info2 = info2;
	}
	
	
}
