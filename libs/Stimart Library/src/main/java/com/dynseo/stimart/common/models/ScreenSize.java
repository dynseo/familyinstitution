package com.dynseo.stimart.common.models;

public class ScreenSize {
	/* device screen size */
	public static int screenWidth = 0;
	public static int screenHeight = 0;
	
	/* a device screen size quote, ex: if device screen width = 400 p,
	 * screenSizeW = 400/800 = 0.5 */
	public static float screenSizeW = 0;
	public static float screenSizeH = 0;
	
	/* device screen size reference, compare with a default device with 1280*800 pixels */
	public static final int widthReference = 1280;
	public static final int heightReference = 800;
	public static final int[] buttonSize = {200,60};
	public static final int[] moodDialogSize = {1100,700};
	public static final int[] moodButtonSize = {256, 256};
	public static final int[] imageButtonSize = {150,150};
	public static final int[] instructionSize = {700,100};
	public static final int[] splashTitleSize = {1280,300};
	public static final int   moodTitleTextSize = 70;
	
	/* Values of marginTop+ of instructions which position is different by game
	 * Example : instruction1 marginTop = 100
	 * 			 instruction2 marginTop += 50
	 * 			 instruction3 marginTop += 200
	 * instructionMarginTop = { 100, 50, 200 } */
	public static final int[] instructionMarginTop1 = { 190, 250, 100};
	public static final int[] instructionMarginTop2 = { 140, 180, 160};
	/* button size for ChooseMusicGameActivity */
	public static final int[] buttonSize2 = {300,80};
	
	/* Values of margin for buttons in the levelActivity */
	public static final int[][] levelButtonsMarginModels = {{540,400}, {540,450}, {830,230}};
	
	/* Values of marginTop  of Game Title in the levelActivity */
	public static final int[][] levelTitleMarginModels = {{60,500}, {200,500}, {60,760}};
	public static final int[] titleSize = {1280,200};
	
	/* Values of marginTop  of Game Title in the SplashActivity {MarginLeft, MarginTop} */
	public static final int[][] splashTitleMarginModels = {{450,450}, {450,650}, {450,250}};
}