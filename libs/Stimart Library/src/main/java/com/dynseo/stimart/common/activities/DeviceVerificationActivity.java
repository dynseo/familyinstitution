package com.dynseo.stimart.common.activities;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.stimart.R;

import com.dynseo.stimart.utils.SubscribeAndAddAccount;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.CheckOrSetDeviceInterface;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.CheckOrSetDeviceTask;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.tools.Tools;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

/**
 * Class for device identification
 * Modified By Viet and Dominique
 * @date 14/03/2016
 *
 */
public class DeviceVerificationActivity extends Activity implements CheckOrSetDeviceInterface{

	private static final String TAG = "DeviceVerification";
    private static final int STEP_IDENTIFICATION = 1;
    private static final int STEP_ENTER_CODE = 2;
    private static int CURRENT_STEP = 0;

	SharedPreferences sharedPrefs;
	Context context = this;
	Dialog dialog;

	Button button;
	TextView text;

	/* listener for button to identify device on server*/
	OnClickListener backToMenu;
	boolean afterProcessing;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");

		afterProcessing = false;
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		initializeDialog();
	}

	public void initializeDialog() {
		dialog = new Dialog(this, R.style.DialogFullscreen);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(R.layout.dialog_device_identification_layout);

		button = (Button) dialog.findViewById(R.id.identifier);
		text = (TextView) dialog.findViewById(R.id.verification_textview);
		setListeners();

		dialog.show();
	}

	protected void setListeners() {

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				Log.d(TAG, "onCancel() - dialog");
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				MainActivity.currentStep = MainActivity.STEP_START;
				finish();
				if (MainActivity.context != null) {
					MainActivity.context.finish();
				}
			}
		});

		/*
		backToMenu = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i(TAG, "onClick() - backToMenu");
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				MenuAppliActivity.currentStep = MenuAppliActivity.STEP_START;
				finish();
				if (MenuAppliActivity.context != null) {
					MenuAppliActivity.context.finish();
				}
			}
		};*/

		OnClickListener identificationListener = new OnClickListener(){
			public void onClick(View v) {
				Log.i(TAG, "onClick() - identificationListener");
				if (Http.isOnline(context)) {
					afterProcessing = true;
					CheckOrSetDeviceTask verifyTask =
							new CheckOrSetDeviceTask(DeviceVerificationActivity.this, sharedPrefs);
					verifyTask.execute();

					text.setText(R.string.identificationencours);
					button.setEnabled(false);
					button.setBackgroundResource(R.drawable.round_button_off);
					//button.setOnClickListener(backToMenu);
				} else {
					Tools.showToast(getApplicationContext(), getString(R.string.erreurconnexioninternet));
				}
			}
		};
		button.setOnClickListener(identificationListener);
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy()");

		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
			dialog = null;
		}
		super.onDestroy();
	}

	// Called when finishing activity or when home button pressed
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onPause()");
		super.onPause();

		if (MainActivity.context != null && !afterProcessing)
			MainActivity.context.finish();
		finish();
	}

	public void finishWithSuccessResult()
	{
	   Intent intent = new Intent();
	   setResult(RESULT_OK, intent);
	   finish();
	}
	public void finishWithFailureResult()
	{
		Intent intent = new Intent();
		setResult(MainActivity.RESULT_KO, intent);
		finish();
	}

    @Override
    public void onDeviceVerificationFailure(JSONObject jsonResult) {
		if ((dialog != null) && dialog.isShowing())
			dialog.dismiss();

        Log.d(TAG, "onDeviceVerificationFailure");

        try {
            ConnectionConstants.setSubscriptionDates(jsonResult, sharedPrefs, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finishWithFailureResult();
    }

    @Override
    public void onDeviceVerificationSuccess(JSONObject jsonResult, boolean showToast) {
		if ((dialog != null) && dialog.isShowing())
			dialog.dismiss();


        Log.d(TAG, "onDeviceVerificationSuccess");
        try {
            ConnectionConstants.setSubscriptionDates(jsonResult, sharedPrefs, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String serialNumberGenerated = jsonResult.optString("serialNumberGenerated");
        if (!serialNumberGenerated.equals("")) {
            sharedPrefs.edit().putString("serialNumberGenerated", serialNumberGenerated).commit();
            AppManager.getAppManager().setSerialNumberGenerated(serialNumberGenerated);
        }

        //Skip if visit mode
        sharedPrefs.edit()
                .putString(ConnectionConstants.SHARED_PREFS_FIRST_LAUNCH, Tools.dateTimeToString(new Date()))
                .commit();

        if (showToast) {
            String institutionType = sharedPrefs.getString(ConnectionConstants.SHARED_PREFS_INSTITUTION, null);
            if (institutionType == null)
                Tools.showToast(getApplicationContext(), getApplicationContext().getString(R.string.identificationreussie));
            else
                Tools.showToast(getApplicationContext(),
                        getApplicationContext().getString(R.string.identificationreussie_institution) + " " + institutionType);
        }
        finishWithSuccessResult();
    }

}
