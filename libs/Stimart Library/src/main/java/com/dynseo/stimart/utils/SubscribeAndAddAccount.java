package com.dynseo.stimart.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;

import com.dynseo.stimart.R;

import com.dynseo.stimart.common.activities.MainActivity;
import com.dynseolibrary.account.ConsumeCodeFragment;
import com.dynseolibrary.account.RetrieveCodeFragment;
import com.dynseolibrary.platform.ConsumeCodeInterface;
import com.dynseolibrary.platform.server.ConsumeCodeTask;
import com.dynseolibrary.platform.server.RenewCodeTask;
import com.dynseolibrary.platform.server.RetrieveCodeTask;
import com.dynseolibrary.tools.Tools;
import com.dynseolibrary.account.Account;
import com.dynseolibrary.account.AttachInstitutionFragment;
import com.dynseolibrary.account.ConnectFragment;
import com.dynseolibrary.account.GenericFormFragment;
import com.dynseolibrary.account.InstitutionOrIndividualFragment;
import com.dynseolibrary.account.SignUpIndividualFragment;
import com.dynseolibrary.account.SignUpOrConnectFragment;
import com.dynseolibrary.account.SubscriptionFragment;
import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;
import com.dynseolibrary.platform.SubscriptionInterface;
import com.dynseolibrary.platform.server.AttachToInstitutionTask;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.CreateAccountTask;
import com.dynseolibrary.platform.server.LogInTask;
import com.dynseolibrary.platform.server.SubscriptionTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bi on 12/02/2016.
 */

//Manages dialogs :
//  - dialogConnectOrSignUp which can trigger:
//          - dialogInstitutionOrIndividual -> dialogSignUpIndividual or dialogAttachInstitution
//          - dialogConnect
//          - or doVisit (no dialog)
//  - dialogSubscribe
public class SubscribeAndAddAccount implements SubscriptionInterface, SignUpOrConnectInterface, ErrorCodeInterface, ConsumeCodeInterface {

    private static SubscribeAndAddAccount subscribeAndAddAccount = null;

    public SignUpOrConnectFragment dialogConnectOrSignUp;
    public SubscriptionFragment dialogSubscribe;
    protected boolean closeAppOnSubscribe;
    protected boolean gotoSubscribe;    //To trigger subscribe after a create account
    //Its allows us to differentiate 2 cases:
    //a) we create account from dialogConnectOrSignUp
    //b) we create account from dialogSubscribe

    SignUpIndividualFragment dialogSignUpIndividual;
    InstitutionOrIndividualFragment dialogInstitutionOrIndividual;
    AttachInstitutionFragment dialogAttachInstitution;
    //public SignUpIndividualFragment signUpIndividualFragment;

    ConsumeCodeFragment consumeCodeFragment;
    RetrieveCodeFragment retrieveCodeFragment;

    public GenericFormFragment currentFragment;
    ConnectFragment dialogConnect;

    public Typeface theAppStyle = null;
    public SharedPreferences sp;

    public Account account;
    protected static Context mContext;

    private SubscribeAndAddAccount(Context context) {
        mContext = context;
        sp = PreferenceManager.getDefaultSharedPreferences(context);

        closeAppOnSubscribe = true;
    }

    public static SubscribeAndAddAccount getInstance(Context context) {
        if (subscribeAndAddAccount == null) {
            subscribeAndAddAccount = new SubscribeAndAddAccount(context);
        }
        else {
            subscribeAndAddAccount.mContext = context;
            subscribeAndAddAccount.sp = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return subscribeAndAddAccount;
    }

    public SignUpOrConnectFragment getDialogConnectOrSignUp(boolean accountIsMandatory) {
        if( dialogConnectOrSignUp == null )
            dialogConnectOrSignUp = new SignUpOrConnectFragment(this, this.theAppStyle, accountIsMandatory);
        return dialogConnectOrSignUp;
    }

    public SignUpIndividualFragment getDialogSignUpIndividual(boolean accountIsMandatory) {
        if (dialogSignUpIndividual == null) {
            dialogSignUpIndividual = new SignUpIndividualFragment(this, this.theAppStyle, accountIsMandatory);
        }
        return dialogSignUpIndividual;
    }

    public void showDialogSubscribe(String textTitle, String textMessage, String textButton, boolean closeApp) {

        closeAppOnSubscribe = closeApp;

        if (dialogSubscribe == null)
            dialogSubscribe = new SubscriptionFragment(this);
        if (dialogSubscribe.isAdded()) {
            return;
        } else {
            dialogSubscribe.setTexts(textTitle, textMessage, textButton);
            dialogSubscribe.show(((Activity) mContext).getFragmentManager(), "subscribe");
        }
    }

    public void showDialogSignUpIndividual() {
        if (dialogSignUpIndividual == null)
            dialogSignUpIndividual = new SignUpIndividualFragment(this, this.theAppStyle, true);
        currentFragment = dialogSignUpIndividual;
        currentFragment.show(((Activity) mContext).getFragmentManager(), "Connexion");
    }

    public void showConsumeCodeFragment() {
        if (consumeCodeFragment == null)
            consumeCodeFragment = new ConsumeCodeFragment(this, this.theAppStyle, true);
        currentFragment = consumeCodeFragment;
        if (currentFragment.isAdded()) {
            return;
        }
        else
            currentFragment.show(((Activity) mContext).getFragmentManager(), "ConsumeCode");

    }

    @Override
    public void doConnect() {
        //dialogConnectOrSignUp.dismiss();
        if( dialogConnect == null ) {
            dialogConnect = new ConnectFragment(this, theAppStyle, true);
        }
        currentFragment = dialogConnect;
        currentFragment.show(((Activity) mContext).getFragmentManager(), "Connexion");
    }

    @Override
    public void doSignUp(boolean isForInstitution) {
        if (dialogInstitutionOrIndividual != null)
            dialogInstitutionOrIndividual.dismiss();

        if (!isForInstitution){
            if( dialogSignUpIndividual == null )
                dialogSignUpIndividual = new SignUpIndividualFragment(this, theAppStyle, true);
            currentFragment = dialogSignUpIndividual;
        } else {
            if( dialogAttachInstitution == null )
                dialogAttachInstitution = new AttachInstitutionFragment(this, theAppStyle, true);
            currentFragment = dialogAttachInstitution;
        }
        if (currentFragment.isAdded()) {
            return;
        }
        else
            currentFragment.show(((Activity) mContext).getFragmentManager(), "Connexion");
    }

    @Override
    public void doVisit() {

        Log.d("Visit Mode", "Visit Mode");

        ((MainActivity)mContext).currentStep = MainActivity.STEP_VISIT;
        ((MainActivity)mContext).manageDialogs();
        dialogConnectOrSignUp.dismiss();
    }

    @Override
    public void doSelectTypeForSignUp(boolean gotoSubscribe) {

        this.gotoSubscribe = gotoSubscribe;
        if (Tools.isResourceTrue(mContext,R.string.only_individual)) {
            doSignUp(false);
        }
        else if (Tools.isResourceTrue(mContext,R.string.only_institution)) {
            doSignUp(true);
        }
        else {
            if (dialogInstitutionOrIndividual == null)
                dialogInstitutionOrIndividual = new InstitutionOrIndividualFragment(this, theAppStyle, true);
            dialogInstitutionOrIndividual.show(((Activity) mContext).getFragmentManager(), "connexion");
        }
    }

    @Override
    public void createAccount(Account anAccount) {
        account = anAccount;
        Log.d("createAccount","Account :" + account );
        CreateAccountTask asyncTask = new CreateAccountTask(this, anAccount, sp); //pseudo, email, password
        asyncTask.execute();
    }

    @Override
    public void logIn(String pseudo, String password) {
        LogInTask asyncTask = new LogInTask(this, pseudo, password);
        asyncTask.execute();
    }


    @Override
    public void attachToInstitution(String email, String institutionName, String institutionCity, String message) {
        AttachToInstitutionTask asyncTask = new AttachToInstitutionTask(this, email, institutionName, institutionCity, message);
        asyncTask.execute();
    }

    @Override
    public void onCreateAccountSuccess(int step, JSONObject jsonResult) {
        //if (mContext instanceof LevelActivity || mContext instanceof MenuAppliActivity || mContext instanceof SelectCategoryActivity) {
        if (step == 0){
            try {
                ConnectionConstants.setSubscriptionDates(jsonResult, sp, true);

                Log.d("before saveInSP ","Account :" + account );
                account.saveInSP(sp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            getDialogSignUpIndividual(true).onSuccess();
        }
        else {       //Step == 1, when click on dialog telling account created with success
            getDialogSignUpIndividual(true).dismiss();
            if (dialogConnectOrSignUp != null)
                dialogConnectOrSignUp.dismiss();
            if (gotoSubscribe) {
                ((MainActivity)(mContext)).deleteGuestPersonAndResults(mContext.getString(R.string.app_name), mContext.getString(R.string.guest));
                ((MainActivity)mContext).clearPerson();
                onSubscriptionAccepted(1, null);
            } else {
                if (mContext instanceof MainActivity) {    //Should always be true
                    goBackToMenuAppliActivity(MainActivity.STEP_CREATE_ACCOUNT_SUCCESS);
                }
            }
        }
    }

    @Override
    public void onAttachInstitutionSuccess(int step, JSONObject jsonResult) {
        if (step == 0){
            dialogAttachInstitution.onSuccess();
        }else{
            currentFragment.dismiss();
            // user is informed by email to use connectBtn instead of createAccountBtn
        }
    }

    @Override
    public void onLoginSuccess(JSONObject jsonResult) {
        if (dialogInstitutionOrIndividual != null)
            dialogInstitutionOrIndividual.dismiss();
        if (dialogConnectOrSignUp != null)
            dialogConnectOrSignUp.dismiss();
        Log.d("onLoginSuccess", "onLoginSuccess" + jsonResult.toString());
        try {
            ConnectionConstants.setSubscriptionDates(jsonResult, sp, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialogConnect.onSuccess();
        goBackToMenuAppliActivity(MainActivity.STEP_LOGIN_SUCCESS);

    }

    private void goBackToMenuAppliActivity(int withStep) {
        ((MainActivity)(mContext)).deleteGuestPersonAndResults(mContext.getString(R.string.app_name), mContext.getString(R.string.guest));
        ((MainActivity)mContext).clearPerson();
        ((MainActivity)mContext).currentStep = withStep;
        ((MainActivity)mContext).manageDialogs();
    }
    @Override
    public void onReturn() {
        MainActivity.currentStep = MainActivity.STEP_START;
    }

    @Override
    public void onSubscriptionAccepted(int step, JSONObject jsonResult) {
        try {
            String email = Account.getEmailFromSP(sp);

            if (step == 0) {
                if (email != null && !email.isEmpty()) {
                    SubscriptionTask asyncTask = new SubscriptionTask(this, email);
                    asyncTask.execute();
                }
                else{
                    doSelectTypeForSignUp(true);
                }
            }
            // after service do subscription
            else if (step == 1) {
                if (dialogSubscribe.isAdded()) {
                    dialogSubscribe.onSuccessOrFailure(
                        String.format(mContext.getString(R.string.subscription_success), email));
                }
            }
            // when click on button "Renouveller"
            else if (step == 2) {
                Account account = Account.getAccountFromSP(sp);
                RenewCodeTask renewCodeTask = new RenewCodeTask(this,account.getCode());
                renewCodeTask.execute();
            }
            // after service renew in BPCE
            else if (step == 3) {
                try {
                    ConnectionConstants.setSubscriptionDates(jsonResult, sp, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (dialogSubscribe.isAdded()) {
                    dialogSubscribe.onSuccessOrFailure(
                            String.format(mContext.getString(R.string.subscription_success),Tools.formattedAsDate(sp.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, ""))));

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSubscriptionIgnored() {
// TODO Auto-generated method stub
        dialogSubscribe.dismiss();
        if( closeAppOnSubscribe )
            ((Activity)mContext).finish();
    }

    public void consumeCode(String code, String password) { //
        ConsumeCodeTask consumeCodeTask;
        if (!password.equals("")){
            consumeCodeTask = new ConsumeCodeTask(this, code, password);
        } else {
            consumeCodeTask = new ConsumeCodeTask(this, code);
        }
        consumeCodeTask.execute();
    }

    //This method is used by BPCE and EA accordingly to the form of the JSON Response
    @Override
    public void onConsumeCodeSuccess(JSONObject jsonResult) {
        Log.d("onConsumeCodeSuccess", "Good Code BPCE");

        if (consumeCodeFragment != null)
            consumeCodeFragment.dismiss();
        try {
            //Set the right dates from the received JSON (just for BPCE), then create account, launch synchro
            ConnectionConstants.setSubscriptionDates(jsonResult, sp, true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        goBackToMenuAppliActivity(MainActivity.STEP_LOGIN_SUCCESS);

    }

    @Override
    public void doRetrieveCode() {
        if (retrieveCodeFragment == null)
            retrieveCodeFragment = new RetrieveCodeFragment(this, this.theAppStyle, true);
        currentFragment = retrieveCodeFragment;
        if (currentFragment.isAdded()) {
            return;
        }
        else
            currentFragment.show(((Activity) mContext).getFragmentManager(), "RetrieveCode");

    }

    @Override
    public void retrieveCode(String email, String numClient) {
        RetrieveCodeTask retrieveCodeTask = new RetrieveCodeTask(this, email, numClient);
        retrieveCodeTask.execute();
        Tools.showToast(mContext, "Votre demande a bien été prise en compte.");
        if (consumeCodeFragment != null) {
            currentFragment = consumeCodeFragment;
        }
    }

    @Override
    public void onError(int errorCode) {
        //TODO Auto-generated method stub
        switch (errorCode) {

            // --- Generic errors
            case ERROR_END_OF_SUBSCRIPTION:
                dialogSubscribe.onTerminal(mContext.getString(R.string.subcription_terminal));
                break;
            case ERROR_UNDEFINED:
                if (dialogSubscribe != null) {
                    dialogSubscribe.onSuccessOrFailure(mContext.getString(R.string.error_undefined)
                            + "\n\n" + mContext.getString(R.string.support_mail_dynseo));
                    break;
                }
                else if (currentFragment != null) {
                    currentFragment.showFormErrorMsg(mContext.getString(R.string.error_undefined));
                    break;
                }

            case ERROR_INVALID_USER:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_invalid_user));
                break;

            //--- Errors on the forms
            case ERROR_FIELD_REQUIRED:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_field_required));
                break;

            case ERROR_NOT_IDENTICAL_PASSWORD:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_not_identical_password));
                break;

            case ERROR_EMAIL_FORMAT:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_email_format));
                break;

            case ERROR_ACCEPT_CGU:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_accept_cgu));
                break;

            case ERROR_PASSWORD_WEEK:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_password_week));
                break;

            //--- Errors from the server
            case ERROR_ACCOUNT_CREATION_FAILED:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_account_creation_failed));
                break;

            case ERROR_DISTANT_SERVER_DOWN:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_server_down));
                break;

            case ERROR_DUPLICATE_EMAIL:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_duplicate_email));
                break;

            case ERROR_DUPLICATE_PSEUDO:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_duplicate_pseudo));
                break;

            case ERROR_INVALID_CODE:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_invalid_code));
                break;

            case ERROR_ALREADY_USED_CODE:
                //currentFragment.showFormErrorMsg(mContext.getString(R.string.error_already_used_code));
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_already_used_code));
                break;

            case ERROR_ALREADY_USED_CODE_BY_ME:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_already_used_code_by_me));
                break;

            case ERROR_USER_NOT_FOUND:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_user_not_found));
                break;

            case ERROR_WRONG_PASSWORD:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_wrong_password));
                break;

            case ERROR_NOT_STIMART_ACCOUNT:
                currentFragment.showFormErrorMsg(mContext.getString(R.string.error_not_stimart_account));
                break;
        }
    }
}
