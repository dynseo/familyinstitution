/**
HOUSSEM
*/
package com.dynseolibrary.tools;

import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.example.dynseolibrary.R;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class AppRater {
    private final static String APP_TITLE = AppManager.getAppManager().getAppName();// App Name

    private final static int DAYS_UNTIL_PROMPT = 3;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

    public static void app_launched(Context mContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        if (prefs.getBoolean(ConnectionConstants.SHARED_PREFS_DONT_SHOW_AGAIN, false)) { return ; }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong(ConnectionConstants.SHARED_PREFS_LAUNCH_COUNT, 0) + 1;
        editor.putLong(ConnectionConstants.SHARED_PREFS_LAUNCH_COUNT, launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong(ConnectionConstants.SHARED_PREFS_DATE_FIRST_LAUNCH, 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong(ConnectionConstants.SHARED_PREFS_DATE_FIRST_LAUNCH, date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch + 
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(mContext, editor);
            }
        }

        editor.commit();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext, R.style.DialogFullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_app_rater);

        TextView tv = (TextView) dialog.findViewById(R.id.dialog_rating_title);
        tv.setText(mContext.getString(R.string.if_enjoy_using) + " " + APP_TITLE + ", " + mContext.getString(R.string.please_rate));

        Button rateBtn = (Button) dialog.findViewById(R.id.rate_app_btn);
        rateBtn.setText(mContext.getString(R.string.rate) + " " + APP_TITLE);
        rateBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName())));
                if (editor != null) {
                    editor.putBoolean(ConnectionConstants.SHARED_PREFS_DONT_SHOW_AGAIN, true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });        

        Button laterBtn = (Button) dialog.findViewById(R.id.later_btn);
        laterBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button noBtn = (Button) dialog.findViewById(R.id.no_btn);
        noBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean(ConnectionConstants.SHARED_PREFS_DONT_SHOW_AGAIN, true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
     
        dialog.show();        
    }
}