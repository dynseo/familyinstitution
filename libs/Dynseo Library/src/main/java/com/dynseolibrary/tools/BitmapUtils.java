package com.dynseolibrary.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

/**
 * solution for OOM ------> compress bitmap
 */
public class BitmapUtils {
    private static final String TAG = "BitmapUtils";

    /**
     * compress an image refer to the goal dimension
     * @param res
     * @param resId
     * @param reqWidth
     * @param reqHeight
     * @return Bitmap
     */
    public static Bitmap createBitmapFromResource(Resources res,int resId,int reqWidth,int reqHeight) {

        return createBitmap(res, resId, null, null, null, null, reqWidth, reqHeight);
    }

    public static Bitmap createBitmapFromFile(String filePath,int reqWidth,int reqHeight) {

        return createBitmap(null, 0, filePath, null, null, null, reqWidth, reqHeight);
    }

    /**
     * compress an image refer to the goal dimension
     *
     * @param data image byte array
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static Bitmap createBitmapFromByteArray(byte[] data,int reqWidth,int reqHeight) {

        return createBitmap(null, 0, null, data, null, null, reqWidth, reqHeight);
    }

    /**
     * calulate the compress rate
     */
    private static int calculateInSampleSize(BitmapFactory.Options opts,int reqHeight,int reqWidth) {
        Log.i(TAG, "calculateInSampleSize");
        if(opts == null) {
            return -1;
        }
        int width = opts.outWidth;
        int height = opts.outHeight;

        int sampleSize = 1;

        if(width > reqWidth || height > reqHeight) {
            int heightRatio = (int) (height/(float)reqHeight);
            int widthRatio = (int) (width/(float)reqWidth);
            sampleSize = (heightRatio > widthRatio) ? widthRatio : heightRatio;
        }
        return sampleSize;
    }



    private static Bitmap createBitmap(Resources res, int resId, String filePath, byte[] data ,String assetFileName, View view , int reqWidth, int reqHeight) {
        Log.i(TAG, "decodeBitmap");
        //First create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        //Then call the decodeBitmapWithType function in order to set the right values of opts
        createBitmapWithType(res, resId, filePath, data, assetFileName, view, opts);

        //Then calculate the sample size from the opts(dimensions of the bitmap) and the view dimensions
        int sampleSize = calculateInSampleSize(opts, reqHeight, reqWidth);
        useOptimizedOptionParams(opts, sampleSize);


        //Finally create the bitmap with the opts (since the options have now been set)
        return createBitmapWithType(res, resId, filePath, data, assetFileName, view, opts);
    }

    private static Bitmap createBitmapWithType(Resources res, int resId, String filePath, byte[] data, String assetFileName, View view , BitmapFactory.Options opts) {

        if (res != null)
            return BitmapFactory.decodeResource(res, resId, opts);
        else if (filePath != null)
            return BitmapFactory.decodeFile(filePath, opts);
        else if (data != null)
            return BitmapFactory.decodeByteArray(data, 0, data.length, opts);
        else if (assetFileName != null && view != null) {
            try {
                InputStream inS = ((Activity)view.getContext()).getAssets().open(assetFileName);
                return BitmapFactory.decodeStream(inS, null, opts);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /*
     * compress an image refer to the dimension of a View
     * @param res
     * @param resId
     * @param reqWidth
     * @param reqHeight
     * @return Bitmap
     */
    public static Bitmap loadToViewFromResource(Resources res, int resId, View view) {
        return loadToView(res, resId, null, null, view);
    }

    public static Bitmap loadToViewFromFile(String filePath, View view) {
        return loadToView(null, 0, filePath, null, view);
    }

    public static Bitmap loadToViewFromAssets(String assetFileName, View view) {
        return loadToView(null, 0, null, assetFileName, view);
        // InputStream inputStream = context.getAssets().open(fileName);
    }

    private static Bitmap loadToView(Resources res, int resId, String filePath, String assetFileName, View view) {
        Log.i(TAG, "loadToViewFromFile");
        Bitmap bitmap = null;
        int reqHeight = 0;
        int reqWidth = 0;
        if(view != null) {
            InputStream inS = null;
            if (filePath != null) {
                Log.d(TAG,"file Path : " + filePath);
                File file = new File(filePath);
                if (!file.exists()) {
                    Log.e(TAG, "File don't exist");
                    return null;
                }
            }
            else if (assetFileName != null) {
                try {
                    System.out.println("------------------ 3) ImageLoader : " + assetFileName);
                    inS = ((Activity)view.getContext()).getAssets().open(assetFileName);
                }
                catch(Exception e) {
                    Log.e(TAG, "asset File don't exist");
                    return null;
                }
            }
            else if(resId == 0) {
                Log.i(TAG,"ressource don't exist");
                return null;
            }

            if (view.getLayoutParams() != null) {
                reqHeight = view.getLayoutParams().height;
                reqWidth = view.getLayoutParams().width;
                Log.e(TAG,"test[reqWidth:"+reqWidth+",reqHeight:"+reqHeight+"]");
                if (reqHeight == -1 || reqWidth == -1) {
                    Log.e(TAG, "unknown view size, use screen size");
                    DisplayMetrics dm = new DisplayMetrics();
                    ((Activity)view.getContext()).getWindowManager().getDefaultDisplay().getMetrics(dm);
                    reqWidth = dm.widthPixels;
                    reqHeight = dm.heightPixels;
                }

            }
            bitmap = createBitmap(res, resId, filePath, null ,assetFileName, view , reqWidth, reqHeight);

            //Here we either have a filePath or a resId
           /* BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            //There was a mistake there, since the bitmap was not created the opts values were not set
            //The bug was corrected by creating a bitmap using decodeFile that sets the opts
            BitmapFactory.decodeFile(filePath, opts);
            int sampleSize = 1;
            if (view.getLayoutParams() != null) {
                int reqHeight = view.getLayoutParams().height;
                int reqWidth = view.getLayoutParams().width;
                Log.e(TAG,"test[reqWidth:"+reqWidth+",reqHeight:"+reqHeight+"]");
                if (reqHeight == -1 || reqWidth == -1) {
                    Log.e(TAG, "unknown view size, use screen size");
                    DisplayMetrics dm = new DisplayMetrics();
                    ((Activity)view.getContext()).getWindowManager().getDefaultDisplay().getMetrics(dm);
                    reqWidth = dm.widthPixels;
                    reqHeight = dm.heightPixels;
                }
                sampleSize = calculateInSampleSize(opts, reqHeight, reqWidth);
            }
            Log.i(TAG,"before[width:"+opts.outWidth+",height:"+opts.outHeight+"]");
            Log.i(TAG,"insamplesize="+sampleSize);
            useOptimizedOptionParams(opts, sampleSize);

            Bitmap bitmap = null;
            if (filePath != null) {
                bitmap = BitmapFactory.decodeFile(filePath, opts);
            }

            else if (assetFileName != null)
                bitmap = BitmapFactory.decodeStream(inS, null, opts);
            else
                bitmap = BitmapFactory.decodeResource(res, resId, opts);*/

            if (bitmap != null) {
                Log.i(TAG,"after[width:"+bitmap.getWidth()+",height:"+bitmap.getHeight()+"]");
            }
            else {
                Log.i(TAG,"load image failed");
            }
            return bitmap;
        }
        return null;
    }



    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        if (bitmap != null){
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                    .getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;}
        return null;
    }

    public static void useOptimizedOptionParams(BitmapFactory.Options opts, int sampleSize) {
        opts.inJustDecodeBounds = false;
        opts.inSampleSize = sampleSize;
       /* newly added */
        opts.inPreferredConfig = Config.RGB_565;
        opts.inPurgeable = true;
        opts.inInputShareable = true;
      /* important (don't scale for high resolution devices) */
        opts.inScaled = false;
    }
}