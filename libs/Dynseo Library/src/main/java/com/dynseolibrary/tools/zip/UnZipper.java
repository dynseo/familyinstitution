package com.dynseolibrary.tools.zip;

import java.io.File;
import android.util.Log;

public class UnZipper {
	
	private static final String TAG = "UnZipper";
	
	public static boolean unZip(String srcPath, String destPath, String fileName) {
		Log.i(TAG, "source file = "+srcPath);
		if (srcPath != null && destPath != null && fileName != null) {
			File srcFile = new File(srcPath + File.separator + fileName);
			if (!srcFile.exists()) {
				Log.e(TAG, "zip source file : "+srcFile.getPath()+" don't exist");
				return false;
			}
			File destFile = new File(destPath);
			if (!destFile.exists()) {
				if (destFile.mkdirs()) {
					Log.i(TAG, "create target folder : "+destFile.getPath());
				}
				else {
					Log.e(TAG, "error - when create target folder : "+destFile.getPath());
					return false;
				}
			}
			ZipUtil zipp = new ZipUtil(20480);
			if (zipp.unZip(srcFile.getPath(),destPath)) {
				/* delete the .zip file */
				srcFile.delete();
			}
			return true;
		}
		else {
			Log.e(TAG, "one of them (String srcPath, String destPath, String fileName) is null");
			return false;
		}
	}
}
