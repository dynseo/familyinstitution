package com.dynseolibrary.tools.ui;

import com.example.dynseolibrary.R;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class QuitButton extends ImageButton {

	public static final int QUIT_BUTTON_POSITION_LEFT_TOP = 1;
	public static final int QUIT_BUTTON_POSITION_RIGHT_TOP = 2;
	public static final int QUIT_BUTTON_POSITION_LEFT_BOTTOM = 3;
	public static final int QUIT_BUTTON_POSITION_RIGHT_BOTTOM = 4;
	
	public QuitButton(Context context, int backgroundButtonQuitId, int position) {
		super(context);

		int width = (int)(getResources().getDimension(R.dimen.quit_button_width));
		int height = (int)(getResources().getDimension(R.dimen.quit_button_height));
		int margin = (int)(getResources().getDimension(R.dimen.quit_button_margin));

		Log.d("QuitButton","width: " + width + ", height: " + height+", margin: "+margin);

		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
		switch (position) {
		case QUIT_BUTTON_POSITION_LEFT_TOP:
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			params.setMargins(margin, margin, 0, 0);
			break;
		case QUIT_BUTTON_POSITION_RIGHT_TOP:
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			params.setMargins(0, margin, margin, 0);
			break;
		case QUIT_BUTTON_POSITION_LEFT_BOTTOM:
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.setMargins(margin, 0, 0, margin);
			break;
		case QUIT_BUTTON_POSITION_RIGHT_BOTTOM:
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.setMargins(0, 0, margin, margin);
			break;
		default:
			break;
		}
		setLayoutParams(params);
		setScaleType(ScaleType.CENTER_INSIDE);
//		int padding = (int)(getResources().getDimension(R.dimen.dp5) * scale + 0.5f);
		
		int padding = (int)(getResources().getDimension(R.dimen.quit_button_margin));
		
		setPadding(padding, padding, padding, padding);
//		setImageResource(R.drawable.cross_red);
		setBackgroundResource(backgroundButtonQuitId);
	}
	
	static public QuitButton add(Activity act, int backgroundButtonQuitId, int idViewGroup, int position, OnClickListener quitListener) {
	    
		QuitButton quitButton = new QuitButton(act, backgroundButtonQuitId, position);
		if (act.findViewById(android.R.id.content) instanceof ViewGroup) {
	    	ViewGroup mainView = ((ViewGroup)act.findViewById(android.R.id.content));
	    	if (mainView.getChildCount() != 0) {	    		
//	    		if (mainView.getChildAt(0).getBackground() != act.getResources().getDrawable(backgroundButtonQuitId)){
	    			((ViewGroup)mainView.getChildAt(0)).addView(quitButton);
	    			Log.d("quitButton", "quitButton 1 : " + mainView.getChildAt(0).getBackground() + " | " + act.getResources().getDrawable(backgroundButtonQuitId));
//	    		}
	    	}
	    	else {
	    		Log.d("quitButton", "quitButton 2");
	    		mainView.addView(quitButton);
	    	}
	    	quitButton.setOnClickListener(quitListener);
	    }
		return quitButton;
	}
}
