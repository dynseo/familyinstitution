package com.dynseolibrary.tools;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static android.os.Build.VERSION;

/**
 * Created by jiaji on 12/10/15.
 */

public class ImageLoader {
    public static final String TAG = "ImageLoader";
    /* a list of Bitmaps generated from images from Resources(images with ids) */
    private SparseArray<Bitmap> bitmapsForImagesWithIds;
    /* a list of Bitmaps generated from images from local file system(images with names) */
    private Map<String, Bitmap> bitmapsForImagesWithNames;

    private Context context;
    private String localStorageImagesPath;
    private boolean searchInAssets;

    /* constructor, localStorageImagesPath can be null if no need to load local storage images */
    public ImageLoader(Context context, String localStorageImagesPath) {
        this.localStorageImagesPath = localStorageImagesPath;
        this.context = context;
        searchInAssets = false;
    }

    public void setSearchInAssets(boolean searchAssets) {
        this.searchInAssets = searchAssets;
    }

    /* load an image from resources for a View as his background */
    public boolean loadImageBackground(int imageId, View view) {
        Log.i(TAG, "loadImageBackground(int, View)");
        try {
            Bitmap bitmap = getBitmap(imageId, view);
            if (VERSION.SDK_INT >= 16) {
                view.setBackground(new BitmapDrawable(context
                        .getResources(), bitmap));
            } else {
                view.setBackgroundDrawable(new BitmapDrawable(context
                        .getResources(), bitmap));
            }
            return true;
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "out of memory");
            return false;
        }
    }

    public boolean loadNinePatchBackground(int imageId, ImageView view){
        Log.i(TAG, "loadNinePatchBackground(int, View)");
        try {
            Bitmap bitmap = getBitmap(imageId, view);
            byte[] chunk = bitmap.getNinePatchChunk();
            Drawable drawable;
            if(NinePatch.isNinePatchChunk(chunk))
                drawable = new NinePatchDrawable(context.getResources(), bitmap, chunk, new Rect(), null);
            else
                drawable = new BitmapDrawable(bitmap);

            if (VERSION.SDK_INT >= 16)
                view.setBackground(drawable);
            else
                view.setBackgroundDrawable(drawable);

//            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            return true;
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "out of memory");
            return false;
        }
    }

    /* load an image from local storage for a View as his background */
    public boolean loadImageBackground(String fileName, View view) {
        Log.i(TAG, "loadImageBackground(String, View)");
        try {
            Bitmap bitmap = getBitmapWithName(fileName, view);
            if (VERSION.SDK_INT >= 16) {
                view.setBackground(new BitmapDrawable(context
                        .getResources(), bitmap));
            } else {
                view.setBackgroundDrawable(new BitmapDrawable(context
                        .getResources(), bitmap));
            }
            return true;
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "out of memory");
            return false;
        }
    }

    /* load an image from resources for a ImageView as his image source */
    public boolean loadImageSource(int imageId, ImageView imageView) {
        Log.i(TAG, "loadImageSource(int, ImageView)");
        try {
            Bitmap bitmap = getBitmap(imageId, imageView);
            imageView.setImageDrawable(new BitmapDrawable(context.getResources(), bitmap));
            return true;
        } catch (OutOfMemoryError e) {
            /* when out of memory, we release bitmaps, and recreate the image */
            Log.e(TAG, "out of memory");
            return false;
        }
    }

    /* load an image from local storage for a ImageView as his image source */
    public boolean loadImageSource(String fileName, ImageView imageView) {
        return loadImageSource(fileName, imageView, false);
    }

    public boolean loadImageSource(String fileName, ImageView imageView, boolean rounded) {
        Log.i(TAG, "loadImageSource(String, ImageView, boolean");
        try {
            Bitmap bitmap = getBitmapWithName(fileName, imageView);
            Log.d("taille de bitmap", ""+ bitmap.getWidth()+"-----"+ bitmap.getHeight());
            if (rounded)
                bitmap = BitmapUtils.getRoundedCornerBitmap(bitmap, 100);
            imageView.setImageDrawable(new BitmapDrawable(context.getResources(), bitmap));
            return true;
        } catch (OutOfMemoryError e) {
			/* when out of memory, we release bitmaps, and recreate the image */
            Log.e(TAG, "out of memory");
            return false;
        }
    }

    public Bitmap getBitmap(int imageId){
        return bitmapsForImagesWithIds.get(imageId);
    }

    private Bitmap getBitmap(int imageId, View view) throws OutOfMemoryError{
        Bitmap bitmap = null;
        if (bitmapsForImagesWithIds == null)
            bitmapsForImagesWithIds = new SparseArray();
        else
            bitmap = bitmapsForImagesWithIds.get(imageId);

        if (bitmap == null) {
            bitmap = BitmapUtils.loadToViewFromResource(context.getResources(),
                    imageId, view);
            bitmapsForImagesWithIds.append(imageId, bitmap);
        }
        else {
            Log.i(TAG, "Bitmap reused");
        }
        return bitmap;
    }


    private Bitmap getBitmapWithName(String fileName, View view) throws OutOfMemoryError{
        Bitmap bitmap = null;
        if (bitmapsForImagesWithNames == null)
            bitmapsForImagesWithNames = new HashMap();
        else
            bitmap = bitmapsForImagesWithNames.get(fileName);

        if (bitmap == null) {
            bitmap = BitmapUtils.loadToViewFromFile(localStorageImagesPath
                    + fileName, view);

            if( bitmap == null && searchInAssets)      //Has been unable to load
                bitmap = BitmapUtils.loadToViewFromAssets(fileName, view);

            if( bitmap != null )
                bitmapsForImagesWithNames.put(fileName, bitmap);
        }
        else {
            Log.i(TAG, "Bitmap reused");
        }
        return bitmap;
    }

    /* recycle all bitmaps and force the Garbage Collector to work */
    public void recycleAllImages() {
        Log.i(TAG, "recycleAllImages()");
        recycleAllImagesWithIds();
        recycleAllImagesWithNames();
        System.gc();
    }

    /* recycle all bitmaps created from resources */
    public void recycleAllImagesWithIds() {
        Log.i(TAG, "recycleAllImagesWithIds()");
        if (bitmapsForImagesWithIds != null) {
            int size = bitmapsForImagesWithIds.size();
            for (int i = 0; i < size; i++) {
                Bitmap bitmap = bitmapsForImagesWithIds.valueAt(i);
                /* if bitmap is not null and not recycled */
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
            bitmapsForImagesWithIds.clear();
        }
    }

    /* recycle all bitmaps created from local storage */
    public void recycleAllImagesWithNames() {
        Log.i(TAG, "recycleAllImagesWithNames()");
        if (bitmapsForImagesWithNames != null) {
            for (Bitmap bitmap : bitmapsForImagesWithNames.values()) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
            bitmapsForImagesWithNames.clear();
        }
    }

    /* recycle all bitmaps created from resources whose id is in a Set of ids */
    public void recycleImagesWithIdsInList(Set<Integer> imageIds) {
        Log.i(TAG, "recycleImagesWithIdsInList()");
        if (bitmapsForImagesWithIds != null && imageIds != null) {
            Iterator<Integer> iterator = imageIds.iterator();
            while (iterator.hasNext()) {
                int imageId = iterator.next();
                Bitmap bitmap = bitmapsForImagesWithIds.get(imageId);
                /* if bitmap is not null and not recycled */
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                    bitmapsForImagesWithIds.remove(imageId);
                }
            }
        }
    }

    /* recycle all bitmaps created from resources whose id is not in a Set of ids */
    public void recycleImagesWithIdsNotInList(Set<Integer> imageIds) {
        Log.i(TAG, "recycleImagesWithIdsNotInList()");
        if (bitmapsForImagesWithIds != null && imageIds != null) {
            int size = bitmapsForImagesWithIds.size();
            for (int i = size - 1; i >= 0; i--) {
                int imageId = bitmapsForImagesWithIds.keyAt(i);
                if (!imageIds.contains(imageId)) {
                    Bitmap bitmap = bitmapsForImagesWithIds.valueAt(i);
                    /* if bitmap is not null and not recycled */
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                    bitmapsForImagesWithIds.remove(imageId);
                }
            }
        }
    }

    /* recycle all bitmaps created from local storage whose name is in a Set of names */
    public void recycleImagesWithNamesInList(Set<String> imageNames) {
        Log.i(TAG, "recycleImagesWithNamesInList()");
        if (bitmapsForImagesWithNames != null && imageNames != null) {
            Iterator<String> iterator = imageNames.iterator();
            while (iterator.hasNext()) {
                String imageName = iterator.next();
                if (bitmapsForImagesWithNames.keySet().contains(imageName)) {
                    Bitmap bitmap = bitmapsForImagesWithNames.get(imageName);
                    /* if bitmap is not null and not recycled */
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                        bitmapsForImagesWithNames.remove(imageName);
                    }
                }
            }
        }
    }

    /* recycle all bitmaps created from local storage whose name is not in a Set of names */
    public void recycleImagesWithNamesNotInList(Set<String> imageNames) {
        Log.i(TAG, "recycleImagesWithNamesNotInList()");
        if (bitmapsForImagesWithNames != null && imageNames != null) {
            Iterator<String> iterator = bitmapsForImagesWithNames.keySet().iterator();
            while (iterator.hasNext()) {
                String imageName = iterator.next();
                if (!imageNames.contains(imageName)) {
                    Bitmap bitmap = bitmapsForImagesWithNames.get(imageName);
                    /* if bitmap is not null and not recycled */
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                    bitmapsForImagesWithNames.remove(imageName);
                }
            }
        }
    }

    public void addBitmap(int imageId, Bitmap aBitmap){
        if (bitmapsForImagesWithIds == null)
            bitmapsForImagesWithIds = new SparseArray();
        bitmapsForImagesWithIds.put(imageId, aBitmap);
    }

    public void addBitmap(String imageName, Bitmap aBitmap){
        if (bitmapsForImagesWithNames == null)
            bitmapsForImagesWithNames = new HashMap();
        bitmapsForImagesWithNames.put(imageName, aBitmap);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getLocalStorageImagesPath() {
        return localStorageImagesPath;
    }

    public void setLocalStorageImagesPath(String localStorageImagesPath) {
        this.localStorageImagesPath = localStorageImagesPath;
    }


    public SparseArray<Bitmap> getBitmapsForImagesWithIds() {
        return bitmapsForImagesWithIds;
    }

    public void setBitmapsForImagesWithIds(SparseArray<Bitmap> bitmapsForImagesWithIds) {
        this.bitmapsForImagesWithIds = bitmapsForImagesWithIds;
    }

    public Map<String, Bitmap> getBitmapsForImagesWithNames() {
        return bitmapsForImagesWithNames;
    }

    public void setBitmapsForImagesWithNames(Map<String, Bitmap> bitmapsForImagesWithNames) {
        this.bitmapsForImagesWithNames = bitmapsForImagesWithNames;
    }

}