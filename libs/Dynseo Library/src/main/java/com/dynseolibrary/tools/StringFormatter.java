package com.dynseo.stimart.utils;

import android.util.Log;

/**
 * Created by jiaji on 22/10/15.
 */
public class StringFormatter {
    private final static String FIRSTNAME = "F";
    private final static String NAME = "N";

    public static String nameFormat(String nameFormat, String name, String firstName) {
        if (nameFormat.equals(NAME+" "+FIRSTNAME )) {
            return ifNull(name) + " " + ifNull(firstName);
        }
        return ifNull(firstName) + " " + ifNull(name);
    }

    static public String ifNull(String value) {
        return (value == null) ? "" : value;
    }
}
