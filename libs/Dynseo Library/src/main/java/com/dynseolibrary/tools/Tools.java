package com.dynseolibrary.tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.GenericServerResponseInterface;
import com.dynseolibrary.platform.SaveSerialInterface;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.platform.server.SaveSerialNumbersTask;
import com.example.dynseolibrary.R;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

@SuppressLint("DefaultLocale")

public class Tools {

	private static final String TAG = "Tools";
	private static final String RIL = "ril.serialnumber:";
	private static final String SYS = "sys.serialnumber:";
	private static final String RO = "ro.serialno:";
	private static final String BOOT = "ro.boot.serialno:";
	public static final int FILE_TYPE_AUDIO = 1;
	public static final int FILE_TYPE_IMAGE = 2;

//    public static String getManufacturerSerialNumber() {
//
//        //It is better to detect serial number generated first : we can then
//        //manage new cases on the server side (for instance if we want to omit serial with "XYZ")
//        String serial = AppManager.getAppManager().getSerialNumberGenerated();
//        if (serial != null)
//            return serial;
//
//        try {
//            Class<?> c = Class.forName("android.os.SystemProperties");
//            Method get = c.getMethod("get", String.class, String.class);
//			/* 1st way to get serial number */
////			serial = (String) get.invoke(c, "ril.serialnumber", "unknown");
//            serial = (String) get.invoke(c, "ril.serialnumber", "undefined");
//            if ( !isSerialNumberValid(serial) ) {
//
//				/* 2nd way to get serial number */
//                serial = (String) get.invoke(c, "sys.serialnumber", "undefined");
//                Log.i(TAG, "serial number from sys = " + serial);
//
//                if ( !isSerialNumberValid(serial) ) {
//					/* 3rd way to get serial number */
//                    serial = (String) get.invoke(c, "ro.serialno", "undefined");
//                    Log.i(TAG, "serial number from ro = " + serial);
//
//                    if ( !isSerialNumberValid(serial) )
//                        serial = null;
//                }
//            } else {
//                Log.i(TAG, "serial number from ril = " + serial);
//            }
//
//        } catch (Exception ignored) {
//            Log.i(TAG, "serial number from build = " + Build.SERIAL);
//        }
//
//        if (serial == null)
//            return "undefined";
//        return serial.toUpperCase(Locale.FRENCH);
//    }

//    public static String getManufacturerSerialNumber() {
//
//        return getManufacturerSerialNumber(false);
//    }

	public static String getManufacturerSerialNumber(SaveSerialInterface requester) {
		String serial=null;

		try {

			Class<?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class, String.class);
			/* 1st way to get serial number */

			serial = (String) get.invoke(c, "ril.serialnumber", "undefined");
			if ( !isSerialNumberValid(serial) ) {                               //ril.serialnumber is invalid

				/* 2nd way to get serial number */
				serial = (String) get.invoke(c, "sys.serialnumber", "undefined");
				Log.i(TAG, "serial number from sys = " + serial);

				if ( !isSerialNumberValid(serial) ) {                           //sys.serialnumber is invalid
					/* 3rd way to get serial number */
					serial = (String) get.invoke(c, "ro.serialno", "undefined");
					Log.i(TAG, "serial number from ro = " + serial);

					//If we get to this point, we choose ro.boot.serialno
					String secondSerial = (String) get.invoke(c, "ro.boot.serialno", "undefined");

					if (isSerialNumberValid(secondSerial)) { //Second serial is valid
						serial = secondSerial;
						secondSerial = (String) get.invoke(c, "ro.serialno", "undefined");
					}else{                                  //Second serial is not valid
						if (!isSerialNumberValid(serial)){
							serial = "undefined";
						}
						else
							Log.d(TAG,"Cas particulier : ro.serialno existe mais ro.boot.serialno non");
					}
					//Send both ro.serialno and ro.boot.serialno to server in case there's a conflict
					buildSerialNumberRequest(requester,serial, secondSerial);

				}
			} else {
				Log.i(TAG, "serial number from ril = " + serial);
			}

		} catch (Exception ignored) {
			Log.i(TAG, "serial number from build = " + Build.SERIAL);
		}

		if (serial == null || serial.equals("undefined"))
			return "undefined";

		return serial.toUpperCase(Locale.FRENCH);
	}

//    public static String getManufacturerSerialNumber(boolean sendSerials) {
//
//        //It is better to detect serial number generated first : we can then
//        //manage new cases on the server side (for instance if we want to omit serial with "XYZ")
//        String serial = AppManager.getAppManager().getSerialNumberGenerated();
//
//        //Special case: if the user got a generated serial and reinstalls the application (with the modification that takes the ro.boot.serialno)
//        //the new serial number could be ro.boot.serialno and would be registered on our network with the generated one.
//        //So to do the migration we would have to find the user not by the generated serial number but by email or pseudo
//
//        if (serial != null)
//            return serial;
//
//        try {
//
//            Class<?> c = Class.forName("android.os.SystemProperties");
//            Method get = c.getMethod("get", String.class, String.class);
//			/* 1st way to get serial number */
//
//            serial = (String) get.invoke(c, "ril.serialnumber", "undefined");
//            if ( !isSerialNumberValid(serial) ) {       //ril.serialnumber is invalid
//
//				/* 2nd way to get serial number */
//                serial = (String) get.invoke(c, "sys.serialnumber", "undefined");
//                Log.i(TAG, "serial number from sys = " + serial);
//
//                if ( !isSerialNumberValid(serial) ) {   //sys.serialnumber is invalid
//					/* 3rd way to get serial number */
//                    serial = (String) get.invoke(c, "ro.serialno", "undefined");
//                    Log.i(TAG, "serial number from ro = " + serial);
//
//                    //If we get to this point, we choose ro.boot.serialno
//                    String secondSerial = (String) get.invoke(c, "ro.boot.serialno", "undefined");
//
//                    //Send both ro.serialno and ro.boot.serialno to server in case there's a conflict
//                    if(sendSerials)
//                        buildSerialNumberRequest(serial, secondSerial);
//
//                    if (isSerialNumberValid(secondSerial)) //Second serial is valid
//                        serial = secondSerial;
//                    else{                                  //Second serial is not valid
//                        if (!isSerialNumberValid(serial))
//                            serial = null;
//                        else
//                            Log.d(TAG,"Cas particulier : ro.serialno existe mais ro.boot.serialno non");
//                    }
//
//                }
//            } else {
//                Log.i(TAG, "serial number from ril = " + serial);
//            }
//
//        } catch (Exception ignored) {
//            Log.i(TAG, "serial number from build = " + Build.SERIAL);
//        }
//
//        if (serial == null)
//            return "undefined";
//        return serial.toUpperCase(Locale.FRENCH);
//    }

	public static void buildSerialNumberRequest(SaveSerialInterface requester, String firstSerial, String secondSerial){
		//First get all the serials
		String allSerials;
		try {
			Class<?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class, String.class);

			allSerials = RIL + get.invoke(c, "ril.serialnumber", "undefined");
			allSerials += '+' + SYS + get.invoke(c, "sys.serialnumber", "undefined");
			allSerials += '+' + RO + firstSerial;
			allSerials += '+' + BOOT + secondSerial;

		} catch (Exception ignored) {
			ignored.printStackTrace();
			allSerials = RO + firstSerial + '+' + BOOT + secondSerial;
		}

		SaveSerialNumbersTask saveSerialNumbersTask = new SaveSerialNumbersTask(requester,firstSerial,secondSerial,allSerials);
		saveSerialNumbersTask.execute();
	}

//    public static void buildSerialNumberRequest(){
//        //First get all the serials
//        String first;
//        String second;
//        try {
//            Class<?> c = Class.forName("android.os.SystemProperties");
//            Method get = c.getMethod("get", String.class, String.class);
//            first = (String) get.invoke(c, "ro.serialno", "undefined");
//            second = (String) get.invoke(c, "ro.boot.serialno", "undefined");
//
//        } catch (Exception ignored) {
//            ignored.printStackTrace();
//            first = "";
//            second = "";
//        }
//        buildSerialNumberRequest(first,second);
//    }

//	public static String getManufacturerSerialNumber() {
//		String serial = null;
//		try {
//			Class<?> c = Class.forName("android.os.SystemProperties");
//			Method get = c.getMethod("get", String.class, String.class);
//			/* 1st way to get serial number */
////			serial = (String) get.invoke(c, "ril.serialnumber", "unknown");
//			serial = (String) get.invoke(c, "ril.serialnumber", "undefined");
//			if ( !isSerialNumberValid(serial) ) {
//
//				/* 2nd way to get serial number */
//				serial = (String) get.invoke(c, "sys.serialnumber", "undefined");
//				Log.i(TAG, "serial number from sys = " + serial);
//
//				if ( !isSerialNumberValid(serial) ) {
//					/* 3rd way to get serial number */
//					serial = (String) get.invoke(c, "ro.serialno", "undefined");
//					Log.i(TAG, "serial number from ro = " + serial);
//				}
//			} else {
//				Log.i(TAG, "serial number from ril = " + serial);
//			}
//
//		} catch (Exception ignored) {
//			Log.i(TAG, "serial number from build = " + Build.SERIAL);
//		}
//		if ( !isSerialNumberValid(serial) ) {
//			serial = AppManager.getAppManager().getSerialNumberGenerated();
//			if (serial == null)
//				serial = "undefined";
//
//			return serial;
//		}
//
//		return serial.toUpperCase(Locale.FRENCH);
////		/* for tests */
////		return "RF2D40H07SYXX1";
//	}

	protected static boolean isSerialNumberValid(String serial) {
		return (serial!=null && !serial.equalsIgnoreCase("undefined") && !serial.startsWith("000000") && serial.indexOf("123456")== -1 && serial.indexOf("ABCDEF")== -1);
	}

	public static Date parseDate(String d) {
		Date date = null;
		if (d != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date = dateFormat.parse(d);
			} catch (ParseException e3) {
			}
		}
		return date;
	}

	public static Date parseDateComplete(String d) {
		Date date = null;
		if (d != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			try {
				date = dateFormat.parse(d);
			} catch (ParseException e3) {
			}
		}
		return date;
	}

	public static String dateTimeToString(Date d) {
		if (d != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
			return dateFormat.format(d);
		}
		return "";
	}

	public static String formattedAsDate(String dateStr) {
		return genericDateTimeFormatter(dateStr, "yyyy-MM-dd", AppManager.getAppManager().getDateFormat());
	}

	public static String formattedAsDateTime(String dateStr) {
		return genericDateTimeFormatter(dateStr, "yyyy-MM-dd HH:mm", AppManager.getAppManager().getDateTimeFormat());
	}

	//--- This method is to be used in specific cases...
	public static String formattedAsDate(String dateStr, String outFormat) {
		return genericDateTimeFormatter(dateStr, "yyyy-MM-dd", outFormat);
	}

	public static String formattedAsDateTime(String dateStr, String outFormat) {
		return genericDateTimeFormatter(dateStr, "yyyy-MM-dd HH:mm", outFormat);
	}

	private static String genericDateTimeFormatter(String dateStr, String baseFormat, String outFormat) {

		if (dateStr != null) {
			if (!outFormat.equals(baseFormat)) {
				try {
					SimpleDateFormat inFormatter = new SimpleDateFormat(baseFormat, Locale.US);
					Date date = inFormatter.parse(dateStr);

					SimpleDateFormat outFormatter = new SimpleDateFormat(outFormat);
					String outDateStr = outFormatter.format(date);
					return outDateStr;
				} catch (Exception e) {
				}
			}
		}
		return dateStr;
	}

	public static String modifyFormatDate(String inputFormat, String date, String outputFormat) {
		String retDate;
		try {
			retDate = String.valueOf(new SimpleDateFormat(outputFormat).format(new SimpleDateFormat(
					inputFormat).parse(date)));
			return (retDate);
		} catch (ParseException e) {
			Log.d("ERROR", "parsing Date");
		}
		return (date);
	}


//	public static void setDatePickerDialog(Context context, final EditText field, DatePickerDialog fromDatePickerDialog,
//										int defaultYear, int defaultMonth, int defaultDayOfMonth){
//		final SimpleDateFormat dateFormatter = new SimpleDateFormat(context.getString(R.string.date_format), Locale.US);
//		if (fromDatePickerDialog == null)
//			fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
//
//				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
//					Calendar newDate = Calendar.getInstance();
//					newDate.set(year, monthOfYear, dayOfMonth);
//					field.setText(dateFormatter.format(newDate.getTime()));
//				}
//
//			},defaultYear, defaultMonth, defaultDayOfMonth);
//		fromDatePickerDialog.getDatePicker().setCalendarViewShown(false);
//	}

	/* a toast with style white (white background and black text, ...) */
	public static Toast showToastBackgroundWhite(Context context, CharSequence text) {
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		toastLayout.setBackgroundResource(R.drawable.background_round_white_half_transparent2);
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setGravity(Gravity.CENTER);
		toastTV.setTextColor(Color.BLACK);
		toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.textsize_toast));
		toastTV.setTypeface(null, Typeface.BOLD);
		toastTV.setShadowLayer(0, 0, 0, 0);
		toast.show();
		return toast;
	}

	/* a toast with style by default (black background and white text, ...) */
	public static Toast showToast(Context context, CharSequence text) {
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		toastLayout.setBackgroundResource(R.drawable.background_round_black);
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setGravity(Gravity.CENTER);
		toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.textsize_toast));
		toastTV.setTypeface(null, Typeface.BOLD);
		toast.show();
		return toast;
	}

	/* a long toast with style white (white background and black text, ...) */
	public static Toast showToastLongBackgroundWhite(Context context, CharSequence text) {
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		toastLayout.setBackgroundResource(R.drawable.background_round_white_half_transparent2);
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setGravity(Gravity.CENTER);
		toastTV.setTextColor(Color.BLACK);
		toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.textsize_toast));
		toastTV.setTypeface(null, Typeface.BOLD);
		toastTV.setShadowLayer(0, 0, 0, 0);
		toast.show();
		toast.show();
		return toast;
	}

	/* a long toast with style by default (black background and white text, ...) */
	public static Toast showToastLong(Context context, CharSequence text) {
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		toastLayout.setBackgroundResource(R.drawable.background_round_black);
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setGravity(Gravity.CENTER);
		toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.textsize_toast));
		toastTV.setTypeface(null, Typeface.BOLD);
		toast.show();
		toast.show();
		return toast;
	}

	/* a toast with style white (white background and black text, ...) */
	public static Toast showToastBackgroundWhite(Context context, CharSequence text, int gravity) {
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(gravity, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		toastLayout.setBackgroundResource(R.drawable.background_round_white_half_transparent2);
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setGravity(Gravity.CENTER);
		toastTV.setTextColor(Color.BLACK);
		toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.textsize_toast));
		toastTV.setTypeface(null, Typeface.BOLD);
		toastTV.setShadowLayer(0, 0, 0, 0);
		toast.show();
		return toast;
	}

	/* a toast with style by default (black background and white text, ...) */
	public static Toast showToast(Context context, CharSequence text, int gravity) {
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(gravity, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		toastLayout.setBackgroundResource(R.drawable.background_round_black);
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setGravity(Gravity.CENTER);
		toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.textsize_toast));
		toastTV.setTypeface(null, Typeface.BOLD);
		toast.show();
		return toast;
	}

	public static boolean isResourceTrue(Context context, int resourceId) {
		String value = context.getResources().getString(resourceId);
		return (value != null && value.equalsIgnoreCase("true"));
	}

	public static boolean[] booleanResourceArray(Context context, int resourceId) {
		String value = context.getResources().getString(resourceId);
		if( value != null ) {
			String[] booleansStr = value.split(",");
			boolean[] values = new boolean[booleansStr.length];
			for( int i=0; i<booleansStr.length; i++ )
				values[i] = (booleansStr[i].equalsIgnoreCase("true"));
			return values;
		}
		return null;
	}

	public static int[] intResourceArray(Context context, int resourceId) {
		String value = context.getResources().getString(resourceId);
		if( value != null ) {
			String[] intsStr = value.split(",");
			int[] values = new int[intsStr.length];
			for( int i=0; i<intsStr.length; i++ )
				values[i] = Integer.parseInt(intsStr[i]);
			return values;
		}
		return null;
	}

	public final static Pattern EMAIL_ADDRESS_PATTERN = Pattern
			.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

	public static boolean isEmailValid(String email) {
		return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
	}

	public static void quitAppCompletely(Context context) {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		context.startActivity(intent);
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public static void scrollToOnButtonClick(final HorizontalScrollView hsv, Button aButton, final boolean scrollToRight){

		aButton.setOnTouchListener(new View.OnTouchListener() {

									   private Handler mHandler;
									   private long mInitialDelay = 300;
									   private long mRepeatDelay = 100;

									   @Override
									   public boolean onTouch(View v, MotionEvent event) {
										   switch (event.getAction()) {
											   case MotionEvent.ACTION_DOWN:
												   if (mHandler != null)
													   return true;
												   mHandler = new Handler();
												   mHandler.postDelayed(mAction, mInitialDelay);
												   break;
											   case MotionEvent.ACTION_UP:
												   if (mHandler == null)
													   return true;
												   mHandler.removeCallbacks(mAction);
												   mHandler = null;
												   break;
										   }
										   return false;
									   }

									   Runnable mAction = new Runnable() {
										   @Override
										   public void run() {
											   if (scrollToRight)
												   hsv.scrollTo((int) hsv.getScrollX() + 100, (int) hsv.getScrollY());
											   else
												   hsv.scrollTo((int) hsv.getScrollX() - 100, (int) hsv.getScrollY());
											   mHandler.postDelayed(mAction, mRepeatDelay);
										   }
									   };
								   }
		);

		aButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (scrollToRight)
					hsv.scrollTo((int) hsv.getScrollX() + 100, (int) hsv.getScrollY());
				else
					hsv.scrollTo((int) hsv.getScrollX() - 100, (int) hsv.getScrollY());
			}
		});
	}

	public static void showDelayedDialog(final Dialog aDialog,final int duration){
		final Handler handlerDelayedDialog = new Handler();
		Runnable runnableDelayedDialog = new Runnable() {
			public void run() {
				try {
					Thread.sleep(duration);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
					return;
				}
				handlerDelayedDialog.post(new Runnable(){
					public void run() {
						aDialog.show();
					}
				});
			}
		};
		Thread threadDelayedDialog = new Thread(runnableDelayedDialog);
		threadDelayedDialog.start();
	}

	public static void hideActionBar(Activity activity){
		if (activity!=null)
			if (activity.getWindow()!=null)
				activity.getWindow().setFlags(
						WindowManager.LayoutParams.FLAG_FULLSCREEN,
						WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	public static void hideButtonBar(Window window){
		View decorView = window.getDecorView();
		// Hide both the navigation bar and the status bar.
		// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
		// a general rule, you should design your app to hide the status bar whenever you
		// hide the navigation bar.
		int uiOptions = 0;
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN;
		}
		decorView.setSystemUiVisibility(uiOptions);
	}


	public static void hideMainView(final View toHide, final Activity context, final int duration){
		//Introducing a thread hiding menuAppli in order to make the splashscreen last for at least 1 second
		toHide.setVisibility(View.INVISIBLE);
		Thread splashThread = new Thread() {
			@Override
			public void run() {
				try {
					sleep(duration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					//runOnUiThread because only the main view can touch its views
					context.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							toHide.setVisibility(View.VISIBLE);
						}
					});
				}
			}
		}; splashThread.start();
	}


	/*.°============================= Managing android 6.0 inApp permissions =========================°.*/
	public static boolean hasPermissions(Context context, String... permissions) {
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}
	/*.°===============================================================================================°.*/
	// check development mode
	public static boolean isDevelopmentMode(Context context) {
		try {
			if ((context.getPackageManager().getPackageInfo( context.getPackageName(), 0).applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
				return true;
			}
		} catch (PackageManager.NameNotFoundException e) {
			throw new IllegalStateException(e);
		}
		return false;
	}

	/*.°============================= Managing available splace on Device =========================°.*/
	private static boolean externalMemoryAvailable() {
		return android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}
	private static String formatSize(long size) {
		String suffix = null;

		if (size >= 1024) {
			suffix = "KB";
			size /= 1024;
			if (size >= 1024) {
				suffix = "MB";
				size /= 1024;
				if (size >= 1024){
					suffix = "GB";
					size /= 1024;
				}
			}
		}
		StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

		int commaOffset = resultBuffer.length() - 3;
		while (commaOffset > 0) {
			resultBuffer.insert(commaOffset, ',');
			commaOffset -= 3;
		}

		if (suffix != null) resultBuffer.append(suffix);
		return resultBuffer.toString();
	}

	private static long getAvailableExternalMemorySizeLong(){
		if (externalMemoryAvailable()) {
			StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
			Log.d(TAG,"exists an external memory with : " + (stat.getAvailableBlocks() * stat.getBlockSize()) + "bits of memory");
			return (stat.getAvailableBlocks() * stat.getBlockSize());
		} else {
			Log.d(TAG,"no external memory");
			return 0;
		}
	}

	private static long getAvailableInternalMemorySizeLong() {
		StatFs stat = new StatFs("/data/data/" + AppManager.getAppManager().getPackageName());
		Log.d(TAG,"internal memory has : " + ((long)stat.getAvailableBlocks() * (long)stat.getBlockSize()) + "bits of memory");
		return (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
	}

	public static long getAvailableMemorySizeOnDeviceLong(){
		return (getAvailableExternalMemorySizeLong()>getAvailableInternalMemorySizeLong()?
				getAvailableExternalMemorySizeLong():getAvailableInternalMemorySizeLong());
	}

	public static boolean isAvailableMemoryEnoughForDownload(int spaceNeeded){
		//For phone integration, gotta check everytime we launch a download
		//1. the remaining space on the device
		//2. if the user is connected on wifi or phone data

		//Added a security especially for phones checking
		//the storage space available before downloading resources
		Log.d(TAG,"Memory test: available test on phone :"+Tools.getAvailableMemorySizeOnDeviceLong());
		Log.d(TAG,"Memory test: limit fixed by code :"
				+(long) spaceNeeded);

		return (Tools.getAvailableMemorySizeOnDeviceLong() >
//				(long) 1000000000 * context.getResources().getInteger(R.integer.space_needed_for_download_resources));
				(long)  spaceNeeded);
	}

	public static String getAvailableMemorySizeOnDevice(){
		return (formatSize(getAvailableMemorySizeOnDeviceLong()));
	}

    /*.°===============================================================================================°.*/
}