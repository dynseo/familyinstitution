/**
HOUSSEM
*/
package com.dynseolibrary.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.util.Log;

import com.dynseolibrary.platform.server.ConnectionConstants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Account {

	String email;
	String pseudo;	
	String password;
    String code;            //can be used as a promo or "white mark" code
	
	public static final String SHARED_PREFS_ACCOUNT = "account";
	
	public Account(String anEmail, String aPseudo, String aPassword, String aCode){
		email = anEmail;
		pseudo = aPseudo;		
		password = aPassword;
        code = aCode;
	}
	
	public Account (JSONObject jsonAccount){
		email = jsonAccount.optString("email");
		pseudo = jsonAccount.optString("pseudo");
		password = jsonAccount.optString("password");
        code = jsonAccount.optString("code");
	}

	public JSONObject toJson() throws JSONException {
		JSONObject jsonAccount = new JSONObject();
		jsonAccount.put("email", email);
		jsonAccount.put("pseudo", pseudo);
		jsonAccount.put("password", password);
        jsonAccount.put("code", code);
		
		return jsonAccount;
	}
	
	public void saveInSP(SharedPreferences sp) throws JSONException{
		JSONObject jsonAccount = this.toJson();
		sp.edit().putString(SHARED_PREFS_ACCOUNT, jsonAccount.toString()).commit();				
	}
	
	
	public String getEmail() {
		return email;
	}

	public String getPseudo() {
		return pseudo;
	}

    public String getCode() {
        return code;
    }

	public String getPassword() {
		return password;
	}

    public void setCode(String code) {
        this.code = code;
    }

	public static String getFromSP(SharedPreferences sp){
		return sp.getString(SHARED_PREFS_ACCOUNT, null);
	}

	public static Account getAccountFromSP(SharedPreferences sp) throws JSONException{
		String accountString = getFromSP(sp);
		Log.d("ACCOUNT", "accountString : " + accountString);
		if (accountString == null)
			return null;
		return new Account(new JSONObject(accountString));
	}

	public static String getEmailFromSP(SharedPreferences sp) throws JSONException{
		Account account = getAccountFromSP(sp);
		return account == null ? null : account.email;
	}

	public String toStringForUrl(){

		String theURL = null;
		try {
			theURL = ConnectionConstants.PARAM_PSEUDO + URLEncoder.encode(this.getPseudo(), ConnectionConstants.UTF8)
					+ ConnectionConstants.PARAM_EMAIL + this.getEmail()
					+ ConnectionConstants.PARAM_PASSWORD + URLEncoder.encode(this.getPassword(), ConnectionConstants.UTF8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		String code = this.getCode();
		if (code != null)
			theURL += ConnectionConstants.PARAM_CODE + code;

		return theURL;
	}
}
