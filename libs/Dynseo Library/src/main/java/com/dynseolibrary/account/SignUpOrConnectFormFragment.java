package com.dynseolibrary.account;

import com.dynseolibrary.platform.SignUpOrConnectInterface;

import android.graphics.Typeface;

abstract public class SignUpOrConnectFormFragment extends GenericFormFragment {

	SignUpOrConnectInterface requester;

	public SignUpOrConnectFormFragment() {
	}
	public SignUpOrConnectFormFragment(SignUpOrConnectInterface signConnect, Typeface anAppStyle, boolean isAccountMandatory) {
		super(anAppStyle, isAccountMandatory);
		requester = signConnect; 
	}
}
