package com.dynseolibrary.account;

import com.example.dynseolibrary.R;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import android.graphics.Typeface;
import android.os.Bundle;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

abstract public class GenericFormFragment extends DialogFragment {

//	SignUpOrConnectInterface requester;
	
	Typeface buttonAppStyle;
	boolean accountIsMandatory;

	Dialog formDialog;
	TextView formErrorTv;
	Button submitButton;

	public GenericFormFragment(Typeface anAppStyle, boolean isAccountMandatory) {
		
//		requester = signConnect; 
		buttonAppStyle = anAppStyle;
		accountIsMandatory = isAccountMandatory;
	}

    public GenericFormFragment() {

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	}
	 
	protected void setDialog(Bundle savedInstanceState, int dialogLayout, int buttonId) {
		
		formDialog = new Dialog(getActivity(), R.style.DialogFullscreen);
		formDialog.setContentView(dialogLayout);
		formDialog.setCanceledOnTouchOutside(false);

	    //========== Configure the button
        submitButton = (Button) formDialog.findViewById(buttonId);
        if(buttonAppStyle != null)
        	submitButton.setTypeface(buttonAppStyle);
        
        //========== Initialize the error text fields
    	formErrorTv = (TextView) formDialog.findViewById(R.id.form_error_msg);
	}

	protected void setTextFields(EditText[] formEditTexts) {

		//==== To clean the error text view when we start to edit 
		for (int i = 0; i < formEditTexts.length; i++) {
			formEditTexts[i].addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
					int before, int count) {
					if (formErrorTv.getVisibility() == View.VISIBLE)
						formErrorTv.setVisibility(View.GONE);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
				}
			});
		}
	}
	
	public void showFormErrorMsg(String anErrorMsg) {

		if (formErrorTv.getVisibility() == View.GONE)
			formErrorTv.setVisibility(View.VISIBLE);
		formErrorTv.setText(Html.fromHtml(anErrorMsg));
		formErrorTv.setMovementMethod(LinkMovementMethod.getInstance());
		Animation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(50); // You can manage the blinking time with this
								// parameter
		anim.setStartOffset(20);
		anim.setRepeatMode(Animation.REVERSE);
		anim.setRepeatCount(Animation.RELATIVE_TO_PARENT);
		formErrorTv.startAnimation(anim);
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onCancel(dialog);
		//getActivity().finish();
	}
}
