package com.dynseolibrary.account;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.dynseolibrary.platform.ConsumeCodeInterface;
import com.dynseolibrary.platform.ErrorCodeInterface;
import com.example.dynseolibrary.R;

/**
 * Created by Viet on 27/06/2016.
 */
public class RetrieveCodeFragment extends GenericFormFragment {
    ConsumeCodeInterface requester;

    public RetrieveCodeFragment() {
        super();
    }

    @SuppressLint("ValidFragment")
    public RetrieveCodeFragment(ConsumeCodeInterface requester, Typeface anAppStyle, boolean isAccountMandatory) {
        super(anAppStyle, isAccountMandatory);
        this.requester = requester;
    }


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        setDialog(savedInstanceState, R.layout.dialog_retrieve_code, R.id.button_confirm);
        final EditText emailText 	= (EditText) formDialog.findViewById(R.id.input_email);
        final EditText numClientText = (EditText) formDialog.findViewById(R.id.input_num_client);
        EditText[] connectEditTexts = {emailText, numClientText};
        setTextFields(connectEditTexts);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email 	= emailText.getText().toString().trim();
                String numClient = numClientText.getText().toString().trim();

                if( email.equals("") || numClient.equals("") ) {

                    requester.onError(ErrorCodeInterface.ERROR_FIELD_REQUIRED);
                }
                else {
                    requester.retrieveCode(email, numClient);
                    formDialog.dismiss();
                }
                return;
            }
        });

        Button cancelButton = (Button) formDialog.findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                formDialog.dismiss();
            }
        });

        return formDialog;
    }
}
