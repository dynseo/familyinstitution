package com.dynseolibrary.account;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.ConsumeCodeInterface;
import com.dynseolibrary.platform.ErrorCodeInterface;
import com.example.dynseolibrary.R;

/**
 * Created by Viet on 20/05/2016.
 */
public class ConsumeCodeFragment extends GenericFormFragment {
    String TAG = "ConsumeCodeFragment";
    ConsumeCodeInterface requester;

    public ConsumeCodeFragment() {
        super();
    }

    @SuppressLint("ValidFragment")
    public ConsumeCodeFragment(ConsumeCodeInterface requester, Typeface anAppStyle, boolean isAccountMandatory) {
        super(anAppStyle, isAccountMandatory);
        this.requester = requester;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG,"onCreateDialog");
        setDialog(savedInstanceState, R.layout.dialog_consume_code, R.id.button_confirm);

        boolean skipAccountConnection = AppManager.getAppManager().skipAccountConnection();

        final EditText code = (EditText) formDialog.findViewById(R.id.input_code);
        final EditText password = (EditText) formDialog.findViewById(R.id.input_password);
        TextView lostCode = (TextView) formDialog.findViewById(R.id.retrieveCode);
        lostCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requester.doRetrieveCode();
            }
        });
        formErrorTv = (TextView) formDialog.findViewById(R.id.form_error_msg);

        //Setting all caps to code for EA Only
        if ( password.getVisibility() == View.VISIBLE )
            code.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        Button cancelButton = (Button) formDialog.findViewById(R.id.button_cancel);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringCode = code.getText().toString();
                String stringPassword = password.getText().toString();

                //If the password field is shown: meaning we are in the EA procedure
                if (stringCode.equals("") || (password.getVisibility() == View.GONE && stringPassword.equals("") ) ) {
                    requester.onError(ErrorCodeInterface.ERROR_FIELD_REQUIRED);
                } else {
                    requester.consumeCode(stringCode, stringPassword);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                getActivity().finishAffinity();
            }
        });
        return formDialog;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCancel(DialogInterface dialog) {
        getActivity().finishAffinity();
    }
}