package com.dynseolibrary.account;

import com.dynseolibrary.platform.SignUpOrConnectInterface;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.graphics.Typeface;
import android.os.Bundle;

import android.view.View;
import android.widget.RadioGroup;

public class InstitutionOrIndividualFragment extends SignUpOrConnectFormFragment {

	public InstitutionOrIndividualFragment() {
	}
	@SuppressLint("ValidFragment")
	public InstitutionOrIndividualFragment(
			SignUpOrConnectInterface signConnect, Typeface aAppStyle,
			boolean isAccountMandatory) {

		super(signConnect, aAppStyle, isAccountMandatory);
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		setDialog(savedInstanceState,
				R.layout.dialog_institution_or_individual, R.id.validate_btn);

		// ========== Configure the actions on buttons
		final RadioGroup group = (RadioGroup) formDialog
				.findViewById(R.id.radiogroup);
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				Log.d("radioCheckedBtn",
//						"radioCheckedBtn : " + group.getCheckedRadioButtonId());
				int radioButtonID = group.getCheckedRadioButtonId();
				View radioButton = group.findViewById(radioButtonID);
				int radioButtonIndex = group.indexOfChild(radioButton);
				if (radioButtonID == -1) {
					showFormErrorMsg(getString(R.string.choose_option));
				} else {
					if (radioButtonIndex == 0)
						// sign up as particular
						requester.doSignUp(false);
					else
						// sign up as institution
						requester.doSignUp(true);

					formDialog.dismiss();
				}
			}
		});

		return formDialog;
	}
}
