package com.dynseolibrary.account;

import com.dynseolibrary.platform.SignUpOrConnectInterface;
import com.dynseolibrary.tools.Tools;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import android.graphics.Typeface;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SignUpOrConnectFragment extends DialogFragment {

	SignUpOrConnectInterface requester;
	Typeface buttonAppStyle;
	boolean accountIsMandatory;

	public SignUpOrConnectFragment() {}

	@SuppressLint("ValidFragment")
	public SignUpOrConnectFragment(SignUpOrConnectInterface signConnect,
			Typeface aAppStyle, boolean isAccountMandatory) {

		requester = signConnect;
		buttonAppStyle = aAppStyle;
		accountIsMandatory = isAccountMandatory;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		Dialog d = new Dialog(getActivity(), R.style.DialogFullscreen);
		d.setContentView(R.layout.dialog_sign_up_connect);
		d.setCanceledOnTouchOutside(false);

		Button connectButton = (Button) d.findViewById(R.id.dialog_connect);
		Button signUpButton = (Button) d.findViewById(R.id.dialog_sign);
		Button visitButton = (Button) d.findViewById(R.id.dialog_visit);

		if (!Tools.isResourceTrue(getActivity().getApplicationContext(),
				R.string.visit_mode))
			visitButton.setVisibility(View.GONE);

		// ========== Configure the actions on buttons
		connectButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				requester.doConnect();
			}
		});
		signUpButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// ask if institution or not
				requester.doSelectTypeForSignUp(false);
			}
		});
		visitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// visit Mode
				requester.doVisit();
			}
		});

		return d;
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onCancel(dialog);
		getActivity().finish();
	}


}
