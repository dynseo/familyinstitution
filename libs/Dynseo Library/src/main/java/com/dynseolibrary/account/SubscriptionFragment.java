/**
 HOUSSEM
 */
package com.dynseolibrary.account;

import com.dynseolibrary.platform.SubscriptionInterface;
import com.dynseolibrary.tools.Tools;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SubscriptionFragment extends DialogFragment {

	SubscriptionInterface requester;

	Dialog dialog;
	Button cancelButton;
	Button submitButton;

	String theTitle;
	String theMessage;
	String theButton;

	public SubscriptionFragment() {
	}

	@SuppressLint("ValidFragment")
	public SubscriptionFragment(SubscriptionInterface aRequester) {
		super();
		requester = aRequester;
	}
	@SuppressLint("ValidFragment")
	public SubscriptionFragment(SubscriptionInterface aRequester, String textForTitle, String textForMessage, String textForButton) {
		this(aRequester);
		theTitle = textForTitle;
		theMessage = textForMessage;
		theButton = textForButton;
	}

	public void setTexts(String textForTitle, String textForMessage, String textForButton) {
		theTitle = textForTitle;
		theMessage = textForMessage;
		theButton = textForButton;

		if( dialog != null ) {
			if( theTitle != null )
				((TextView)dialog.findViewById(R.id.title_subscription)).setText(theTitle);

			if( theMessage != null )
				((TextView)dialog.findViewById(R.id.form_error_msg)).setText(theMessage);

			if( theButton != null )
				submitButton.setText(theButton);
		}
	}
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	protected void setDialog(Bundle savedInstanceState, int dialogLayout, int buttonId) {

		dialog = new Dialog(getActivity(), R.style.DialogFullscreen);
		dialog.setContentView(dialogLayout);
		dialog.setCanceledOnTouchOutside(false);

		//========== Configure the button
		submitButton = (Button)dialog.findViewById(buttonId);

		if( theTitle != null )
			((TextView)dialog.findViewById(R.id.title_subscription)).setText(theTitle);

		if( theMessage != null )
			((TextView)dialog.findViewById(R.id.form_error_msg)).setText(theMessage);

		if( theButton != null )
			submitButton.setText(theButton);
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		setDialog(savedInstanceState, R.layout.dialog_subscribe, R.id.button_confirm);

		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Context context = getActivity().getApplicationContext();
				if (Tools.isResourceTrue(context, R.string.needsCode)) {
					requester.onSubscriptionAccepted(2,null);
				}
				else
					requester.onSubscriptionAccepted(0,null);
			}
		});

		cancelButton = (Button) dialog.findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				requester.onSubscriptionIgnored();
			}
		});

		return dialog;
	}

	public void onSuccessOrFailure(String msg){

		//formErrorTv.setMovementMethod(LinkMovementMethod.getInstance());
		//
		((TextView)dialog.findViewById(R.id.form_error_msg)).setText(msg);
		cancelButton.setVisibility(View.GONE);
		submitButton.setText(getString(R.string.close));
		submitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requester.onSubscriptionIgnored();
			}
		});
	}

	public void onTerminal(String msg) {
		onSuccessOrFailure(msg);
		ImageView iconJoe = (ImageView) dialog.findViewById(R.id.icon_Joe);
		iconJoe.setVisibility(View.VISIBLE);

		iconJoe.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.dynseo.stimart.joe"));
				Log.d("go to second App",""+intent);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getDialog().setOnKeyListener(new OnKeyListener()
		{
			@Override
			public boolean onKey(android.content.DialogInterface dialog, int keyCode,android.view.KeyEvent event) {

				if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK))
				{
					//Hide your keyboard here!!!
					requester.onSubscriptionIgnored();
					return true; // pretend we've processed it
				}
				else
					return false; // pass on to be processed as normal
			}
		});
	}
}
