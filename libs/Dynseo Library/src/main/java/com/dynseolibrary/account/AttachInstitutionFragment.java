package com.dynseolibrary.account;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;
import com.dynseolibrary.tools.Tools;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.graphics.Typeface;
import android.os.Bundle;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AttachInstitutionFragment extends SignUpOrConnectFormFragment {
	
	EditText signUpEmail;
	EditText signUpInstitutionName;
	EditText signUpInstitutionCity;
	EditText signUpMessage;
	CheckBox signUpCheckBoxCGU;
	TextView signUpLinkCGU;	
	Button cancelButton;
	
	boolean isFinished;

	public AttachInstitutionFragment() {}

	@SuppressLint("ValidFragment")
	public AttachInstitutionFragment(SignUpOrConnectInterface signConnect,
									 Typeface aAppStyle, boolean isAccountMandatory) {
		super(signConnect, aAppStyle, isAccountMandatory);
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		setDialog(savedInstanceState, R.layout.dialog_attach_institution,
				R.id.button_confirm);

		// ========== Initialize the text fields		
		signUpEmail = (EditText) formDialog.findViewById(R.id.input_sign_up_email);
		signUpInstitutionName = (EditText) formDialog.findViewById(R.id.input_sign_up_institution_name);
		signUpInstitutionCity = (EditText) formDialog.findViewById(R.id.input_sign_up_institution_city);
		signUpMessage = (EditText) formDialog.findViewById(R.id.input_sign_up_message);
		
		signUpCheckBoxCGU = (CheckBox) formDialog.findViewById(R.id.input_sign_up_legal);
		signUpLinkCGU = (TextView) formDialog.findViewById(R.id.cgu_link);
		signUpLinkCGU.setMovementMethod(LinkMovementMethod.getInstance());
		
		// ==== To clean the error text view when we start to edit
		EditText[] signUpEditTexts = { signUpEmail, signUpInstitutionName,
				signUpInstitutionCity, signUpMessage};
		setTextFields(signUpEditTexts);

		// // ========== Configure the actions on buttons
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				if (isFinished)
				{
					requester.onAttachInstitutionSuccess(1, null);
					
				}
				else{

					String email = signUpEmail.getText().toString().trim();
					String institutionName = signUpInstitutionName.getText().toString().trim();
					String institutionCity = signUpInstitutionCity.getText().toString().trim();
					String message = signUpMessage.getText().toString().trim();
	
					if (email.equals("")
							|| institutionName.equals("") || institutionCity.equals("")) {
						requester.onError(ErrorCodeInterface.ERROR_FIELD_REQUIRED);
					} else if (!Tools.isEmailValid(email)) {
						requester.onError(ErrorCodeInterface.ERROR_EMAIL_FORMAT);
					} else if (!signUpCheckBoxCGU.isChecked()) {
						requester.onError(ErrorCodeInterface.ERROR_ACCEPT_CGU);
					} else {
						// send institution attachment demand
						requester.attachToInstitution(email, institutionName, institutionCity, message);
					}
				}
				return;
			}
		});

		cancelButton = (Button) formDialog.findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				formDialog.dismiss();
			}
		});

		return formDialog;
	}
	
	public void onSuccess(){
		RelativeLayout content = (RelativeLayout) formDialog.findViewById(R.id.content);
		content.setVisibility(View.GONE);
		cancelButton.setVisibility(View.GONE);
		TextView successTv = (TextView) formDialog.findViewById(R.id.success_tv);
		successTv.setVisibility(View.VISIBLE);
		isFinished = true;
	}
}
