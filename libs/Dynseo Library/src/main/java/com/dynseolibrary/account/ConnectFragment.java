package com.dynseolibrary.account;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.graphics.Typeface;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ConnectFragment extends SignUpOrConnectFormFragment {

	EditText connectPseudo;
	EditText connectPassword;
	
//	Button cancelButton;
//	boolean isFinished;
	public ConnectFragment() {}
	@SuppressLint("ValidFragment")
	public ConnectFragment(SignUpOrConnectInterface signConnect, Typeface aAppStyle, boolean isAccountMandatory) {
		
		super(signConnect, aAppStyle, isAccountMandatory);
	}
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	}
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	 
		setDialog(savedInstanceState, R.layout.dialog_connect, R.id.button_confirm);
		
        //========== Initialize the text fields
        connectPseudo 	= (EditText) formDialog.findViewById(R.id.input_connect_email_or_pseudo);
    	connectPassword = (EditText) formDialog.findViewById(R.id.input_connect_password);
    	
    	//formErrorTv = (TextView) dialogView.findViewById(R.id.form_error_msg);
		
		//==== To clean the error text view when we start to edit 
    	EditText[] connectEditTexts = {connectPseudo, connectPassword};
    	setTextFields(connectEditTexts); 
  
        //========== Configure the actions on buttons
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	
            	String pseudo 	= connectPseudo.getText().toString().trim();
            	String password = connectPassword.getText().toString().trim();

            	if( pseudo.equals("") || password.equals("") ) {
            		
            		requester.onError(ErrorCodeInterface.ERROR_FIELD_REQUIRED);
            	}
            	else {
            		requester.logIn(pseudo, password);
            	}
            	return;
            }
        });
        
        Button cancelButton = (Button) formDialog.findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				formDialog.dismiss();
			}
		});
       
	    return formDialog;
	}
	
	public void onSuccess(){
		formDialog.dismiss();
	}
}
