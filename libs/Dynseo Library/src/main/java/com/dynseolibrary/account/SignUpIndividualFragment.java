package com.dynseolibrary.account;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;
import com.dynseolibrary.tools.Tools;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.graphics.Typeface;
import android.os.Bundle;

import android.os.StrictMode;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SignUpIndividualFragment extends SignUpOrConnectFormFragment {

	public final static int PASSWORD_MIN_LENGTH = 6;
	
	EditText signUpPseudo;
	EditText signUpEmail;
	EditText signUpPassword;
	EditText signUpPasswordConfirm;
    EditText signUpCode;
	CheckBox signUpCheckBoxCGU;
	TextView signUpLinkCGU;
	Button cancelButton;
	
	EditText[] formEditTexts;
	boolean isFinished;

	public SignUpIndividualFragment() {

	}

	@SuppressLint("ValidFragment")
	public SignUpIndividualFragment(SignUpOrConnectInterface signConnect,
									Typeface aAppStyle, boolean isAccountMandatory) {
		super(signConnect, aAppStyle, isAccountMandatory);
		isFinished = false;

	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		setDialog(savedInstanceState, R.layout.dialog_sign_up_individual,
				R.id.button_confirm);

		// ========== Initialize the text fields
		signUpPseudo = (EditText) formDialog.findViewById(R.id.input_sign_up_pseudo);
		signUpEmail = (EditText) formDialog.findViewById(R.id.input_sign_up_email);
		signUpPassword = (EditText) formDialog.findViewById(R.id.input_sign_up_password);
		signUpPasswordConfirm = (EditText) formDialog.findViewById(R.id.input_sign_up_password_confirm);

		signUpCheckBoxCGU = (CheckBox) formDialog.findViewById(R.id.input_sign_up_legal);
		signUpLinkCGU = (TextView) formDialog.findViewById(R.id.cgu_link);
		formErrorTv = (TextView) formDialog.findViewById(R.id.form_error_msg);
		signUpLinkCGU.setMovementMethod(LinkMovementMethod.getInstance());

        // ==== To clean the error text view when we start to edit
        formEditTexts = new EditText [] {signUpPseudo, signUpEmail, signUpPassword, signUpPasswordConfirm} ;


//        EditText[] formEdts = { signUpPseudo, signUpEmail,
//                signUpPassword, signUpPasswordConfirm };

        final int checkExistence = this.getResources().getIdentifier("input_sign_up_code", "id", this.getActivity().getPackageName());
        if (checkExistence != 0) {
            signUpCode = (EditText) formDialog.findViewById(R.id.input_sign_up_code);

            formEditTexts = new EditText[]{ signUpPseudo, signUpEmail, signUpPassword, signUpPasswordConfirm, signUpCode };
        }

		//formEditTexts = formEdts;
		setTextFields(formEditTexts);

		// // ========== Configure the actions on buttons
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (isFinished)
				{
					requester.onCreateAccountSuccess(1, null);
					
				}
				else{
					String pseudo = signUpPseudo.getText().toString().trim();
					String email = signUpEmail.getText().toString().trim();
					String password = signUpPassword.getText().toString().trim();
					String passwordConfirm = signUpPasswordConfirm.getText().toString().trim();
                    String code = null;
                    if (signUpCode != null)
                        code = signUpCode.getText().toString().trim();

					if (pseudo.equals("") || email.equals("")
							|| password.equals("") || passwordConfirm.equals("")) {
						requester.onError(ErrorCodeInterface.ERROR_FIELD_REQUIRED);
					}
                    else if (!password.equals(passwordConfirm)) {
						requester.onError(ErrorCodeInterface.ERROR_NOT_IDENTICAL_PASSWORD);
					}
                    else if(password.length() < PASSWORD_MIN_LENGTH){
						requester.onError(ErrorCodeInterface.ERROR_PASSWORD_WEEK);
					}
                    else if (!Tools.isEmailValid(email)) {
						requester.onError(ErrorCodeInterface.ERROR_EMAIL_FORMAT);
					}
                    else if (!signUpCheckBoxCGU.isChecked()) {
						requester.onError(ErrorCodeInterface.ERROR_ACCEPT_CGU);
					}
                    else if (signUpCode != null && (code == null || code.equals(""))) {
                        requester.onError(ErrorCodeInterface.ERROR_FIELD_REQUIRED);
                    }
                    else {
						requester.createAccount(new Account(email, pseudo, password, code));
					}
				}
				return;
			}
		});

		cancelButton = (Button) formDialog
				.findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				formDialog.dismiss();
			}
		});

		return formDialog;
	}
	
	public void onSuccess(){
		RelativeLayout content = (RelativeLayout) formDialog.findViewById(R.id.content);
		content.setVisibility(View.GONE);
		cancelButton.setVisibility(View.GONE);
		LinearLayout successTvs = (LinearLayout) formDialog.findViewById(R.id.success_tvs);
		successTvs.setVisibility(View.VISIBLE);
		isFinished = true;
	}
}
