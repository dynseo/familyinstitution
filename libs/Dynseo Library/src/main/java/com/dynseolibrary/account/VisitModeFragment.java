/**
HOUSSEM
*/
package com.dynseolibrary.account;

import com.dynseolibrary.platform.VisitModeInterface;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class VisitModeFragment extends  GenericFormFragment {

	VisitModeInterface requester;
	
	Button cancelButton;

	public VisitModeFragment() {}
	@SuppressLint("ValidFragment")
	public VisitModeFragment(VisitModeInterface aRequester, Typeface anAppStyle, boolean isAccountMandatory) {
		super(anAppStyle, isAccountMandatory);
		requester = aRequester; 
	}
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	}
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		setDialog(savedInstanceState, R.layout.dialog_visit_mode, R.id.button_confirm);
		
		submitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requester.onVisitModeQuitApp();
			}
		});
		
		cancelButton = (Button) formDialog.findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				formDialog.dismiss();	
				requester.onVisitModeBackToMenu();
				}
		});
		
		return formDialog;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		 getDialog().setOnKeyListener(new OnKeyListener()
		    {
		        @Override
		        public boolean onKey(android.content.DialogInterface dialog, int keyCode,android.view.KeyEvent event) {

		            if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK))
		                {
		            		requester.onVisitModeBackToMenu();
		                     return true; // pretend we've processed it
		                }
		            else 
		                return false; // pass on to be processed as normal
		        }
		    });
	}
}
