package com.dynseolibrary.platform.server;

import android.os.AsyncTask;
import android.util.Log;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.GenericServerResponseInterface;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alexis on 13/09/2016.
 */
public class SendRegistrationTokenTask extends AsyncTask<Void, Void, String> {
    String TAG = "SendRegistrationTokenTask";

    GenericServerResponseInterface requester;
    String notificationToken;
    boolean isUpdate;

    public SendRegistrationTokenTask(GenericServerResponseInterface requester, String notificationToken, boolean isUpdate) {
        this.requester = requester;
        this.notificationToken = notificationToken;
        this.isUpdate = isUpdate;
    }

    @Override
    protected String doInBackground(Void... voids) {
        Log.d(TAG, "doInBackground()");
        try {
            String path = ConnectionConstants.urlSendRegistrationToken(notificationToken);
//            String path;
//            path = isUpdate ? ConnectionConstants.urlUpdateRegistrationToken(notificationToken) : ConnectionConstants.urlSendRegistrationToken(notificationToken);

            Log.i(TAG, "path = " + path);
            return Http.queryServer(path);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        JSONObject jsonResult;

        int returnCode;
        Log.d("result of serviece sendRegistrationToken" , result);
        try {
            jsonResult = new JSONObject(result);
            Log.e(TAG, " result : " + jsonResult);

            returnCode = ErrorCodeInterface.SUCCESS;

            String error = jsonResult.optString("error");
            if (!error.equals("")) {
                returnCode = ErrorCodeInterface.ERROR_UNDEFINED;
                requester.onFailure(jsonResult, returnCode);
            } else {
                requester.onSuccess(jsonResult);
            }
            Log.e(TAG, result);
            Log.e(TAG, " returnCode : " + returnCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
