package com.dynseolibrary.platform.server;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.ConsumeCodeInterface;
import com.dynseolibrary.platform.ErrorCodeInterface;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Viet on 20/05/2016.
 */
public class ConsumeCodeTask extends AsyncTask<Void, Void, String> {
    public static final String TAG = "ConsumeCodeTask";
    protected ConsumeCodeInterface requester;
    protected String code;

    private String password;

    public ConsumeCodeTask(ConsumeCodeInterface enterKeyInterface, String code, String password) {
        this(enterKeyInterface,code);
        this.password = password;
    }

    public ConsumeCodeTask(ConsumeCodeInterface enterKeyInterface, String code) {
        this.requester = enterKeyInterface;
        this.code = code;
    }
    @Override
    protected String doInBackground(Void... params) {
        Log.d(TAG,"doInBackground()");
        String path = getURL();
        Log.i(TAG, "path = " + path);
        return Http.queryServer(path);
    }

    private String getURL(){
        try{
            if( password != null )
                return  ConnectionConstants.urlConsumeCode(code,password);
            return ConnectionConstants.urlConsumeCode(code);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG,"onPostExecute()");

        try {
            Log.d(TAG, "result is : " + result);
            JSONObject jsonObject = new JSONObject(result);

            int returnCode;
            String error = jsonObject.optString(ConnectionConstants.ERROR);
            if ( !error.equals("")) {

                switch (error) {
                    case "createAccountFailed" :
                        //For EA this return error represents all the case of account creation failure : duplicateUserPseudo, Mail, Device etc...
                        returnCode = ErrorCodeInterface.ERROR_ACCOUNT_CREATION_FAILED;
                        break;
                    case "serverDown" :
                        returnCode = ErrorCodeInterface.ERROR_DISTANT_SERVER_DOWN;
                        break;
                    case "invalidCode":
                        returnCode = ErrorCodeInterface.ERROR_INVALID_CODE;
                        break;
                    case "alreadyUsedCode":
                        returnCode = ErrorCodeInterface.ERROR_ALREADY_USED_CODE;
                        break;
                    case "alreadyUsedCodeByMe":
                        returnCode = ErrorCodeInterface.ERROR_ALREADY_USED_CODE_BY_ME;
                        break;
                    case "duplicateUserEmail":
                        returnCode = ErrorCodeInterface.ERROR_DUPLICATE_EMAIL;
                        break;
                    case "duplicateUserPseudo":
                        returnCode = ErrorCodeInterface.ERROR_DUPLICATE_PSEUDO;
                        break;
                    default:
                        returnCode = ErrorCodeInterface.ERROR_UNDEFINED;
                        break;
                }
                requester.onError(returnCode);
            }
            else {
                requester.onConsumeCodeSuccess(jsonObject);
            }
        }
        catch (JSONException e) {
            Log.e(TAG,"instanciate json exception");
        }
    }
}
