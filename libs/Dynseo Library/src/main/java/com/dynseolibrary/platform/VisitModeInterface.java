package com.dynseolibrary.platform;

public interface VisitModeInterface {
	
	void onVisitModeQuitApp();
	void onVisitModeBackToMenu();
}
