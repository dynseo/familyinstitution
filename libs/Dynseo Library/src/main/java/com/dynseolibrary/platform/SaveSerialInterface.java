package com.dynseolibrary.platform;

/**
 * Created by Maxime Gelly on 24/02/2017.
 */

public interface SaveSerialInterface {

    void onSuccess(String serial);
    void onError();

}
