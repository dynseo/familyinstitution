package com.dynseolibrary.platform;

import org.json.JSONObject;

/**
 * Created by Alexis on 13/09/2016.
 */
public interface GenericServerResponseInterface {

    void onSuccess(JSONObject jsonResult);
    void onFailure(JSONObject jsonResult, int errorCode);
}
