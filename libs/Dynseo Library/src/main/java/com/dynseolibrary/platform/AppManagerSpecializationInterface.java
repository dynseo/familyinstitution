/**
HOUSSEM
*/
package com.dynseolibrary.platform;

public interface AppManagerSpecializationInterface {

    boolean authorizeSynchro(String typeOfSynchro);    //To change standard behavior of authorizer

    boolean authorizePlayGame(String gameName);
}
