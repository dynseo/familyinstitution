package com.dynseolibrary.platform.server;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import android.util.Log;

/**
 * Created by Dominique on 24.06.2015.
 */
public class LogInTask extends AsyncTask<Void, Void, String> {

	private final static String LOG_TAG = "AsyncLogIn";

	JSONObject jsonResult;
	int returnCode;

	SignUpOrConnectInterface requester;

	String somePseudoOrEmail;
	String somePassword;
	String someToken;
	String someNotificationToken;


	public LogInTask(SignUpOrConnectInterface aRequester, String email, String password, String token, String notificationToken) {
		this(aRequester, email, password);
		this.someToken = token;
		this.someNotificationToken = notificationToken;
	}

	public LogInTask(SignUpOrConnectInterface aRequester, String aPseudo, String aPassword) {
		requester = aRequester;
		somePseudoOrEmail = aPseudo;
		somePassword = aPassword;
	}



	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(Void... voids) {
		try {
			jsonResult = null;
			String path;
			if (someToken == null && someNotificationToken == null)
				path = ConnectionConstants.urlLogIn(somePseudoOrEmail, somePassword);
			else
				path = ConnectionConstants.urlLogInForAppUser(somePseudoOrEmail, somePassword, someToken, someNotificationToken);


			Log.e(LOG_TAG, path);
			return Http.queryServer(path);
		}
		catch (Exception e) {
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		try {
			jsonResult = new JSONObject(result);

			returnCode = ErrorCodeInterface.SUCCESS;

			String error = jsonResult.optString("error");
			if( !error.equals("") ) {

				if( error.equals("UNKNOWN_LOGIN") || error.equals("userNotFound") )
					returnCode = ErrorCodeInterface.ERROR_USER_NOT_FOUND;
				else if( error.equals("WRONG_PASSWORD") )
					returnCode = ErrorCodeInterface.ERROR_WRONG_PASSWORD;
				else if( error.equals("NOT_STIMART_ACCOUNT") )
					returnCode = ErrorCodeInterface.ERROR_NOT_STIMART_ACCOUNT;
				else if (error.equals("alreadyUsedCodeByMe"))
					returnCode = ErrorCodeInterface.ERROR_ALREADY_USED_CODE_BY_ME;
				else
					returnCode = ErrorCodeInterface.ERROR_UNDEFINED;

				requester.onError(returnCode);
			}
			else {
				requester.onLoginSuccess(jsonResult);
			}
			Log.e(LOG_TAG, result);
			Log.e(LOG_TAG, " returnCode : " + returnCode);
		}
		catch(JSONException e) {
		}
	}

	public int getReturnCode() {
		return returnCode;
	}
	public JSONObject getJsonResult() {
		return jsonResult;
	}
}
