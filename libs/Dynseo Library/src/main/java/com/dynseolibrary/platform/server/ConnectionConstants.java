package com.dynseolibrary.platform.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.tools.Tools;



public class ConnectionConstants {

	/* For a production version, do not forget to check: BASE_URL and in the manifest
	 * 			versionCode, versionName and google map key
	 */

	public static final String DEFAULT_BASE_URL = "http://www.stimart.com/app?newPilotSession=true";
	public static final String DEFAULT_BASE_URL_POST = "http://www.stimart.com/pilot?service=";	//url used to send message with multipart

	public static String BASE_URL = DEFAULT_BASE_URL;
	public static String BASE_URL_POST = DEFAULT_BASE_URL_POST;

	public static void setBaseUrl(String baseUrl, String baseUrlPost) {
		BASE_URL  = baseUrl;
		BASE_URL_POST  = baseUrlPost;
	}

	/* for identify the correction app to download / update */
	public static final String PARAM_APP = "&app=";
	public static final String PARAM_APP_TYPE = "&appType=";
	public static final String PARAM_SERVICE = "&service=";
	public static final String PARAM_DEVICE_TYPE = "&deviceType=A";
	public static final String PARAM_DEVICE_FORMAT = "&deviceFormat=";
	public static final String PARAM_TOKEN = "&token=";
	public static final String PARAM_NOTIFICATION_TOKEN = "&notificationToken=";
	public static final String PARAM_NAME = "&name=";
	public static final String PARAM_FIRSTNAME  = "&firstname=";
	public static final String PARAM_CODE		= "&code=";
	public static final String PARAM_SERIAL_NUMBER = "&serialNumber=";
	public static final String PARAM_APP_LANG		= "&lang=";
	public static final String PARAM_APP_LANG_TYPE  = "&langType=";
	public static final String PARAM_DEVICE_LANG		= "&deviceLang=";
	public static final String PARAM_APP_MODE = "&mode=";
	public static final String PARAM_MODEL = "&model=";
	public static final String PARAM_BRAND = "&brand=";
	public static final String PARAM_APP_VERSION	= "&appVersion=";
	public static final String PARAM_APP_VERSION_CODE = "&versionCode=";
	public static final String PARAM_FORCE_TO_ROLE = "&forceToRole=";
	public static final String PARAM_CURRENT_VERSION = "&currentVersion=";
	public static final String PARAM_VISIT_MODE = "&visitMode=";
	public static final String PARAM_TARGET_VERSION  = "&targetVersion=";
	public static final String PARAM_OS_VERSION = "&osVersion=";
	public static final String PARAM_SEX = "&sex=";
	public static final String PARAM_BIRTHDATE = "&birthdate=";
	public static final String PARAM_EXIST_LOCAL = "&existLocal=";
	public static final String PARAM_SERVERID	 = "&serverId=";
	public static final String PARAM_GAME = "&game=";
	public static final String PARAM_DATE = "&date=";
	public static final String PARAM_PSEUDO = "&pseudo=";
	public static final String PARAM_EMAIL = "&email=";
	public static final String PARAM_PASSWORD = "&password=";
	public static final String PARAM_CITY = "&city=";
	public static final String PARAM_MESSAGE = "&message=";
	public static final String PARAM_FORMAT_OUTPUT_JSON = "&formatOutput=json";
	public static final String PARAM_TYPE = "&type=";
	public static final String PARAM_TYPE_RESOURCES = "&type=resources";
	public static final String PARAM_CONSUME_TRUE = "&consume=true";
	public static final String PARAM_CONSUME_RENEW = "&consume=renew";
	public static final String PARAM_EXTRA_DATA = "&extraData=";
	public static final String PARAM_APP_EXTENSION = "&appExtension=";
	public static final String PARAM_FORCE_DOWNLOAD_PHOTOS_PAINTINGS = "&forceDownloadPhotosPaintings=true";
	public static final String PARAM_FORCE_DOWNLOAD_MAPS = "&forceDownloadMaps=true";
	public static final String PARAM_FIRST_SERIAL = "&firstSerial=";
	public static final String PARAM_SECOND_SERIAL = "&secondSerial=";
	public static final String PARAM_ALL_SERIALS = "&allSerials=";
	public static final String PARAM_ID = "&id=";
	public static final String PARAM_ACTIVE = "&active=";

	/* for the first synchronization or when user delete his data */
	public static final String PARAM_SYNCHRO_DATE = "&synchroDate=";
	public static final String PARAM_DURATION 	  = "&duration=";
	public static final String PARAM_CONFLICT = "&conflictId=";

	public static final String VAL_DEVICE_TYPE 		= "A";
	public static final String VAL_ACTIVE_ACCOUNT 	= "A";
	public static final String VAL_INACTIVE_ACCOUNT	= "I";

	/* Services */
	private static final String VAL_CHECK_UPDATE = "checkAppUpdate";
	private static final String VAL_DOWNLOAD_UPDATE = "downloadUpdate";

	private static final String VAL_CHECK_RES_UPDATE = "checkResourcesUpdate";
	private static final String VAL_DOWNLOAD_RES_UPDATE = "downloadResources";
	private static final String VAL_GET_RES_FOR_APP = "getResourcesForApp";

	private static final String VAL_CHECK_OR_SET_DEVICE = "checkOrSetDevice";
	private static final String VAL_SYNCHRO_END 	= "synchroEnd";
	private static final String VAL_ACTIVATE_SUBSCRIPTION 	= "activateSubscription";

	private static final String VAL_CHECK_OR_CONSUME_CODE = "checkOrConsumeCode";
	private static final String VAL_RETRIEVE_CODE = "retrieveCode";
	private static final String VAL_SAVE_SERIALS = "saveSerials";

	public static final String PERSON_DELETED = "D";
	public static final String PERSON_UPDATED = "U";

	/*Account Services*/
	protected static final String VAL_CREATE_ACCOUNT = "createAccount";
	private static final String VAL_LOG_IN = "login";
	private static final String VAL_ATTACH_INSTITUTION 	= "attachToInstitution";
	private static final String VAL_SUBSCRIPTION 	= "askForPayment";
	private static final String VAL_ADD_NOTIFICATION_DEVICE = "addNotificationDevice";

	private static final String VAL_LOG_IN_APP_USER = "loginAppUser";
	public static final String SHARED_PREFS_APP_TOKEN 	 = "app_token";
	public static final String SHARED_PREFS_SYNCHRO_DATE		= "synchroDate";
	public static final String SHARED_PREFS_SYNCHRO_RES_DATE	= "synchroResDate";
	public static final String SHARED_PREFS_MODE				= "mode";
	public static final String SHARED_PREFS_INSTITUTION			= "institution";
	public static final String SHARED_PREFS_INSTITUTION_TYPE	= "type";
	public static final String SHARED_PREFS_LANG_ALTER				= "lang_alter";

	public static final String SHARED_PREFS_BEGIN_SUBSCRIPTION = "beginSubscription";
	public static final String SHARED_PREFS_END_SUBSCRIPTION   = "endSubscription";
	public static final String SHARED_PREFS_STATE_SUBSCRIPTION = "subscriptionState";
	public static final String SHARED_PREFS_CODE = "code";

	public static final String SHARED_PREFS_FIRST_LAUNCH       = "firstLaunch";
	public static final String SHARED_PREFS_UPDATE_RESOURCES_DATE = "updateResourcesDate";
	// SP for app rating
	public static final String SHARED_PREFS_DONT_SHOW_AGAIN		= "dontshowagain";
	public static final String SHARED_PREFS_LAUNCH_COUNT       	= "launch_count";
	public static final String SHARED_PREFS_DATE_FIRST_LAUNCH   = "date_firstlaunch";

	public static final String INDENTIFICATION_ERROR = "identification_error";
	public static final String SUBSCRIPTION_ERROR	 = "subscription_error";


	public static final int DEFAULT_SUBSCRIPTION_DURATION = 3 ;

	public static final String JSON_PARAM_STATUS	= "status";
	public static final String JSON_PARAM_ERROR		= "error";
	public static final String JSON_PARAM_BEGIN_SUBSCRIPTION 	= "beginDate" ;
	public static final String JSON_PARAM_SUBSCRIPTION_DURATION = "subscription" ;
	public static final String JSON_PARAM_DEVICE_NOT_EXISTS 	= "deviceNotExists";
	public static final String JSON_PARAM_RESULT_ID   = "resultId";
	public static final String JSON_PARAM_SERVER_ID   = "serverId";
	public static final String JSON_PARAM_CONFLICT_ID = "conflictId";
	public static final String JSON_PARAM_SOLVED	  = "solved";
	public static final String JSON_PARAM_FIRSTNAME   = "firstname";
	public static final String JSON_PARAM_NAME		  = "name";
	public static final String JSON_PARAM_ACTIVE	  = "active";
	public static final String JSON_PARAM_SEX		  = "sex";
	public static final String JSON_PARAM_PERSON_ROLE = "role";
	public static final String JSON_PARAM_BIRTHDATE = "birthdate";
	public static final String JSON_PARAM_UPDATE_NEEDED		= "updateNeeded";
	public static final String JSON_PARAM_NEW_APP_VERSION	= "versionNumber";
	public static final String JSON_PARAM_PERSON_ID = "personId";

	public static final String PARAM_INSTITUTION_NAME = "&institutionName=";
	public static final String PARAM_INSTITUTION_TYPE = "&institutionType=";
	public static final String PARAM_ADDRESS = "&address=";
	public static final String PARAM_ZIP = "&zipCode=";
	public static final String PARAM_PHONE_NUMBER = "&phone=";
	public static final String PARAM_CONTACT_NAME = "&contactName=";

	public static final String ERROR = "error";
	//	public static final String TOKEN = "token";
//	public static final String TRUE	 = "true";
//	public static final String FALSE = "false";
//	public static final String YES 	 = "YES";
//	public static final String NO 	 = "NO";
//	public static final String MERGE = "Merge";
//	public static final String SPLIT = "Split";
	public static final String UTF8	 = "UTF-8";
	public static int PARTICULAR = 0;
	public static int PROFESSIONAL = 1;
	public static int INSTITUTION = 2;

	public static final int RESULT_CODE_VISIT_MODE	 = 1;
	//public static final int RESULT_CODE_DO_SYNCHRO	 = 2;
	/*         Build URL methods                                                       */
	//================ Build the URL to check and set device =====================//
	public static String urlCheckAndSetDevice(SharedPreferences sharedPrefs) throws UnsupportedEncodingException{
		//parameters are serialNumber, appVersion, osVersion
		//optional parameters brand and model could be passed
		return urlCheckDevice(sharedPrefs)
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_APP_VERSION_CODE + AppManager.getAppManager().getVersionCode()
				+ PARAM_DEVICE_FORMAT + AppManager.getAppManager().getDeviceFormat()
				+ PARAM_OS_VERSION + Build.VERSION.RELEASE;
	}

	//================ Build the URL to check device =====================//
	public static String urlCheckDevice(SharedPreferences sharedPrefs) throws UnsupportedEncodingException{
		//parameters are serialNumber
		String theURL = BASE_URL + PARAM_SERVICE + VAL_CHECK_OR_SET_DEVICE
				//For the identification, we send the serials to server
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_MODEL + URLEncoder.encode(Build.MODEL, UTF8)
				+ PARAM_BRAND + URLEncoder.encode(Build.MANUFACTURER, UTF8);

		String freemiumName = AppManager.getAppManager().getFreemiumName();
		if (freemiumName != null)
			theURL += PARAM_APP_MODE + freemiumName;
		else {
			String mode = AppManager.getAppManager().getMode();
			if( mode.equals("") )		//This is the first call
				theURL += PARAM_APP_MODE + "firstLaunch";
			else {
				theURL += PARAM_APP_MODE + mode;

				if( mode.equals("visit") ) {    //This is the first call
					theURL += "&" + SHARED_PREFS_BEGIN_SUBSCRIPTION + "=" + sharedPrefs.getString(SHARED_PREFS_BEGIN_SUBSCRIPTION, "");
					theURL += "&" + SHARED_PREFS_END_SUBSCRIPTION + "=" + sharedPrefs.getString(SHARED_PREFS_END_SUBSCRIPTION, "");
				}
			}
		}
		return theURL;
	}

	//================ Build the URL to consume code =====================//

	public static String urlConsumeCode(String code) throws UnsupportedEncodingException{
		return urlConsumeCode(code,null);
	}

	public static String urlConsumeCode(String code, String password) throws UnsupportedEncodingException{
		//parameters are serialNumber
		String theURL = BASE_URL + PARAM_SERVICE + VAL_CHECK_OR_CONSUME_CODE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_APP_VERSION_CODE + AppManager.getAppManager().getVersionCode()
				+ PARAM_DEVICE_TYPE
				+ PARAM_CODE + code
				+ PARAM_FORMAT_OUTPUT_JSON
				+ PARAM_CONSUME_TRUE;

		if (password != null)
			if (!password.equals(""))
				theURL+= PARAM_PASSWORD + password;

		return theURL;
	}

	//================ Build the URL to renew code =====================//
	public static String urlRenewCode(String code) throws UnsupportedEncodingException{
		//parameters are serialNumber
		String theURL = BASE_URL + PARAM_SERVICE + VAL_CHECK_OR_CONSUME_CODE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_APP_VERSION_CODE + AppManager.getAppManager().getVersionCode()
				+ PARAM_DEVICE_TYPE
				+ PARAM_CODE + code
				+ PARAM_FORMAT_OUTPUT_JSON
				+ PARAM_CONSUME_RENEW;

		return theURL;
	}


	//================ Build the URL to retrieve code =====================//
	public static String urlRetrieveCode(String email, String extraData) throws UnsupportedEncodingException{
		//parameters are serialNumber
		String theURL = BASE_URL + PARAM_SERVICE + VAL_RETRIEVE_CODE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_APP_VERSION_CODE + AppManager.getAppManager().getVersionCode()
				+ PARAM_DEVICE_TYPE
				+ PARAM_EMAIL + email
				+ PARAM_EXTRA_DATA + URLEncoder.encode(extraData, UTF8)
				+ PARAM_FORMAT_OUTPUT_JSON;

		return theURL;
	}


	//================ Build the URL to create account =====================//
	public static String urlCreateAccount(Account anAccount, SharedPreferences sharedPrefs, String serviceName)
			throws UnsupportedEncodingException {

		// String pseudo, email, password
		String theURL =  BASE_URL + PARAM_SERVICE + serviceName
				+ PARAM_SERIAL_NUMBER +  AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_DEVICE_TYPE
				+ anAccount.toStringForUrl()
				+ PARAM_FORMAT_OUTPUT_JSON;

		Log.d("log", theURL);
		String mode = AppManager.getAppManager().getFreemiumName();
		if (mode != null)
			theURL += PARAM_APP_MODE + mode;

		//Server will ignore if empty strings and will associate the dates to a device if dates exist
		theURL += "&" + SHARED_PREFS_BEGIN_SUBSCRIPTION + "=" + sharedPrefs.getString(SHARED_PREFS_BEGIN_SUBSCRIPTION, "");
		theURL += "&" + SHARED_PREFS_END_SUBSCRIPTION + "=" + sharedPrefs.getString(SHARED_PREFS_END_SUBSCRIPTION, "");
		return theURL;
	}




	//================ Build the URL to log to an account on the server =====================//
	public static String urlLogIn(String pseudo, String password)
			throws UnsupportedEncodingException  {
		return 	BASE_URL + PARAM_SERVICE + VAL_LOG_IN
				+ PARAM_SERIAL_NUMBER +  AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_DEVICE_TYPE
				+ PARAM_EMAIL + URLEncoder.encode(pseudo, UTF8) // this could be pseudo or password
				+ PARAM_PASSWORD + URLEncoder.encode(password, UTF8)
				+ PARAM_FORMAT_OUTPUT_JSON;
	}

	public static String urlLogInForAppUser(String email, String password, String token, String notificationToken)
			throws UnsupportedEncodingException  {
		return 	BASE_URL + PARAM_SERVICE + VAL_LOG_IN_APP_USER
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_DEVICE_TYPE
				+ PARAM_PSEUDO + URLEncoder.encode(email, UTF8) // this could be pseudo or password
				+ PARAM_PASSWORD + URLEncoder.encode(password, UTF8)
				+ PARAM_TOKEN + token
				+ PARAM_NOTIFICATION_TOKEN + notificationToken
				+ PARAM_FORMAT_OUTPUT_JSON;

	}

	//================ Build the URL to attach device to institution =====================//
	public static String urlAttachToInstitution(String email, String name, String city, String message)
			throws UnsupportedEncodingException  {
		String theURL = 	BASE_URL + PARAM_SERVICE + VAL_ATTACH_INSTITUTION
				+ PARAM_SERIAL_NUMBER +  AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_DEVICE_TYPE
				+ PARAM_EMAIL + email
				+ PARAM_NAME + URLEncoder.encode(name, UTF8)
				+ PARAM_CITY + URLEncoder.encode(city, UTF8);

		if( message != null )
			theURL += PARAM_MESSAGE + URLEncoder.encode(message, UTF8);

		return theURL;
	}

	public static String urlSubscription(String email){
		return	BASE_URL + PARAM_SERVICE +  VAL_SUBSCRIPTION
				+ PARAM_SERIAL_NUMBER +  AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_EMAIL + email;

	}

	//================ Build the URL to check or download the app =====================//
	public static String urlCheckAppUpdate() {
		//parameters are serialNumber, app, lang, appVersion
		return BASE_URL + PARAM_SERVICE + VAL_CHECK_UPDATE
				+ PARAM_SERIAL_NUMBER +AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_APP_VERSION_CODE + AppManager.getAppManager().getVersionCode()
				+ PARAM_DEVICE_TYPE;
	}

	public static String urlDownloadAppUpdate(String targetVersion) {
		//parameters are serialNumber, app, lang, appVersion
		return BASE_URL + PARAM_SERVICE + VAL_DOWNLOAD_UPDATE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP +  AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_APP_VERSION + targetVersion
				+ PARAM_DEVICE_TYPE;
	}

	//================ Build the URL to check or download the resources =====================//
	public static String urlCheckResourcesUpdate(String currentVersion, String mode) {
		//parameters are serialNumber, app, lang, targetVersion, currentVersion
		String theURL = BASE_URL + PARAM_SERVICE + VAL_CHECK_RES_UPDATE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_TARGET_VERSION  + AppManager.getAppManager().getVersionName();

		if( currentVersion != null )
			theURL += PARAM_CURRENT_VERSION + currentVersion;

		if (mode.equals("visit"))
			theURL += PARAM_APP_MODE + mode;
		else {
			String freemiumName = AppManager.getAppManager().getFreemiumName();
			if (freemiumName != null)
				theURL += PARAM_APP_MODE + freemiumName;
		}
		return theURL;
	}
	public static String urlDownloadResourcesUpdate(String currentVersion, String mode) {
		//parameters are serialNumber, app, lang, targetVersion, currentVersion

		// For test purposes
		// return "http://10.1.11.232:8080/examples/resources.zip";

		String theURL = BASE_URL + PARAM_SERVICE + VAL_DOWNLOAD_RES_UPDATE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_TARGET_VERSION  + AppManager.getAppManager().getVersionName();

		if (currentVersion != null)
			theURL += PARAM_CURRENT_VERSION + currentVersion;

		if (mode.equals("visit"))				//allows to download resources without an account
			theURL += PARAM_APP_MODE + mode;
		else {
			String freemiumName = AppManager.getAppManager().getFreemiumName();
			//if ( freemiumName != null && !AppManager.getAppManager().isSubscriptionValid() )
			if (freemiumName != null)
				theURL += PARAM_APP_MODE + freemiumName;
		}
		return theURL;
	}

	public static String urlGetResourcesForApp(boolean useCodeForSynchro, String code) {
		return urlGetResourcesForApp(useCodeForSynchro, code, false,false);
	}

	public static String urlGetResourcesForApp(boolean useCodeForSynchro, String code, boolean downloadPhotosPaintings, boolean downloadMaps) {
		//parameters are serialNumber, app, lang
		AppManager ap = AppManager.getAppManager();
		String theURL = BASE_URL + PARAM_SERVICE + VAL_GET_RES_FOR_APP
				+ PARAM_DEVICE_TYPE
				+ PARAM_APP + ap.getAppSubName()
				+ PARAM_APP_LANG + ap.getLang()
				+ PARAM_APP_LANG_TYPE + ap.getLangAlter()
				+ PARAM_APP_TYPE + ap.getAppSubTypeName();

		if (ap.getAppExtension() != null)
			theURL += PARAM_APP_EXTENSION + ap.getAppExtension();

		if( useCodeForSynchro )
			theURL += PARAM_CODE + code;
		else 		//Standard case
			theURL += PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber();

		theURL += PARAM_APP_MODE + AppManager.getAppManager().getMode();

		if (downloadPhotosPaintings) {
			theURL += PARAM_FORCE_DOWNLOAD_PHOTOS_PAINTINGS;
		}
		if (downloadMaps) {
			theURL += PARAM_FORCE_DOWNLOAD_MAPS;
		}


		return theURL;
	}

	public static String urlSynchroEnd(boolean dataType, boolean useCodeForSynchro, String code) {
		String theURL = BASE_URL + PARAM_SERVICE + VAL_SYNCHRO_END
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_VERSION + AppManager.getAppManager().getVersionName()
				+ PARAM_APP_VERSION_CODE + AppManager.getAppManager().getVersionCode()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang();

		if( !dataType )
			theURL += PARAM_TYPE_RESOURCES;

		if( useCodeForSynchro )
			theURL += PARAM_CODE + code;
		else 		//Standard case
			theURL += PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber();

		theURL += PARAM_APP_MODE + AppManager.getAppManager().getMode();

		return theURL;
	}

	public static String urlActivateSubscription() {
		String theURL = BASE_URL + PARAM_SERVICE + VAL_ACTIVATE_SUBSCRIPTION
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang();
		String mode = AppManager.getAppManager().getFreemiumName();
		if (mode != null)
			theURL += PARAM_APP_MODE + mode;
		return theURL;
	}

	public static String urlSendRegistrationToken(String notificationToken)	{
		String theURL = BASE_URL + PARAM_SERVICE + VAL_ADD_NOTIFICATION_DEVICE
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_DEVICE_LANG + AppManager.getAppManager().getDeviceLang()
				+ PARAM_DEVICE_TYPE
				+ PARAM_FORMAT_OUTPUT_JSON
				+ PARAM_NOTIFICATION_TOKEN + notificationToken;

		return theURL;

	}

	public static String urlSaveSerials(String firstSerial, String secondSerial, String allSerials)	{
		String theURL = BASE_URL + PARAM_SERVICE + VAL_SAVE_SERIALS
				+ PARAM_SERIAL_NUMBER + AppManager.getAppManager().getSerialNumber()
				+ PARAM_APP + AppManager.getAppManager().getAppSubName()
				+ PARAM_APP_LANG + AppManager.getAppManager().getLang()
				+ PARAM_DEVICE_LANG + AppManager.getAppManager().getDeviceLang()
				+ PARAM_FIRST_SERIAL + firstSerial
				+ PARAM_SECOND_SERIAL + secondSerial
				+ PARAM_ALL_SERIALS + allSerials
				+ PARAM_DEVICE_TYPE;

		Log.d("Built urlSaveSerials", "built URL : " + theURL);
		return theURL;

	}

	/*         Other methods                                                                 */
	//================ Methods to ease the management of server response =====================//
	public static boolean isDeviceValid(JSONObject jsonObj)
	{
		String error = jsonObj.optString(JSON_PARAM_ERROR, null);
		return ( error == null || (!error.equals("wrongSerialNumber") && !error.equals("invalidDevice")) );
	}


	public static void setSubscriptionDates(JSONObject jsonObj, SharedPreferences sharedPrefs,
											boolean fullDescription) throws JSONException {

		//subscriptionState prevails on endSubscription (if tablet wrong time)
		//beginSub and endSub are for display purposes
		String stateSub = jsonObj.optString(SHARED_PREFS_STATE_SUBSCRIPTION, null); //OK or KO
		String beginSub = jsonObj.optString(SHARED_PREFS_BEGIN_SUBSCRIPTION, null);
		String endSub = jsonObj.optString(SHARED_PREFS_END_SUBSCRIPTION, null);

		String newSynchroDate = jsonObj.optString(SHARED_PREFS_SYNCHRO_DATE, null);
		if (newSynchroDate != null) {
			sharedPrefs.edit()
					.putString(SHARED_PREFS_SYNCHRO_DATE, newSynchroDate)
					.apply();
		}
		newSynchroDate = jsonObj.optString(SHARED_PREFS_SYNCHRO_RES_DATE, null);
		if (newSynchroDate != null) {
			sharedPrefs.edit()
					.putString(SHARED_PREFS_SYNCHRO_RES_DATE, newSynchroDate)
					.apply();
		}

		String serialNumberGenerated = jsonObj.optString("serialNumberGenerated");
		if (!serialNumberGenerated.equals("")) {
			sharedPrefs.edit().putString("serialNumberGenerated", serialNumberGenerated).apply();
			AppManager.getAppManager().setSerialNumberGenerated(serialNumberGenerated);
		}

		//mode can be test, subscription, freemium_xxx
		String mode = jsonObj.optString(SHARED_PREFS_MODE, null);

		if (fullDescription) {
			String institutionType = jsonObj.optString(SHARED_PREFS_INSTITUTION_TYPE, null);
			String institution = jsonObj.optString(SHARED_PREFS_INSTITUTION, null);

			Log.d("institutionType", "institutionType : " + institutionType);

			//if (institutionType != null && institutionType.equals("HOME")){
			if ((institutionType != null && institutionType.equals("HOME")) || AppManager.getAppManager().needsCode()) {
				String accountString = Account.getFromSP(sharedPrefs);
				if ((accountString == null && !jsonObj.optString("email").equals("")) || (!jsonObj.optString("code").equals(""))) {
					Account account = new Account(jsonObj);
					account.saveInSP(sharedPrefs);
				}
			}

			sharedPrefs.edit()
					.putString(SHARED_PREFS_INSTITUTION, institution)
					.putString(SHARED_PREFS_INSTITUTION_TYPE, institutionType)
					.apply();
		}
		Log.d("mode", "mode : " + mode);

		// mode can be : visit, subscription, freemium_xxx (when freemium, never changes)
		if (mode != null) {
			Log.d("mode", "sharedPrefs : " + sharedPrefs);
			sharedPrefs.edit().putString(SHARED_PREFS_MODE, mode).apply();
			AppManager.getAppManager().setMode(mode);
			if (mode.startsWith("freemium"))
				AppManager.getAppManager().setFreemiumName(mode);    //reset if lost after reinstallation
		}

		if (stateSub != null && beginSub != null && endSub != null) {
			AppManager.getAppManager().setIsSubscriptionValid(stateSub);
			sharedPrefs.edit()
					.putString(SHARED_PREFS_STATE_SUBSCRIPTION, stateSub)
					.putString(SHARED_PREFS_BEGIN_SUBSCRIPTION, beginSub)
					.putString(SHARED_PREFS_END_SUBSCRIPTION, endSub)
					//	.putString(SHARED_PREFS_MODE, mode)
					.apply();
		}
		Log.d("setSubscriptionDates", "Mode : " + sharedPrefs.getString(ConnectionConstants.SHARED_PREFS_MODE, null)
				+ ", End : " + sharedPrefs.getString(ConnectionConstants.SHARED_PREFS_END_SUBSCRIPTION, null));
	}


	public static int width=0;
	public static int height=0;
	public static int toWidthRate(int percent) {
		return percent*width/100;
	}
	public static int toHeightRate(int percent){
		return percent*height/100;
	}
}