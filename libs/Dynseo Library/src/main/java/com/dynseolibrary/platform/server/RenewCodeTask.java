package com.dynseolibrary.platform.server;

import android.os.AsyncTask;
import android.util.Log;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SubscriptionInterface;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Viet on 27/06/2016.
 */
public class RenewCodeTask extends AsyncTask<Void, Void, String> {
    public static final String TAG = "RenewCodeTask";
    SubscriptionInterface requester;
    String code;

    public RenewCodeTask(SubscriptionInterface requester, String code) {
        this.requester = requester;
        this.code = code;
    }
    @Override
    protected String doInBackground(Void... params) {
        try{
            String path = ConnectionConstants.urlRenewCode(code);
            Log.i(TAG, "path = " + path);
            return Http.queryServer(path);
        }
        catch (Exception e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d(TAG,"onPostExecute()");

        try {
            Log.d(TAG, "result is : " + result);
            JSONObject jsonObject = new JSONObject(result);
            int returnCode = ErrorCodeInterface.SUCCESS;
            String error = jsonObject.optString(ConnectionConstants.ERROR);
            if ( !error.equals("")) {
                if( error.equals("endOfSubscription"))
                    returnCode = ErrorCodeInterface.ERROR_END_OF_SUBSCRIPTION;
                else
                    returnCode = ErrorCodeInterface.ERROR_UNDEFINED;

                requester.onError(returnCode);
            }

            else {
                requester.onSubscriptionAccepted(3, jsonObject);
            }
        }
        catch (JSONException e) {
            Log.e(TAG,"instanciate json exception");
        }
    }
}
