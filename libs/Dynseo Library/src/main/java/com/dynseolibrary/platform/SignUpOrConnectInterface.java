package com.dynseolibrary.platform;

import org.json.JSONObject;

import com.dynseolibrary.account.Account;

public interface SignUpOrConnectInterface {
	
	void doConnect();
	void doSignUp(boolean isForInstitution);
	void doVisit();
	void doSelectTypeForSignUp(boolean gotoSubscribe);
	
	void onError(int errorCode);

	void createAccount(Account anAccount);
	void logIn(String pseudo, String password);
	void attachToInstitution(String email, String institutionName, String institutionCity, String message);
	
	void onCreateAccountSuccess(int step, JSONObject jsonResult);
	void onAttachInstitutionSuccess(int step, JSONObject jsonResult);
	void onLoginSuccess(JSONObject jsonResult);

	void onReturn();
}
