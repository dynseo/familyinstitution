/**
HOUSSEM
 */
package com.dynseolibrary.platform;

import org.json.JSONObject;


public interface CheckOrSetDeviceInterface {
	
	// on failure
	public void onDeviceVerificationFailure(JSONObject jsonResult);

	// on success
	public void onDeviceVerificationSuccess(JSONObject jsonResult, boolean showToast);
}
