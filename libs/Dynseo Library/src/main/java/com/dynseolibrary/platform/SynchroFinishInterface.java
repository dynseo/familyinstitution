package com.dynseolibrary.platform;

/**
 * Created by Viet on 22/04/2016.
 */
public interface SynchroFinishInterface {
    public void onSynchroFinish();
}
