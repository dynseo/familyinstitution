package com.dynseolibrary.platform;

import org.json.JSONObject;

/**
 * Created by Viet on 20/05/2016.
 */
public interface ConsumeCodeInterface {
    public void consumeCode(String code, String password);
    public void onConsumeCodeSuccess(JSONObject jsonResult);

    public void retrieveCode(String email, String numClient);
    public void doRetrieveCode();

    void onError(int errorCode);
}
