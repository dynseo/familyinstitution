package com.dynseolibrary.platform.server;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import android.util.Log;

/**
 * Created by Houssem on 20.04.2015.
 */
public class CreateAccountTask extends AsyncTask<Void, Void, String> {

    private final static String TAG = "CreateAccountTask";

    JSONObject jsonResult;
    int returnCode;
    String serviceName = ConnectionConstants.VAL_CREATE_ACCOUNT;

    SignUpOrConnectInterface requester;

    Account account;
    SharedPreferences sp;

    public CreateAccountTask(SignUpOrConnectInterface aRequester, Account anAccount, SharedPreferences prefs, String serviceName) {
        this(aRequester, anAccount, prefs);
        this.serviceName = serviceName;
    }
    public CreateAccountTask(SignUpOrConnectInterface aRequester, Account anAccount, SharedPreferences prefs) {
        requester = aRequester;
        account = anAccount;	// String aPseudo, String aEmail, String aPassword
        sp = prefs;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            jsonResult = null;
            String path = ConnectionConstants.urlCreateAccount(account, sp, serviceName);

            Log.e(TAG, path);
            return Http.queryServer(path);
        }
        catch (Exception e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            jsonResult = new JSONObject(result);

            returnCode = ErrorCodeInterface.SUCCESS;

            String error = jsonResult.optString(ConnectionConstants.ERROR);
            if( !error.equals("") ) {
                if( error.equals("duplicateUserEmail") || error.equals("duplicateEmail"))
                    returnCode = ErrorCodeInterface.ERROR_DUPLICATE_EMAIL;
                else if( error.equals("duplicateUserPseudo") || error.equals("duplicatePseudo"))
                    returnCode = ErrorCodeInterface.ERROR_DUPLICATE_PSEUDO;
                else if (error.equals("duplicatePhone"))
                    returnCode = ErrorCodeInterface.ERROR_DUPLICATE_PHONE;
                else if( error.equals("invalidCode"))
                    returnCode = ErrorCodeInterface.ERROR_INVALID_CODE;
                else if( error.equals("alreadyUsedCode"))
                    returnCode = ErrorCodeInterface.ERROR_ALREADY_USED_CODE;
                else            //for instance failureAccount
                    returnCode = ErrorCodeInterface.ERROR_UNDEFINED;

                requester.onError(returnCode);
            }

            else {
                requester.onCreateAccountSuccess(0, jsonResult);
            }
            Log.e(TAG, result);
            Log.e(TAG, " returnCode : " + returnCode);
        }
        catch(JSONException e) {
        }
    }

    public int getReturnCode() {
        return returnCode;
    }
    public JSONObject getJsonResult() {
        return jsonResult;
    }
}
