package com.dynseolibrary.platform.server;


import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.WindowManager;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SynchroFinishInterface;
import com.dynseolibrary.platform.SynchroInterface;

import org.json.JSONException;
import org.json.JSONObject;

import static java.security.AccessController.getContext;

/**
 * User synchronization
 * modified by Dominique
 * @date 01/03/2016
 */


//==============================================================================
// AsyncTask to synchronization to the server
//==============================================================================
public class SynchronizationTask extends AsyncTask<Void, Void, String> {

	private final static String TAG = "SynchronizationTask";

    SynchroFinishInterface requesterUI;
	SynchroInterface requester;
	JSONObject jsonResult;
	int returnCode;

	SharedPreferences sp;

	public SynchronizationTask(SynchroInterface aRequester, SynchroFinishInterface aRequesterUI, SharedPreferences aSharedPref){
        requesterUI = aRequesterUI;
		requester = aRequester;
		sp = aSharedPref;
	}

		@Override
		public void onPreExecute() {
		}

		@Override
		protected String doInBackground(Void... voids) {

			Log.d(TAG,"doInBackground()");
            if(requester.authorizeSynchro()) {
				Log.d("SynchronizationTask", "passe");
				requester.doSynchro();

				// === Publish the end of synchronization
				return requester.endOfSynchro();
			}
			return null;
		}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		//Log.d("result synchronization", result);
		try {

            Log.e(TAG, "resultV : " + result);
			jsonResult = new JSONObject(result);

			returnCode = ErrorCodeInterface.SUCCESS;

			String error = jsonResult.optString(ConnectionConstants.ERROR);
			if( !error.equals("") ) {
				if( error.equals("synchroFailed"))
					returnCode = ErrorCodeInterface.ERROR_SYNCHRO_FAILED;

				requester.onError(returnCode);
			}
			else {
                requester.onSynchroSuccess(0);
                requesterUI.onSynchroFinish();
			}
			//Log.e(TAG, result);
			Log.e(TAG, " returnCode : " + returnCode);
		}
		catch(JSONException e) {
		}
	}
}
