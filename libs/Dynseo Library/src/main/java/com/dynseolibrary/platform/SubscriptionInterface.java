package com.dynseolibrary.platform;

import org.json.JSONObject;

public interface SubscriptionInterface {
	
	void onSubscriptionAccepted(int step, JSONObject jsonResult);
	void onSubscriptionIgnored();
	
	void onError(int errorCode);
}
