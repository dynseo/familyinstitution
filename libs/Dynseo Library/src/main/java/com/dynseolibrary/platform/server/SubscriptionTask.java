package com.dynseolibrary.platform.server;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SubscriptionInterface;

import android.os.AsyncTask;

import android.util.Log;

/**
 * Created by Dominique on 24.06.2015.
 */
public class SubscriptionTask extends AsyncTask<Void, Void, String> {

	private final static String LOG_TAG = "SubscriptionTask";	
	
	JSONObject jsonResult;
	int returnCode;
	
	SubscriptionInterface requester;
	String email;
	
    public SubscriptionTask(SubscriptionInterface aRequester, String anEmail) {
    	requester = aRequester;
    	email = anEmail;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
//        	jsonResult = null;
        	String path = ConnectionConstants.urlSubscription(email);
        	Log.e(LOG_TAG, path);
        	return Http.queryServer(path);
        } 
        catch (Exception e) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        
        try {
        	jsonResult = new JSONObject(result);
            Log.e(LOG_TAG, " result : " + result);

        	returnCode = ErrorCodeInterface.SUCCESS;
        	
        	String error = jsonResult.optString("error");
        	if( !error.equals("") ) {
        		returnCode = ErrorCodeInterface.ERROR_UNDEFINED;        		
        		requester.onError(returnCode);
        	}
        	else {
        		requester.onSubscriptionAccepted(1, jsonResult);    //With step equal to 1
        	}
        	Log.e(LOG_TAG, result);
        	Log.e(LOG_TAG, " returnCode : " + returnCode);
        }
        catch(JSONException e) {
        }
    }
    
    public int getReturnCode() {
        return returnCode;
    }
    public JSONObject getJsonResult() {
    	return jsonResult;
    }
}
