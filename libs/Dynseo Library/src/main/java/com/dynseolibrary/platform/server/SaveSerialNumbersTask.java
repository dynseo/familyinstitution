package com.dynseolibrary.platform.server;

import android.os.AsyncTask;
import android.util.Log;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SaveSerialInterface;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maxime Gelly on 23/02/2017.
 */

public class SaveSerialNumbersTask extends AsyncTask<Void, Void, String> {
    private String TAG = "SaveSerialNumbersTask";
    private String firstSerial;
    private String secondSerial;
    private String thirdSerial;
    private SaveSerialInterface requester;

    public SaveSerialNumbersTask(SaveSerialInterface requester, String firstSerial, String secondSerial, String thirdSerial) {
        this.firstSerial = firstSerial;
        this.secondSerial = secondSerial;
        this.thirdSerial = thirdSerial;
        this.requester = requester;
    }
    @Override
    protected String doInBackground(Void... params) {
        Log.d(TAG,"doInBackground()");
        try{
            String path = ConnectionConstants.urlSaveSerials(firstSerial, secondSerial, thirdSerial);
            Log.i(TAG, "path = " + path);
            return Http.queryServer(path);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Get the server response
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        JSONObject jsonResult;

        int returnCode;
        if(result!= null){
            try {
                jsonResult = new JSONObject(result);
                Log.d(TAG, " result : " + jsonResult);

                returnCode = ErrorCodeInterface.SUCCESS;

                String status = jsonResult.optString("status");
                if (status.equals("OK")) {
                    requester.onSuccess(firstSerial);
                }
                Log.d(TAG, result);
                Log.d(TAG, " returnCode : " + returnCode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            requester.onError();
        }
    }
}
