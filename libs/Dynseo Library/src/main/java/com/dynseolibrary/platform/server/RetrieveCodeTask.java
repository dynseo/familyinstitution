package com.dynseolibrary.platform.server;

import android.os.AsyncTask;
import android.util.Log;

import com.dynseolibrary.platform.ConsumeCodeInterface;

/**
 * Created by Viet on 27/06/2016.
 */
public class RetrieveCodeTask extends AsyncTask<Void, Void, Void> {
    String TAG = "RetrieveCodeTask";
    ConsumeCodeInterface requester;
    String email;
    String numClient;

    public RetrieveCodeTask(ConsumeCodeInterface requester, String email, String numClient) {
        this.requester = requester;
        this.email = email;
        this.numClient = numClient;
    }
    @Override
    protected Void doInBackground(Void... params) {
        Log.d(TAG,"doInBackground()");
        try{
            String path = ConnectionConstants.urlRetrieveCode(email, numClient);
            Log.i(TAG, "path = " + path);
            Http.queryServer(path);
        }
        catch (Exception e) {
        }
        return null;
    }
}
