/**
HOUSSEM
*/
package com.dynseolibrary.platform;

public interface SynchroInterface {

    boolean authorizeSynchro();
	void doSynchro();
	void nextStepSynchro();
	String endOfSynchro();

	void manageResultOfResourcesDownload();

	void onSynchroSuccess(int step);
	void onSynchroFailure();
	
	void onError(int errorCode);
}
