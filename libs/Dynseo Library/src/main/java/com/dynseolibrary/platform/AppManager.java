/**
 HOUSSEM
 */
package com.dynseolibrary.platform;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dynseolibrary.account.Account;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.tools.Tools;
import com.example.dynseolibrary.R;

import org.json.JSONObject;

import java.util.Locale;


/**
 * AppManager is used to get the current version of application (for updates)
 * @author Dominique Sauquet
 */
public class AppManager implements SaveSerialInterface {

	private static final String TAG = "AppManager";
	public static final String KEY_CURRENT_VERSION_NAME = "current_version_name";

	private static int SP_VERSION = 1;

	private static AppManager appManager;        //this is a singleton

	private AppManagerSpecializationInterface appManagerSpecializer;
	private SharedPreferences sp;

	private int versionCode;
	private String versionName;

	private String appName;
	private String appSubName;
	private String appSubTypeName;
	private String appSubDisplayName;
	private String appExtension;

	private String appUserRole;
	private String lang;
	private String langAlter;

	private String deviceFormat;

	private String packageName;

	private String dateTimeFormat;
	private String dateFormat;

	private String freemiumName;
	private String resourcesType;

	private boolean isSubscriptionValid = false;
	private String mode;
	private boolean needsCode;
	private boolean skipAccountConnection;
	private Context context;

	private String serialNumberGenerated;
	private String serialNumber;

	private Class rootClass;

	private AppManager(Context context) {

		versionCode = -1;
		try {
			versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}

		versionName = null;
		try {
			versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (Exception e) {
			versionName = "";
			Log.e(TAG, e.getMessage());
		}

		appName = context.getResources().getText(R.string.app_name).toString();
		appSubName = context.getResources().getText(R.string.app_sub_name).toString();
		appUserRole = context.getResources().getText(R.string.app_user_role).toString();
		deviceFormat = context.getResources().getText(R.string.device_format).toString();
		appSubTypeName = null;
		int resourceId = context.getResources().getIdentifier("app_sub_type_name", "string", context.getPackageName());
		if (resourceId != 0)    // the resource exists...
			appSubTypeName = context.getResources().getText(resourceId).toString();
		appExtension = null;
		resourceId = context.getResources().getIdentifier("app_extension", "string", context.getPackageName());
		if (resourceId != 0)    // the resource exists...
			appExtension = context.getResources().getText(resourceId).toString();

		sp = upgradedSharedPreferences(context);

		lang = context.getResources().getText(R.string.lang).toString();    //Not the language of the device
		//but the one of the app
		langAlter = sp.getString(ConnectionConstants.SHARED_PREFS_LANG_ALTER, null);

		packageName = context.getPackageName();

		freemiumName = null;
		resourceId = context.getResources().getIdentifier("freemium_name", "string", context.getPackageName());
		if (resourceId != 0)    // the resource exists...
			freemiumName = context.getResources().getText(resourceId).toString();

		resourcesType = context.getString(R.string.freemium_resources);

		appSubDisplayName = null;
		resourceId = context.getResources().getIdentifier("app_sub_display_name", "string", context.getPackageName());
		if (resourceId != 0)    // the resource exists...
			appSubDisplayName = context.getResources().getText(resourceId).toString();

		dateTimeFormat = context.getResources().getText(R.string.date_time_format).toString();
		dateFormat = context.getResources().getText(R.string.date_format).toString();


		if (sp.contains("serialNumberGenerated"))
			serialNumberGenerated = sp.getString("serialNumberGenerated", null);
		else if (sp.contains("serialNumber"))
			serialNumber = sp.getString("serialNumber", null);
		else
			serialNumber = Tools.getManufacturerSerialNumber(AppManager.this);
		//buildSerialNumber();
		/*
		if (sp.contains("serialNumberGenerated"))
			serialNumberGenerated = sp.getString("serialNumberGenerated", null);

		if (sp.contains("serialNumber"))
			serialNumber = sp.getString("serialNumber", null);
		else
			buildSerialNumber();
*/
		setIsSubscriptionValid( sp.getString("subscriptionState", null) );

		mode = sp.getString("mode", null);
		needsCode = Tools.isResourceTrue(context, R.string.needsCode);
		skipAccountConnection = Tools.isResourceTrue(context, R.string.skip_account_connection);
		this.context = context;

		this.rootClass = context.getClass();
		Log.d(TAG, "root class : " + this.rootClass.getName());
	}

	public static AppManager getAppManager(Context context) {    //This one for initialization

		if (appManager == null)
			appManager = new AppManager(context);

		return appManager;
	}

	public static AppManager getAppManager() {                    //This one, to be used once initialized
		return appManager;
	}

	public String getSavedVersion(Context context) {
		//Compare the version name in SP and the one of the current apk
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

		String savedVersionName = sp.getString(KEY_CURRENT_VERSION_NAME, "");
		return savedVersionName.equals(versionName) ? null : savedVersionName;
	}
	public boolean isNewVersion(Context context) {
		//Compare the version name in SP and the one of the current apk
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

		String savedVersionName = sp.getString(KEY_CURRENT_VERSION_NAME, "");
		return !savedVersionName.equals(versionName);
	}

//	public boolean maxdebug(){
//		return 0;
//	}

	public void setAppManagerSpecializer(AppManagerSpecializationInterface appSpecializer) {
		appManagerSpecializer = appSpecializer;
	}
	public boolean authorizeSynchro(String typeOfSynchro) {

		if( appManagerSpecializer != null )		//We allow having a specific behaviour
			return appManagerSpecializer.authorizeSynchro(typeOfSynchro);

		//synchrodata never done, we authorize it
		if (typeOfSynchro.equals("resources") && PreferenceManager.getDefaultSharedPreferences(context).getString(ConnectionConstants.SHARED_PREFS_SYNCHRO_RES_DATE, null) == null)
			return true;

		//For the moment, basic behavior does not use typeOfSynchro
		return  (isSubscriptionValid() && !getMode().equals("visit"));
	}

	public boolean authorizePlayGame(String gameName, boolean limitOfGamesExceeded) {

		if( appManagerSpecializer != null )		//We allow having a specific behaviour
			return appManagerSpecializer.authorizePlayGame(gameName);

		// I can play when nb tries is lower than the method (if visit)
		// 			  when my subscription is valid,
		//            when my app is a freemium one
		//
		if ( getMode().equals("visit") && limitOfGamesExceeded )
			return false;

		if( getFreemiumName() != null )
			return true;

		if( isSubscriptionValid() )
			return true;

		return false;
	}

	public void setIsSubscriptionValid(String stateSub) {
		isSubscriptionValid = (stateSub != null && stateSub.equals("OK"));
	}
	public boolean isSubscriptionValid() {
		return isSubscriptionValid;
	}


	/* ===== All the getter methods ==== */

	public int getVersionCode() {
		return versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public String getVersionNameAndCode() {
		return (versionName + " (" + versionCode + ")");
	}

	public String getAppName() {
		return appName;
	}

	public String getAppSubName() {
		return appSubName;
	}

	public String getAppSubTypeName() {
		return ( appSubTypeName == null ) ? appSubName : appSubTypeName;
	}

	public String getAppExtension() {
		return appExtension;
	}

	public String getAppSpecificName() {
		return ( appSubTypeName == null ) ? null : appSubName;
	}

	public String getAppFullName() {
		return appName + " - " + appSubName;
	}

	public String getAppFullDisplayName() {
		if (appSubDisplayName != null)
			if (appSubDisplayName.equals("")) {
				return appName;
			} else {
				return appName + " - " + appSubDisplayName;
			}
		return getAppFullName();
	}

	public String getAppUserRole() {
		return appUserRole;
	}

	public String getLang() {
		return lang;
	}

	// langAlter for service getResourcesForApp (FrancoPhone version : switzerland, belgium, luxembourg)
	public String getLangAlter() {
		return (langAlter == null || langAlter.equals("")) ? lang : langAlter;
	}

	public void setLangAlter(String langAlter){
		this.langAlter = langAlter;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getDateTimeFormat() {
		return dateTimeFormat;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public String getMode() {
		return mode == null ? "" : mode; }

	public void setMode(String aMode) {
		mode = aMode;
	}

	public boolean needsCode() {
		return needsCode;
	}

	public boolean skipAccountConnection() {
		return skipAccountConnection;
	}

	public String getFreemiumName() {
		return freemiumName;
	}

	public void setFreemiumName(String aFreemName) {
		this.freemiumName = aFreemName;
	}

	public boolean isFreemiumActivated() {
		//return true when apk is an apk of type freemium AND not bought yet
		return( freemiumName != null && !isSubscriptionValid );
	}
	/*	public String getSerialNumberGenerated() {
            return serialNumberGenerated;
        }
    */
	public void setSerialNumberGenerated(String aSerialNumberGenerated) {
		serialNumberGenerated = aSerialNumberGenerated;
	}

	public String getSerialNumber(){
		if(serialNumberGenerated!=null)
			return serialNumberGenerated;

		return serialNumber;
	}

	public void setSerialNumber(String aSerialNumber){this.serialNumber = aSerialNumber;}

	public String getDeviceFormat(){
		return deviceFormat;
	}

	private void buildSerialNumber(){
		//Serial number not found in sp

		//If not, do the dynamic to get the serialNumber
		//Send to server
		//If server response is ok store in sp
		//If server response is ko store in AppMgr

		//It is better to detect serial number generated first : we can then
		//manage new cases on the server side (for instance if we want to omit serial with "XYZ")
//        String serial = getSerialNumberGenerated();

		//Special case: if the user got a generated serial and reinstalls the application (with the modification that takes the ro.boot.serialno)
		//the new serial number could be ro.boot.serialno and would be registered on our network with the generated one.
		//So to do the migration we would have to find the user not by the generated serial number but by email or pseudo

		if (serialNumberGenerated != null)
			serialNumber = serialNumberGenerated;
		else {
			serialNumber = Tools.getManufacturerSerialNumber(AppManager.this);
		}

	}



	public String getResourcesType() {
		return resourcesType;
	}

	public void setResourcesType(String resourcesType) {
		this.resourcesType = resourcesType;
	}

	public boolean isHome(SharedPreferences sp) {
		String institutionType = sp.getString(ConnectionConstants.SHARED_PREFS_INSTITUTION_TYPE, null);
		return (institutionType != null && institutionType.equals("HOME"));
	}
	protected SharedPreferences upgradedSharedPreferences(Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

		try {
			int actualSPVersion = sp.getInt("spVersion", -1);
			if( actualSPVersion < SP_VERSION ) {	//We need to upgrade sp
				//Adapt shared preferences
				if( actualSPVersion < 1 ) {		//We need to effect modifications linked to SP_VERSION == 1

					String accountString = sp.getString(Account.SHARED_PREFS_ACCOUNT, null);
					if (accountString != null) {
						JSONObject jsonAccount = new JSONObject(accountString);

						String value = jsonAccount.optString("subscriptionState");
						if (value != null) {
							sp.edit().putString("subscriptionState", value);
						}
						value = jsonAccount.optString("testMode");
						if (value != null && value.equals("Y")) {
							sp.edit().putString("mode", "visit");
						}
						value = jsonAccount.optString("type");
						if (value != null && value.equals("particulier")) {
							sp.edit().putString("type", "HOME");
						}
						Account account = new Account(jsonAccount);
						account.saveInSP(sp);	//The new one does not contain subscriptionState nor testMode
					}
				}

//				if( actualSPVersion < 2 ) {		//We need to effect modifications linked to SP_VERSION == 2
//					sp.edit().putBoolean("deleteOldResources", true).commit();
//				}

				//Store new version of spVersion
				sp.edit().putInt("spVersion", SP_VERSION)
						.commit();
			}
		}
		catch(Exception e) {
		}
		return sp;
	}

	public Class getRootClass()	{
		return this.rootClass;
	}

	public String getDeviceLang () {
		return Locale.getDefault().getLanguage() ;
	}

	@Override
	public void onSuccess(String serial) {
		sp.edit().putString("serialNumber",serial).apply();
	}

	@Override
	public void onError() {
		Log.d(TAG,"error saving serialNumber");
	}
}

