/**
HOUSSEM
*/
package com.dynseolibrary.platform.server;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseolibrary.platform.CheckOrSetDeviceInterface;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

//==============================================================================
// AsyncTask to verify device
//==============================================================================

public class CheckOrSetDeviceTask extends AsyncTask<Void, Void, String> {
	
	private static final String TAG = "CheckOrSetDeviceTask";
	CheckOrSetDeviceInterface checkDeviceInterface;
	SharedPreferences sp;

	public CheckOrSetDeviceTask(CheckOrSetDeviceInterface checkDevInterface, SharedPreferences aSp) {
		this.checkDeviceInterface = checkDevInterface;
		this.sp = aSp;
	}

	@Override
	protected String doInBackground(Void... params) {
		Log.d(TAG,"doInBackground()");
		try{
			String path = ConnectionConstants.urlCheckAndSetDevice(sp);
			Log.i(TAG, "path = " + path);
			return Http.queryServer(path);
		}
		catch (Exception e) {
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		Log.d(TAG,"onPostExecute()");
		
		try {
			Log.d(TAG, "result is : " + result);
			JSONObject o = new JSONObject(result);

			boolean isDeviceValid = ConnectionConstants.isDeviceValid(o);
			
			if ( isDeviceValid ) {
				Log.i(TAG,"status = ok");
				checkDeviceInterface.onDeviceVerificationSuccess(o, true);
			}
			else {
				Log.d(TAG,"Cette tablette n'est pas autorisee a utiliser Stimart plus d'info etc...");
				checkDeviceInterface.onDeviceVerificationFailure(o);
			}
		} 
		catch (JSONException e) {
			Log.e(TAG,"instanciate json exception");
		}
	}
}
