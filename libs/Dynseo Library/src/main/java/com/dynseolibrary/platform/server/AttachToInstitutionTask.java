/**
HOUSSEM
*/
package com.dynseolibrary.platform.server;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.dynseolibrary.platform.ErrorCodeInterface;
import com.dynseolibrary.platform.SignUpOrConnectInterface;

//==============================================================================
//AsyncTask to attach device to institution
//==============================================================================

public class AttachToInstitutionTask extends AsyncTask<Void, Void, String> {
	
	JSONObject jsonResult;
	int returnCode;
	
	SignUpOrConnectInterface requester;
	
	private static final String TAG = "AttachToInstitutionTask";
	String email, name, city, message;
	
	public AttachToInstitutionTask(SignUpOrConnectInterface aRequester, String anEmail, String aName, String aCity, String aMessage) {
		
		requester = aRequester;
		
		email = anEmail;
		name = aName;
		city = aCity;
		message = aMessage;
	}
	
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

	@Override
	protected String doInBackground(Void... params) {
		Log.d(TAG,"doInBackground()");		
		 try {
	        	jsonResult = null;
	        	String path = ConnectionConstants.urlAttachToInstitution(email, name, city, message); 
	        	Log.e(TAG, path);
	        	return Http.queryServer(path);
	        } 
	        catch (Exception e) {
	        }
	        return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		Log.d(TAG,"onPostExecute()");
		
		  try {
	        	jsonResult = new JSONObject(result);
	        	
	        	returnCode = ErrorCodeInterface.SUCCESS;
	        	String error = jsonResult.optString(ConnectionConstants.ERROR);
	        	if( !error.equals("") ) {
	        			returnCode = ErrorCodeInterface.ERROR_UNDEFINED;
	        			requester.onError(returnCode);
	        	}
	        	else{
	        		requester.onAttachInstitutionSuccess(0, jsonResult);
	        	}
	        	Log.e(TAG, result);
	        	Log.e(TAG, " returnCode : " + returnCode);
	        }
	        catch(JSONException e) {
	        }
	}	
	
    public int getReturnCode() {
        return returnCode;
    }
    public JSONObject getJsonResult() {
    	return jsonResult;
    }
}

