/**
HOUSSEM
*/
package com.dynseolibrary.synchronization;

import com.dynseolibrary.platform.SynchroInterface;
import com.example.dynseolibrary.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SynchroFragment extends DialogFragment {

	Dialog d;
	Button lunchSynchroBtn;
	
	SynchroInterface requester;
	Typeface buttonAppStyle;
	boolean accountIsMandatory;

	boolean isFinished;


	public SynchroFragment() {}

	@SuppressLint("ValidFragment")
	public SynchroFragment(SynchroInterface synchro, Typeface aAppStyle, boolean isAccountMandatory) {

		requester = synchro;
		buttonAppStyle = aAppStyle;
		accountIsMandatory = isAccountMandatory;
	}

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	}
	
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	 
		d = new Dialog(getActivity(), R.style.DialogFullscreen);
		d.setContentView(R.layout.dialog_synchronization);
		d.setCanceledOnTouchOutside(false);
		
		lunchSynchroBtn = (Button) d.findViewById(R.id.button_launch_synchro);
		
		 //========== Configure the actions on buttons
		lunchSynchroBtn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
        	  if (isFinished)
        		  requester.onSynchroSuccess(1);
        	  else
        		  requester.doSynchro();
          }
      });
      
		return d;
	}
	
	public void onSuccess(){
		TextView tv = (TextView) d.findViewById(R.id.synchro_body);
		tv.setText(R.string.synchro_success);
		lunchSynchroBtn.setText(R.string.continuer);
		isFinished = true;
	}
}
