package com.dynseo.family.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.family.dao.ExtractorFamily;
//import com.dynseo.stimart.common.dao.Extractor;
//import com.dynseo.stimart.common.models.Game;
import com.dynseo.family.models.Link;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;
import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseo.stimart.common.server.DownloadFile;
import com.dynseo.stimart.common.server.DownloadFileInterface;
import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseo.stimart.utils.Tools;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.Http;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class HTTPRequest implements DownloadFileInterface {
//public class HTTPRequest{

	private static final String TAG = "HTTPRequest";

	private static ExtractorFamily extractor;
	private static List<Message> messagesPicturesToDownload = new ArrayList<>();
	private static final String IMAGE_SAVE_PATH = AppResourcesManager.getAppResourcesManager().getPathImagesFamily();

	private static int maxSizePhoto = 800;

	//=======================================================================================//

	@SuppressLint("LongLogTag")
	public void downloadPics() {
		Log.i(TAG + " download pictures","start");
		AppManager appManager = AppManager.getAppManager();
		Log.d("downloadPics" , IMAGE_SAVE_PATH);

		//Thread was no necessary and created a parallel task
//		Thread downloadPictureThread = new Thread() {
//			public void run() {

				DownloadFile downloadFile = new DownloadFile(HTTPRequest.this, IMAGE_SAVE_PATH);

				for (Message message : messagesPicturesToDownload) {
					final String imgName = message.getServerId().toString();
					final String downloadPath = StimartConnectionConstants.urlDownloadPath(imgName);
					Log.d(TAG + " download picture", downloadPath);
					downloadFile.downloadFileByUrl(downloadPath, imgName + ".jpg");
				}
//			}
//		};
//		downloadPictureThread.start();

	}

	@SuppressLint("LongLogTag")
	public static void sendMessages(Context context)	{
		Log.v(TAG, "sendMessages");
		if (extractor == null) {
			extractor = new ExtractorFamily(context);
		}
		extractor.open();
		List<Message> messages = extractor.getMessageList(Extractor.COL_SERVER_ID
				+ " is null AND " + ExtractorFamily.COL_FROM_USER_TO_PERSON
				+ " LIKE \"F\" ");

		for (Message messageToSend : messages) {
			Log.i(TAG + " sendNewMessages", "send new message");

			String url = StimartConnectionConstants.urlSendMessage();

			//Send photo in message is not available now on stimart but when we wan't to do that the code below can be used
			// set photo into bitmap
			ByteArrayOutputStream baos = null;
			Log.d(TAG + " sendNewMessages", "(send new message)URL IMAGE : " +messageToSend.getUrlImageServer() );
			Uri myUri = Uri.parse(messageToSend.getUrlImageServer());

			if(!messageToSend.getUrlImageServer().equals("")){
				Bitmap b = null;
				try {

					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inSampleSize = 2;
					b = BitmapFactory.decodeFile(myUri.getPath(),
							options);

				} catch (OutOfMemoryError e) {
					e.printStackTrace();
					Log.i(TAG + " sendNewMessages", "OUT OF MEMORY");
				}

				// resize bitmap before send to server
				//final int maxSize = maxSizePhoto;
				int outWidth;
				int outHeight;

				if (b!=null){
					int inWidth = b.getWidth();
					int inHeight = b.getHeight();
					if(inWidth > inHeight){
						outWidth = maxSizePhoto;
						outHeight = (inHeight * maxSizePhoto) / inWidth;
					} else {
						outHeight = maxSizePhoto;
						outWidth = (inWidth * maxSizePhoto) / inHeight;
					}
					b = Bitmap.createScaledBitmap(b, outWidth, outHeight, false);

					//send bitmap to server
					baos = new ByteArrayOutputStream();
					b.compress(Bitmap.CompressFormat.PNG, 0, baos);
				}
			}

			try {
				HTTPClient client = new HTTPClient(url);
				client.connectForMultipart();

				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_RECEIVER_UUID, messageToSend.getUserId());
				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_OBJECT_MESSAGE, messageToSend.getObjetMessage());
				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_MESSAGE, messageToSend.getMessage());
				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_SENDER_PERSON_ID, messageToSend.getPersonId());
				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_FROM_USER_TO_PERSON, messageToSend.getFromUser2Person());
				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_DATE, messageToSend.getDateStrEN());
				client.addFormPart(StimartConnectionConstants.MULTI_PART_PARAM_CHECKED, messageToSend.getChecked());

//				Multipart fields to send image in message
				client.addFormPart(StimartConnectionConstants.MULTI_PART_DIRECTORY, Tools.SERVER_IMAGE_DIRECTORY);
				if(!messageToSend.getUrlImageServer().equals("") && baos != null)
					client.addFilePart(StimartConnectionConstants.MULTI_PART_FILE, Tools.IMAGE_NAME, baos.toByteArray());

				client.finishMultipart();

				//get response from server
				String serverResponse = client.getResponse();
				JSONObject jsonObject = null;
				Log.d(TAG + " sendMessages " , "server response : " + serverResponse);
				try {
					jsonObject = new JSONObject(serverResponse);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (jsonObject != null) {
					try {
						String serverId = jsonObject.getString(StimartConnectionConstants.JSON_PARAM_SERVER_ID);
						Log.i(TAG + " sendMessages", "serverId :" + serverId);
						extractor.setServerId(messageToSend, serverId);
						Log.d(TAG + " sendMessages", "messageUpdated :"+ messageToSend.toString());

					}catch(JSONException e) {
						e.printStackTrace();
					}
				}
			}catch(Throwable t) {
				t.printStackTrace();
			}

		}
		extractor.close();
		Log.e("sendMessages", "send finished");
	}

	//=======================================================================================================//
	public static void sendUpdatedMessages() {
		Log.i(TAG,"in sendUpdatedMessages");

		List<Message> messagesToUpdate = new ArrayList<Message>();
		extractor.open();

		messagesToUpdate  = extractor.getMessageList( ExtractorFamily.COL_FROM_USER_TO_PERSON + " LIKE \"" + "T" + "\"" + " AND " +
				ExtractorFamily.COL_CHECKED + " LIKE \"" + "T" + "\"");

		for (Message messageToUpdate : messagesToUpdate) {

			String path = StimartConnectionConstants.urlUpdateMessage(messageToUpdate.getServerId(), messageToUpdate.getTrash());
			Log.d("sendUpdatedMessages","path : " +path);
			String serverResponse = Http.queryServer(path);
			if(serverResponse != null & !serverResponse.equals("")) {
				Log.d("sendUpdatedMessages","serverResponse : " + serverResponse);
				String answer = null;

				try {
					JSONObject o = new JSONObject(serverResponse);
					try {
						answer = o.getString(StimartConnectionConstants.JSON_PARAM_MESSAGE_ID);
						Log.d(" sendUpdatedMessages", "server message id :" + answer);

						extractor.setCheckedServer(messageToUpdate);
						Log.i(" sendUpdatedMessages", "messageUpdated :"+ messageToUpdate.toString());

					} catch (JSONException e) {
						e.printStackTrace();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		extractor.close();
		messagesToUpdate.clear();
		Log.i(TAG, "sendUpdateMessages finished");
	}

	//=======================================================================================================//
	@SuppressLint("LongLogTag")
	public static void getMessagesAndUsers() {
		Log.i("TAG","in getMessagesAndUsers");

		String path = StimartConnectionConstants.urlGetMessagesAndUsers();
		extractor.open();

		Log.e("getpath",path);
		String serverResponse = Http.queryServer(path);
		Log.e("getresponse", serverResponse);

		try {
			JSONObject jsonMessUsers = new JSONObject(serverResponse);

			Log.i(TAG,"INTERNET");

			//============== Messages
			JSONArray jsonArray = jsonMessUsers.optJSONArray("messages");
			if( jsonArray != null ) {
				Log.d("getMessagesAndUsers","jsonArray messages :" + jsonArray.toString());
				int len = jsonArray.length();
				Log.e("len users", String.valueOf(len));

				for (int i = 0; i < len; i++) {
					try {
						JSONObject o = jsonArray.getJSONObject(i);
						Message messageToAdd = new Message(o);

						String urlImageServer = messageToAdd.getUrlImageServer();
						if (  urlImageServer != null && !urlImageServer.equals("")) {
							messagesPicturesToDownload.add(messageToAdd);
						}

						//insert or update message into the local database
						if ( !extractor.existMessageServerId(messageToAdd) ) {
							Log.v(TAG + " getMessagesAndUsers","getMessages() - add message ok");
							extractor.insertMessage(messageToAdd);
						}
						else {
							if( messageToAdd.isChecked() ) {
								Log.v(TAG + " getMessagesAndUsers","getMessages() - update message ok");
								extractor.setCheckedServer(messageToAdd);//here if an update is made, the field checked is put to local true value and we want this field has checked server true value
							}
						}
					}
					catch (JSONException e) {
						e.printStackTrace();
					}
				}			//end of loop on each message
			}				//end of array of messages not null

			//============== Users
			jsonArray = jsonMessUsers.optJSONArray("users");
			if( jsonArray != null ) {
				Log.d("getMessagesAndUsers","jsonArray users : "+ jsonArray.toString());
				int len = jsonArray.length();
				Log.e("len", String.valueOf(len));

				for (int i = 0; i < len; i++) {
					try {
						JSONObject o = jsonArray.getJSONObject(i);
						User userToAdd = new User(o);
						if ( !extractor.existUserServerId(userToAdd) ) {
							Log.v(TAG + " getMessagesAndUsers","getUsers() - add users ok");
							extractor.insertUser(userToAdd);
						}
					}
					catch (JSONException e) {
						e.printStackTrace();
					}
				}			//end of loop on each user
			}				//end of array of users not null

			//============== Links
			jsonArray = jsonMessUsers.optJSONArray("links");
			if( jsonArray != null ) {
				Log.d("getMessagesAndUsers","jsonArray  links : " + jsonArray.toString());
				int len = jsonArray.length();
				Log.d("len links", String.valueOf(len));

				for (int i = 0; i < len; i++) {
					try {
						JSONObject o = jsonArray.getJSONObject(i);
						Link linkToAdd = new Link(o);
						if ( !extractor.existLinkServerId(linkToAdd) ) {
							Log.v(TAG + " getMessagesAndUsers","getLinks() - add links ok");
							extractor.insertLink(linkToAdd);
						}
					}
					catch (JSONException e) {
						e.printStackTrace();
					}
				}			//end of loop on each link
			}				//end of array of links not null

		}
		catch (JSONException e) {
			e.printStackTrace();

			Log.i(TAG,"PAS INTERNET");
		}
		extractor.close();
	}

	//=============================================================================================//

	@Override
	public void onDownloadSuccess() {
	}

	@Override
	public void onDownloadFailure(Exception e) {

	}

	@Override
	public void onDownloadProgress(long total, int lengthOfFile) {

	}
}
