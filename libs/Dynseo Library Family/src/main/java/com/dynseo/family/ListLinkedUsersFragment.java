package com.dynseo.family;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.R;
//import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.family.models.Link;
import com.dynseo.family.models.User;
import com.dynseo.stimart.utils.DialogManager;


public class ListLinkedUsersFragment extends Fragment {
	
	private final String TAG = "DisplayListUsers";
	
	private ExtractorFamily extractor;
	private List<Link> listLinks;
	ListView list;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

	Bundle savedInstanceState) {

		Log.d(TAG, TAG);
		View rootView = inflater.inflate(
				R.layout.family_display_list_messages_or_users_layout, container,
				false);

		list = (ListView) rootView.findViewById(R.id.messages_or_users_listView);
		
		extractor = new ExtractorFamily(getActivity().getApplicationContext());
		extractor.open();
		listLinks = extractor.getLinks(ExtractorFamily.COL_LINK_PERSON_ID + " = \"" + Person.currentPerson.getServerId() + "\"");
		extractor.close();

		int nbUsers = listLinks.size();
		if (nbUsers == 0) {
			Log.d(TAG, "nbUser = " + nbUsers);
			TextView txtNoUsers = (TextView) rootView.findViewById(R.id.item_txtNoMessage);
			txtNoUsers.setText(getString(R.string.noUsersAvailable));
			txtNoUsers.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}

		populateListView();
		registerClickCallback(rootView);

		return rootView;

	}
	
	private void populateListView() {
		ArrayAdapter<Link> adapter = new myListAdapter();
		list.setAdapter(adapter);
	}

	protected void gotoWriteMessageFragment() {
		WriteMessageFragment f = new WriteMessageFragment();
		android.app.FragmentManager fragmentManager = getFragmentManager();

		fragmentManager.beginTransaction().replace(R.id.frame_container, f).commit();
	}

	private void registerClickCallback(View rootView)
	{
		ListView list = (ListView) rootView.findViewById(R.id.messages_or_users_listView);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() 
		{			
			@Override
			public void onItemClick(AdapterView<?> parent, View viewClicked,int position, long id) 
			{		
				extractor = new ExtractorFamily(getActivity().getApplicationContext());
				extractor.open();
				User ClickedUser = extractor.getUserWithServerId(listLinks.get(position).getUserId());
				User.currentUserOpen = ClickedUser;
				Link.currentLinkOpen = listLinks.get(position);
				extractor.close();

				gotoWriteMessageFragment();
			}
		});
	}
	
	private class myListAdapter extends ArrayAdapter<Link>{
		private myListAdapter()
		{
			super(getActivity(), R.layout.family_item_user_layout, listLinks);
		}
		
		@NonNull
		@Override
		public View getView(int position, View convertView, @NonNull ViewGroup parent) {
			View itemView;
			itemView = getActivity().getLayoutInflater().inflate(R.layout.family_item_user_layout,parent,false);
			extractor = new ExtractorFamily(getActivity().getApplicationContext());
			extractor.open();

			User currentUser = extractor.getUserWithServerId(listLinks.get(position).getUserId());
			TextView FirstnameText = (TextView) itemView.findViewById(R.id.item_txtFirstname);
			FirstnameText.setText(currentUser.getPseudo(getActivity().getApplicationContext()));
			extractor.close();
			return itemView;
		}
	}
}
