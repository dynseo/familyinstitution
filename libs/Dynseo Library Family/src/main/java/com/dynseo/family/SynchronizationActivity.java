package com.dynseo.family;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.dynseo.family.R;
import com.dynseo.stimart.common.models.SynchronizationFactory;
import com.dynseolibrary.platform.SynchroInterface;
import com.dynseolibrary.platform.server.Http;
import com.dynseolibrary.platform.server.SynchronizationTask;
import com.dynseolibrary.tools.Tools;

import java.lang.reflect.Constructor;

/**
 * Created by Dynseo on 28/10/2016.
 */

public class SynchronizationActivity extends com.dynseo.stimart.common.activities.SynchronizationActivity {
    static SynchronizationFactory synchronizationFactory;

    static public void setSynchronizationFactory (SynchronizationFactory aSynchroFactory) {
        synchronizationFactory = aSynchroFactory;
    }

    static public SynchronizationFactory getSynchronizationFactory () {
        if (synchronizationFactory == null) {
            synchronizationFactory = new com.dynseo.family.models.SynchronizationFactory();
        }
        return synchronizationFactory;
    }

    @Override
    protected void launch(String valueType) {
        if (Http.isOnline(getApplicationContext())) {
            SynchroInterface synchronization = null;
            if (synchroClass != null) {
                try {
                    Class syncClass = Class.forName(synchroClass);
                    Constructor constructor =
                            syncClass.getDeclaredConstructor(Context.class, SharedPreferences.class, String.class);
                    synchronization = (SynchroInterface) (constructor.newInstance(this, sharedPrefs, valueType));
                } catch (Exception e) {
                    return;
                }
            }
            if (valueType == null || valueType.equals("data")) {
                if (synchronization == null)
                    synchronization = getSynchronizationFactory().createSynchronizationData(this, sharedPrefs, valueType);
                text.setText(R.string.synchroencours);
                endMessage = R.string.synchrotermine;
            }
            //            else if (valueType.equals("resources")) {
            //
            //                if (synchronization == null) {
            //                    //Log.e("ALEX TO DELETE synchroActivity", "code2 : " + codeForEndOfSynchro);
            //                    synchronization = getSynchronizationFactory().createSynchronizationResources(this, sharedPrefs, valueType, codeForEndOfSynchro);
            //                }
            //                text.setText(R.string.synchroencours_res);
            //                endMessage = R.string.synchrotermine_res;
            //            }
            if (synchronization != null){
                SynchronizationTask synchronizationTask = new SynchronizationTask(synchronization, this, sharedPrefs);
                synchronizationTask.execute();
                continueBtn.setEnabled(false);
                continueBtn.setBackgroundResource(R.drawable.round_button_off);
                continueBtn.setOnClickListener(quit);
                progressBar.setVisibility(View.VISIBLE);
            }
        } else
            Tools.showToast(getApplicationContext(), getString(R.string.erreurconnexioninternet));
    }
}