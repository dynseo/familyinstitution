package com.dynseo.family.server;

import android.content.Context;
import android.content.SharedPreferences;

import com.dynseo.family.utils.HTTPRequest;
import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.server.Synchronization;
import com.dynseolibrary.tools.Tools;

/**
 * Created by Mathias on 20/10/2016.
 */

public class SynchronizationData extends Synchronization implements com.dynseo.stimart.common.models.SynchronizationData {

    Extractor extractor;

    public SynchronizationData(Context context, SharedPreferences sharedPrefs, String type) {
        super(context, sharedPrefs, type);

        extractor = new Extractor(context);
    }

    @Override
    public void doSynchro() {
        this.sendNewPersons(context);
        this.getPersons();
        this.nextStepSynchro();
    }

    @Override
    public void nextStepSynchro() {
        HTTPRequest.sendMessages(context);
        HTTPRequest.sendUpdatedMessages();
        HTTPRequest.getMessagesAndUsers();
        //HTTPRequest.downloadPics();
        new HTTPRequest().downloadPics();
    }

}
