package com.dynseo.family;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dynseo.family.R;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;
import com.dynseo.stimart.common.models.AppResourcesManager;
import com.dynseolibrary.platform.AppManager;

import java.io.File;

public class DisplayMessageFragment extends Fragment {

	private static final String TAG = "DisplayMessageFragment";

	String firstname;
	String name;
	String objetMessage;
	String message;
	String urlImg;
	String serverIdMessage;

	public String IMAGE_SAVE_PATH;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	Bundle savedInstanceState) {

		Log.d(TAG, TAG);
		final View rootView = inflater.inflate(R.layout.family_display_message_layout, container, false);

		IMAGE_SAVE_PATH = AppResourcesManager.getAppResourcesManager().getPathImagesFamily();

		final ImageView imgV =(ImageView) rootView.findViewById(R.id.item_image);
		final TextView txtName = (TextView) rootView.findViewById(R.id.item_txtName1);
		final TextView txtMessage = (TextView) rootView.findViewById(R.id.item_txtMessage);
		final TextView txtObjectMessage = (TextView) rootView.findViewById(R.id.item_txtFirstname);

//      if (intent != null) {
      	firstname = User.currentUserOpen.getFirstname();
      	name = User.currentUserOpen.getName();
      	objetMessage = Message.currentMessageOpen.getObjetMessage();
      	message = Message.currentMessageOpen.getMessage();
      	urlImg = Message.currentMessageOpen.getUrlImageServer();
      	serverIdMessage = Message.currentMessageOpen.getServerId();

	     	String url = IMAGE_SAVE_PATH + File.separator + serverIdMessage + ".jpg";
	     	if (urlImg.equals("")) {
	     	} else {
	     		  Uri uri = Uri.parse(url );
	     		  Log.d(TAG, "uri : " + uri);
	     		  imgV.setImageURI(uri);
	     	}
   		txtName.setText(firstname + " " + name);
   		txtMessage.setText(message);
   		txtObjectMessage.setText(objetMessage);
//      }

      Button btnRespond = (Button) rootView.findViewById(R.id.respond);
      final Button btnZoomPlus = (Button) rootView.findViewById(R.id.zoom_plus);
      final Button btnZoomMoins = (Button) rootView.findViewById(R.id.zoom_moins);

      imgV.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

        	  DisplayFullScreenPictureFragment f = new DisplayFullScreenPictureFragment();
    		android.app.FragmentManager fragmentManager = getFragmentManager();

    		fragmentManager.beginTransaction().replace(R.id.frame_container, f)
    				.commit();
          }
      });

      btnRespond.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
        	  // go to WriteMessageFragment
        	  WriteMessageFragment f = new WriteMessageFragment();
    		android.app.FragmentManager fragmentManager = getFragmentManager();

    		fragmentManager.beginTransaction().replace(R.id.frame_container, f)
    				.commit();
          }
      });

      btnZoomMoins.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
          	txtMessage.setTextSize(38);
          	txtObjectMessage.setTextSize(38);
          	btnZoomMoins.setBackgroundResource(R.drawable.message_zoom_out_disable);
          	btnZoomPlus.setBackgroundResource(R.drawable.message_zoom_in);
          }
      });

      btnZoomPlus.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
          	txtMessage.setTextSize(70);
          	txtObjectMessage.setTextSize(70);
          	btnZoomMoins.setBackgroundResource(R.drawable.message_zoom_out);
          	btnZoomPlus.setBackgroundResource(R.drawable.message_zoom_in_disabled);
          }
      });

		return rootView;
	}
}
