/**
HOUSSEM
 */
package com.dynseo.family;

import com.dynseo.family.R;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.stimart.utils.DialogManager;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MessagesActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.family_message_activity_layout);

		final boolean isAnimator = Person.currentPerson.isAnimator();

		// lunch first fragment "received messages"
		DisplayListMessagesFragment f = new DisplayListMessagesFragment(isAnimator, false);
		android.app.FragmentManager fragmentManager = getFragmentManager();

		fragmentManager.beginTransaction().replace(R.id.frame_container, f).commit();

		final Button writeMessageBtn = (Button) findViewById(R.id.btnListUsers);
		final Button receivedMessageBtn = (Button) findViewById(R.id.btnListMessages);
		final Button backToStimartBtn = (Button) findViewById(R.id.btnBack);

		writeMessageBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isAnimator) {
					DisplayListMessagesFragment f = new DisplayListMessagesFragment(
							isAnimator, true);
					android.app.FragmentManager fragmentManager = getFragmentManager();

					fragmentManager.beginTransaction().replace(R.id.frame_container, f).commit();
					
					writeMessageBtn.setBackgroundResource(R.drawable.message_button4);					
					
				} else {
					ListLinkedUsersFragment f = new ListLinkedUsersFragment();
					android.app.FragmentManager fragmentManager = getFragmentManager();

					fragmentManager.beginTransaction().replace(R.id.frame_container, f).commit();
					
					writeMessageBtn.setBackgroundResource(R.drawable.message_button2);
				}	
				
				receivedMessageBtn.setBackgroundResource(R.drawable.message_button1b);

			}
		});

		receivedMessageBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				receivedMessageBtn.setBackgroundResource(R.drawable.message_button1);
				if (isAnimator) 
					writeMessageBtn.setBackgroundResource(R.drawable.message_button1b);
				else
					writeMessageBtn.setBackgroundResource(R.drawable.message_button2b);
				
				DisplayListMessagesFragment f = new DisplayListMessagesFragment(isAnimator, false);
				android.app.FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.frame_container, f).commit();				
			}
		});

		backToStimartBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(MessagesActivity.this,
//						MenuAppliActivity.class);
//				startActivity(intent);
				overridePendingTransition(android.R.anim.fade_in,
						android.R.anim.fade_out);
				finish();
			}
		});

		if (isAnimator) {
			writeMessageBtn.setBackgroundResource(R.drawable.message_button1b);
			writeMessageBtn.setText(R.string.buttonSentMessage);
		} else {
			writeMessageBtn.setBackgroundResource(R.drawable.message_button2b);
			writeMessageBtn.setText(R.string.buttonWriteMessage);
		}
	}
	
	public void onBackPressed() {
	
	Log.i("backpress", "sucessfully pressed");
	Log.i("backpress", getString(R.string.quit));
	Log.i("backpress", getString(R.string.backToStimart));
	final DialogManager currentDialog = new DialogManager(MessagesActivity.this, R.drawable.background_button_menu, R.drawable.background_dialog_menu, 0, getString(R.string.quit), getString(R.string.backToStimart));
	currentDialog.setDialogString();

	currentDialog.oui.setOnClickListener(new OnClickListener() {
		public void onClick(View v) {
			currentDialog.endDialog();
			overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
			finish();
		}
	});
	currentDialog.setCloseListener(false,true);
	currentDialog.showDialog();
}

}
