package com.dynseo.family.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dynseo.stimart.common.dao.DatabaseHelper;
import com.dynseo.stimart.common.dao.Extractor;

/**
 * Created by Dynseo on 28/10/2016.
 */

public class DatabaseHelperFamily extends DatabaseHelper {
    String TAG = "DatabaseHelperFamily";
    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    public DatabaseHelperFamily(Context context) {
        super(context);
        Log.d(TAG, "DatabaseHelperFamily");
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);
        Log.d(TAG, "onCreate()");
        db.execSQL(ExtractorFamily.TABLE_MESSAGE_CREATE);
        db.execSQL(ExtractorFamily.TABLE_USER_CREATE);
        db.execSQL(ExtractorFamily.TABLE_LINK_CREATE);
        Log.i(TAG, "TABLE_MESSAGE_CREATE");
        Log.i(TAG, "TABLE_USER_CREATE");
        Log.i(TAG, "TABLE_LINK_CREATE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Log.d(TAG, "onUpgrade()");
        Cursor cursor4 = db.query(ExtractorFamily.TABLE_MESSAGE, null, null, null, null, null, null);

        boolean message_trash_exist = false;

        String names4 [] = cursor4.getColumnNames();

        for (String name : names4){
            if (name.equals(ExtractorFamily.COL_TRASH)) {
                message_trash_exist = true;
            }
        }

        if (!message_trash_exist){
            db.execSQL(ExtractorFamily.ALTER_TABLE_MESSAGE_ADD_TRASH);
            Log.d(TAG, "Extractor.ALTER_TABLE_MESSAGE_ADD_TRASH");
        }

        cursor4.close();
		/* Add TABLE MESSAGE, USER, LINK if they do not exist */

        db.execSQL(ExtractorFamily.TABLE_MESSAGE_CREATE);
        db.execSQL(ExtractorFamily.TABLE_USER_CREATE);
        db.execSQL(ExtractorFamily.TABLE_LINK_CREATE);
        Log.i(TAG, "TABLE_MESSAGE_CREATE");
        Log.i(TAG, "TABLE_USER_CREATE");
        Log.i(TAG, "TABLE_LINK_CREATE");
    }


}
