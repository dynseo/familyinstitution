package com.dynseo.family.models;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.stimart.R;
import com.dynseo.stimart.utils.StringFormatter;
import com.dynseo.stimart.utils.StimartConnectionConstants;

public class User {

	private int _id;
	private String _serverId;
	
	private String _name;
	private String _firstname;
	private String _role;
	
	static public User currentUserOpen;
	
	//==================================================================================================//
	// *** CONSTRUCTOR *** //
	public User() {
	}
	public User(JSONObject jsonUser) throws JSONException {
		
		_serverId 	= jsonUser.getString(StimartConnectionConstants.JSON_PARAM_SERVER_ID);
		_name 		= jsonUser.getString(StimartConnectionConstants.JSON_PARAM_NAME_USER);
		_firstname	= jsonUser.getString(StimartConnectionConstants.JSON_PARAM_FIRSTNAME_USER);
		_role		= jsonUser.getString(StimartConnectionConstants.JSON_PARAM_ROLE);	
	}
	
	public User(String serverId, String name, String firstname, String role)
	{
		this._serverId = serverId;
		this._name = name;
		this._firstname = firstname;
		this._role = role;
	}
	
	//==================================================================================================//
	// *** GETTERS *** //
	public int getId() {
		return _id;
	}
	public String getServerId() {
		return _serverId;
	}
	public String getName() {
		return _name;
	}
	public String getFirstname() {
		return _firstname;
	}
	public String getRole() {
		return _role;
	}
	public String getPseudo(Context context) {
		return StringFormatter.nameFormat(context.getString(R.string.local_name_format), _name, _firstname);
	}
	
	//==================================================================================================//
	// *** SETTERS *** //
	public void setId(int _id) {
		this._id = _id;
	}
	
	public void setServerId(String _serverId) {
		this._serverId = _serverId;
	}
	
	public void setName(String _name) {
		this._name = _name;
	}
	
	public void setFirstname(String _firstname) {
		this._firstname = _firstname;
	}
	
	public void set_role(String _role) {
		this._role = _role;
	}
	//==================================================================================================//	
}
