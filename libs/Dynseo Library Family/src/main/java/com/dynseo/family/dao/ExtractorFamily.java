package com.dynseo.family.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dynseo.family.models.Link;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;
import com.dynseo.stimart.common.dao.DatabaseHelper;
import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mathias on 14/10/2016.
 */

public class ExtractorFamily extends Extractor{

    private static final String TAG = "ExtractorFamily";

    public static final String TABLE_MESSAGE = "message";
    public static final String TABLE_USER = "user";
    public static final String TABLE_LINK = "link";

    // ********************** TABLES FOR MESSAGE MANAGEMENT ************ //
    // ************************* TABLE MESSAGE ************************* //
    public static final String COL_USER_ID = "userId";
    public static final String COL_OBJET_MESSAGE = "objetMessage";
    public static final String COL_MESSAGE = "message";
    public static final String COL_PERSON_ID = "personId";
    public static final String COL_FROM_USER_TO_PERSON = "fromUser2Person";
    public static final String COL_CHECKED = "checked";
    public static final String COL_TRASH = "trash";
    public static final String COL_DATE = "date";
    public static final String COL_URL_IMAGE_SERVER = "urlImageServer";

    public static final int	NUM_COL_USER_ID = 2;
    public static final int	NUM_COL_OBJET_MESSAGE = 3;
    public static final int NUM_COL_MESSAGE = 4;
    public static final int	NUM_COL_PERSON_ID = 5;
    public static final int NUM_COL_FROM_USER_TO_PERSON = 6;
    public static final int NUM_COL_CHECKED = 8;
    public static final int NUM_COL_DATE = 7;
    public static final int NUM_COL_URL_IMAGE_SERVER = 9;
    public static final int NUM_COL_TRASH = 10;

    // ************************* TABLE USER ************************* //
    public static final String COL_USER_NAME = "name";
    public static final String COL_USER_FIRSTNAME = "firstname";
    public static final String COL_ROLE = "role";

    public static final int	NUM_COL_USER_NAME = 2;
    public static final int NUM_COL_USER_FIRSTNAME = 3;
    public static final int NUM_COL_ROLE = 4;

    // ************************* TABLE LINK ************************* //
    public static final String COL_LINK_USER_ID = "userId";
    public static final String COL_LINK_PERSON_ID = "personId";
    public static final String COL_ALIAS_PERSON = "aliasPerson";

    public static final int	NUM_COL_LINK_USER_ID = 2;
    public static final int NUM_COL_LINK_PERSON_ID = 3;
    public static final int NUM_COL_ALIAS_PERSON = 4;


    // *** TABLE MESSAGERIE *** //
    static final String TABLE_MESSAGE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_MESSAGE + " (" + COL_ID + " PRIMARY KEY NOT NULL , "
            + COL_SERVER_ID + " ," + COL_USER_ID + " ," + COL_OBJET_MESSAGE + " ,"
            + COL_MESSAGE + " ," + COL_PERSON_ID + "," + COL_FROM_USER_TO_PERSON + " ,"
            + COL_DATE + " ," + COL_CHECKED + " ," + COL_URL_IMAGE_SERVER + " ," + COL_TRASH + " CHAR(1));";


    public static final String TABLE_USER_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_USER + " (" + COL_ID + " PRIMARY KEY NOT NULL,"
            + COL_SERVER_ID + "," + COL_USER_NAME + " ," +
            COL_USER_FIRSTNAME + " ," + COL_ROLE + ");";


    public static final String TABLE_LINK_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_LINK + " (" + COL_ID + " PRIMARY KEY NOT NULL,"
            + COL_SERVER_ID + "," + COL_LINK_USER_ID + ","+
            COL_LINK_PERSON_ID + " ," + COL_ALIAS_PERSON + ");";

    static final String ALTER_TABLE_MESSAGE_ADD_TRASH = "ALTER TABLE "+ TABLE_MESSAGE + " ADD COLUMN " + COL_TRASH + " CHAR(1);";


    public ExtractorFamily(Context context) {
        super(context);
        Log.d(TAG, "ExtractorFamily");
        dbHelper = new DatabaseHelperFamily(context);
    }


    public long insertMessage(Message message) {
        ContentValues values = new ContentValues();
        values.put(COL_ID, this.getCountOfMessages(""));
        values.put(COL_SERVER_ID, message.getServerId());
        values.put(COL_USER_ID, message.getUserId() );
        values.put(COL_OBJET_MESSAGE, message.getObjetMessage() );
        values.put(COL_MESSAGE, message.getMessage());
        values.put(COL_PERSON_ID, message.getPersonId());
        values.put(COL_FROM_USER_TO_PERSON, message.getFromUser2Person());
        values.put(COL_DATE, message.getDateStrEN());
        values.put(COL_CHECKED, message.getChecked());
        values.put(COL_URL_IMAGE_SERVER, message.getUrlImageServer());
        values.put(COL_TRASH, message.getTrash());

        Log.i(TAG, "insertMessage success ");
        return db.insert(TABLE_MESSAGE, null, values);
    }

    public long insertUser(User user) {
        ContentValues values = new ContentValues();
        values.put(COL_ID, this.getCountOfUsers(""));
        values.put(COL_SERVER_ID, user.getServerId());
        values.put(COL_USER_NAME, user.getName());
        values.put(COL_USER_FIRSTNAME, user.getFirstname());
        values.put(COL_ROLE, user.getRole());

        Log.i(TAG, "insertUser success ");
        return db.insert(TABLE_USER, null, values);
    }

    public long insertLink(Link link) {
        ContentValues values = new ContentValues();
        values.put(COL_ID, this.getCountOfLink(""));
        values.put(COL_SERVER_ID, link.getServerId());
        values.put(COL_LINK_USER_ID, link.getUserId());
        values.put(COL_LINK_PERSON_ID, link.getPersonId());
        values.put(COL_ALIAS_PERSON, link.getAliasPerson());
        Log.i(TAG, "INSERT LINK = " + link.toString());

        Log.i(TAG, "insertLink success ");
        return db.insert(TABLE_LINK, null, values);
    }

    // *** GET COUNT OF LIST *** //
    public int getCountOfMessages(String condition)
    {
        Cursor cursor = db.query(TABLE_MESSAGE, null, condition, null, null, null, null);
        int len = cursor.getCount();
        cursor.close();
        return len;
    }

    public int getCountOfUsers(String condition)
    {
        Cursor cursor = db.query(TABLE_USER, null, condition, null, null, null, null);
        int len = cursor.getCount();
        cursor.close();
        return len;
    }

    public int getCountOfLink(String condition)
    {
        Cursor cursor = db.query(TABLE_LINK, null, condition, null, null, null, null);
        int len = cursor.getCount();
        cursor.close();
        return len;
    }

    private static Message cursorToMessage(Cursor c) {

        if(c.getCount() == 0)
            return null;
        if(c.getPosition() == -1)
            c.moveToFirst();

        Message message = new Message();
        message.setId(c.getInt(NUM_COL_ID));
        message.setServerId(c.getString(NUM_COL_SERVER_ID));
        message.setUserId(c.getString(NUM_COL_USER_ID));
        message.setObjetMessage(c.getString(NUM_COL_OBJET_MESSAGE));
        message.setMessage(c.getString(NUM_COL_MESSAGE));
        message.setPersonId(c.getString(NUM_COL_PERSON_ID));
        message.setFromUser2Person(c.getString(NUM_COL_FROM_USER_TO_PERSON));
        message.setDate(c.getString(NUM_COL_DATE));
        message.setChecked(c.getString(NUM_COL_CHECKED));
        message.setUrlImageServer(c.getString(NUM_COL_URL_IMAGE_SERVER));
        message.setTrash(c.getString(NUM_COL_TRASH));

        return message;
    }

    private static User cursorToUser(Cursor c) {

        if(c.getCount() == 0)
            return null;
        if(c.getPosition() == -1)
            c.moveToFirst();

        User user = new User();
        user.setId(c.getInt(NUM_COL_ID));
        user.setServerId(c.getString(NUM_COL_SERVER_ID));
        user.setName(c.getString(NUM_COL_USER_NAME));
        user.setFirstname(c.getString(NUM_COL_FIRSTNAME));
        user.set_role(c.getString(NUM_COL_ROLE));

        return user;
    }

    private static Link cursorToLink(Cursor c) {

        if(c.getCount() == 0)
            return null;
        if(c.getPosition() == -1)
            c.moveToFirst();

        Link link = new Link();
        link.setId(c.getInt(NUM_COL_ID));
        link.setServerId(c.getString(NUM_COL_SERVER_ID));
        link.setUserId(c.getString(NUM_COL_LINK_USER_ID));

        Log.i(TAG, "cursorToLink = " + c.getString(NUM_COL_LINK_USER_ID));

        link.setPersonId(c.getString(NUM_COL_LINK_PERSON_ID));
        link.setAliasPerson(c.getString(NUM_COL_ALIAS_PERSON));
        return link;
    }


    // *** MESSAGE *** //
    public Message getMessageWithId(int id){
        Cursor c = db.query(TABLE_MESSAGE,null, COL_ID + " = " + id , null, null, null, null);
        Message m = cursorToMessage(c);
        c.close();
        return m;
    }

    public Message getMessageWithServerId(String serverId){
        Cursor c = db.query(TABLE_MESSAGE,null, COL_SERVER_ID + " LIKE \"" + serverId +"\"", null, null, null, null);
        Message m = cursorToMessage(c);
        c.close();
        return m;
    }

    // *** USER *** //
    public boolean existUserServerId(User user) {
        User userIdExistant = getUserWithServerId(user.getServerId());
        return (userIdExistant != null);
    }

    public User getUserWithId(int id){
        Cursor c = db.query(TABLE_USER,null, COL_ID + " = " + id , null, null, null, null);
        User u = cursorToUser(c);
        c.close();
        return u;
    }

    public User getUserWithServerId(String serverId){
        Cursor c = db.query(TABLE_USER,null, COL_SERVER_ID + " = \"" + serverId + "\"" , null, null, null, null);
        User u = cursorToUser(c);
        c.close();
        return u;
    }

    public String getNameWithUserId(String userId){
        Cursor cursor = db.query(TABLE_USER, null, COL_USER_ID + " LIKE \"" + userId + "\"" , null, null, null, null);
        String result = cursor.getString(0);
        return result;
    }

    // *** LINK *** //
    public Link getLinkWithServerId(String serverId){
        Cursor c = db.query(TABLE_LINK,null, COL_SERVER_ID + " = \"" + serverId + "\"", null, null, null, null);
        Link l = cursorToLink(c);
        c.close();
        return l;
    }

    public boolean existLinkServerId(Link link) {
        Link linkIdExistant = getLinkWithServerId(link.getServerId());
        return (linkIdExistant != null);
    }

    public void setServerId(Message message, String serverId) {
        if (message != null && serverId != null && db != null) {
            ContentValues values = new ContentValues();
            values.put(COL_SERVER_ID, serverId);
            db.update(TABLE_MESSAGE, values, COL_ID + " = " + String.valueOf(message.getId()), null);
            message.setServerId(serverId);
        }
    }

    public void setChecked(Message message) {
        if (message != null && db != null) {
            ContentValues values = new ContentValues();
            values.put(COL_CHECKED, "T");
            db.update(TABLE_MESSAGE, values, COL_ID + " = " + String.valueOf(message.getId()), null);
            message.setChecked("T");
        }
    }

    public void setTrash(Message message) {
        if (message != null && db != null) {
            ContentValues values = new ContentValues();
            values.put(COL_TRASH, "T");
            db.update(TABLE_MESSAGE, values, COL_ID + " = " + String.valueOf(message.getId()), null);
            message.setTrash("T");
        }
    }

    public void setCheckedServer(Message message) {
        if (message != null && db != null) {
            ContentValues values = new ContentValues();
            values.put(COL_CHECKED, "S");
            db.update(TABLE_MESSAGE, values, COL_ID + " = " + String.valueOf(message.getId()), null);
            message.setChecked("S");
        }
    }

    public void updateMessage(Message message) {
        if (message != null && db != null) {
            ContentValues values = new ContentValues();
            values.put(COL_USER_ID, message.getUserId() );
            values.put(COL_OBJET_MESSAGE, message.getObjetMessage() );
            values.put(COL_MESSAGE, message.getMessage() );
            values.put(COL_PERSON_ID, message.getPersonId() );
            values.put(COL_FROM_USER_TO_PERSON, message.getFromUser2Person());
            values.put(COL_CHECKED, message.getChecked() );
            db.update(TABLE_MESSAGE, values, COL_SERVER_ID + " LIKE \"" + message.getServerId() + "\"", null);
        }
    }

    public void deleteMessage (Message message) {
        if (message != null && db != null) {
            db.delete(TABLE_MESSAGE, COL_SERVER_ID + " LIKE \"" + message.getServerId() + "\"", null);
        }
    }

    public boolean existMessageServerId(Message message) {
        Message serverIdExistant = getMessageWithServerId(message.getServerId());
        return (serverIdExistant != null);
    }

    //============================================================================================================//
    // *** GET LIST *** //
    public List<Message> getMessageList(String condition) {
        List<Message> result = null;
        if (db != null) {
            Cursor cursor = db.query(TABLE_MESSAGE, null, condition , null, null, null, COL_DATE + " DESC");
            result = new ArrayList<Message>();
            int len = cursor.getCount();
            Log.e(TAG, "cursorlen = " + String.valueOf(len));

            int j = 0;
            while(cursor.moveToNext()) {
                result.add(j, cursorToMessage(cursor));
                j++;
            }
            cursor.close();
        }
        return result;
    }

    public List<User> getUsers(String selection) {
        List<User> result = null;
        if (db != null) {
            Cursor cursor = db.query(TABLE_USER, null, selection, null, null, null, COL_ID);
            result = new ArrayList<User>();

            int j = 0;
            while(cursor.moveToNext()) {
                result.add(j, cursorToUser(cursor));
                j++;
            }
            cursor.close();
        }
        return result;
    }

    public List<Link> getLinks(String selection) {
        List<Link> result = null;
        if (db != null) {
            Cursor cursor = db.query(TABLE_LINK, null, selection, null, null, null, COL_ID);
            result = new ArrayList<Link>();

            int j = 0;
            while(cursor.moveToNext()) {
                result.add(j, cursorToLink(cursor));
                j++;
            }
            cursor.close();
        }
        return result;
    }

    public Message[] getMessages(String condition) {
        Cursor cursor = db.query(TABLE_MESSAGE, null, condition, null, null, null, null);
        int len = cursor.getCount();
        Log.e("cursorlen",String.valueOf(len));
        Message[] messages = new Message[len];

        int j = 0;
        while(cursor.moveToNext()) {
            messages[j] = cursorToMessage(cursor);
            j++;
        }
        cursor.close();
        return messages;
    }

}
