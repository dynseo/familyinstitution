package com.dynseo.family.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.stimart.utils.StimartConnectionConstants;
import com.dynseolibrary.tools.Tools;

public class Message {

	public static String messageTrashed = "T";

	private String _serverId;
	private int _id;
	
	private String _userId;
	private String _objectMessage;
	private String _message;
	private String _personId;
	private String _fromUser2Person;
	private String _checked;
	private String _trash;
	private String _urlImageServer;
	private Date _date;
	
	static public Message currentMessageOpen;
	
	public Message() {
	}
	
	public Message(JSONObject jsonMessage) throws JSONException {
		
		_serverId		= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_SERVER_ID);
		_userId			= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_USER_ID);
		_objectMessage	= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_OBJET_MESSAGE);
		_message		= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_MESSAGE);
		_personId 		= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_PERSON_ID);
//		_fromUser2Person	= jsonMessage.optString(StimartConnectionConstants.JSON_PARAM_FROM_USER_TO_PERSON);
		_fromUser2Person	= "T";		//This param takes "T" value because when it comes from server message is always sent by user to person
		_checked 		= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_CHECKED);
		_trash 		= jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_TRASH);
		_urlImageServer = jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_URL_IMAGE_SERVER);
		
		_date = Tools.parseDateComplete( jsonMessage.getString(StimartConnectionConstants.JSON_PARAM_DATE) );	
	}
	
	public Message(String objetMessage, String message, String userId, String personId, String checked, String urlImageServer) {
		this._objectMessage = objetMessage;
		this._message = message;
		this._userId = userId;
		this._personId = personId;
		this._checked = checked;
		this._urlImageServer = urlImageServer;
	}
	public Message(String objetMessage, String message, String userId, String personId, Date date, String checked, String urlImageServer) {
		this(objetMessage, message, userId, personId, checked, urlImageServer);
		
		this._fromUser2Person = "F";
		this._date = date;
	}
	public Message(String serverId, String userId, String objetMessage, String message, String personId, String fromUser2Person, String date, String checked, String urlImageServer) {
		this(objetMessage, message, userId, personId, checked, urlImageServer);
		
		this._serverId =  serverId;	
		this._fromUser2Person = fromUser2Person;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			_date = dateFormat.parse(date);
		} catch (ParseException e) {
			_date = null;
		}	
	}
	public Message(String serverId, String userId, String objetMessage, String message, String personId, String fromUser2Person, Date date, String checked, String urlImageServer) {
		this(objetMessage, message, userId, personId, checked, urlImageServer);
		this._serverId =  serverId;	
		this._fromUser2Person = fromUser2Person;
		this._date = date;
	}
	
	public String getUrlImageServer() {
		return _urlImageServer;
	}
	public void setUrlImageServer(String _urlImageServer) {
		this._urlImageServer = _urlImageServer;
	}
	public String getChecked() {
		return _checked;
	}
	public void setChecked(String _checked) {
		this._checked = _checked;
	}
	public String getTrash() {
		return _trash;
	}
	public void setTrash(String _trash) {this._trash = _trash;}
	public Date getDate() {
		return _date;
	}
	public String getDateStr(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(_date);
	}
	public String getDateStrEN() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return dateFormat.format(_date);
	}
	public String getDateStrENShort() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(_date);
	}
	public String getDateStrFR() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		return dateFormat.format(_date);
	}
	public void setDate(Date date) {
		_date = date;
	}
	public void setDate(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			_date = dateFormat.parse(date);
		} catch (ParseException e) {
			_date = null;
		}
	}
	public String getFromUser2Person() {
		return _fromUser2Person;
	}
	public void setFromUser2Person(String _fromUser2Person) {
		this._fromUser2Person = _fromUser2Person;
	}
	public String getPersonId() {
		return _personId;
	}
	public void setPersonId(String _personId) {
		this._personId = _personId;
	}
	public String getServerId() {
		return _serverId;
	}
	public void setServerId(String _serverId) {
		this._serverId = _serverId;
	}
	public int getId() {
		return _id;
	}
	public void setId(int _id) {
		this._id = _id;
	}
	public String getUserId() {
		return _userId;
	}
	public void setUserId(String _userId) {
		this._userId = _userId;
	}
	public String getObjetMessage() {
		return _objectMessage;
	}
	public void setObjetMessage(String _objetMessage) {
		this._objectMessage = _objetMessage;
	}
	public String getMessage() {
		return _message;
	}
	public void setMessage(String _message) {
		this._message = _message;
	}

	//===== Utility methods
	public boolean isChecked() {
		return( _checked != null && _checked.equals(StimartConnectionConstants.VAL_CHECKED_MESSAGE) );
	}
	
	@Override
	public String toString(){
		 
		return "\nobjet" + this._objectMessage + "\nmessage : " + this._message + "\npersonId : " 
				+ this._personId + "\nuserId : " + this._userId + "\nserverId (message id on server) : " + _serverId; 
//		return null;
	}
}
