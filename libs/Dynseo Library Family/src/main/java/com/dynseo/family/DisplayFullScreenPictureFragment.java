package com.dynseo.family;


import com.dynseo.family.R;
import com.dynseo.family.models.Message;
import com.dynseo.stimart.common.models.AppResourcesManager;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;

public class DisplayFullScreenPictureFragment extends Fragment {
	
	private static final String TAG = "DisplayFullScreenPictureFragment";
	
	String serverIdMessage;
	public String IMAGE_SAVE_PATH;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

	Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.family_display_fullscreen_picture_layout, container, false);
	
	    IMAGE_SAVE_PATH = AppResourcesManager.getAppResourcesManager().getPathImagesFamily();
	    final ImageView imgV =(ImageView) rootView.findViewById(R.id.item_image);
        	serverIdMessage = Message.currentMessageOpen.getServerId();
//        	String url = Environment.getExternalStorageDirectory() + IMAGE_SAVE_PATH + serverIdMessage + ".jpg";
        	String url = IMAGE_SAVE_PATH + File.separator + serverIdMessage + ".jpg";
        	Uri uri = Uri.parse(url );
        	Log.d(TAG, "uri : " + uri);
	     	imgV.setImageURI(uri);

        imgV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	DisplayMessageFragment f = new DisplayMessageFragment();
        		android.app.FragmentManager fragmentManager = getFragmentManager();

        		fragmentManager.beginTransaction().replace(R.id.frame_container, f).commit();
            }
        });
		
		return rootView;	
	}	
}




