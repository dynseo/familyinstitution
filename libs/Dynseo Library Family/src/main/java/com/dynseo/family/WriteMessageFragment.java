package com.dynseo.family;

import java.util.Calendar;

import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.family.R;
//import com.dynseo.stimart.common.dao.Extractor;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;
import com.dynseo.stimart.utils.DialogManager;
import com.dynseolibrary.platform.AppManager;
import com.dynseolibrary.platform.server.ConnectionConstants;
import com.dynseolibrary.tools.CustomKeyboard;
import com.dynseolibrary.tools.Tools;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WriteMessageFragment extends Fragment {
	
	 public ExtractorFamily extractor;
	 DialogManager currentDialog;
	 String userId;
	 String personId = Person.currentPerson.getServerId();

	SharedPreferences preferences;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

	Bundle savedInstanceState) {

		// Log.d(TAG, TAG);
		final View rootView = inflater.inflate(R.layout.family_write_message_layout,
				container, false);

		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

//		// use our custom keybord "azerty_single_word" (azerty without the SPACE key)
		KeyboardView mKeyboardView = (KeyboardView) getActivity().findViewById(R.id.custom_keyboard);
		CustomKeyboard customKeyboard = new CustomKeyboard(getActivity(), mKeyboardView, R.xml.azerty);
		// hide left Panel
		final LinearLayout leftPanel = (LinearLayout) getActivity().findViewById(R.id.leftPanel);
		leftPanel.setVisibility(View.GONE);

		final FrameLayout frameContainer = (FrameLayout) getActivity().findViewById(R.id.frame_container);
		frameContainer.getLayoutParams().width = FrameLayout.LayoutParams.MATCH_PARENT;

		TextView txtName = (TextView) rootView.findViewById(R.id.item_txtName);
		txtName.setText(User.currentUserOpen.getPseudo(getActivity().getApplicationContext()));
		userId = User.currentUserOpen.getServerId();

		final Button btnZoomPlus = (Button) rootView.findViewById(R.id.zoom_plus);
		final Button btnZoomMoins = (Button) rootView.findViewById(R.id.zoom_moins);
		final Button btnCancel = (Button) rootView.findViewById(R.id.cancel);
		Button btnSendMessage = (Button) rootView.findViewById(R.id.send);
		final EditText editObjetMessage = (EditText) rootView.findViewById(R.id.txtObjetMessage);
		final EditText editMessage = (EditText) rootView.findViewById(R.id.txtMessage);

		if ( !AppManager.getAppManager().isFreemiumActivated() ) {
			customKeyboard.registerEditText(editObjetMessage);
			customKeyboard.registerEditText(editMessage);
			customKeyboard.showCustomKeyboard(rootView);
		}

		btnZoomMoins.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				editMessage.setTextSize(38);
				editObjetMessage.setTextSize(38);
				btnZoomMoins.setBackgroundResource(R.drawable.message_zoom_out_disable);
				btnZoomPlus.setBackgroundResource(R.drawable.message_zoom_in);
			}
		});

		btnZoomPlus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				editMessage.setTextSize(70);
				editObjetMessage.setTextSize(70);
				btnZoomMoins.setBackgroundResource(R.drawable.message_zoom_out);
				btnZoomPlus.setBackgroundResource(R.drawable.message_zoom_in_disabled);
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().finish();
				startActivity(new Intent(getActivity(), MessagesActivity.class));
			}
		});

		btnSendMessage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (editObjetMessage.getText().toString().equals("")) {
					Tools.showToast(getActivity(),
							getString(R.string.empty_object));
					return;
				}

				if (editMessage.getText().toString().equals("")) {
					Tools.showToast(getActivity(),
							getString(R.string.empty_message));
					return;
				}

//				leftPanel.setVisibility(View.VISIBLE);

				insertMessage(rootView);

				currentDialog = new DialogManager(getActivity(),
						R.drawable.background_button_menu,
						R.drawable.background_dialog_menu,
						0,
						getString(R.string.sent),
						getString(R.string.askStayInDynseoFamily));
				currentDialog.setDialogString();

				currentDialog.oui.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {

						currentDialog.endDialog();

						getActivity().finish();
						startActivity(new Intent(getActivity(), MessagesActivity.class));
					}
				});
				currentDialog.non.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						currentDialog.endDialog();
						getActivity().finish();
					}
				});
				currentDialog.showDialog();
			}
		});

		return rootView;
	}

	
	public void insertMessage(View rootView) {
		EditText msgObject = (EditText) rootView.findViewById(R.id.txtObjetMessage);
		EditText msgText = (EditText) rootView.findViewById(R.id.txtMessage);
		Calendar calendar = Calendar.getInstance();

		Message messageToAdd = new Message(msgObject.getText().toString(),
				msgText.getText().toString(), userId, personId,
				calendar.getTime(), "F", "");
		Log.i("calendar.getTime()", calendar.getTime().toString());
		Log.i("messageToAddTime", messageToAdd.getDateStrFR());
		extractor = new ExtractorFamily(getActivity().getApplicationContext());
		extractor.open();
		extractor.insertMessage(messageToAdd);
		extractor.close();

		/* DISABLE: after 2.2 message will be send with synchronisation */
		/*
		 * if (CheckInternet.isOnline(getApplicationContext())){ ConnexionTask
		 * connexion = new
		 * ConnexionTask(PreferenceManager.getDefaultSharedPreferences
		 * (getApplicationContext()), getApplicationContext(), false);
		 * connexion.execute(); }
		 */
	}
}