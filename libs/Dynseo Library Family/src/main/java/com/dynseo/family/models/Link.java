package com.dynseo.family.models;

import org.json.JSONException;
import org.json.JSONObject;

import com.dynseo.stimart.utils.StimartConnectionConstants;

public class Link {

	private int _id;
	private String _serverId;
	
	private String _userId;
	private String _personId;
	private String _aliasPerson;
	
	static public Link currentLinkOpen;
	
	//=====================================================================================//
	// *** CONSTRUCTOR *** //
	public Link(){
	}
	public Link(JSONObject jsonLink) throws JSONException {
		
		_serverId 	= jsonLink.getString(StimartConnectionConstants.JSON_PARAM_SERVER_ID);
		_userId 	= jsonLink.getString(StimartConnectionConstants.JSON_PARAM_USER_ID);
		_personId	= jsonLink.getString(StimartConnectionConstants.JSON_PARAM_PERSON_ID);
		_aliasPerson = jsonLink.getString(StimartConnectionConstants.JSON_PARAM_ALIAS_PERSON);	
	}
	public Link(String serverId, String userId, String personId, String aliasPerson)
	{
		this._serverId = serverId;
		this._userId = userId;
		this._personId = personId;
		this._aliasPerson = aliasPerson;
	}
	
	//=====================================================================================//
	// *** GETTERS *** //
	public int getId() {
		return _id;
	}
	public String getServerId() {
		return _serverId;
	}
	public String getAliasPerson() {
		return _aliasPerson;
	}
	public String getUserId() {
		return _userId;
	}
	public String getPersonId() {
		return _personId;
	}
	//=====================================================================================//
	// *** SETTERS *** //
	public void setId(int _id) {
		this._id = _id;
	}
	public void setServerId(String _serverId) {
		this._serverId = _serverId;
	}
	public void setUserId(String _userId) {
		this._userId = _userId;
	}
	public void setPersonId(String _personId) {
		this._personId = _personId;
	}
	public void setAliasPerson(String _aliasPerson) {
		this._aliasPerson = _aliasPerson;
	}
	//=====================================================================================//
	public String toString(){
		return "LINK ID = " + _id + "( serverId : " + _serverId + " - userId : "+ _userId 
				+ " - personId : " + _personId + " - aliasPerson : " + _aliasPerson + " )";
	}
}
