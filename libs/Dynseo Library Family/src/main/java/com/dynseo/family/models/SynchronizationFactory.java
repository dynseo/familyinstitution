package com.dynseo.family.models;


import android.content.Context;
import android.content.SharedPreferences;

import com.dynseo.stimart.common.models.SynchronizationData;
import com.dynseo.stimart.common.models.SynchronizationResources;

/**
 * Created by Mathias on 20/10/2016.
 */

public class SynchronizationFactory implements com.dynseo.stimart.common.models.SynchronizationFactory {


    public SynchronizationData createSynchronizationData(Context context, SharedPreferences sharedPrefs, String type) {
        return new com.dynseo.family.server.SynchronizationData(context, sharedPrefs, type);
    }

    @Override
    public SynchronizationResources createSynchronizationResources(Context context, SharedPreferences sharedPrefs, String type, String codeForEndOfSynchro) {
        return null;
//        return new com.dynseo.family.server.SynchronizationResources(context, sharedPrefs, type, codeForEndOfSynchro);
    }
}
