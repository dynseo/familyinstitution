package com.dynseo.family;

import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dynseo.family.models.Message;
import com.dynseo.family.R;
import com.dynseo.family.dao.ExtractorFamily;
import com.dynseo.stimart.common.models.Person;
import com.dynseo.family.models.Message;
import com.dynseo.family.models.User;

public class DisplayListMessagesFragment extends Fragment {

	protected boolean isAnimator;
	protected boolean isForAnimatorSentMessage;
	protected final String TAG = "DisplayListMessagesFragment";
	protected ExtractorFamily extractor;
	protected List<Message> listMessages;
	protected int nbMessages;
	ListView list;

	public DisplayListMessagesFragment() {
	}

	@SuppressLint("ValidFragment")
    public DisplayListMessagesFragment(boolean isAnimator,
                                       boolean isForAnimatorSentMessage) {
		this.isAnimator = isAnimator;
		this.isForAnimatorSentMessage = isForAnimatorSentMessage;
	}

	@SuppressLint("LongLogTag")
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.family_display_list_messages_or_users_layout, container,false);

		list = (ListView) rootView.findViewById(R.id.messages_or_users_listView);
		extractor = new ExtractorFamily(getActivity().getApplicationContext());
		extractor.open();
		String request;
			/* complete list of messages */
			 if (isAnimator) {
				 if (isForAnimatorSentMessage) {
					 request = ExtractorFamily.COL_FROM_USER_TO_PERSON + " LIKE \"F\" AND " + ExtractorFamily.COL_CHECKED + " LIKE \"F\" AND " +
							 ExtractorFamily.COL_TRASH + " NOT LIKE \"T\" AND " + ExtractorFamily.COL_SERVER_ID + " IS NULL";
//					 listMessages = extractor.getMessageList(
//					 ExtractorFamily.COL_FROM_USER_TO_PERSON + " LIKE \"F\" AND " +
//					 ExtractorFamily.COL_CHECKED + " LIKE \"F\" AND " +
//					 ExtractorFamily.COL_TRASH + " NOT LIKE \"T\" AND " +
//					 ExtractorFamily.COL_SERVER_ID + " IS NULL");
				 } else {
					 request = ExtractorFamily.COL_FROM_USER_TO_PERSON + " LIKE \"T\" AND " + ExtractorFamily.COL_CHECKED + " LIKE \"F\" AND " +
							 ExtractorFamily.COL_TRASH + " NOT LIKE \"T\"";
//					 listMessages = extractor.getMessageList(ExtractorFamily.COL_FROM_USER_TO_PERSON
//					 + " LIKE \"T\" AND "
//					 + ExtractorFamily.COL_CHECKED + " LIKE \"F\" AND "
//					 + ExtractorFamily.COL_TRASH + " NOT LIKE \"T\"");
				 }
			 } else {
				 	 request = ExtractorFamily.COL_PERSON_ID + " = \"" + Person.currentPerson.getServerId() + "\" AND " +
							 ExtractorFamily.COL_FROM_USER_TO_PERSON + " = \"T\" AND " + ExtractorFamily.COL_TRASH + " NOT LIKE \"T\"";
//						 listMessages = extractor.getMessageList(ExtractorFamily.COL_PERSON_ID
//				 + " = \"" + Person.currentPerson.getServerId() + "\" AND "
//				 + ExtractorFamily.COL_FROM_USER_TO_PERSON + " = \"T\" AND "
//				 + ExtractorFamily.COL_TRASH + " NOT LIKE \"T\""
//				 );
		 }
		 Log.d(TAG,"request : " + request + ", isAnimator : " + isAnimator + ", isForAnimatorSentMessage : " + isForAnimatorSentMessage);
		 listMessages = extractor.getMessageList(request);
		
		 /* delete error from list of messages */
		 for(int i = 0; i<listMessages.size(); i++){
			 Message currentMesssage = listMessages.get(i);
			 User currentUser = extractor.getUserWithServerId(currentMesssage.getUserId());
			 Person currentPerson = extractor.getResidentWithServerId(currentMesssage.getPersonId());

			 if(currentPerson==null){
				 Log.i("ERROR",
				 "remove message error from list +1 (no Person attached) ");
				 listMessages.remove(i);
			 }

			 if(currentUser==null){
				 Log.i("ERROR", "remove message error from list +1 (no User attached) ");
				 listMessages.remove(i);
			 }
		 }
		
		 //*****************************GRAPHICS**********************************
		 nbMessages = listMessages.size();
		 Log.d("messageList", "nbMes=" + nbMessages);
		 if (nbMessages == 0) {
			 TextView txtNoMessage = (TextView) rootView.findViewById(R.id.item_txtNoMessage);

			 if (isForAnimatorSentMessage)
				 txtNoMessage.setText(R.string.noMessageSent);

			 txtNoMessage.setVisibility(View.VISIBLE);
			 list.setVisibility(View.GONE);

		 }
		 extractor.close();
		
		 populateListView(rootView);
		 registerClickCallback(rootView);

		return rootView;

	}

	protected void gotoDisplayMessageFragment(Message message) {
		DisplayMessageFragment f = new DisplayMessageFragment();
		android.app.FragmentManager fragmentManager = getFragmentManager();

		fragmentManager.beginTransaction().replace(R.id.frame_container, f)
				.commit();
	}

	private void registerClickCallback(View rootView) {
		 ListView list = (ListView) rootView.findViewById(R.id.messages_or_users_listView);
		
		 if (!isAnimator) {
			 list.setOnItemClickListener(new AdapterView.OnItemClickListener()
			 {
			 @Override
			 public void onItemClick(AdapterView<?> parent, View viewClicked,int
			 position, long id)
			 {
			 extractor = new ExtractorFamily(getActivity().getApplicationContext());
			 extractor.open();
			 Message clickedMessage = listMessages.get(position);

			 if(clickedMessage.getChecked().equals("T") ||
					 clickedMessage.getChecked().equals("S")){
			 }else{
			 	Log.e("MESSAGE", "SET CHECKED");
			 	extractor.setChecked(clickedMessage);
			 }
			 Message.currentMessageOpen = clickedMessage;
			 User.currentUserOpen =
			 extractor.getUserWithServerId(clickedMessage.getUserId());
			 extractor.close();

				 gotoDisplayMessageFragment(clickedMessage);

	//		 Log.e("INTENT", "OPEN");
	//		 Intent intent = new Intent(getActivity(),
	//		 DisplayMessageActivity.class);
	//		 startActivity(intent);
	//		 getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
	//		 getActivity().finish();
			 }
			 });
		 }
	 }
		
		 private void populateListView(View rootView) {
			 ArrayAdapter<Message> adapter = new myListAdapter();
	//		 ListView list = (ListView) rootView.findViewById(R.id.messages_or_users_listView);
			 list.setAdapter(adapter);
		 }

		 
		 private class myListAdapter extends ArrayAdapter<Message>{
			 public myListAdapter()
			 {
			 super(getActivity(), R.layout.family_item_message_layout,
			 listMessages);
			 }
			
			 @SuppressLint("NewApi") @Override
			 public View getView(int position, View convertView, ViewGroup parent)
			 {
			 View itemView = convertView;
			 if(itemView==null){
			 itemView= getActivity().getLayoutInflater().inflate(R.layout.family_item_message_layout,parent,false);
			 }
			 Message currentMessage = listMessages.get(position);
			
			 //*****************************GRAPHICS**********************************
			 extractor.open();
			 ImageView imgMail = (ImageView) itemView.findViewById(R.id.item_icon);
			 if (isForAnimatorSentMessage && isAnimator)
			 	imgMail.setImageResource(R.drawable.message_mailbis);
			 if (currentMessage.getChecked().equals("T") ||
				 currentMessage.getChecked().equals("S")){
			 	imgMail.setImageResource(R.drawable.message_mail);
			 	imgMail.setImageAlpha(125);
			 }else{
			 	imgMail.setImageResource(R.drawable.message_mail);
			 	imgMail.setImageAlpha(255);
			 }
			 ImageView imgPhoto = (ImageView) itemView.findViewById(R.id.item_img);
			 if (currentMessage.getUrlImageServer().equals("")) {
			 }
			 else {
			 	imgPhoto.setImageResource(R.drawable.message_image);
			 }
			 TextView nameText = (TextView) itemView.findViewById(R.id.item_txtName);
			 TextView objetText = (TextView) itemView.findViewById(R.id.item_txtObjetMessage);
			 TextView dateText = (TextView) itemView.findViewById(R.id.item_txtDate);
			
			 if (isAnimator) {
				 if (extractor == null) {
				 Log.e("test","extractor null");
				 }
				 if (currentMessage.getPersonId() == null) {
				 Log.e("test","person id null");
				 }
				 if (extractor.getResidentWithServerId(currentMessage.getPersonId()) ==
				 null) {
				 Log.e("test","getResidentWithServerId null");
				 }
//				 nameText.setText(
//				 extractor.getResidentWithServerId(currentMessage.getPersonId()).getPseudo(getActivity().getApplication()));
				 if (isForAnimatorSentMessage) {
					 nameText.setText(Html.fromHtml("<font color='#888888'>" + getString(R.string.newMessageSent) + "</font> " +
							 extractor.getResidentWithServerId(currentMessage.getPersonId()).getPseudo(getActivity().getApplication())));
//				 	objetText.setText(getString(R.string.newMessageSent));
				 }
				 else {
					 nameText.setText(Html.fromHtml("<font color='#888888'>" + getString(R.string.newMessageReceived) + "</font> " +
							 extractor.getResidentWithServerId(currentMessage.getPersonId()).getPseudo(getActivity().getApplication())));
//				 	objetText.setText(getString(R.string.newMessageReceived));
				 }
			 } else {
				 if (extractor.getUserWithServerId(currentMessage.getUserId()) != null) {
				 nameText.setText(extractor.getUserWithServerId(currentMessage.getUserId()).getPseudo(getActivity().getApplication())
				 );
				 }
				 objetText.setText(currentMessage.getObjetMessage());
			 }

			 Log.e("LANGUAGE",Locale.getDefault().getDisplayLanguage());
			 if (Locale.getDefault().getDisplayLanguage().equals("English")) {
			 dateText.setText(getString(R.string.sentIn)+currentMessage.getDateStrENShort());
			 }
			 else {
			 dateText.setText(getString(R.string.sentIn)+currentMessage.getDateStrFR());
			 }
			 extractor.close();
			 //*****************************GRAPHICS**********************************
			
			 return itemView;
			 }
			 }

}
